\chaptr{Couplages}{1}

Un couplage est un ensemble d'arêtes indépendantes, \cad sans extrémités communes. Cette notion, apparaît naturellement lorsqu'il s'agit de coupler deux à deux des objets, certaines associations seulement étant possibles (celles des arêtes du graphe).

\tikzset{main node/.style={circle,fill=blue!20,draw,minimum size=1cm,inner sep=0pt},
}
\tikzset{square node/.style={fill=blue!20,draw,minimum size=1cm,inner sep=0pt},
}


\begin{itemize}
	\item Par exemple, l'ensemble des personnes à marier (polygamie interdite).
	\item Une assemblée de personnes parlant diverses langues, on veut former des groupes de deux qui peuvent se comprendre. On modélise alors par un graphe avec une arête entre deux personnes (sommets) si elles parlent la même langue.
\end{itemize}

Le problème en général est posé comme un problème d'optimisation. Plus il y a d'arêtes, mieux c'est. On recherche à maximiser le nombre de couples. 

\section{Couplage dans un graphe}

\index{couplage}
\begin{defi}
	Soit $G = (S, A)$ un graphe (non orienté). Un couplage de $G$ est un ensemble $C \subseteq A$ d'arêtes indépendantes. Un sommet $s \in S$ est incident à au plus une arête de $C$. Un couplage est dit :
	\begin{itemize}
		\item maximal (au sens de l'inclusion) s'il n'est pas strictement inclus dans un couplage de $G$ (on ne peut plus ajouter d'arêtes).
		\item maximum (ou optimal ou maximal pour le cardinal) si son cardinal est plus grand ou égal que celui de tout couplage de $G$.
		\item parfait si tout sommet de $G$ est incident à une arête du couplage.
	\end{itemize}
\end{defi}

\begin{figure}[!h]
	\begin{subfigure}{.5\textwidth}
		\centering
		\begin{tikzpicture}[scale=0.7]
			\node[main node] (1) {};
			\node[main node] (2) [below left = 1cm and 1cm of 1] {};
			\node[main node] (3) [below right = 1cm and 1cm of 1] {};
			\node[main node] (4) [below right = 1cm and 1cm of 3] {};
			\node[main node] (5) [below = 1cm of 2] {};
			\node[main node] (6) [below = 1cm of 3] {};
			\node[main node] (7) [below left = 1cm and 1cm of 6] {};
			\node[main node] (8) [below = 1cm of 4] {};
			
			\draw[color=blue, dashed] (1) node {} -- (2);
			\draw[color=red] (1) node {} -- (3);
			\draw[color=blue, dashed] (3) node {} -- (4);
			\draw[color=red] (2) node {} -- (5);
			\draw[color=blue, dashed] (2) node {} -- (6);
			\draw[color=blue, dashed] (3) node {} -- (6);
			\draw[color=red] (6) node {} -- (8);
			\draw[color=blue, dashed] (5) node {} -- (7);
			\draw[color=blue, dashed] (7) node {} -- (8);
			\draw[color=blue, dashed] (4) node {} -- (8);
		\end{tikzpicture}
		\caption{En rouge, les arcs du couplage $C$ et en bleu $A \backslash C$}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\begin{tikzpicture}
			\node[main node] (1) {};
			\node[main node] (2) [below left = 1cm and 1cm of 1] {};
			\node[main node] (3) [below right = 1cm and 1cm of 1] {};
			\node[main node] (4) [below right = 1cm and 1cm of 3] {};
			\node[main node] (5) [below = 1cm of 2] {};
			\node[main node] (6) [below = 1cm of 3] {};
			\node[main node] (7) [below left = 1cm and 1cm of 6] {};
			\node[main node] (8) [below = 1cm of 4] {};
			
			\draw[color=red] (1) node {} -- (2);
			\draw[color=blue, dashed] (1) node {} -- (3);
			\draw[color=red] (3) node {} -- (4);
			\draw[color=blue, dashed] (2) node {} -- (5);
			\draw[color=blue, dashed] (2) node {} -- (6);
			\draw[color=blue, dashed] (3) node {} -- (6);
			\draw[color=red] (6) node {} -- (8);
			\draw[color=red] (5) node {} -- (7);
			\draw[color=blue, dashed] (7) node {} -- (8);
			\draw[color=blue, dashed] (4) node {} -- (8);
		\end{tikzpicture}
		\caption{On a ici un couplage maximal, parfait et optimal.}
	\end{subfigure}
	\caption{Exemples de couplage}
\end{figure}

\index{sommet!couvert}
\index{sommet!libre}
\begin{defi}
	Soit $G = (S, A)$ un graphe et $C \subseteq A$ un couplage de $G$. On dit que $s \in S$ est couvert par $C$ si $s$ est extrémité d'une arête de $C$ et libre sinon.
\end{defi}

\index{couplage!maximal}
\index{couplage!parfait}
\index{couplage!optimal}
\remarklist{
	\item Comme un couplage peut ne contenir aucune arête ($C = \emptyset$), l'ensemble des couplages d'un graphe est non-vide et fini (car $A$ est fini). Donc tout graphe possède un couplage optimal.
	\item Toute arête d'un graphe est un couplage de ce graphe de cardinal $1$.
	\item Un couplage est maximal \ssi il n'existe pas deux sommets libres incidents (sinon il suffirait d'ajouter cette arête).
	\item Les couplages maximaux n'ont pas nécessairement tous le même cardinal.
	\item Un couplage maximal n'est pas toujours optimal, mais la réciproque est vraie.
	\item Le cardinal d'un couplage est inférieur à $\frac{\abs{S}}{2}$.
	\item Un couplage est parfait \ssi son cardinal est $\frac{\abs{S}}{2}$. Le graphe doit être d'ordre pair.
	\item Une étoile (un noeud central et $n-1$ noeuds uniquement reliés au noeud central), notée $S_n$, est un exemple où tout couplage non-vide est de cardinal $1$.
	\item Un graphe complet d'ordre pair admet un couplage parfait.
}

Mais alors, comment trouver un couplage maximal ? On ajoute les arêtes si on peut. De même, on peut se demander comment option un couplage optimal.

\index{chemin}
\begin{defi}
	Soit $G = (S, A)$ un graphe et $C$ un couplage de $G$. Un chemin alternant (avec sommets distincts deux à deux) de $G$ pour $C$ est un chemin dont les arêtes sont alternativement dans $C$ et dans $A \backslash C$. Un chemin améliorant est un chemin alternant d'extrémités libres.
\end{defi}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node[main node] (0) {};
		\node[main node] (1) [right = 1cm of 0] {};
		\node[main node] (2) [right = 1cm of 1] {};
		\node[main node] (3) [right = 1cm of 2] {};
		\node[main node] (4) [right = 1cm of 3] {};
		\draw[red] (0) node {} -- (1);
		\draw[blue, dashed] (1) node {} -- (2);
		\draw[red] (2) node {} -- (3);
		\draw[blue, dashed] (3) node {} -- (4);
	\end{tikzpicture}
	\caption{Exemple de chemin alternant}
\end{figure}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node[main node] (0) {};
		\node[main node] (1) [right = 1cm of 0] {};
		\node[main node] (2) [right = 1cm of 1] {};
		\node[main node] (3) [right = 1cm of 2] {};
		\node[main node] (4) [right = 1cm of 3] {};
		\node[main node] (5) [right = 1cm of 4] {};
		
		\draw[blue, dashed] (0) node {} -- (1);
		\draw[red] (1) node {} -- (2);
		\draw[blue, dashed] (2) node {} -- (3);
		\draw[red] (3) node {} -- (4);
		\draw[blue, dashed] (4) node {} -- (5);
	\end{tikzpicture}
	\caption{Exemple de chemin améliorant}
\end{figure}

\remarklist{
	\item La longueur d'un chemin améliorant est impair et les arêtes ont leurs extrémités dans $A \backslash C$
	\item On trouve aussi augmentant, améliorants, etc...	
}
\index{{\scshape Lemme}!du chemin améliorant}
\begin{lem}[du chemin améliorant]
	Soit $C$ un couplage de cardinal $K$ d'un graphe $G = (S, A)$ et $D$ un chemin améliorant pour $C$ dans $G$. On note $C \triangle D = C \backslash D \cup D \backslash C$ l'ensemble des arêtes qui sont dans le couplage ou exclusif dans le chemin. Alors $C \triangle D$ est un couplage de $G$ de cardinal $k+1$.
\end{lem}

\begin{proof}
	On a $\abs{C \triangle D} = \abs{C \backslash D} + \abs{D \backslash C} = \abs{C} + 1$. 
	
	\medskip
	
	Une arrête de $D$ à $A \backslash D$ n'est pas dans le couplage. En effet, les extrémités de $D$ sont libres donc ne sont pas incidentes à une arête du couplage. Les autres sont couvertes par une arête de $D$. Donc elle ne peuvent pas non plus être incidente à une arête de $C \backslash D$. Donc, $C \backslash D$ reste un couplage.
	
	\medskip
	
	$D \backslash C$ est un couplage puisque l'on prend une arête sur deux dans un chemin dont les sommets sont tous deux à deux distincts. Donc, toutes ces arêtes sont indépendantes. De plus, aucun sommet n'est incident à la fois à une arête de $C \backslash D$ et de $D \backslash C$. qui sont donc indépendants donc $C \triangle D = C \backslash D \cup D \backslash C$ est un couplage. 
\end{proof}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node[main node] (0) {};
		\node[main node] (1) [left = 1cm of 0] {};
		\node[main node] (2) [below = 1cm of 1] {};
		\node[main node] (3) [right = 1cm of 2] {};
		\node[main node] (5) [below right = 1cm and 0.5cm of 2] {};
		\node[main node] (6) [below right = 1cm and 1cm of 3] {};
		\node[main node] (7) [below right = 0.5cm and 1cm of 0] {};
		\node[main node] (8) [above right = 1cm and 1cm of 0] {};
		\path[draw]
		(0) edge node {} (1)
		(1) edge node {} (2)
		(2) edge node {} (3)
		(3) edge node {} (0)
		(2) edge node {} (5)
		(3) edge node {} (5)
		(3) edge node {} (6)
		(3) edge node {} (7)
		(0) edge node {} (7)
		(0) edge node {} (8);
		\path[draw, color=red]
		(2) edge node {} (5)
		(3) edge node {} (6)
		(0) edge node {} (7);
				
	\end{tikzpicture}
	\caption{En rouge, un couplage optimal déterminé en utilisant le lemme.}
\end{figure}

Étant donné un couplage, on peut donc chercher des chemins améliorants pour trouver un meilleur couplage.

\index{{\scshape Théorème}!de Berge}
\begin{theorem}[Berge, 1957]
	Soit $G = (S, A)$ un graphe et $C$ un couplage de $G$. Le couplage $C$ est optimal \ssi il n'admet aucun chemin améliorant pour $C$.
\end{theorem}

\begin{proof}
	Le lemme ci-dessus établit la condition nécessaire. En effet, si $C$ possède un chemin améliorant, alors il n'est pas optimal. Pour la condition suffisante, on procède par contraposée. Soit $C$ un couplage non-optimal. Montrons que $C$ possède un chemin améliorant. 
	
	\medskip
	
	On considère un couplage optimal $C' \subseteq A$. On a donc $\abs{C} < \abs{C'}$. On considère $C \triangle C'$ et on note $H = (S, C \triangle C')$. Considérons un sommet de $H$. Il est incident à au plus une arête provenant de $C$ et au plus une arête provenant de $C'$. Donc, dans $H$, tous les sommets ont un degré dans $\{0, 1, 2\}$. 
	
	\medskip
	
	Le long d'une chaine ou d'un cycle de $H$, les arêtes alternent entre celles de $C$ et celles de $C'$. Donc, les cycles sont de longueur paire et contiennent autant d'arêtes de $C$ et de $C'$. Comme $\abs{C'} > \abs{C}$, au moins une composante connexe de $H$ contient plus d'arêtes de $C'$ que de $C$. Ce n'est pas un cycle donc une chaîne. Cette chaîne est améliorante car elle alterne entre des arêtes de $C$ et des arêtes de $C' \backslash C$ avec plus d'arêtes de $C' \backslash C$ que de $C$.
\end{proof}

\begin{figure}[!h]
	\begin{subfigure}{.5\textwidth}
		\centering
		\begin{tikzpicture}[scale=0.7]
			\node[main node] (1) {};
			\node[main node] (2) [below left = 1cm and 1cm of 1] {};
			\node[main node] (3) [below right = 1cm and 1cm of 1] {};
			\node[main node] (4) [below right = 1cm and 1cm of 3] {};
			\node[main node] (5) [below = 1cm of 2] {};
			\node[main node] (6) [below = 1cm of 3] {};
			\node[main node] (7) [below left = 1cm and 1cm of 6] {};
			\node[main node] (8) [below = 1cm of 4] {};
			
			\draw[color=blue, dashed] (1) node {} -- (2);
			\draw[color=red] (1) node {} -- (3);
			\draw[color=blue, dashed] (3) node {} -- (4);
			\draw[color=red] (2) node {} -- (5);
			\draw[color=blue, dashed] (2) node {} -- (6);
			\draw[color=blue, dashed] (3) node {} -- (6);
			\draw[color=red] (6) node {} -- (8);
			\draw[color=blue, dashed] (5) node {} -- (7);
			\draw[color=blue, dashed] (7) node {} -- (8);
			\draw[color=blue, dashed] (4) node {} -- (8);
		\end{tikzpicture}
		\caption{On a un couplage $C$ en rouge.}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\begin{tikzpicture}
			\node[main node] (1) {};
			\node[main node] (2) [below left = 1cm and 1cm of 1] {};
			\node[main node] (3) [below right = 1cm and 1cm of 1] {};
			\node[main node] (4) [below right = 1cm and 1cm of 3] {};
			\node[main node] (5) [below = 1cm of 2] {};
			\node[main node] (6) [below = 1cm of 3] {};
			\node[main node] (7) [below left = 1cm and 1cm of 6] {};
			\node[main node] (8) [below = 1cm of 4] {};
			
			\draw[color=red] (1) node {} -- (2);
			\draw[color=blue, dashed] (1) node {} -- (3);
			\draw[color=blue, dashed] (3) node {} -- (4);
			\draw[color=blue, dashed] (2) node {} -- (5);
			\draw[color=blue, dashed] (2) node {} -- (6);
			\draw[color=blue, dashed] (3) node {} -- (6);
			\draw[color=red] (6) node {} -- (8);
			\draw[color=red] (5) node {} -- (7);
			\draw[color=blue, dashed] (7) node {} -- (8);
			\draw[color=blue, dashed] (4) node {} -- (8);
		\end{tikzpicture}
		\caption{Et un autre couplage $C'$ en rouge.}
	\end{subfigure}	
	\caption[Construction de $H$ dans le théorème de Berge : étape $1$]{On cherche à construire $H$ (de la preuve) avec la différence symétrique.}
\end{figure}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node[main node] (1) {};
		\node[main node] (2) [below left = 1cm and 1cm of 1] {};
		\node[main node] (3) [below right = 1cm and 1cm of 1] {};
		\node[main node] (4) [below right = 1cm and 1cm of 3] {};
		\node[main node] (5) [below = 1cm of 2] {};
		\node[main node] (6) [below = 1cm of 3] {};
		\node[main node] (7) [below left = 1cm and 1cm of 6] {};
		\node[main node] (8) [below = 1cm of 4] {};
		
		\draw[color=red] (1) node {} -- (2);
		\draw[color=red] (1) node {} -- (3);
		\draw[color=blue, dashed] (3) node {} -- (4);
		\draw[color=red] (2) node {} -- (5);
		\draw[color=blue, dashed] (2) node {} -- (6);
		\draw[color=blue, dashed] (3) node {} -- (6);
		\draw[color=blue, dashed] (6) node {} -- (8);
		\draw[color=red] (5) node {} -- (7);
		\draw[color=blue, dashed] (7) node {} -- (8);
		\draw[color=blue, dashed] (4) node {} -- (8);
	\end{tikzpicture}
	\caption[Construction de $H$ dans le théorème de Berge : étape $2$]{On a construit $H$ comme étant la différence symétrique de $C$ et de $C'$.}
\end{figure}

\begin{prop}
	Un graphe dont tous les sommets sont de degré inférieur ou égal à $2$ est une réunion de chaînes et de cycles.
\end{prop}

\begin{proof}
	Laissé à titre d'exercice.
\end{proof}

\textbf{Remarque :} On déduit l'algorithme suivant pour trouver un couplage optimal.

\index{{\scshape Algorithme}!de couplage optimal}
\begin{algorithm}
	\caption*{Algorithme pour trouver un couplage optimal}\label{a}
	\begin{algorithmic}
		\Require Un graphe $G = (S, A)$
		\Ensure Un couplage $C$ optimal
		\State $C := \emptyset$
		\While{il existe un chemin améliorant $D$ pour $C$}
		\State $C := C \triangle D$
		\EndWhile
		\State \Return $C$
	\end{algorithmic}
\end{algorithm}

A noter que l'on sait réaliser cet algorithme en tant polynomial.

\medskip

\textbf{Remarque :} Justifions plus précisément que les extrémités du chemin construit sont bien libres pour $C$. Ces extrémités sont incidentes à une arête de $C' \backslash C$ (car il y a strictement plus d'arêtes de $C' \backslash C$ que de $C \backslash C'$ sur ce chemin alternant). Cette extrémité n'est donc pas incidente à une autre arête de $C'$ (car $C'$ est un couplage).

\medskip

Si cette extrémité est incidente à une arête de $C$, celle-ci n'étant pas dans $C'$, elle serait dans $C \backslash C'$ donc dans $H$. Ce sommet serait de degré $2$ alors qu'il est de degré $1$.

\section{Graphes bipartis}

\index{ensemble indépendant}
\begin{defi}
	Un stable ou ensemble indépendant est un sous-ensemble $X \subseteq S$ de sommets deux à deux non adjacents. Autrement dit, aucune arête de $G$ n'a ses deux extrémités dans $X$. Autrement dit, le sous-graphe induit par $X$ est isolé.
\end{defi}

\index{graphe!biparti}
\begin{defi}
	Un graphe est biparti si on peut partitionner $S = X \coprod Y$ en deux ensembles $X$ et $Y$ indépendants. Autrement dit, toute arête a une extrémité dans $X$ et l'autre dans $Y$. On note $G = (X, Y, A)$.
\end{defi}

\textbf{Remarque :} Le cas des graphes bipartis est celui où la notion de couplage est la plus naturelle. Typiquement, on a deux ensembles d'objets $X$ et $Y$ ($X$ les candidats, $Y$ les écoles par exemple) et un ensemble d'arêtes entre des sommets de $X$ et des sommets de $Y$.

\begin{exemple}
	On peut construire plusieurs exemples : les auteurs avec des articles, ou encore des agents avec des tâches.
\end{exemple}

Un avantage des graphes bipartis (outre le fait que de nombreux problèmes de couplages se modélisent bien par des graphes bipartis) est que l'on sait déterminer s'il existe des chemins augmentant relativement facilement, et en trouver le cas échéant.

\medskip

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node[main node] (0) [label=above:{X}] {};
		\node[main node] (1) [below = 1cm of 0] {};
		\node[main node] (2) [below = 1cm of 1] {};
		\node[main node] (3) [below = 1cm of 2] {};
		
		\node[square node] (4) [label=above:{Y}, right = 3cm of 0] {};
		\node[square node] (5) [below = 1cm of 4] {};
		\node[square node] (6) [below = 1cm of 5] {};
		\node[square node] (7) [below = 1cm of 6] {};
		
		\draw
		(0) edge node {} (4)
		(1) edge node {} (6)
		(2) edge node {} (7);
		
		\draw[dashed] (1) node {} -- (5);
		\draw[dashed] (6) node {} -- (3);
	\end{tikzpicture}
	\caption[Chemin augmentant sur un graphe biparti]{Chemin augmentant sur ce graphe biparti.}
\end{figure}

\remarklist{
	\item Un chemin augmentant dans un graphe biparti commence par un sommet libre de $X$, alterne entre des sommets de $X$ et de $Y$ et termine par un sommet libre de $Y$.
	\item Quand on va de $X$ à $Y$, on prend une arête de $A \backslash C$ et quand on va de $Y$ à $X$, on prend une arête de $C$.
}

Pour déterminer si un graphe biparti avec un couplage $C$ possède un chemin augmentant, il suffit de faire un parcours en profondeur à partir des sommets libres de $X$ jusqu'à arriver à un sommet libre de $Y$, en utilisant uniquement les arêtes de $A \backslash C$ entre $X$ et $Y$ et de $C$ entre $Y$ et $X$. Un tel chemin sera bien augmentant et si il existe un chemin augmentant, il s'agit d'un tel chemin. La complexité est en $O(\abs{S} + \abs{A})$.

\medskip

\textbf{Autre présentation possible} : A un graphe biparti $G = (S, A)$ et à un couplage $C$, on peut associer un graphe orienté $G_C = (S', A)$ comme suit. On se donne deux nouveaux sommets $s, f \not\in S$ et $S'  =S \cup \{s, f\}$.
\begin{itemize}
	\item Il y a un arc entre $s$ et tout sommet libre de $X$.
	\item Il y a un arc entre tout sommet libre de $Y$ et $f$.
	\item Il y a un arc $x \to y$ pour $x \in X$, $y \in Y$ si $\{x, y\} \not\in C$.
	\item Il y a un arc $y \to x$ pour tout $x \in X, y \in Y, \{x, y\} \in C$.
\end{itemize}

\begin{prop}
	Les chemins augmentant de $G$ pour le couplage $C$ sont en bijection avec les chemins élémentaires de $s$ vers $f$ dans $G_C$.
\end{prop}

\remarklist{
	\item Quand je suis sur un sommet de $Y$, il y a une seule possibilité de retour : je prend une arête du couplage pour revenir sur $X$.
	\item D'un côté il faut parcourir tous les voisins, de l'autre, c'est immédiat. C'est asymétrique.
}
On a donc un algorithme pour trouver un couplage optimal dans un graphe biparti en $O(\abs{S}(\abs{S} + \abs{A}))$. A chaque fois que l'on trouve un chemin augmentant, on ajoute au moins une arête au couplage et deux nouveaux sommets couverts. Il y a au plus $\frac{\abs{S}}{2}$ recherche de chemins augmentant, chacune étant en complexité $O(\abs{S} + \abs{A})$. Le calcul de $C \leftarrow C \triangle D$ peut se faire en $O(\abs{S} + \abs{A})$.

\begin{algorithm}
	\caption*{Recherche de couplage optimal}\label{b}
	\begin{algorithmic}
		\Require Un graphe $G = (S, A)$
		\Ensure Un couplage $C$ optimal
		\State $C := \emptyset$
		\While{il existe un chemin améliorant $D$ pour $C$}
		\State $C := C \triangle D$
		\EndWhile
		\State \Return $C$
	\end{algorithmic}
\end{algorithm}

Il y a au plus $\frac{\abs{S}}{2}$ itération avec la boucle tant que, et le "il existe un chemin améliorant" se fait en $O(\abs{S} + \abs{A})$.