\chaptr{Structure de données pour ensembles disjoints}{1}

Dans de nombreuses applications, on peut avoir besoin de gérer une partition d'un ensemble à $n$ éléments. En particulier, on peut ainsi représenter une relation d'équivalence. Deux éléments étant équivalents \ssi ils appartiennent à la même classe. 

\medskip

Dans ce chapitre, on s'intéresse à la conception d'une structure de données pour gérer efficacement une collection dynamique d'ensembles finis disjoints $S = S_1 \coprod ... \coprod S_k$ de classes. Les ensembles $S_i$ sont appelés les classes de $S$.

\medskip

Le mot dynamique veut dire "qui va évaluer au fil du temps", par opposition à statique qui veut dire fixer une fois pour toute au début.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}[scale=1.5]
		\node [draw,ellipse,minimum width=8cm,minimum height=3cm,thick](ell) at (0,0){};
		
		\draw[orange,ultra thick] (ell.150) to [out=-90, in=70]  coordinate[pos=0.38](p1)(ell.220);
		\draw[orange,ultra thick] (p1) to [out=10, in=100] (ell.310) coordinate[pos=0.55](p2);
		\draw[orange,ultra thick] (p2) to [out=45, in=-90] (ell.60) coordinate[pos=0.6](p3);
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (-2.5,1.) {$S$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-2,0) {$S_1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,0.6) {$S_2$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,-0.5) {$S_3$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.5,0) {$S_4$};
	\end{tikzpicture}
	\caption[Exemple de partition d'un ensemble]{Exemple de partition de $S$.}
\end{figure}

\begin{exemple}
	On observe notamment l'exemple de la représentation de graphes avec des matrices d'adjacence, ou des listes d'adjacence. Mais ce n'est pas efficace pour un ensemble qui varie. On peut aussi utiliser une table de hachage de liste d'adjacence.
\end{exemple}

\begin{exemple}
	On cherche à maintenir les composantes connexes d'un graphe non-orienté, mais avec des arrêtes qui peuvent être ajoutées et / ou retirées. 
	
	On peut rechercher les composantes connexes par un parcours en $O(\abs{S} + \abs{A})$ (voir chapitre suivant) mais il faudrait relancer la fonction à chaque fois.
\end{exemple}

\section{Unir et trouver}

\subsection{Représentant}

Dans un premier temps, il semble important de pouvoir "appeler" une classe par un nom, sans ambiguïté.

\newpage

\index{représentant}
\begin{defi}
	On peut représenter chaque classe par un identifiant, par exemple $i$ pour la classe $S_i$. On peut aussi choisir un élément particulier pour représenter la classe. On appelle cet élément le représentant de la classe.
\end{defi}

\begin{exemple}
	Dans $\znz$, on représente la classe de $i \in \brak{0, n-1}$ par $i$ (ou par $i + 3n$). Chaque classe est ainsi représenté par un unique représentant modulo $n$
\end{exemple}

\textbf{Notation :} Si $x \in S$, on note $S_x$ la classe de $x$ et $\overline{x}$ le représentant de la classe $S_x$. On a donc $S_x = S_{\overline{x}}$.

\remarklist{
	\item Dans certaines applications, on peut imposer des propriétés particulières sur ce représentant : par exemple, le plus petit. On demande en général que deux requêtes renvoient toujours le même représentant.
	\item Pour déterminer si deux éléments $x, y \in S$ sont équivalents, \cad appartiennent à la même classe, il suffit de comparer leurs représentants. On doit répondre à la question $\overline{x} \overset{?}{=} \overline{y}$. On s'intéresse
}

On s'intéresse donc à savoir trouver efficacement le représentant d'une classe ou d'un élément de cette classe : trouver $x \mapsto \overline{x}$.

\medskip

On définit une opération importante :
\index{union-find}
\begin{defi}
	La collection d'ensembles étant dynamique, on peut vouloir réaliser l'union ou la fusion de deux classes, par exemple réunion les classes des éléments $x$ et $y$ de $S$. On appelle cette opération \codeCaml{unir} (pas \codeCaml{union} car c'est un mot-clé en C).
	
	\medskip
	
	Cela explique le nom de la structure étudiée : union-find (unir et trouver).
\end{defi}

\subsection{Représentation par des noeuds (ici en C)}

Pour réaliser cette structure de données, on peut représenter chaque élément de $S$ âr un objet que l'on va appeler noeud et qui comporte cet élément, plus, éventuellement, d'autres informations utiles à la structure.

\medskip

\begin{listingsbox}{myc}{C}
	struct noeud {
		int val; // ou un autre type
		...
	};
	
	typedef struct noeud noeud;
\end{listingsbox}

\medskip

On souhaite obtenir les primitives suivantes : 

\begin{itemize}
	\item \codeCc{noeud* creer(int x)} qui permet de créer un nœud correspondant à un singleton $\{x\}$. Cela revient à ajouter une classe formée d'un élément à notre structure. Son représentant est $x$.
	\item \codeCc{noeud* trouver(noeud* x)} qui détermine les représentants $\overline{x}$ associé à un élément $x$.
	\item \codeCc{void unir(noeud* x, noeud* y)} réalisant la fusion des classes $S_x$ et $S_y$ : $S_x \cup S_y$, \cad si $\mathcal{P}$ est la partition de $S$, $\mathcal{P} \rightarrow \mathcal{P} \backslash \{S_x, S_y\} \cup \{S_x \cup S_y\}$. Les deux ensembles $S_x$ et $S_y$ sont supposés disjoints ou égaux avant l'opération.
\end{itemize}

\remarklist{
	\item Après l'opération, $S_x = S_y$.
	\item En général, le représentant de \codeCc{unir(x, y)} sera $\overline{x}$ ou $\overline{y}$.
	\item Il est clair que \codeCc{unir(x, y)} $=$ \codeCc{unir(a, b)} où $a$ et $b$ sont respectivement d'autres représentants des classes de $x$ et $y$. Lors de la réunion, on peut prendre n'importe quel élément des classes.
	\item \codeCc{unir(x, y) = unir(trouver(x), trouver(y))}. On note $n$ le nombre d'éléments de $S$ et donc, ce qui revient au même, le nombre d'appels à \codeCc{creer}. On note $m$ le nombre total d'appels à \codeCc{creer}, \codeCc{unir} et \codeCc{trouver}.
	\item Au plus $n-1$ appels à \codeCc{unir} réalisent quelque chose.
}

\subsection{Représentation par une structure}

Sans perte de généralité, on suppose que $S = \{0, 1, ..., n-1\}$ (sinon, on numérote les éléments et on utilise une table d'association). On représente l'information dans une structure à part. On suppose ici que l'on commence par créer les $n$ singletons.

\medskip

En OCaml, on obtient donc :

\medskip
 
\begin{listingsbox}{myocaml}{OCaml}
	type partition
	val creer : int -> partition (* cree la partition {{0}, ..., {n-1}} *)
	val trouver : partition -> int -> int (* trouver le representant d'un element *)
	val unir : partition -> int -> int -> unit
\end{listingsbox}

\medskip

\textbf{Remarque :} On peut faire en C ce que l'on fait en OCaml et vice-versa.

\section{Représentation naïve}

On implémente \codeCaml{partition} par un tableau \codeCaml{id} tel que \codeCaml{id.(i)} contient le représentant de $i$ qui est toujours le plus petit élément de la classe.

\medskip

On obtient donc l'implémentation suivante : 

\medskip

\begin{listingsbox}{myocaml}{OCaml}
	type partition = int array
	
	let creer n = Array.init n (fun i -> i)
	
	let trouver p i = p.(i)
	
	let union p x y =
		let rx = trouver p x in
		let ry = trouver p y in
		let subsititute t a h =
			for i = 0 to Array.length t - 1 do
				if t.(i) = a then t.(i) <- h
			done
		in substitute p (max rx ry) (min rx ry)
\end{listingsbox}

\medskip

On étudie les complexités des trois fonctions : 
\begin{itemize}
	\item \codeCaml{creer} est en $O(n)$.
	\item \codeCaml{trouver} est en $O(1)$.
	\item \codeCaml{union} est en $O(n)$.
\end{itemize}

\remarklist{
	\item A noter que l'on peut rajouter, avant la dernière ligne de la fonction \codeCaml{union}, une condition : \codeCaml{if rx <> ry then}.
	\item On peut commencer la boucle \codeCaml{for} à $a$. En effet, l'identifiant est toujours plus petit.
	\item \codeCaml{trouver} est efficace, mais \codeCaml{unir} ne l'est pas.
}

On peut se demander quelle peut être la complexité d'une séquence de $n-1$ appels à \codeCaml{unir} dans le pire cas. La solution est $\Theta(n^2)$.

\section{Représentation par une forêt}

On peut organiser les noeuds de la manière suivante : chaque noeud pointe vers un autre noeud de sa classe. Le représentant pointe vers lui-même, et c'est le seul de la classe à faire cela. On s'interdit les cycles. Ceci forme alors une forêt qui représente une partition. On trouve le représentant en remontant les pointeurs.

\medskip

\remarklist{
	\item On peut aussi faire pointer les représentants vers \codeCc{NULL}.
	\item On obtient une forêt, \cad un graphe acyclique, réunion de graphes connexes acycliques.
	\item Ici, on ne représente que les pointeurs parents.
}

\medskip

En C, on obtient l'implémentation suivante :

\medskip

\begin{listingsbox}{myc}{C}
	struct noeud {
		int val;
		struct noeud* parent;
	};
	
	typedef struct noeud noeud;
	
	noeud* creer(int val) 
	{
		noeud* n = malloc(sizeof(noeud));
		n -> val = val;
		n -> parent = n;
		return n;
	}
	
	noeud* trouver(noeud* x)
	{
		while (x -> parent != x)
		{
			x = x->parent;
		}
		return x;
	}
	
	void unir(noeud* x, noeud* y)
	{
		noeud* rx = trouver(x);
		rx -> parent = y;
	}	
\end{listingsbox}

\medskip

La création d'un noeud est en $O(1)$, \codeCaml{trouver} est linéaire en la profondeur du noeud dans l'arbre. Dans le pire cas, c'est en $O(n)$. Finalement, \codeCaml{unir} est en $O(1) +$ les appels à \codeCaml{trouver} donc en $O(n)$ dans le pire cas. On peut chercher en exercice une suite d'opérations qui réalise ce pire cas.
\medskip

\textbf{Remarque :} On peut améliorer la fonction \codeCaml{unir}. Le code suivant permet à priori de réduire la hauteur des arbres.

\medskip

\begin{listingsbox}{myc}{C}
	void unir(noeud* x, noeud* y)
	{
		noeud* rx = trouver(x);
		noeud* ry = trouver(y);
		rx->parent = ry;
	}
\end{listingsbox}

\medskip

En OCaml, on donnera l'implémentation suivante :

\medskip

\begin{listingsbox}{myocaml}{OCaml}
	type partition = int array
	
	let creer n = Array.init n (fun i -> i)
	
	let rec trouver p x =
		if p.(x) = x then x
		else trouver p p.(x)
	
	let unir p x y = p.(trouver p x) <- y
\end{listingsbox}

\medskip

\textbf{Remarque :} Il n'y a pas de conflit quand le représentant de $x$ est le même représentant que $y$.

\section{Union par rang}

On introduit une heuristique pour améliorer les complexités. 

\medskip

Dans \codeCaml{unir}, il vaut mieux faire pointer la racine de l'arbre le moins haut vers la racine de l'arbre le plus haut. Il n'est pas très difficile de garantir une forme d'équilibrage des arbres pour obtenir \codeCaml{trouver} et \codeCaml{unir} en $O(\log n)$.

\medskip

Il faut veiller, lors de l'union, à choisir comme racine celle de l'arbre le plus haut. Ainsi, la hauteur des arbres ne peut augmenter que lors de l'union de deux arbres de même hauteur. Bien sûr, il est hors de question de recalculer la hauteur à chaque fois, on les stock dans les noeuds. 

\index{rang}
\begin{defi}
	On appelle cela le rang. C'est un majorant de la hauteur.
\end{defi}

On obtient l'implémentation suivante en C :

\medskip

\begin{listingsbreakbox}{myc}{C}
	typedef struct noeud
	{
		int val;
		int rang;
		struct noeud* parent;
	} noeud;
	
	noeud* creer(int val)
	{
		noeud* n = malloc(sizeof(noeud));
		n->val = val;
		n->rang = 0;
		n->parent = n;
		return n;
	}
	
	// trouver est identique
	
	void unir(noeud* x, noeud* y)
	{
		noeud* rx = trouver(x);
		noeud* ry = trouver(y);
		if (rx == ry) {return; }
		if (rx->rang > ry->rang)
		{
			ry->parent = rx;
		} else 
		{
			rx->parent = ry;
			if (rx->rang == ry->rang)
			{
				ry->rang++;
			}
		}
	}
	
\end{listingsbreakbox}

\section{Compression de chemins}

\begin{listingsbox}{myc}{C}
	noeud* trouver(noeud* x) {
		if (x -> parent != x) {
			x -> parent = trouver(x -> parent);
		}
		return x -> parent;
	}
\end{listingsbox}

\medskip

En exercice, on essaiera de refaire ces deux heuristiques en OCaml.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}[scale=0.9]
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (-0.5,4.5) {$5$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (-3,3) {$4$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v5) at (2,3) {$8$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v4) at (-1.5,1.5) {$0$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (-4.5,1.5) {$9$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v7) at (0.5,1.5) {$3$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (3.5,1.5) {$1$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v8) at (-1,0) {$7$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (2,0) {$6$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v10) at (0.5,-1.5) {$2$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v11) at (3.5,-1.5) {$10$};
		\draw  (v1) edge (v2);
		\draw  (v2) edge (v3);
		\draw  (v2) edge (v4);
		\draw  (v1) edge (v5);
		\draw  (v5) edge (v6);
		\draw  (v5) edge (v7);
		\draw  (v7) edge (v8);
		\draw  (v7) edge (v9);
		\draw  (v9) edge (v10);
		\draw  (v9) edge (v11);
		
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v12) at (9,4.5) {$5$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v13) at (8,3) {$3$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v15) at (10,3) {$8$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v20) at (6,3) {$4$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v17) at (12,3) {$6$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v14) at (8,1.5) {$7$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v16) at (10,1.5) {$1$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v21) at (7,1.5) {$0$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v22) at (5,1.5) {$9$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v19) at (11,1.5) {$2$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v18) at (13,1.5) {$10$};
		\draw  (v12) edge (v13);
		\draw  (v13) edge (v14);
		\draw  (v12) edge (v15);
		\draw  (v15) edge (v16);
		\draw  (v12) edge (v17);
		\draw  (v17) edge (v18);
		\draw  (v17) edge (v19);
		\draw  (v12) edge (v20);
		\draw  (v20) edge (v21);
		\draw  (v20) edge (v22);
		
		\draw [-latex] plot[smooth, tension=.7] coordinates {(2.5,4.5) (4,5) (5.5,4.5)};
		\node [outer sep=0,inner sep=0,minimum size=0] at (4,5.5) {compression};
	\end{tikzpicture}
	\caption[Compression de chemins]{Compression après recherche du représentant de la classe de $6$.}
\end{figure}

\section{Analyse de la complexité}

Sans heuristique, la complexité de \codeCaml{unir} et \codeCaml{trouver} est en $O(n)$. Que devient-elle en ajoutant l'heuristique d'union par rang et de compression de chemins ?

\begin{prop}
	Si $x$ n'est pas représentant, $\rg(x) < \rg(\pere(x))$
\end{prop}

\begin{proof}
	Soit $y$ le père de $x$. Au moment où $y$ devient le père de $x$ (nécessairement dans \codeCaml{unir}) :
	\begin{itemize}
		\item soit $\rg(x) < \rg(y)$.
		\item soit $\rg(x) = \rg(y)$ et alors on incrémente le rang de $y$.
	\end{itemize}
	Dans les deux cas, on a $\rg(x) < \rg(y)$ après l'appel \codeCaml{unir}. De plus le rang de $x$ est alors fixé pour toujours (le rang d'un non représentant ne peut pas changer et un non représentant ne peut pas le devenir).
	D'un autre côté le rang de $y$ ne peut que croître.
	Donc on a toujours $rg(x) < rg(y)$
\end{proof}

\remarklist{
	\item Les rangs sont strictement décroissants sur une branche quelconque.
	\item Le rang d'un ancêtre propre de $x$ est strictement plus grand que le rang de $x$.
	\item La propriété reste vrai même avec compression de chemin. Dans ce cas, on peut associer à un noeud $x$ un nouveau père qui est un ancêtre de $x$ donc de rang strictement supérieur.
}

\begin{prop}
	Un noeud $x$ de rang $K$ est racine d'un arbre de hauteur au plus $K$. Autrement dit, le rang majore la hauteur.
\end{prop}

\begin{proof}
	 Les noeuds sur un chemin de $x$ à une feuille sont de rang dans $\brak{0, K}$ tous différents puisque le rang de $x$ est $K$, le rang d'une feuille est positif et strictement décroissant. Il y a donc au plus $K+1$ sommets sur ce chemin qui est donc de longueur au plus $K$.
\end{proof}

\begin{prop}
	Un noeud de rang $K$ est racine d'un arbre comportant au moins $2^K$ noeuds.
\end{prop}

\begin{proof}
	On procède par récurrence sur $K \in \mathbb{N}$.
	
	\medskip 
	
	\textbf{Initialisation} : si $K = 0$, un noeud de rang $0$ est racine d'un arbre qui le contient au moins (et même exactement) lui même et donc de taille au moins $1$.
	
	\medskip
	
	\textbf{Hérédité} : On suppose que la propriété est vraie au rang $K \in \mathbb{N}$. Soit $x$ de rang $K+1$.
	Comme $x$ est de rang $K+1$ il est devenu père d'un noeud $y$ dans un appel à \codeCaml{unir} avec, au moment de cette liaison, $x$ de rang $K$ et $y$ de rang $K$ (seul moyen pour avoir un incrément de rang).
	Par hypothèse de récurrence, $x$ et $y$ sont racines d'arbres comportant au moins $2^K$ noeuds et donc $x$ comporte au moins $2^{K+1}$ noeuds.
	Même en cas de compression de chemin, comme $x$ est représentant, tous ses descendants restent ses descendants.
	Il est possible d'avoir d'autres appels à \codeCaml{unir} avec $x$ comme racine ce qui ne peut qu'augmenter le nombre de noeuds.
\end{proof}

\remarklist{
	\item Il y a au plus $0$ noeuds de rang supérieur à $\lfloor \log_{2}n \rfloor + 1$.
	\item On vient de voir que tous les arbres de la forêt ont une hauteur majorée par $\lfloor \log_{2}n \rfloor$. Ils sont équilibrés. On a donc \codeCaml{trouver} et \codeCaml{unir} en $O(\log n)$.
}

\index{fonction!d'Ackermann}
\begin{prop}
	En utilisant les deux heuristiques la complexité est en réalité excellente.
	Une série de $m$ opérations donc $n$ appels à \codeCaml{creer} est en complexité $O(m\alpha(n))$ où $\alpha$ est une forme de réciproque de la fonction d'Ackermann.
	On a $\alpha(n) \leq 4$ pour tout $n \in \brak{0, 10^{100}}$.
	Autrement dit, la complexité amorti de cette structure est en $O(\alpha(n))$, \cad $\Theta(1)$.
\end{prop}