\chaptr{Algorithme $A^*$}{1}

Dans cette partie, on considère des graphes pondérés et (a priori) orientés. On notera $\rho(e)$ le poids d'un arc $e$.

\section{Rappels sur l'algorithme de Dijkstra}

\index{distance}
\begin{defi}
	Si $G$ est un graphe pondéré ne possédant pas de cycle de poids strictement négatif, on définit la distance de $x$ à $y$ par :
	$$d_G(x, y) = \inf \{\rho(c) \mid c \text{ chemin de } x \text{ à } y\},$$
	avec la convention que $\inf \emptyset = +\infty$. Un plus court chemin de $x$ à $y$ est un chemin $c$ de $x$ à $y$ vérifiant $\rho(c) = d(x, y)$.
\end{defi}

\remarklist{
	\item Cette définition a bien un sens, puisque la condition sur l'absence de cycle de poids négatif permet de se limiter aux chemins élémentaires (à partir d'un chemin quelconque de $x$ à $y$ de poids $p$, on peut toujours obtenir un chemin élémentaire de $x$ à $y$ de poids $p' \leq p$ en supprimant les cycles). Les chemins élémentaires étant en nombre fini (ils possèdent au plus $n-1$ arêtes), on a en fait $d_G(x, y) = \min \{\rho(c) \mid c \text{ chemin élémentaire de } x \text{ à } y\}$.
	\item On peut ici avoir une distance négative, puisqu'on n'exige pas que les arcs soient à poids positif.
}

\begin{prop}
	Soit $G = (V, E, \rho)$ un graphe pondéré sans cycle de poids strictement négatif.
	\begin{itemize}
		\item $d_G$ vérifie l'inégalité triangulaire $d_G(x, z) \leq d_G(x, y) + d_G(y, z)$.
		\item Si $G$ est à poids strictement positif, alors $d_G(x, y) \geq 0$ et $d_G(x, y) = 0$ équivaut à $x = y$.
		\item Si $G$ est non-orienté, alors $d_G$ est symétrique et $d_G(x, y) = d_G(y, x)$.
		\item Si $G$ est non orienté, connexe, à poids strictement positif, alors $d_G$ est une distance au sens mathématique du terme.
	\end{itemize}
\end{prop}

\textbf{Version classique de Dijkstra :} L'algorithme de Dijkstra est fondamentalement une adaptation du parcours en largeur au cas des graphes pondérés, obtenue en remplaçant la file du BFS par une file de priorité.

\index{{\scshape Algorithme}! de Dijkstra}
\begin{algorithm}[!h]
	\caption{Dijkstra}
	\begin{algorithmic}
		\Require un graphe pondéré à poids positifs $G$ à $n$ sommets et un sommet $s$.
		\Ensure un tableau \codeCaml{dist} tel que \codeCaml{dist[k] = } $d(s, k)$ pour $0 \leq k < n$.
		
		\Function{Dijkstra}{G, s}
		\State dist $\leftarrow (\infty, \dots, \infty)$
		\State dist[s] = 0
		 \State ouverts $\leftarrow$ FilePrioVide()
		 \State Insérer(ouverts, s, 0)
		 \While{ouverts $\neq \emptyset$}
		 \State (j, $d_j$) $\leftarrow$ ExtraireMin(ouverts)
		 \For{k successeur de j}
		 \State $d \leftarrow$ $d_j + \rho(j \rightarrow k)$
		 \If{d $<$ dist[k]}
		 \If{dist[k] $< \infty$}
		 \State DiminuerPriorité(ouverts, k, d)
		 \Else
		 \State Insérer(ouverts, k, d)
		 \EndIf
		 \State dist[k] $\leftarrow$ d
		 \EndIf
		 \EndFor
		 \EndWhile
		 \State \Return dist
		 \EndFunction
	\end{algorithmic}
\end{algorithm}

\begin{theorem}
	Si $G = (V, E, \rho)$ est un graphe pondéré à poids positifs, l'algorithme de Dijkstra :
	\begin{itemize}
		\item extrait les sommets de la file par distance croissante au sommet initial $s$.
		\item renvoie un tableau \codeCaml{dist} vérifiant \codeCaml{dist[v]} = $d_G(s, x)$ pour tout sommet $v$.
		\item a une complexité en espace $O(\abs{V})$.
		\item effectue au plus $\abs{V}$ opérations \codeCaml{Insérer}, $\abs{V}$ opérations \codeCaml{ExtraireMin} et $\abs{E}$ opérations \codeCaml{Diminuer-Priorité}.
		\item peut être implémenté avec une complexité $O((\abs{E} + \abs{V})\log \abs{V})$ en réalisant la file de priorité par un tas binaire.
	\end{itemize}
\end{theorem}

\textbf{Remarque :} Il existe des réalisations de la structure de file de priorité (tas de Fibonacci, en particulier) dotées de meilleurs complexités (amorties) que les tas binaires pour certaines opérations, dont en particulier \codeCaml{DiminuerPriorité}. On peut ainsi obtenir une complexité totale pour Dijkstra en $O(\abs{E} + \abs{V} \cdot \log \abs{V})$. Notons toutefois que cette amélioration est surtout théorique : un tas de Fibonacci est bien plus compliqué à implémenter qu'un tas binaire, et sera plus lent en pratique dans presque tous les cas.

\medskip

\textbf{Variante simplifiée :} Il est possible d'implémenter l'algorithme de Dijkstra en utilisant une file de priorité "basique", ne proposant pas l'opération \codeCaml{DiminuerPriorité} :

\begin{algorithm}[!h]
	\caption{Dijkstra}
	\begin{algorithmic}
		\Require un graphe pondéré à poids positifs $G$ à $n$ sommets et un sommet $s$.
		\Ensure un tableau \codeCaml{dist} tel que \codeCaml{dist[k] = } $d(s, k)$ pour $0 \leq k < n$.
		
		\Function{Dijkstra}{G, s}
		\State dist $\leftarrow (\infty, \dots, \infty)$
		\State ouverts $\leftarrow$ FilePrioVide()
		\State Insérer(ouverts, s, 0)
		\While{ouverts $\neq \emptyset$}
		\State (j, $d_j$) $\leftarrow$ ExtraireMin(ouverts)
		\If{$d_j <$ dist[j]}
		\State dist[j] $\leftarrow$ $d_j$
		\For{k successeur de j}
		\State $d \leftarrow$ $d_j + \rho(j \rightarrow k)$
		\If{d $<$ dist[k]}
		\State Insérer(ouverts, k, d)
		\EndIf
		\EndFor
		\EndIf
		\EndWhile
		\State \Return dist
		\EndFunction
	\end{algorithmic}
\end{algorithm}

Dans cette variante, on ré-insère le sommet avec une priorité plus basse au lieu de diminuer sa priorité dans la file. On extraira donc plusieurs fois un même sommet, mais on ignorera toutes ces extractions sauf la première.

\medskip

Un sommet étant inséré au plus une fois pour chacun de ses prédécesseurs, on effectue au plus $\abs{E}$ appels à \codeCaml{Extraire} et la file peut contenir jusqu'à $\abs{E}$ éléments. On a donc une complexité temporelle en $O(\abs{E}\log\abs{E} + \abs{V})$ (le $\abs{V}$ venant de l'initialisation du tableau \codeCaml{dist}). Comme $\abs{E} \leq \abs{V}^2$, on a $\log \abs{E} \leq 2\log\abs{V}$ et la complexité est donc inchangée par rapport à la version avec \codeCaml{DiminuerPriorité}. La complexité spatiale, en revanche, passe de $O(\abs{V})$ à $O(\abs{E} + \abs{V})$. Ce n'est que très rarement gênant.

\newpage

\section{Plus court chemin d'une source à un but}

L'algorithme de Dijkstra recherche des plus courts chemins depuis un sommet source vers tous les sommets du graphe. Il arrive souvent qu'on ne soit intéressé que par un plus court chemin depuis un sommet source vers un sommet but : c'est le cas pour une recherche d'itinéraire dans un réseau de transport, pour la planification  de mouvement en robotique ou dans le jeu vidéo...

\medskip

On peut bien sûr modifier l'algorithme de Dijkstra pour qu'il s'arrête dès qu'un plus court chemin vers un certain sommet but : il suffit d'ajouter deux lignes dans le pseudo code.

\begin{algorithm}[!h]
	\caption{Dijkstra}
	\begin{algorithmic}
		\Require un graphe pondéré à poids positifs $G$ à $n$ sommets, un sommet source $s$ et un sommet but $b$.
		\Ensure la distance de $s$ à $b$ dans le graphe $G$.
		
		\Function{Dijkstra}{G, s}
		\State dist $\leftarrow (\infty, \dots, \infty)$
		\State dist[s] = 0
		\State ouverts $\leftarrow$ FilePrioVide()
		\State Insérer(ouverts, s, 0)
		\While{ouverts $\neq \emptyset$}
		\State (j, $d_j$) $\leftarrow$ ExtraireMin(ouverts)
		\If{$j = b$}
		\State \Return dist[b]
		\EndIf
		\For{k successeur de j}
		\State $d \leftarrow$ $d_j + \rho(j \rightarrow k)$
		\If{$d <$ dist[k]}
		\If{dist[k] $< \infty$}
		\State DiminuerPriorité(ouverts, k, d)
		\Else 
		\State Insérer(ouverts, k, d)
		\EndIf
		\State dist[k] $\leftarrow$ d
		\EndIf
		\EndFor
		\EndWhile
		\State \Return $\infty$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

Cependant, cette modification ne change au caractère omnidirectionnel de la recherche : on examine tous les sommets par distance croissante à l'origine, et l'on s'arrête dès que l'on atteint notre but.

\section{Algorithme $A^*$}

\index{{\scshape Algorithme}!A*}
Le principe de l'algorithme $A^*$ est d'utiliser une heuristique pour orienter la recherche vers le but. Cette heuristique prend la forme d'une fonction $h$ qui associe à chaque sommet $v$ une valeur $h(v)$, censée être une approximation de $d(v, \text{but})$. Au lieu d'explorer les sommets $v$ par distance $d(\text{source}, v)$ croissante à la source, on les explore par le coût minimal estimé d'un chemin de la source au bout passant par $v$. Cette estimation est composée de deux termes :
\begin{itemize}
	\item un terme $d'(\text{source}, v)$ qui correspond au plus court chemin connu de source à $v$ (et qui sera donc une majoration de $d(\text{source}, v)$).
	\item un terme $h(v)$ estimant la distance qui nous reste à parcourir une fois arrivé au sommet $v$. Nous verrons plus loin que la correction de l'algorithme ne sera assurée que si cette estimation est une majoration de la distance $d(v, \text{but})$.
\end{itemize}

Le cas le plus classique, et le plus intuitif, est celui d'un graphe euclidien, où chaque sommet correspond à un point du plan, et le poids d'un arc $v \to v'$, s'il existe, vaut $\norm{v' - v}_2$. Cela correspond essentiellement à une recherche de plus court chemin dans une ville dans laquelle la portion d'une rue comprise entre deux intersections est un segment de droite. On peut alors prendre pour $h(v)$ la distance "à vol d'oiseau" de $v$ à but.

\medskip

Le principe de l'algorithme est le suivant :
\begin{itemize}
	\item le tableau \codeCaml{dist} indique pour chaque sommet $v$ le poids du plus court chemin trouvé jusqu'à présent de source à $v$.
	\item la file de priorité ouverts contient les sommets $v$ à explorer, avec comme priorité les valeurs de \codeCaml{dist[v] + h(v)}.
	\item le tableau \codeCaml{parents} code un arbre permettant de reconstruire un chemin depuis la source jusqu'à chaque sommet exploré.
\end{itemize}

\medskip

\textbf{Remarque :}  En pratique, on utiliserait plutôt des dictionnaires que des tableaux pour \codeCaml{dist} et \codeCaml{parents}. En effet, l'intérêt de $A^*$ est qu'on peut le plus souvent n'explorer qu'une petite partie du graphe (voir page suivante), et il est donc dommage de réserver une quantifié de mémoire proportionnelle au nombre total de sommets. Dans le cas assez fréquent où le graphe n'est connu que de manière implicite, cela peut même s'avérer impossible.

\begin{algorithm}[p]
	\caption{$A^*$}
	\begin{minipage}{0.48\textwidth}
		\begin{algorithmic}
			\Function{InsérerOuDiminuer}{file, v, p}
			\If{$v \in$ file}
			\State DiminuerPriorité(file, v, p)
			\Else
			\State Insérer(file, v, p)
			\EndIf
			\EndFunction
		\end{algorithmic}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
		\begin{algorithmic}
			\Function{Reconstruire}{parents, source, but}
			\State chemin $\leftarrow$ (but)
			\State sommet $\leftarrow$ but
			\While{sommet $\neq$ source}
			\State sommet $\leftarrow$ parents[sommet]
			\State chemin $\leftarrow$ sommet, chemin
			\EndWhile
			\State \Return chemin
			\EndFunction
			\State
		\end{algorithmic}
	\end{minipage}
	
	\begin{algorithmic}
		\Require un graphe pondéré $G = (V, E, \rho)$, une heuristique $h$, un sommet \codeCaml{source} et un sommet \codeCaml{but}.
		\Ensure un chemin (de poids minimal ?) de \codeCaml{source} à \codeCaml{but}, s'il en existe un.
		
		\Function{$A^*$}{G, source, but}
		\State dist $\leftarrow (\infty, \dots, \infty)$
		\State dist[source] = 0
		\State parents $\leftarrow$ (Nil, \dots, Nil)
		\State ouverts $\leftarrow$ FilePrioVide()
		\State Insérer(ouverts, source, h(source))
		\While{ouverts $\neq \emptyset$}
		\State (v, \_) $\leftarrow$ ExtraireMin(ouverts)
		\If{$v =$ but}
		\State \Return Reconstruire(parents, source, but)
		\EndIf
		\For{v' successeur de v}
		\State $d \leftarrow$ dist[v] $+ \rho(v \rightarrow v')$
		\If{$d <$ dist[k]}
		\State parents[v'] $\leftarrow$ v
		\State dist[v'] $\leftarrow$ d
		\State InsérerOuDiminuer(ouverts, v', d+h(v'))
		\EndIf
		\EndFor
		\EndWhile
		\State \Return $\emptyset$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\begin{figure}[p]
	\centering
	\captionsetup{justification=centering}
	\includegraphics[scale=0.8]{img/astarlyon.jpg}
	\caption[Exemple visuel du $A^*$]{Plus court chemin du Lycée du Parc à l'ENS Lyon, algorithme de Dijkstra à gauche et $A^*$ à droite. Les sommets sont coloriés en fonction de leur instant d'exploration. (figure de M. Bianquis)}
\end{figure}

\newpage

\section{Heuristique admissible, heuristique cohérente}

Si un chemin de la source vers le but existe, la fonction en renverra bien un (et elle détectera l'absence de chemin le cas échéant). En revanche, sans hypothèse sur l'heuristique, il n'y a aucune raison que le chemin renvoyé soit de poids minimal.

\medskip

\index{heuristique!admissible}
\index{heuristique!cohérente}
\begin{defi}
    Une heuristique $h$ est dite : 
    \begin{itemize}
    \item \textbf{admissible} si $h(v) \leq d(v, \text{but})$.
    \item \textbf{cohérente} (ou monotone) si  $h(v) \leq \rho(v \to v') + h(v')$ pour tout arc $v \to v'$.
    \end{itemize}
\end{defi}

\textbf{Remarque :} Une heuristique est donc admissible si elle est "optimiste", c'est-à-dire si elle ne surestime jamais la distance d'un sommet au but.

\begin{prop}
    Si $h$ est cohérente, alors la quantité $h(v) + d(x, v)$ est croissante le long d'un chemin de poids minimal de $x$ à but (pour tout sommet $x$ ). De plus, si $h$ est cohérente et si $h(\text{but})$, alors $h$ est admissible.
\end{prop}

\begin{proof}
    Si source = $v_0, \ldots, v_k =$ but est un chemin de poids minimal, alors on a $d(v_i, \text{but}) = d(v_{i+1}, \text{but}) + \rho(v_i \to v_{i+1})$ pour $0 \leq i < k$. Si $h$ est cohérente, on en déduit $h(v_i) + d(v_0, v_i) \leq \rho(v_i \to v_{i+1}) + h(v_{i+1}) + d(v_0, v_i) = h(v_{i+1}) + d(v_0, v_{i+1})$.

    \medskip

    En considérant un chemin de poids minimal de $v$ à but, on a donc $h(v) + d(v, v) \leq h(\text{but}) + d(v, \text{but})$, c'est-à-dire $h(v) \leq d(v, \text{but})$ puisqu'on a supposé $h(\text{but}) =0$.
\end{proof}

\begin{exemple}
    Dans un graphe euclidien, la distance euclidienne (i.e. à vol d'oiseau) fournit une heuristique cohérente qui vérifie $h(\text{but}) = 0$ (et est donc admissible).
\end{exemple}

\begin{prop}
    Si l'heuristique $h$ est admissible, alors l'algorithme $A^*$ renvoie un chemin de poids minimal.
\end{prop}

\newcommand{\score}{\text{score}}
\newcommand{\dist}{\text{dist}}
\newcommand{\but}{\text{but}}
\newcommand{\source}{\text{source}}

\begin{proof}
    Pour un sommet $v$, notons $\score(v) = \dist[v] + h(v)$. Cette quantité varie au cours de l'exécution de l'algorithme, mais il est clair qu'elle est décroissante (au sens large). Considérons alors un chemin $c : \source = v_0, \ldots, v_k = \but$ de poids minimal $\rho(c)$, et supposons que l'algorithme renvoie un chemin $c'$ de poids total $\rho(c') > \rho(c)$. L'exécution s'est terminée lorsqu'on a extrait but, dont le score valait dist[but] $+ h(\but) = \dist[\but]$ (l'heuristique étant admissible). Or, dist[but] est précisément du poids du meilleur chemin que l'on a trouvé, donc, $\rho(c') = \dist[\but]$. Ainsi, tout sommet qui a eu pendant l'exécution de l'algorithme un score strictement inférieur à $\rho(c')$ a été extrait avant la fin de l'exécution. 

    \medskip 

    Montrons par récurrence sur $i \leq k$ que le sommet $v_i$ a été extrait avant but, et que $\dist[v_i] \leq \rho(v_0 \to \ldots \to v_i)$ au moment de cette extraction.
    \begin{itemize}
        \item C'est vrai pour $v_0$, le sommet source qui est exploré en premier avec $\dist[v_0] = 0$.
        \item En supposant la propriété vraie pour $v_i$, on considère l'arc $v_i \to v_{i+1}$ au moment de l'exploration de $v_i$, et l'on fixe donc $\dist[v_{i+1}]$ à une valeur inférieure ou égal à $\dist[v_i] + \rho(v_i \to v_{i+1}) = \rho(v_0 \to \ldots \to v_{i+1})$ par hypothèse de récurrence. On a alors $\score(v_{i+1}) \leq \rho(v_0 \to \ldots \to v_{i+1}) + h(v_{i+1})$. Comme l'heuristique est admissible, on en déduit $\score(v_{i+1}) \leq \rho(v_0 \to \ldots v_{i+1}) + d(v_{i+1}, \but) \leq \rho(c) < \rho(c')$. Donc, $v_{i+1}$ sera extrait avant but.
    \end{itemize}

    On conclut donc que $v_k = \but$ est extrait avant but, avec une valeur de $\dist[v_k] \le \rho(c) < \rho(c')$, ce qui est absurde.
\end{proof}

Notons toutefois que si l'heuristique est seulement supposée admissible, un sommet (autre que sommet but) peut être extrait à plusieurs reprises de la file, ce qui peut dans certains cas pathologiques résulter en une complexité exponentielle en le nombre de sommets du graphe.

\begin{prop}
    Si l'heuristique $h$ est cohérente, alors un nœud est extrait au plus une fois de la file de priorité.
\end{prop}

\begin{proof}
    Lorsqu'on extrait un sommet $v$ de la file, avec un score $\score(v) = \dist[v] + h(v)$ à cet instant, on insère (certains de) ses voisins $v'$ avec des scores $\score(v') = \dist[v] + \rho(v \to v') + h(v')$. Comme l'heuristique est cohérente, on a $\rho(v \to v') + h(v') \ge  h(v)$ et donc $\score(v') \ge \score(v)$. Ainsi, les scores des sommets extraits de la file croissent au cours du temps.

    \medskip

    Si l'on extrait ultérieurement un prédécesseur $v''$ de $v$, on aura donc $\dist[v''] + h(v'') \geq \dist[v] + h(v)$. A nouveau, $h(v'') \leq \rho(v'' \to v) + h(v)$, donc $\dist[v''] + \rho(v'' \to v) \ge \dist[v] : v$ n'est pas ré-inséré.
\end{proof}
