\chaptr{Algorithmes d'approximation}{1}

\section{Motivation}

Savoir qu'un problème est $\cNP$-complet élimine tout espoir de le résoudre en tant polynomial. La résolution d'un tel problème nécessite (aujourd'hui) pratiquement toujours d'explorer un nombre exponentiel de solution candidates (même après élagage dans une approche de retour sur trace). 

\medskip

Mais il ne faut pas baisser les bras pour autant, ni perdre espoir de résoudre le problème de manière plus ou moins satisfaisante.

\begin{itemize}
	\item Il n'existe pas d'algorithme polynomial dans le pire cas. Mais les instances pire cas sont parfois assez exotiques. Un algorithme (exponentiel) peut très bien se comporter correctement en pratique (en moyenne). 
	\item On peut imaginer des heuristiques efficaces pour une approche de retour sur trace (et éliminer $99,999\%$ de l'espace de recherche)
	\item On peut utiliser des algorithmes probabilistes qui donne une solution correcte dans la plupart des cas ou une solution exacte de manière souvent rapide.
	\item On peut parfois se contenter d'une "bonne" solution à défaut d'une solution optimale. On utilise des algorithmes d'approximation. Il est en général essentiel de quantifier la qualité de cette approximation.
\end{itemize}

Un grand nombre de problèmes $\cNP$-complet peuvent être résolus en faisant certaines concessions : soit le temps de calcul, soit la qualité de la solution.

\section{Facteur d'approximation}

Dans ce chapitre, on ne considère que des problèmes d'optimisation :
\begin{defi}
	On définit un problème d'optimisation par :
	\begin{itemize}
		\item \textit{INSTANCE :} $x \in X$
		\item \textit{SOLUTIONS :} $Y(x) \subseteq Y$ des solutions admissibles pour $x$.
		\item \textit{OPTIMISATION :} Une fonction de coût $\fonct{f}{X \times Y}{\RR^*_+}{(x, y)}{f(x, y)}$ pour $x \in X$ et $y \in Y(x)$. $f(x, y)$ est le coût de la solution $y$ pour $x$.
	\end{itemize}
	
	On cherche $c^*(x) = \underset{y \in Y(x)}{\min} f(x, y)$ et le plus souvent, une solution $y^*$ telle que $c^*(x) = f(x, y^*)$. 
\end{defi}

\medskip

Un algorithme $A$ (non exact) pour un problème $P$ calcule pour $x \in X$ une solution $y \in Y(x) = A(x)$. On note $c_A(x) = y$. 

\medskip

Mais alors, comment quantifier la qualité de la solution $y$. On a $0 < c^*(x) \leq y = c(x)$. On aimerait par exemple garantir que pour tout $x \in X, \abs{c^*(x) - c_A(x)} \leq \beta$ avec $\beta \in \RR^*$ une constante. Il y a très peu de problèmes $\cNP$-complets approximables avec cette définition. 

\medskip

Seul exemple que le prof connais : EDGE-COLOR. Est-il possible de colorier les arêtes d'un graphe $G = (S, A)$ avec $k$ couleurs : il faut au moins $\deg(G)$ couleurs et au maximum $\deg(G) + 1$. On va plutôt prendre un facteur multiplicatif.

\index{algorithme!d'approximation}
\index{approximation}
\begin{defi}
	Soit $P$ un problème d'optimisation $(X, x \to Y(x), f)$. Pour une instance de $x \in X$, on note $c^*(x) = \underset{y \in Y(x)}{\min} f(x, y)$ la valeur optimale pour $x \in X$. On considère un algorithme $A$ pour $P$. Pour $x \in X$, on note $y = A(x)$ la solution renvoyée par $A$ et $c_A(x) = f(x, y)$ la valeur de cette solution. Pour $\alpha \geq 1$, on dit que $A$ est un algorithme d'approximation de $P$ de facteur $\alpha$ ou que $A$ est une $\alpha$-approximation de $P$ si pour tout $x \in X$, $c_A(x) \leq \alpha c^*(x)$. Autrement dit, la solution renvoyée par $A$ est à un facteur $\alpha$ près de la solution optimale.
\end{defi}

\remarklist{
	\item On a bien sûr $c^*(x) \leq c_A(x)$. Au final, $0 < c^*(x) \leq c_A(x) \leq \alpha c^*(x)$.
	\item $\frac{c_A(x)}{c^*(x)} \leq \alpha$.
}

\begin{exemple}
	Une $2$-approximation est une solution au pire deux fois plus grande que la solution optimale.
\end{exemple}

\remarklist{
	\item Si $\alpha \leq \beta$, une $\alpha$-approximation est une $\beta$-approximation. Le meilleur ratio d'approximation possible (le meilleur facteur d'approximation pour un problème $P$ approximable) est $\underset{A \text{ algorithme d'approximation de } P}{\inf} \underset{x \in X}{\sup} \frac{c_A(x)}{c^*(x)}$.
	\item Si $A$ est une $1$-approximation de $P$, alors $A$ résout exactement le problème $P$.
	\item Il faut que $A$ soit efficace. On demande à ce que $A$ soit polynomial (dans la définition).
}

On a présenté tout ceci dans le contexte de la minimisation. Si $P$ est un problème de maximisation, alors pour être une $\alpha$-approximation avec $\alpha \leq 1$, il faut que pour tout $x \in X$, $c^*(x) \geq c_A(x) \geq \alpha c^*(x) \geq 0$. D'autres conventions peuvent exister, mais elles seront définies au début des sujets.

\begin{exemple}
	Pour $\alpha = \frac{1}{2}$, on a un exemple similaire à celui développé précédemment.
\end{exemple}

\remarklist{
	\item On peut aussi parler de $\alpha(n)$-approximation où $\alpha$ dépende de $n = \abs{x}$ la taille de l'entrée.
	\item Les algorithmes gloutons sont souvent de bons algorithmes d'approximation.
}

\section{Exemples}

\index{{\scshape Problème}!COUVERTURE-MIN}
\subsection{COUVERTURE-MIN}

\begin{exemple}
	\begin{itemize}
		\item \textit{INSTANCE :} $G = (S, A)$.
		\item \textit{SOLUTION :} $X \subseteq S$ tel que pour tout $\{x, y\} \in A$, $x \in X$ ou $y \in X$.
		\item \textit{OPTIMISATION :} $\min \abs{X}$.
	\end{itemize}
\end{exemple}

\textbf{Idée n°$1$ :} $A$ renvoie toujours $X = S$. Soit $\alpha \geq 1$, si $\abs{A} = 1$, la solution renvoyée a pour cout $\abs{S}$ la solution optimale $1$ et $\abs{S} \leq \alpha$. Ceci pour tout $n = \abs{S} \in \NN$. 

\medskip

\textbf{Attention :} ce n'est pas $\alpha(x)$ mais $\alpha$. C'est une $n$-approximation cependant.

\medskip

\textbf{Idée n°$2$ :}
\begin{algorithm}
	\caption*{Algorithme $A$}\label{c}
	\begin{algorithmic}
		\State $X = \emptyset$
		\While{il reste des arêtes non-couvertes}
		\State prendre une de ces arêtes $\{x, y\}$
		\State $X \leftarrow X \cup \{x\} \cup \{y\}$
		\EndWhile
		\Return \State $X$
	\end{algorithmic}
\end{algorithm}

$A$ renvoie bien une couverture et est bien polynomial. Ce n'est pas une $\alpha$-approximation. Un contre-exemple se trouve avec $S_{2 + \ceil{\alpha}}$, un graphe étoile. On devrait avoir $c_A(x) \leq \alpha c^*(x)$ donc $\alpha + 1 \leq \alpha \times 1$. Absurde.

\medskip

\textbf{Remarque :} L'algorithme ainsi modifié calcule un couplage maximal de $G$. On propose donc la reformulation suivante :
\begin{itemize}
	\item On calcule un couplage maximal (algorithme glouton).
	\item On renvoie les sommets couverts par ce couplage.
\end{itemize}

\medskip

$A$ est bien polynomial et renvoie bien une couverture. En effet, si $\{x, y\} \in A$ pas couverte, alors ses extrémités sont libres et rajouter cette arête donne un couplage contenant strictement le couplage maximal. Absurde.

\medskip

Comment montrer que c'est une $2$-approximation ? On doit montrer que $c_A(x) \leq \alpha c^*(x)$, sachant que $c^*(x)$ est inconnu. Soit $X$ une solution optimale (*X* couvre toutes les arêtes). Soit $X_A$ la solution renvoyée par $A$ et $B$ le couplage (maximal). On a : $c_A(x) = \abs{X_A} = 2\abs{B}$. Si $a \in B$, alors l'arête est couverte par un sommet de $X$. Chaque arête de $B$ doit être couverte par un sommet différent de $X$ car $B$ est un couplage. Donc, $\abs{X} \geq \abs{B}$. Donc, $2\abs{X} \geq 2\abs{B} = \abs{X_A}$, d'où $2c^*(x) \geq c_A(x)$. Donc cet algorithme est une $2$-approximation. $c^*(x) \leq c_A(x)$ mais aussi $c_A(x) \leq 2c^*(x)$.

\subsection{VOYAGEUR-DE-COMMERCE}
\index{{\scshape Problème}!VOYAGEUR-DE-COMMERCE}
\begin{exemple}
	\begin{itemize}
		\item \textit{INSTANCE :} $G = (S, A, \omega)$ pondéré complet, $\omega : A \to \NN^*$.
		\item \textit{SOLUTION :} $\pi$ permutation des sommets.
		\item \textit{OPTIMISATION :} $\min \summ_{i=0}^{\abs{S}-1} \omega(s_{\pi(i)}, s_{\pi(i+1) \pmod{\abs{S}}})$.
	\end{itemize}
\end{exemple}

On peut également proposer une version décisionnelle :
\begin{itemize}
	\item \textit{INSTANCE :} $G = (S, A, \omega)$ pondéré complet, $\omega : A \to \NN^*$ et $p \in \NN^*$ un seuil.
	\item \textit{QUESTION :} Existe-t-il une permutation $\pi \in G_{\abs{S}}$ de poids $\summ_{i=0}^{\abs{S}-1} \omega(s_{\pi(i)}, s_{\pi(i+1) \pmod{\abs{S}}}) \leq p$.
\end{itemize}

Ce problème est $\cNP$. Un certificat est une permutation de $\pi \in G_{\abs{S}}$ et on sait calculer en temps polynomial en $\abs{S} + \abs{A}$ le poids du chemin correspondant et vérifier que ce poids est inférieur à $p$. 

\begin{prop} 
	VOYAGEUR-DE-COMMERCE est $\cNP$-complet si on suppose que $\omega$ vérifie l'inégalité triangulaire, \cad que pour tout $x, y, z \in S, \omega(x, z) \leq \omega(x, y) + \omega(y, z)$.
\end{prop}

\begin{proof}
	On a déjà vu qu'il était $\cNP$. On essaye de réduire le problème du cycle hamiltonien. A un graphe $G = (S, A)$, on associe le graphe $G' = (S', A', \omega)$ avec $A' = \mathcal{P}_2(S) = \{\{s, t\} \mid s, t \in S, s \neq t \}$ et $\fonct{\omega}{A'}{\NN^*}{a}{\begin{cases}
			\omega(a) = 1 \text{ si } a \in A \\
			\omega(a) = 2 \text{ sinon }
	\end{cases}}$

	\medskip
	
	A noter que $\omega$ vérifie l'inégalité triangulaire car $a + b \leq c$ est toujours vraie pour $a, b, c \in \{1, 2\}$.
	
	Montrons que $G$ possède un cycle hamiltonien \ssi $G'$ possède un chemin $\pi$ de poids $\omega(\pi) \leq \abs{S}$.
	\begin{itemize}
		\item Si $G$ possède un cycle hamiltonien $c$, c'est aussi un cycle hamiltonien de $G'$, dont toutes les arêtes sont de poids $1$. Donc, $\omega(c) = \abs{S}$ et $G'$ possède bien un cycle hamiltonien de poids au plus $\abs{S}$.
		\item Inversement, si $c$ est un cycle de $G$' de poids au plus $\abs{S}$, il possède $\abs{S}$ arêtes de poids $1$ ou $2$. Donc la somme est inférieure à $\abs{S}$ et toutes les arêtes sont de poids $1$. Elles sont toutes dans $G$ qui admet donc un cycle hamiltonien.
	\end{itemize}
	
	Cette transformation est bien polynomiale, et car CYCLE-HAMILTONIEN est $\cNP$-fort, VOYAGEUR-DE-COMMERCE l'est aussi.
\end{proof}

\textbf{Algorithme d'approximation pour le voyageur de commerce avec poids strictement positifs et qui vérifie l'inégalité triangulaire :}
\begin{itemize}
	\item Considérons une solution optimale au problème du voyageur de commerce, \cad un $\abs{S}$-cycle que l'on note $C^*$.
	\item Enlevons une arête quelconque à $C^*$. On obtient bien un arbre couvrant $T$. De plus, $\omega(C^*) > \omega(T)$. Si $T^*$ est un arbre couvrant de poids minimal, on a même $\omega(C^*) > \omega(T) \geq \omega(T^*)$.
	\item On triche un peu et on suppose qu'on peut passer deux fois par chaque arête. On calcule $T^*$, un MST (minimum spanning tree). On parcourt les arêtes de $T^*$ deux fois et on obtient un chemin fermé $U$ avec $\omega(U) = 2 \omega(T^*)$.
	\item On construit une tournée à partir du chemin $U$ en ignorant les sommets déjà vus en prenant le raccourci. On obtient un cycle hamiltonien $C$.
	\item Par inégalité triangulaire, le cout $\omega(C) \leq \omega(U)$. On a donc trouvé une solution $C$ tel que $2\omega(C) \geq 2\omega(T^*) \leq \omega(U) \leq \omega(C) \leq \omega(C^*) > 0$. On a donc construit une $2$-approximation.
\end{itemize}

\textbf{Remarque :} Il existe une $\frac{3}{2}$-approximation.

\begin{prop}
	Il est impossible d'avoir une $\alpha$-approximation pour le problème du voyageur de commerce dans le cas général, si $\cP \neq \cNP$.
\end{prop}

\begin{proof}
	Montrons cette dernière remarque par l'absurde, et procédons par l'absurde. Supposons disposer d'une $\alpha$-approximation $A$ avec $\alpha \geq 1$. Construisons alors un algorithme polynomial pour le problème du cycle hamiltonien. Pour un graphe $G = (S, A)$, on associe le graphe $G' = (S, A', \omega)$ comme précédemment, mais au lieu de prendre $\omega(a) = 2$ si $a \not\in A$, on pose $\omega(a) = \alpha\abs{S} + 1$.
	
	\medskip
	
	Cette construction est bien polynomiale.
	
	\medskip
	
	On applique notre algorithme $A$ d'approximation sur ce graphe $G'$ (temps polynomial). Si la tournée (a priori non-optimale) renvoyée est $\omega(c) \leq \abs{S}\alpha$, on renvoie vrai. Sinon, on renvoie faux. Considérons un cycle hamiltonien de $G'$ :
	\begin{itemize}
		\item S'il ne passe que par des arêtes de $G$ alors son poids est $\abs{S}$.
		\item S'il passe par au moins une arête qui n'est pas dans $G$, son poids est au moins $\abs{S} + \abs{S}\alpha = \abs{S}(\alpha + 1)$.
	\end{itemize}
	S'il existe un cycle dans $G$, alors la solution optimale est de taille $\abs{S}$. Donc la solution renvoyée par l'algorithme d'approximation est de taille au plus $\alpha\abs{S}$ et on renvoie vrai. Inversement, s'il n'existe pas de cycle hamiltonien de $G$, la solution apportée ne peut qu'être un cycle de poids au moins $\abs{S}(\alpha + 1) > \abs{S}\alpha$ et on renvoie bien faux.
\end{proof}

\section{Bilan}

Il existe plusieurs types de problèmes $\cNP$-complets issus d'un problème d'optimisation :
\begin{itemize}
	\item Ceux pour lesquels il n'existe pas du tout d'$\alpha$-approximation et ce pour tout $\alpha$ (si $\cP \neq \cNP$).
	\item Ceux pour lesquels il existe une $\alpha$-approximation avec une valeur minimale d'$\alpha$ (par exemple, seulement pour $\alpha \geq 2$, COUVERTURE (toujours si $\cP \neq \cNP$)).
	\item Ceux pour lesquels pour tout $\epsilon > 0$, il existe une $(1 + \epsilon)$-approximation (comme par exemple le problème du sac-à-dos).
\end{itemize}