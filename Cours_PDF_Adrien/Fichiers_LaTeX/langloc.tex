\chaptr{Langages locaux}{1}

Le cours est une copie du TD05.

\medskip

Intuitivement, un langage local est un langage pour lequel on peut tester l'appartenance d'un mot en faisant glisser sur le mot une "fenêtre" de largeur $2$.

\section{Définitions}

Soit $\Sigma$ un alphabet et $L \subseteq \Sigma^*$ un langage sur $\Sigma$. On définit les ensembles des préfixes, suffixes et facteurs de longueur $2$ de $L$ de la manière suivante : 
\begin{itemize}
	\item $P(L) = \{a \in \Sigma, a\Sigma^* \cap L \ne \emptyset\}$
	\item $S(L) = \{a \in \Sigma, \Sigma^*a \cap L \ne \emptyset\}$
	\item $F(L) = \{m \in \Sigma^2, \Sigma^*m\Sigma^* \cap L \ne \emptyset\}$
	\item $N(L) = \Sigma^2 \backslash F(L)$.
\end{itemize}

$P(L)$ est l'ensemble des premières lettres des mots de $L$, $S(L)$ celui des dernières lettres des mots de $L$, $F(L)$ l'ensemble des facteurs de longueur $2$ que l'on trouve dans des mots de $L$ et $N$ celui des facteurs de longueur $2$ qui sont évités par tous les mots de $L$ ($N$ pour non-facteur).

\begin{prop}
	Pour tout langage $L \subseteq \Sigma^*$, on a $$L \backslash \{\epsilon\} \subseteq (P(L)\Sigma^* \cap \Sigma^*S(L)) \backslash \Sigma^*N(L)\Sigma^*.$$
\end{prop}

\begin{proof}
	Soit $u = u_1\dots u_n \in L \backslash \{e\}$ avec donc $n \geq 1$. On a $u_1(u_2 \dots u_n) \in u_1 \Sigma^* \cap L$ donc $u_1 \in P(L)$ et $u = u_1(u_2 \dots u_n) \in P(L)\Sigma^*$. De même, $u_n \in S(L)$ et $u = (u_1 \dots u_{n-1}) u_n \in \Sigma^* S(L)$. Donc, $u \in P(L)\Sigma^* \cap \Sigma^*S(L)$. Soit $m \in N(L) = \Sigma^* \backslash F(L)$, on a $\Sigma^*m\Sigma^* \cap L = \emptyset$ puisque $m \in \Sigma^2$ mais $m \not\in F(L)$. Donc, $u \not\in \Sigma^*N(L)\Sigma^*$ et on a bien $u \in (P(L)\Sigma^* \cap \Sigma^*S(L)) \backslash \Sigma^* N(L) \Sigma^*$.
\end{proof}

Cette proposition évidente signifie que tout mot non vide de $L$ a sa première lettre dans $P(L)$, sa dernière lettre dans $S(L)$ et qu'aucun de ses facteurs de longueur $2$ ne sont dans $N(L)$ : ils sont tous dans $F(L)$.

\medskip

L'autre inclusion n'est pas toujours vérifiée. On dit qu'un langage est local si cette inclusion est en réalité une égalité. Plus précisément, 

\index{langage!local}
\begin{defi}
	Un langage $L \subseteq \Sigma^*$ est local s'il existe $P \subseteq \Sigma$, $S \subseteq \Sigma$ et $N \subseteq \Sigma^2$ tels que $$L \backslash \{\epsilon\} = (P(L)\Sigma^* \cap \Sigma^*S(L)) \backslash \Sigma^*N(L)\Sigma^*.$$
	On a alors $P(L) \subseteq P$, $S(L) \subseteq S$ et $N \subseteq N(L)$ et égalité dans la formule précédente.
\end{defi}

\begin{proof}
	Montrons ce qu'il y a à prouver dans la définition. Soit $L \subseteq \Sigma^*$ et $P \subset \Sigma, S \subseteq \Sigma$ et $N \subseteq \Sigma^2$ tels que $L \backslash \{\epsilon\} = (P\Sigma^* \cap \Sigma^*S) \backslash \Sigma^* N \Sigma^*$. Soit $a \in P(L)$. On a alors $a\Sigma^* \cap L \neq \emptyset$ donc il existe $u \in \Sigma^*$ tel que $au \in L$ et donc $au \in P\Sigma^*$ et alors $a \in P$. On a montré que $P(L) \subseteq P$. De même, si $a \in S(L)$, il existe $u \in \Sigma^*$ tel que $ua \in L$ et donc $ua \in \Sigma^*S$, ce qui donne $a \in S$ et donc $S(L) \subseteq S$. 
	
	\medskip
	
	Notons $F = \Sigma^2 \backslash N$. Soit $m \in F(L)$, il existe $u, v \in \Sigma^*$ tel que $umv \in L$ et donc $umv \not\in \Sigma^*N\Sigma^*$. Comme $m \in \Sigma^2$, on a $m \not\in N$. Autrement dit, $m \in F$. On a montré que $F(L) \subseteq F$ ou, en passant au complémentaire dans $\Sigma^2$, que $N \subseteq N(L)$. Reste à montrer que l'on a égalité. 
	
	\medskip
	
	Soit $u \in (P(L)\Sigma^* \cap \Sigma^*S(L)) \backslash \Sigma^*N(L)\Sigma^*$. On a $u \in P(L)\Sigma^* \subseteq P\Sigma^*$ et $u \in \Sigma^*S(L) \subseteq \Sigma^*S$ donc $u \in P\Sigma^* \cap \Sigma^*S$. Comme $\Sigma^*N\Sigma^* \subseteq \Sigma^*N(L)\Sigma^*$, et que $u \not\in \Sigma^*N(L)\Sigma^*$, $u \not\in \Sigma^*N\Sigma^*$. Finalement, $u \in (P\Sigma^* \cap \Sigma^*S) \backslash \Sigma^*N\Sigma^* = L \backslash \{\epsilon\}$. On a montré l'inclusion de droite à gauche, d'où l'égalité.
\end{proof}

On comprend alors la dénomination "local" : pour tester si un mot non vide appartient au langage $L$, il suffit de vérifier si sa première lettre est dans $P$, si sa dernière lettre est dans $S$ et si aucun de ses facteurs n'est dans $N$.

\remarklist{
	\item Il n'y a pas nécessairement unicité de $P$, $S$, et $N$. Les redondances dans les contraintes peuvent permettre d'avoir $P$ et $S$ "plus grands que nécessaire" ou de ne pas être obligé d'interdire dans $N$ certains facteurs de taille $2$ qui seront de toute façon impossible.
	\item Par exemple, si $\Sigma = \{a, b\}, P = \{a\}$ et $N = \{ab, bb, ba\}$, on peut prendre aussi bien $S = \{a\}$ que $S = \{a, b\}$ pour avoir $L \backslash \{\epsilon\} = a^+$. Dans le cas $S = \{a\}$ par exemple, on peut prendre simplement $N = \{ab\}$ ou bien $N = \{ab, bb\}$ ou encore $N = \{ab, ba\}$. Comme un mot (non vide) commence et termine toujours par $a$, tant que $ab$ est interdit, peu importe de s'intéresser aux facteurs qui commencent par $b$. 
	\item En revanche, il y a toujours unicité de $P(L), S(L)$ et $N(L)$. Même si en général il est plus simple de chercher directement $P(L), S(L)$ et $N(L)$, on peut se contenter de n'importe quelles parties $P, S$ et $N$ qui donnent l'égalité au dessus.
	\item On peut toujours proposer $F$ (ou $F(L)$) à la place de $N$ (ou $N(L)$), puisque l'on retrouve l'un à partir de l'autre par simple complémentaire. On utilise $N(L)$ dans les définitions car il permet une écriture ensembliste plus simple. 
}

\textbf{Méthode :} Pour montrer qu'un langage $L$ est local, on trouve $P, S$ et $N$ (ou $F$) et on montre que l'on a l'égalité. Pour montrer qu'un langage $L$ n'est pas local, on calcule $P(L), S(L)$ et $N(L)$ (ou $F(L)$) et on montre que l'on n'a pas égalité.

\begin{exemple}
	On pose $\Sigma = \{a, b\}$. 
	\begin{itemize}
		\item Les langages locaux associés à $P = \{a, b\}, S = \{a, b\}$ et $N = \{bb\}$ sont \begin{center} $(a \mid ba)^*(b \mid \epsilon)$ et $(a \mid ba)^*(b \mid a)$. \end{center}
		\item Les langages locaux associés à $P = \{a\}, S = \{b\}$ et $N = \{ab, ba\}$ sont $\emptyset$ et $\{\epsilon\}$.
	\end{itemize}
\end{exemple}

\begin{exemple}
	On pose $\Sigma = \{a, b, c\}$. 
	\begin{itemize}
		\item Le langage $(abc)^*$ est local avec les paramètres $P = \{a\}, S = \{c\}$ et $F = \{ab, bc, ca\}$.
		\item Le langage $L = a^*ba$ n'est pas local. On calcule $P(L) = \{a, b\}, S(L) = \{a\}$ et $F(L) = \{aa, ab, ba\}$. Un langage local associé à ces paramètres contient aussi $baa \not\in L$.
	\end{itemize}
\end{exemple}

On utilise le type suivant pour représenter une expression régulière en OCaml : 

\begin{listingsbox}{myocaml}{OCaml}
	type regexp = 
	| Vide
	| Epsilon
	| Lettre of char
	| Union of regexp * regexp
	| Concat of regexp * regexp
	| Kleene of regexp
\end{listingsbox}

\section{Propriété de clôture}

\begin{prop}
	L'intersection de deux langages locaux et l'étoile de Kleene d'un langage local sont des langages locaux.
\end{prop}

\begin{proof}
	Soit $L_1, L_2 \in \mathcal{P}(\Sigma^*)$ deux langages. On a $P(L_1 \cap L_2) = P(L_1) \cap P(L_2)$. En effet, une première lettre d'un mot de $L_1$ et de $L_2$ est bien première lettre d'un mot de $L_1$ et première lettre d'un mot de $L_2$. De même, $S(L_1 \cap L_2) = S(L_1) \cap S(L_2)$ et $F(L_1 \cap L_2) = F(L_1) \cap F(L_2)$. Pour vérifier que $L_1 \cap L_2$ est local lorsque $M_1$ et $L_2$ le sont, il faut donc vérifier l'inclusion manquante dans la proposition $1$. Si $u \in \Sigma^*$ a sa première lettre dans $P(L_1 \cap L_2)$, donc dans $P(L_1)$ et dans $P(L_2)$, sa dernière lettre dans $S(L_1 \cap L_2)$ donc dans $S(L_1)$ et dans $S(L_2)$, et tous ses facteurs de taille $2$ dans $F(L_1 \cap L_2)$, donc dans $F(L_1)$ et dans $F(L_2)$, comme $L_1$ et $L_2$ sont locaux, cela est suffisant pour montrer que $u$ appartient à $L_1$ et à $L_2$.
	
	\medskip
	
	Pour $L \subseteq \Sigma^*$, on peut vérifier que $P(L^*) = P(L)$, $S(L^*) = S(L)$ et $F(L^*) = F(L) \cup S(L)P(L)$. Supposons $L$ local et soit $u \in \Sigma^+$, de première lettre dans $P(L^*)$, de dernière lettre dans $S(L^*)$ et dont tous les facteurs de taille $2$ sont dans $F(L^*)$. On veut montrer que $u \in L^*$. On a directement la condition sur la première et la dernière lettre de $u$. Montrons par récurrence sur le nombre de facteurs de taille $2$ de $u$ qui ne sont pas dans $F(L)$ qu'un tel mot $u$ est dans $L^*$. Si tous les facteurs de $u$ sont dans $F(L)$, alors comme $L$ est local, on a $u \in L \subseteq L^*$. Sinon, $u$ admet un facteur $m = ab \in F(L^*) \backslash F(L) = S(L)P(L)$, avec donc $a \in S(L)$ et $b \in P(L)$, c'est-à-dire qu'il existe $v, w \in \Sigma^*$ tels que $u = vabw$. Mais alors $va$ et $wb$ vérifient tous les deux la condition de première lettre dans $P(L) = P(L^*)$, de dernière lettre dans $S(L) = S(L^*)$ et leurs facteurs de taille $2$ sont bien tous dans $F(L^*)$, avec strictement moins de facteurs hors de $F(L)$ que pour $u$. Par hypothèse de récurrence, on a alors $va, bw \in L^*$ et donc $u \in L^*$.
\end{proof}

L'union ou la concaténation de deux langages locaux n'est pas nécessairement un langage local, comme le montre l'exercice suivant.

\medskip

On comprend que dans le cas de l'union ou de la concaténation, il peut y avoir en quelque sorte "interférence" entre les facteurs autorisés de $L_1$ et $L_2$, ce qui permet de créer des mots qui ne sont ni dans $L_1$, ni dans $L_2$ ou bien de créer un mot de $L_2$ suivi d'un mot de $L_1$, ce qui n'est pas nécessairement un mot de $L_1L_2$. En revanche, on a :

\begin{prop}
	L'union et la concaténation de deux langages locaux construits sur des alphabets disjoints sont des langages locaux.
\end{prop}

\begin{proof}
	Notons $\Sigma(L)$ l'alphabet effectivement utilisé par un langage $L$ et on considère $L_1, L_2 \in \mathcal{P}(\Sigma^*)$ deux langages locaux avec $\Sigma(L_1) \cap \Sigma(L_2) = \emptyset$.
	
	\medskip
	
	On calcule $P(L_1 \cup L_2) = P(L_1) \cup P(L_2)$, $S(L_1 \cup L_2) = S(L_1) \cup S(L_2)$ et $F(L_1 \cup L_2) = F(L_1) \cup F(L_2)$ et on veut montrer l'inclusion réciproque dans la proposition $1$. Soit $u = u_1u_2 \dots u_n \in (P(L_1 \cup L_2)\Sigma^* \cap \Sigma^*S(L_1 \cup L_2)) \backslash \Sigma^*N(L_1 \cup L_2) \Sigma^*$. On a $u_1 \in P(L_1 \cup L_2) = P(L_1) \cup P(L_2)$. Supposons, sans perte de généralité que $u_1 \in P(L_1)$ et donc $u_1 \in \Sigma(L_1)$, on a $u_2 \in \Sigma(L_1)$ et $u_1u_2 \in F(L_1)$. On montre par récurrence sur $i \in \brak{1, n}$ que $u_i \in \Sigma(L_1)$ et que si $i \leq n-1$, $u_iu_{i+1} \in F(L_1)$. On vient de montrer l'initialisation et l'hérédité se montre de même? Enfin, on a $u_n \in S(L_1 \cup L_2) = S(L_1) \cup S(L_2)$ et comme $u_n \in \Sigma(L_1)$ et que $S(L_1)$ et $S(L_2)$ sont disjoints, $u_n \in S(L_1)$. Finalement, la première lettre de $u$ est dans $P(L_1)$, sa dernière lettre dans $S(L_1)$ et tous ses facteurs de taille $2$ dans $F(L_1)$ ce qui montre que $u \in L_1$ puisque $L_1$ est local. Si on avait eu $u_1 \in P(L_2)$, on aurait eu $u \in L_2$. Au final, on a bien $u \in L_1 \cup L_2$ et ce dernier langage est local.
	
	\medskip
	
	Pour l'intersection, on vérifie que $$P(L_1L_2) = \begin{cases}
	P(L_1) & \text{ si } \epsilon \not\in L_1 \\
	P(L_1) \cup P(L_2) & \text{ sinon}
	\end{cases} ~~~~~~~~~S(L_1L_2) = \begin{cases}
	S(L_2) & \text{ si } \epsilon \not\in L_2\\
	S(L_1) \cup S(L_2) & \text{ sinon}
	\end{cases}$$ et $F(L_1L_2) F(L_1) \cup F(L_2) \cup S(L_1)P(L_2)$. Soit $u = u_1\dots u_n \in (P(L_1L_2)\Sigma^* \cap \Sigma^*S(L_1L_2)) \backslash \Sigma^*N(L_1L_2)\Sigma^*$.
	\begin{itemize}
		\item Si $u_1 \in \Sigma(L_2)$, alors $u_1 \not\in P(L_1)$ et on a alors $\epsilon \in L_1$ et $u_1 \in P(L_2)$. On montre alors comme précédemment que $u \in \Sigma(L_2)^*$ et que tous ses facteurs de taille $2$ sont dans $F(L_2)$ et que $u_n \in S(L_2)$. Finalement, $u$ est de première lettre dans $P(L_2)$, de dernière lettre dans $S(L_2)$ et avec tous ses facteurs dans $F(L_2)$, et donc, puisque $L_2$ est local, $u \in L_2$. Comme $\epsilon \in L_1$, on a bien $u \in L_1L_2$.
		\item Si $u \in \Sigma(L_1)$, notons $v = v_1v_2 \dots v_k$ le plus long préfixe de $u$ qui soit dans $\Sigma(L_1)^*$. On a donc $u = vw$ avec $v \in \Sigma(L_1)^*$ et $w \in \Sigma^*$. On montre encore comme précédemment que $v_1 \in P(L_1)$, $v_k \in S(L_1)$ et pour tout $j \in \brak{1, k-1}$, $v_jv_{j+1} \in F(L_1)$. On distingue deux cas :
		\begin{itemize}
			\item Si $u = v$, alors comme $v_k \in S(L_1L_2) \cup \Sigma(L_1)$, c'est que $v_k \in S(L_1)$ et que $\epsilon \in L_2$. Comme $u$ est alors de première lettre dans $P(L_1)$, de dernière lettre dans $S(L_1)$ et avec tous les facteurs dans $F(L_1)$ et que $L_1$ est local, c'est que $u \in L_1$ et comme $\epsilon \in L_2$, on a bien $u \in L_1L_2$.
			\item Sinon, $w = w_1w_2\dots w_p$ avec $p \geq 1$. Comme $w_1 \not\in \Sigma(L_1)$, par maximalité de $v$, c'est que $w_1 \in \Sigma(L_2)$. On a de plus $v_k \in \Sigma(L_1)$, et $v_kw_1 \in F(L_1L_2)$ donc $v_kw_1 \in S(L_1)P(L_2)$. Ainsi, $v_k \in S(L_1)$ et comme précédemment, $v \in L_1$. De même que précédemment, on montre que $w \in \Sigma(L_2)^*$ de dernière lettre dans $S(L_2)$ et de facteurs de taille $2$ dans $F(L_2)$ et que finalement $w$ est un mot du langage local $L_2$. Ainsi $u = vw \in L_1L_2$.
		\end{itemize}
	\end{itemize}
\end{proof}

Intuitivement, il ne peut plus y avoir interférence entre les paramètres $P, S$ et $F$ des deux langages. Pour l'union, la première lettre et la condition sur les facteurs de taille $2$ impose au mot d'être construit exclusivement sur un des deux alphabets et donc le mot est dans l'un ou l'autre des langages, mais pas dans un mélange des deux. Pour la concaténation, al première lettre et la condition sur les facteurs impose de rester dans le premier alphabet avant de basculer, définitivement, dans le deuxième, ce qui induit une factorisation $u = u_1u_2$ avec $u_1 \in L_1$ et $u_2 \in L_2$.

\section{Expressions régulières linéaires}

\begin{defi}
	Une expression régulière sur $\Sigma$ est linéaire si chaque lettre de $\Sigma$ y apparait au plus une fois.
\end{defi}

\begin{exemple}
	Les expressions régulières $a^*, (a \mid b)$ et $(abcdef)^*$ sont linéaires. En revanche, l'expression régulière $(ab)^*a$ ne l'est pas.
\end{exemple}

\textbf{Remarque :} Attention, $a^+ = aa^*$ n'est pas une expression régulière. $a^+$ est un raccourci.

\begin{prop}
	Le langage régulier associé à une expression régulière linéaire est local.
\end{prop}


\begin{proof}
	La preuve est laissée au lecteur à titre d'exercice. Voici la mienne.
	
	\medskip
	
	On procède par induction sur l'expression régulière linéaire, notée $e$ :
	\begin{itemize}
		\item si $e = \emptyset$, alors $\mathcal{L}(e) = \emptyset$. C'est un langage local en prenant $P = S = F = \emptyset$.
		\item si $e = \epsilon$, alors $\mathcal{L}(e) = \{\epsilon\}$. C'est un langage local en prenant $P = S = F = \emptyset$.
		\item si $e = a \in \Sigma$, alors $\mathcal{L}(e) = \{a\}$. C'est un langage local avec $P = S = \{a\}$ et $F = \emptyset$.
		\item si $e = (e_1 \mid e_2)$, alors $e_1, e_2$ sont linéaires, et $\mathcal{L}(e_1)$ et $\mathcal{L}(e_2)$ sont des langages locaux. Car $e$ est linéaire, ces deux langages sont définis sur des alphabets disjoints. Donc $\mathcal{L}(e) = L_1 \cup L_2$ est local par stabilité sous union.
		\item si $e = (e_1 \cdot e_2)$, alors $e_1, e_2$ sont linéaires, et $\mathcal{L}(e_1)$ et $\mathcal{L}(e_2)$ sont des langages locaux. Car $e$ est linéaire, ces deux langages sont définis sur des alphabets disjoints. Donc $\mathcal{L}(e) = L(e_1) \cdot L(e_2)$ est local par stabilité sous concaténation.
		\item si $e = e_1^*$, alors $\mathcal{L}(e_1)^*$ est local.
	\end{itemize}
\end{proof}