\chaptr{Classes de complexité P et NP}{1}

\section{Temps de calcul}

Dans ce chapitre, on ne s'intéresse qu'aux algorithmes dont la terminaison est garantie, et on appelle complexité d'un algorithme son temps de calcul (autrement dit le temps nécessaire à son exécution). On s'intéresse à l'évolution de cette complexité en fonction de la taille des données : autrement dit, étant donnée une procédure $p$, on s'intéresse à la variation de la durée du calcul de $p(x)$ en fonction de la taille de l'argument $x$.

\medskip

Cette définition de la complexité semble reposer sur une définition précise du temps de calcul ; or tout le monde sait que celui-ci, exprimé par exemple en secondes, dépend de la machine utilisée (vitesse du processeur, temps d'accès à la mémoire, etc...) et décroit rapidement avec les progrès de la technologie. Pour se rapprocher d'une notion indépendante de la technologie, on adopte la convention suivante :

\index{temps de calcul}
\begin{defi}
	Le temps de calcul de $p(x)$ est le nombre d'instructions élémentaires exécutées pendant ce calcul.
\end{defi}

La définition d'une instruction élémentaire dépend à son tour du modèle de calcul considéré, mais un consensus est facile à trouver en pratique : en cas de doute, on se ramène à une estimation du nombre d'instructions effectuées par le processeur. Une autre solution, employée en particulier pour démontrer les théorèmes fondamentaux de la théorie de la complexité, est d'utiliser le modèle des machines de Turing, pour lequel la définition d'une instruction élémentaire (appelée aussi transition) est non ambigüe. Nous reviendrons plus loin sur ce point.

\subsection{Taille des données}

Il faut prendre garde aussi à la définition de la taille des données : celle-ci est par définition égale au nombre de bits nécessaire à la représentation de l'argument $x$ en mémoire, et dépend évidemment du codage de cet argument. Le codage des données est implicite dans la définition de la procédure $p$, lorsque celle-ci est écrite en $C$ (par exemple) : lorsqu'un algorithme est décrit de façon moins précise, comme ce sera souvent le cas dans ce chapitre, il faut souvent préciser séparément comment sont codées les données.

\begin{exemple}
	Si l'argument $x$ d'une procédure est lui-même un entier, alors sa taille $n$ vaut $n = \log_2(x)$ car avec $n$ bits, on représente les entiers $x$ compris entre $2^{n-1}$ et $2^n - 1$ : le logarithme binaire doit être interprété comme une valeur entière arrondie (par excès). Par exemple, il faut $5$ bits pour écrire l'entier $20$ en binaire, car $20$ est compris entre $2^4 = 16$ et $2^5 - 1 = 31$ ; donc $\log_2(20)$ est compris entre $4$ et $5$ et l'on arrondit à $5$.
	
	\medskip
	
	Si l'argument d'une procédure est un graphe $G$ comportant $v$ sommets et $e$ arcs ou arêtes, on peut coder raisonnablement $G$ par la donnée de l'entier $v$ et de $e$ couples d'entiers - un couple $(i, j)$ pour chaque arc ou arête joignant le sommet numéroté $i$ au sommet numéroté $j$. La taille du graphe $G$ est donc proportionnelle au nombre de ses arêtes et s'il n'y a pas d'arêtes multiples (\cad s'il y a au plus une arête reliant deux sommets donnés), $e$ est borné par $v^2$.
\end{exemple}

\subsection{Envisager le pire}

En général, le temps de calcul ne dépend pas que de la taille des données : par exemple tester (par divisions successives) si un entier $p$ est premier est beaucoup plus rapide s'il possède un petit facteur (dans ce cas l'algorithme répond rapidement non), que si $p$ est effectivement premier. On peut essentiellement distinguer deux façons de définir une fonction $\tau(n)$ qui exprime le temps de calcul en fonction de la taille $n$ de l'argument $x$ :
\begin{itemize}
	\item complexité moyenne : dans ce cas, il faut aussi définir la loi de probabilité de l'argument $x$
	\item complexité au pire : \cad valeur maximale de la complexité pour un argument de taille $n$.
\end{itemize}

Dans ce chapitre, nous nous intéressons seulement  à la seconde définition : la complexité $\tau(n)$ d'un algorithme est par définition le temps de calcul maximal pour une donnée de taille $n$.

\subsection{Notation grand-O}

\index{grand-O}
La complexité de certains algorithmes importants, autrement dit la fonction $\tau(n)$ peut être étudiée très en détail dans un cours d'algorithmique ; la référence dans ce domaine est $\underline{\text{The Art of Computer Programming}}$ par Donald Knuth, qui n'a pas hésité à inventer son propre modèle de machine pour obtenir des résultats exceptionnellement précis (que ce soit d'ailleurs pour la complexité en moyenne ou pour la complexité au pire, que nous étudions ici). Mais la théorie de la complexité ne s'intéresse qu'à l'ordre de grandeur de la complexité, sans se préoccuper des constantes intervenant dans l'expression de $\tau(n)$ ; en outre, on se contente de borner cette fonction, en accord avec la décision de n'étudier que la complexité au pire. 

\begin{defi} 
	La notation suivante, dite grand-O (prononcer "grand-oh") est d'un usage constant :
	\begin{center}
		$\tau(n) = O(f(n))$ signifie :\\
		il existe une constante $C$ telle que pour tout $n$ suffisamment grand, $\tau(n) < Cf(n)$.
	\end{center}
\end{defi}

Avec cette notation, on peut affirmer par exemple, sans plus de détails, que la recherche d'un élément dans une liste de taille $n$ a une complexité $O(n)$ ; celle-ci peut tomber à $O(\log(n))$ si la liste est triée et si on utilise un algorithme de recherche dichotomique. Une méthode de tri naïve a une complexité $O(n^2)$, de même qu'un algorithme naïf pour multiplier deux entiers de $n$ bits ; dans les deux cas, il existe des algorithmes plus sophistiqués en $O(n\log n)$ - l'expression "en ..." est une contraction de "en temps ...". La méthode de Gauss, pour inverser une matrice carré de $m$ lignes et $m$ colonnes possède une complexité $O(m^3)$, autrement dit $O(n^{\frac{3}{2}})$ si $n = m^2$ désigne la taille de la matrice, etc...

\medskip

Bien entendu, une complexité $O(n)$ est aussi $O(n^2)$, ou $O(n \log n)$ mais l'inverse n'est pas vrai. On essaie d'habitude de ne pas tromper le client, en donnant la meilleure estimation possible. D'autre part, la définition spécifie "pour tout $n$ suffisamment grand", afin d'éviter de se préoccuper de la phase d'initialisation de l'algorithme, dont le temps de calcul incompressible devient négligeable dès que $n$ est grand ; dans le contexte de ce chapitre,c e scrupule est d'ailleurs secondaire.

\section{Classe $\cP$}

\subsection{Algorithmes polynomiaux}

\index{algorithme!polynomiaux}
\begin{defi}
	La complexité d'un algorithme est polynomial si elle est $O(n^k)$ pour un certain entier $k$ : on dit aussi, pour abréger, que l'algorithme lui-même est polynomial.
\end{defi}
	
Il existe plusieurs raisons de s'intéresser à cette classe d'algorithmes.

\medskip

D'abord les algorithmes efficaces sont polynomiaux, autrement dit les algorithmes non-polynomiaux sont certainement inefficaces. Cette classification est très grossière, car un algorithme linéaire, donc de complexité $O(n)$ est en général plus efficace qu'un algorithme quadratique, de complexité $O(n^2)$ - faire attention quand même aux constantes cachées dans la notation grand-O ; et un algorithme dont la complexité est proportionnelle à $10^{12}n^{100}$ est polynomial, et certainement pas efficace du tout. Notons aussi que certains algorithmes non polynomiaux peuvent être assez efficaces en moyenne, ou pour des classes restreintes de données ; on est seulement sûr qu'il existe des données de taille relativement faible pour lesquelles le temps de calcul est prohibitif (supérieur par exemple à $2^{100}$, qui est lui-même nettement supérieur au nombre d'instructions exécutées par un processeur qui fonctionnerait à $1$ GHz depuis ... la création de l'univers).

\medskip

En second lieu, cette notion est robuste, \cad indépendante de la technologie. La définition du temps de calcul dépend du modèle de calcul : outre le nombre d'instructions effectuées par un processeur, on peut définir le temps de calcul comme le nombre de transitions effectuées par une machine de Turing avant de s'arrêter, ou le nombre de $\beta$-réduction appliquées  à une $\lambda$-expression pour la normaliser, etc... ; mais les preuves d'équivalence entre tous les modèles connus (à l'exception du modèle quantique qui sort du cadre de ce cours), peuvent être complétées pour montrer que la complexité se transforme de manière polynomiale (en général quadratique).

\medskip

Enfin, les algorithmes polynomiaux forment une classe stable : la composition de deux algorithmes polynomiaux reste polynomiale et un algorithme construit polynomialement à partir d'appels à des procédures de complexité polynomiale, reste polynomial.

\medskip

\remarklist{
	\item Une complexité non-polynomiale est souvent exponentielle, mais il existe des cas intermédiaires.
	\item Une fonction qui croît plus vite que toute fonction polynomiale est dite super-polynomiale et une fonction qui croit moins vite que toute fonction exponentielle est dite sous-exponentielle. 
	\item Par exemple, la fonction $n \mapsto n^{\log n}$ est à la fois super-polynomiale et sous-exponentielle ; la fonction $n \mapsto \exp(n^{\frac{1}{2}})$ est en général considérée comme sous-exponentielle (mais cela dépend des auteurs et du contexte).
}

\subsection{Problèmes et langages polynomiaux}
\index{classe!\cP}
La théorie de la complexité cherche à classer les problèmes de décision (ceux qui ont une réponse par oui ou par non), selon la complexité des algorithmes capables de les résoudre : nous reviendrons plusieurs fois sur les (bonnes) raisons de se retreindre à ce type de problème, pour l'instant considérons qu'il s'agit d'un choix simplificateur. La première classe fondamentale est la suivante :

\begin{defi}
	La classe $\cP$ est formée des problèmes de décision qui peuvent être résolus par un algorithme polynomial.
\end{defi}

Il peut tout à fait exister des algorithmes de complexité exponentielle pour résoudre un problème qui est dans $\cP$. Cependant, il en existe au moins un de complexité polynomiale. En particulier, ce n'est pas parce que l'on ne connait que des algorithmes exponentiels pour résoudre un problème que celui-ci n'est pas dans $\cP$.

\medskip

Voici deux exemples de problèmes dans $\cP$ ; on utilise pour les décrire un jargon un peu spécial, traditionnel dans ce contexte (de même que l'habitude de nommer les problèmes en majuscules) : un prédicat est appelé, comme ci-dessus, problème, et l'argument du prédicat devient une instance du problème.

\index{{\scshape Problème}!DIVCOM}
\begin{exemple}
	Problème DIVCOM :
	\begin{itemize}
		\item \textit{Instance :} un couple d'entiers $(a, b)$.
		\item \textit{Problème :} $a$ et $b$ possèdent-ils un diviseur commun (en dehors de $1$) ?
 	\end{itemize}
 	
 	DIVCOM appartient à $\cP$ ; en d'autres termes, il existe une procédure \textit{divcom}, de complexité polynomiale, telle que 
 	$$\textit{divcom}(a, b) = \begin{cases}
 	1 \text{ si } a \text{ et } b \text{ possèdent un diviseur commun supérieur à } 1\\
 	0 \text{ sinon }
 	\end{cases}$$
 	Remarquons qu'une procédure naïve, qui incrémente un diviseur $d$ à partir de $2$, jusqu'à ce que $d$ divise à la fois $a$ et $b$, ou jusqu'à atteindre le plus petit d'entre eux (disons $a$) ne convient pas, car sa complexité au pire (lorsqu'il n'existe pas de diviseur commun) est exponentielle par rapport à l'ordre de la taille des données (si $a$ et $b$ sont représentés sur $n$ bits, le nombre d'itérations peut être de l'ordre de $2^n$).
 	
 	\medskip
 	
  	Par contre l'algorithme d'Euclide permet de répondre à la question posée en temps polynomial : une caractéristique fondamentale de cet algorithme est que le nombre de divisons nécessaires pour calculer le $\pgcd$ est linéaire (\cad proportionnel au nombre $n$ de chiffres utilisés pour représenter $a$ ou $b$), et chaque division de complexité quadratique, d'où une complexité $O(n^3)$. Une fois le $\pgcd$ calculé, résoudre DIVCOM est immédiat ; noter le paradoxe:  pour résoudre efficacement le problème de décision (existence d'un diviseur commun supérieur à $1$), on commence par résoudre un problème apparemment plus difficile (trouver le plus grand diviseur commun).	
\end{exemple}

\index{{\scshape Problème}!CHEMIN}
\begin{exemple}
	Problème CHEMIN :
	\begin{itemize}
		\item \textit{Instance :} un graphe orienté $G$ et deux sommet $s$ et $t$.
		\item \textit{Problème :} existe-t-il un chemin dans $G$ reliant le sommet $s$ au sommet $t$ ?
	\end{itemize}
	
	CHEMIN appartient à $\cP$ ; en d'autres termes, il existe une procédure \textit{chemin} de complexité polynomiale telle que : 
	$$\textit{chemin}(G, s, t) = \begin{cases}
		1 \text{ s'il existe dans } G \text{ un chemin } s \to t\\
		0 \text{ sinon }
	\end{cases}$$
	
	Ici encore, une procédure naïve, qui examine successivement tous les chemins de source $s$ jusqu'à en trouver un d'extrémité $t$ n'est pas en général de complexité polynomiale, car le nombre de chemins dans un graphe est en général une fonction exponentielle de la taille du graphe : pendant la construction d'un chemin, on a un choix à faire, à chaque sommet, entre les différents arcs issus de ce sommet.
	
	\medskip
	
	Au lieu de parcourir tous les chemins, on peut se contenter de marquer les sommets $i$ tels qu'il existe un arc $s \to i$ puis de marquer les sommets $j$ qu'on peut atteindre à partir de ceux déjà marqués, et ainsi de suite, jusqu'à marquer $t$ ou jusqu'à stabilisation de l'ensemble des sommets marqués. Cet algorithme est de complexité polynomiale : le nombre de sommets marqués augmente au moins de $1$ entre deux étapes consécutives de l'algorithme (sinon celui-ci est terminé), donc le nombre d'étapes est borné par le nombre $n$ de sommets du graphe ; et chaque étape (examiner les arcs issus des sommets déjà marqués) est clairement polynomiale. La structure de données utilisée pour représenter le graphe influe sur la complexité exacte de l'algorithme, et trouver une structure optimale est un sujet d'étude classique en algorithmique ; mais la complexité reste polynomiale même avec une représentation brutale de la liste des arcs (sans accès direct aux arcs de source données).
\end{exemple}

\textit{Terminologie :} dans cette section, un élément de $\cP$ est un problème de décision ; dans beaucoup de cours et d'articles sur la complexité, un élément de $\cP$ est un langage. Cela revient au même, car un problème de décision est un prédicat $A$ dont l'argument $u$ peut être considéré comme un mot de $n$ bits, et le langage $L$ associé à $A$ est l'ensemble des mots dont $A$ est la fonction caractéristique : autrement dit, la relation entre le problème de décision $A$ et le langage $L$ est la suivante : $$u \in L \iff A(u) = 1$$
Remarquons aussi que, lorsque $A$ est un prédicat, écrire "$A(u) = 1$" est un pléonasme (comme le savent les -rares- programmeurs sérieux), car cette expression est vraie \ssi $A$ est vrai ; on écrira donc plus simplement ; $$L = \{u \mid A(u)\}$$
D'autre part, le mot algorithme est souvent remplacé par machine de Turing, qui est plus précis, mais fait référence (souvent inutilement) à un modèle de calcul particulier. Avec cette terminologie, la définition de $\cP$ devient : 
\begin{prop}
	La classe $\cP$ est formée des langages reconnus (ou acceptés) par les machines de Turing polynomiales.
\end{prop}

\section{Classe \cEXP}
\index{classe!\cEXP}
\begin{defi}
	La classe $\cEXP$ correspond aux problèmes de complexité au moins exponentielle. La classe $\cEXP$ est formées des problèmes de décision qui peuvent être résolus par un algorithme exponentiel.
	
	\medskip 
	
	La complexité d'un algorithme est dite exponentielle si elle est en $O(2^{n^k})$ pour un certain entier $k$.
\end{defi} 

On a $\cP \subsetneq \cEXP$ et on sait le montrer. La résolution des échecs généralisés est un exemple de problème qui est dans $\cEXP$ mais pas dans $\cP$.

\section{Classe \cNP}

\subsection{Algorithmes non déterministes}
\index{classe!\cNP}
La classe $\cNP$ possède une définition moins naturelle que celle de $\cP$ et son nom est trompeur : il ne signifie pas "non polynomial", mais polynomial non-déterministe. La classe $\cNP$ est donc une extension de la classe $\cP$ en autorisant des choix non-déterministes pendant l'exécution de l'algorithme. Si le modèle de calcul est celui des machines de Turing, on autorise plusieurs transitions à partir d'un état et d'un symbole lu sur la bande.
	
\medskip

Cette notion est directement inspirée de celle d'automate non déterministe, où la lecture d'une lettre d'un mot peut conduire, à partir d'un état donné, à plusieurs états différents. Rappelons qu'un mot $u$ est accepté (on dit aussi reconnu) par un automate non-déterministe s'il existe un chemin, étiqueté par $u$, qui conduise d'un état initial à un état terminal.

\medskip

Les différentes exécutions d'un algorithme non-déterministe pour un argument $u$ sont les branches d'un arbre, autrement dit les chemins depuis la racine (départ de l'exécution) vers les feuilles (terminaison de l'exécution) ; chaque noeud interne de l'arbre correspond à un choix. Pour définir $\cNP$, on se restreint au cas où toutes les branches sont finies, autrement dit où toutes les exécutions terminent ; restent deux questions : 
\begin{itemize}
	\item que calcule un algorithme non déterministe ?
	\item comment mesurer le temps de calcul d'un tel algorithme ?
\end{itemize}

La première question n'a guère de réponse satisfaisante en général, c'est pourquoi on entend peu parler d'algorithme non déterministe dans les cours de programmation... Il faut se restreindre aux problèmes de décision pour pouvoir définir une convention intéressante, analogue à celle employée pour les automates : c'est la première raison pour laquelle la classe $\cP$ ne contient, par définition, que des problèmes de décision ; sinon étendre $\cP$ en autorisant le non-déterminisme n'aurait guère de sens.

\medskip

A partir de maintenant, on ne considère donc que les algorithmes non déterministes possédant la propriété suivante : chaque branche de l'arbre d'exécution, pour un argument $u$ donné, calcule $0$ ou $1$ (on dit aussi que chaque exécution se termine par refuse ou accepte). On convient alors, comme pour les automates, que la réponse de l'algorithme est "oui" s'il existe une branche qui calcul $1$ (autrement dit s'il existe une exécution qui accepte l'argument $u$) : par conséquent la réponse est "non" si toutes les branches calculent $0$ (autrement dit si toutes les exécutions refusent l'argument $u$) ; on notera la dissymétrie de cette définition. 

\medskip

En ce qui concerne le temps de calcul, on convient, conformément au principe "envisager le pire", que c'est celui de la plus longue branche (la longueur d'une branche est mesurée en nombre d'instructions), que celle-ci calcule $0$ ou $1$.

\subsection{$\cNP = \cP$ non déterministe}

Avec ces conventions, on a la définition fondamentale suivante :

\begin{defi}
	La classe $\cNP$ est formée des problèmes de décision qui peuvent être résolus par un algorithme polynomial non déterministe.
\end{defi}

\textit{Variante :} avec une autre terminologie, équivalente et fréquente dans ce contexte (voir ci-dessus), cette définition devient :
\begin{defi}
	La classe $\cNP$ est formée des langages reconnaissables par une machine de Turing polynomiale non déterministe.
\end{defi}

Tout algorithme non déterministe peut être simulé par un algorithme déterministe, qui parcourt récursivement l'arbre de tous les choix possibles ; les fonctions calculables dans un modèle le sont donc dans l'autre, le non-déterminisme n'apporte rien de ce point de vue. Par contre la complexité d'un algorithme non-déterministe est mesurée par le temps d'exécution de la plus longue branche, alors que la complexité de sa simulation déterministe est égale au temps d'exécution de l'arbre entier (ou de la portion d'arbre qui précède une acceptation) ; lorsque la première est polynomiale, la seconde est en général exponentielle (dès que l'arbre est suffisamment équilibré).

\medskip

En fait, il existe beaucoup de problèmes dans $\cNP$ pour lesquels aucun algorithme polynomial déterministe n'est connu, ce qui semble indiquer avec une grande vraisemblance que la classe $\cNP$ est plus large que la classe $\cP$. La réponse à la question $\cP = \cNP$ est donc très vraisemblablement négative, mais, aucune démonstration de ce résultat négatif n'a été trouvée en $30$ ans de recherche très active, d'où la célébrité de ce problème, qui intrigue tous les théoriciens de l'informatique.

\subsection{Certificats et vérifieurs}
\index{certificats}
\index{vérifieurs}
Il existe une variante de la définition $\cNP$ qui élimine le non-déterminisme, en codant la succession de choix non déterministes par un mot $x$ ; en effet, une branche d'un arbre peut être codée par une suite de (petits) entiers : par exemple $4, 1, 2, \dots$ code la branche qui débute par le quatrième fils de la racine (appelons-le $s$), puis passe par le premier fils de $s$ (appelons-le $t$), puis par le second fils de $t$, etc... Le mot $x$ devient le second argument de l'algorithme ; cet algorithme déterministe à deux variables (le non-déterminisme se trouve en quelque sorte externalisé dans le second argument), est appelé vérifieur pour le problème de décision correspondant :

\begin{defi}
	Une procédure (déterministe) booléenne $V$ à deux variables est un vérifieur pour le problème de décision $A$ \ssi $A(u)$ \ssi $\exists x, V(u, x)$.
\end{defi}

\begin{defi}
	Un argument $x$ qui satisfait le prédicat $V(u, x)$ est appelé un certificat de $u$ ; d'autre part. Un vérifieur est dit polynomial si sa complexité est $O(n^k)$, où $n$ désigne la taille du premier argument $u$
\end{defi}

cette convention garantit que la taille du certificat est elle-même bornée par une fonction polynomiale de $n$ (sinon, $V$ n'aurait même pas le temps de lire le certificat).

\medskip

En comparant les conventions adoptées pour définir ce que calcule un algorithme non-déterministe, et pour définir la complexité d'un tel algorithme, avec les conventions données ci-dessus pour un vérifieur, on prouve facilement (nous ne donnerons pas les détails) le résultat suivant :
\begin{defi}
	La classe $\cNP$ est formée des problèmes de décision qui possèdent un vérifieur polynomial.
\end{defi}

Cette nouvelle définition de $\cNP$ est celle qu'on utilise presque toujours en pratique. Elle illustre ainsi la nature des problèmes appartenant à cette classe, qu'on peut résumer par "deviner et vérifier" en temps polynomial ; on n'a pas à rechercher le certificat (ce qui demande en général un temps de calcul exponentiel), on est autorisé à la deviner, mais ensuite, on a un algorithme polynomial de vérification du certificat.

\medskip

Pour donner une intuition de ce principe, imaginez que vous êtes stagiaire dans une entreprise de fabrication de cadenas. Vous disposez d'une boîte contenant des cadenas à code à $4$ chiffres fermés. On vous a informé que certains de ces cadenas sont défectueux et, quel que soit le code saisi, le cadenas ne s'ouvrira pas. Malheureusement, votre premier travail est de trier les cadenas, pour ne garder que ceux qui ne sont pas défectueux.

\medskip

Votre premier approche est naïve mais simple : elle consiste à tester pour chaque cadenas les $10,000$ codes possibles, jusqu'à ce que l'un ouvre le cadenas, ou qu'aucun code n'ait fonctionné. Après avoir ainsi trié $3$ cadenas, vous commencez à trouver le temps long mais, heureusement, une autre stagiaire vient vous aider.

\medskip

Par chance, elle pratique la divination et, lorsqu'un cadenas n'est pas défectueux, elle peut deviner le code qui l'ouvre et vous en informe, par exemple "le code est 2587". Lorsqu'elle vous donne un code, il suffit de tester ce code pour vérifier immédiatement s'il ouvre effectivement le cadenas.

\medskip

Cependant, quand vous tombez sur un cadenas défectueux, même si elle vous affirme qu'aucun code ne l'ouvre, vous n'avez aucun moyen de le vérifier rapidement ; vous ne pouvez que la croire sur parole.

\medskip

Comme on l'a déjà mentionné, les problèmes dans $\cNP$ sont en général plus difficiles à résoudre (du point de vue du temps de calcul) que les problèmes dans $\cP$, mais on verra dans la section suivante (et encore plus dans le chapitre suivant) qu'il existe des problèmes naturels et simples à formuler encore plus difficiles : appartenir à $\cNP$ est une contraire (celle de la vérification en temps polynomial), autrement dit la classe $\cNP$ est loin d'être la classe des problèmes les plus difficiles à résoudre, comme la célébrité de la question $\cP = \cNP$ pourrait le laisser croire.

\textbf{Attention :} il faut écrire noir sur blanc qu'on a vérifier que le certificat est de taille polynomial, sinon on se fait démarrer (vraiment).

\subsection{Premiers exemples}

Il est temps de donner deux exemples, sur le modèle de ceux donnés pour illustrer la définition de $\cP$.
\index{{\scshape Problème}!COMPOSITE}
\begin{exemple}
	Problème COMPOSITE.
	\begin{itemize}
		\item \textit{Instance :} un entier $m$.
		\item \textit{Problème :} $m$ possède-t-il un diviseur $d$ non trivial ?
	\end{itemize}
	C'est un problème qui rentre manifestement dans la catégorie "deviner et vérifier", un certificat est un diviseur $d$ ! On vérifie ensuite en temps polynomial que $d$ est différent de $1$ et $m$ (facile en temps linéaire, en comparant les chiffres de $d$ et ceux de $m$), puis que $d$ divise $m$ (l'algorithme élémentaire de division est de complexité quadratique en fonction du nombre de chiffres des opérandes).
	
	\medskip
	
	\index{{\scshape Problème}!CHEMIN-HAMILTONIEN}
	Problème CHEMIN-HAMILTONIEN.
	\begin{itemize}
		\item \textit{Instance :} un graphe orienté $G$ et deux sommets $s$ et $t$.
		\item \textit{Problème :} existe-t-il un chemin dans $G$ reliant le sommet $s$ au sommet $t$ et passant par chaque sommet de $G$ une et une seule fois ?
	\end{itemize}
	C'est à nouveau un problème de la catégorie "deviner et vérifier", un certificat est un chemin $\gamma$. On vérifie ensuite en temps linéaire (en fonction du nombre $n$ de sommets de $G$) que $\gamma$ part bien de $s$, arrive bien en $t$t et passage par chaque sommet une et une seule fois (un tableau de compteurs de visites, un par sommet, fait l'affaire, pendant qu'on lit les $n$ premiers sommets qui composent $\gamma$).
\end{exemple}

On ne connaît pas d'algorithme (déterministe) polynomial pour résoudre le problème CHEMIN-HAMILTONIEN. L'astuce utilisée pour résoudre le problème CHEMIN (voir section $2$), ne fonctionne plus car la contrainte "hamiltonienne" oblige à garder en mémoire les sommets déjà visités, et à explorer les chemins sans répétitions un par un. D'autre part, on verra plus loin que ce problème est $\cNP$-complet, ce qui signifie que la découverte d'un algorithme polynomial pour le résoudre impliquerait $\cP = \cNP$ (et rapporterait accessoirement un million de dollars à son auteur), ce qui est très peu vraisemblable.

\medskip

\index{{\scshape Problème}!PRIME}
Le problème COMPOSITE a un statut très différent, pour deux raisons :
\begin{itemize}
	\item le problème complémentaire, défini en inversant les réponses "oui" et "non", consiste à savoir si un entier est premier, et est appelé PRIME par les anglo-saxons. Ce problème illustre la dissymétrie de la définition de $\cNP$ (voir section suivante) : un certificat pour le problème COMPOSITE est évident, et consiste en un diviseur, comme on l'a vu ; un certificat de primalité est très différent, et on peut douter à première vue qu'il en existe. C'est pourtant vrai, mais produire un tel certificat utilise des résultats mathématiques assez sophistiqués (principalement : lorsque $p$ est premiers, les entiers non nuls modulo $p$ forment un groupe pour la multiplication, et ce groupe est cyclique) ; bref PRIME est dans $\cNP$, mais c'est un exemple où la définition du problème ne débute pas par un quantificateur existentiel le faisant entrer immédiatement dans la catégorie "deviner et vérifier".
	\item on connaît depuis longtemps des algorithmes déterministes presque polynomiaux (au sens en particulier où certains de ces algorithmes sont polynomiaux si certains conjectures classiques en théorie des nombres sont vraies) pour déterminer si un entier est premier ou non, mais c'est seulement en $2002$ qu'a été découvert un algorithme polynomial (algorithme AKS, du nom de ses inventeurs, Agrawal, Kayal et Saxena), prouvant que PRIME et COMPOSITE (lorsque l'algorithme est déterministe, échanger les réponses "oui" et "non" est indolore) sont en fait dans $\cP$. Noter que l'algorithme AKS, dans sa version originale, est beaucoup moins efficace que les tests de primalité classiques utilisés jusque là, ce qui confirme que regrouper tous les algorithmes polynomiaux dans la même classe est très grossier, du point de vue pratique (voir les remarques au début de la section $2$) ; voir www.primepages.org pour tous les détails.
\end{itemize}