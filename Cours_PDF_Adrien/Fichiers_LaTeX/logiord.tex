\chaptr{Logique du premier ordre}{1}

\section{Syntaxe}

\begin{defi}
	La signature d'une théorie est la donnée :
	\begin{itemize}
		\item d'un nombre de symboles de fonctions, dotés chacun d'une arité (éventuellement nulle, on parle alors de symbole de constante)
		\item d'un nombre de symboles de relation (ou symboles de prédicats), doté chacun d'une arité (éventuellement nulle, on parle alors de variable propositionnelle).
	\end{itemize}
	Sauf exception, on considèrera que l'on dispose toujours d'un symbole de relation $=$ d'arité $2$.
\end{defi}

\begin{exemple}
	Pour la théorie des groupes, on aurait la signature suivante :
	\begin{itemize}
		\item le symbole d'égalité,
		\item un symbole de fonction $*$ d'arité $2$ (que l'on peut choisir de noter de manière infixe),
		\item un symbole de fonction d'arité $1$, que l'on note $^{-1}$ de manière postfixe (c'est-à-dire $x^{-1}$),
		\item un symbole de constante (fonction d'arité $0$) noté $e$.
	\end{itemize}
\end{exemple}

\begin{defi}
	On se dote d'un ensemble infini dénombrable $\mathfrak{X}$ de variables (qui ne sont pas des variables propositionnelles). Un terme (sur une certaine signature) est un arbre construit à partir de variables et de symboles de fonctions, en respectant leur arité.
\end{defi}

\remarklist{
	\item Les symboles de relation ne peuvent pas apparaître dans un terme. 
	\item Les feuilles de l'arbre seront donc étiquetées soit par des variables, soit par des symboles de constantes, et les noeuds internes par des symboles de fonctions d'arité strictement positive.
}

\begin{exemple}
	Avec la signature donnée pour la théorie des groupes, on peut par exemple construire les termes $e$, $x$ (variable), $(x \star y)^{-1} \star (y \star e), ...$
	
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v1) at (0,2) {$\star$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v6) at (1.5,1) {$\star$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v2) at (-1.5,1) {$-1$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v8) at (2.5,-0.5) {$e$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v7) at (0.5,-0.5) {$y$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v3) at (-1.5,-0.5) {$\star$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v5) at (-0.5,-2) {$y$};
			\node [draw,outer sep=0,inner sep=1,minimum size=13] (v4) at (-2.5,-2) {$x$};
			\draw  (v1) edge (v2);
			\draw  (v2) edge (v3);
			\draw  (v3) edge (v4);
			\draw  (v3) edge (v5);
			\draw  (v1) edge (v6);
			\draw  (v6) edge (v7);
			\draw  (v6) edge (v8);
		\end{tikzpicture}
		\caption{Un terme en théorie des groupes.}
	\end{figure}
\end{exemple}

\begin{defi}
	Une formule atomique (sur une certaine signature) est un arbre de la forme $R(t_1, ..., t_k)$ où $R$ est un symbole de relation d'arité $k$ et $t_1, ..., t_k$ sont des termes.
\end{defi}

\begin{exemple}
	Toujours avec la même signature, $x \star e = x$ et $x \star y = y \star x$ sont des formules atomiques.
\end{exemple}

\begin{defi}
	Une formule du calcul des prédicats est :
	\begin{itemize}
		\item soit une formule atomique,
		\item soit de la forme $\exists x \phi$ où $x$ est une variable et $\phi$ une formule,
		\item soit de la forme $\forall x \phi$ où $x$ est une variable et $\phi$ une formule,
		\item soit de la forme $\non \phi, \phi \et \phi', \phi \ou \phi'$ ou $\phi \implique \phi'$, où $\phi$ et $\phi'$ sont des formules.
	\end{itemize}
\end{defi}

\textbf{Remarque :} Ces formules sont bien sûr des arbres, que l'on note partiellement en préfixe (quantificateurs, symboles de fonctions...) et partiellement en infixe (connecteurs logiques binaires, égalité...), voire partiellement en postfixe (symbole de fonction $^{-1}$ pour notre signature de théorie des groupes).

\begin{defi}
	Une occurrence d'une variable $x$ dans une formule $\phi$ est un noeud de $\phi$ étiqueté par $x$. Une occurrence de $x$ est dite :
	\begin{itemize}
		\item libre si le chemin reliant l'occurrence à la racine ne contient pas de noeud $\forall x$ (ou plus précisément, pas de noeud $\forall$ dont le fils gauche est $x$), ni de noeud $\exists x$.
		\item liée si le point de liaison de l'occurrence est le premier noeud $\forall x$ ou $\exists x$ rencontré en remontant de l'occurrence vers la racine.
	\end{itemize}
\end{defi}


\begin{exemple}
	L'arbre de formule ci-dessous sera noté $\forall x (\exists y \ x \star y = y) \implique x = e$ avec les conventions usuelles de priorité. On peut rajouter des parenthèses si on le souhaite : $\forall (x (\exists y \ x \star y = y) \implique x = e)$.
	
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (2.5,2) {$\forall$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (1,0.5) {$x$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (4,0.5) {$\rightarrow$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v4) at (5.5,-1) {$=$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v7) at (2.5,-1) {$\exists$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v5) at (4.5,-2.5) {$x$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (6.5,-2.5) {$e$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (3.5,-2.5) {$=$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v8) at (1.5,-2.5) {$y$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v10) at (2.5,-4) {$\star$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v12) at (4.5,-4) {$y$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v11) at (3.5,-5.5) {$y$};
			\node [draw,outer sep=0,inner sep=1,minimum size=10] (v13) at (1.5,-5.5) {x};
			\draw  (v1) edge (v2);
			\draw  (v1) edge (v3);
			\draw  (v3) edge (v4);
			\draw  (v4) edge (v5);
			\draw  (v4) edge (v6);
			\draw  (v3) edge (v7);
			\draw  (v7) edge (v8);
			\draw  (v7) edge (v9);
			\draw  (v9) edge (v10);
			\draw  (v10) edge (v13);
			\draw  (v10) edge (v11);
			\draw  (v9) edge (v12);
		\end{tikzpicture}
		\caption{Représentation d'une formule.}
	\end{figure}
\end{exemple}

\textbf{Remarque :} La porté d'un quantification $\forall x$ (ou $\exists x$) est le sous-arbre enraciné en ce quantificateur. Toutes les occurrences de $x$ dans ce sous-arbre sont liées, mais leur point de liaison n'est pas nécessairement la racine du sous-arbre.

\begin{defi}
	Une variable est dite libre dans une formule $\phi$ si elle y a au moins une occurrence libre, liée si elle y a au moins une occurrence liée. On note $\mathcal{V}(\phi)$ l'ensemble des variables qui apparaissent dans $\phi$, $\mathcal{F}(\phi)$ l'ensemble de ses variables libres, $\mathcal{B}(\phi)$ l'ensemble de ses variables liées.
\end{defi}

\textbf{Remarque :} Une variable peut être libre et liée dans une même formule : c'est par exemple le cas de $x$ dans $x = e \et \forall x \ x \star x = e$. En règle générale, on évite d'écrire de telles formules.

\medskip

\textbf{Renommage d'une variable liée :} En mathématiques, on dit souvent variable muette plutôt que variable liée, et il est implicite que le nom des variables muettes n'a pas d'importance. Pour traduire cette intuition, nous allons considérer les formules "à renommage des variables liées près" ; cependant, il faut être soigneux si l'on veut définir cela correctement.
\begin{itemize}
	\item On veut clairement identifier les formules $\forall x \ f(f(x)) = x$ et $\forall y \ f(f(y)) = y$.
	\item De même, pour $\forall x \exists y \ x + 1 = y$ et $\forall y \exists x \ y + 1 = x$.
	\item C'est toujours le cas pour $\forall y \ x \star y = y$ et $\forall z \ x \star z = z$.
	\item En revanche, on ne peut pas identifier :
	\begin{itemize}
		\item $\forall y \ x \star y = y$ et $\forall y z \star y = y$ (renommage d'une variable libre),
		\item $\forall y \ x \star y = y$ et $\forall x x \star x = x$ (renommage d'une variable liée en un nom utilisé par une variable libre qui est "capturée"),
		\item $\forall y \exists x \quad y +1 = x$ et $\forall x \exists x \quad x + 1 = x$ (renommage d'une variable liée qui résulte en sa "capture" par un autre quantificateur).
	\end{itemize}
\end{itemize}

\begin{defi}
	Si l'on peut passer de $\phi$ à $\phi'$ en renommant toutes les occurrences d'une variable $x$ liées par un même quantificateur en $y$, où $y \not\in \mathcal{V}(\phi)$, alors $\phi$ et $\phi'$ sont considérées comme égales.
\end{defi}

\newpage

\remarklist{
	\item On peut se contenter d'une compréhension intuitive de cette règle : on se demande juste de ne pas faire n'importe quoi.
	\item Le renommage permet  se ramener à une formule propre, c'est-à-dire telle que :
	\begin{itemize}
		\item une même variable n'apparaît pas à la fois liée et libre,
		\item une variable est liée par au plus un quantificateur.
	\end{itemize}
	Par exemple, on peut passer de \[x > 0  \et \forall x (x = f(y) \implique \forall x \ f(f(x)) = f(y))\] à \[x > 0  \et \forall z (z = f(y) \implique \forall t \ f(f(t)) = f(y)).\]
	\item Le problème du renommage d'une variable liée se pose aussi si l'on veut renommer un identifiant dans un programme, ou un morceau de programme :
	\begin{itemize}
		\item si l'identifiant est libre, on ne peut pas le renommer sans changer le sens (c'est une variable globale, ou en tout cas dont la portée dépasse celle du fragment considéré),
		\item s'il est lié, on peut renommer toutes les occurrences dont le point de liaison est le même ne utilisant un nom "frais".
	\end{itemize}
}

\begin{exemple}
	Considérons la fonction suivante :
	
	\begin{listingsbox}{myc}{C}
		int f(int x, int n) {
			int z = 0;
			for (int i = 0 ; i < n ; i++) {
				z = z + x;
				for (int x = 0 ; x < i ; x++) {
					z = z + a + x;
				}
			}
			return z;
		}
	\end{listingsbox}
	
	Le seul identifiant libre est $a$, les identifiants liés sont $x, n, z$ et $i$. L'identifiant $x$ est lié deux fois : une fois à la ligne $1$ en tant que paramètre de la fonction et une fois ligne $5$ (c'est une très mauvaise idée, bien sûr, mais c'est légal).
	\begin{itemize}
		\item On ne peut pas renommer $a$,
		\item On peut renommer $n$ tant qu'on utilise un nom frais.
		\item De même pour $i$
		\item On peut renommer le $x$ externe en $y$ (qui est frais) pour améliorer la lisibilité.
	\end{itemize}
	
	On obtient le code suivant :	
	
	\medskip
	
	\begin{listingsbox}{myc}{C}
		int f(int y, int n) {
			int z = 0;
			for (int i = 0 ; i < n ; i++) {
				z = z + y;
				for (int x = 0 ; x < i ; x++) {
					z = z + a + x;
				}
			}
			return z;
		}
	\end{listingsbox}
\end{exemple}

\begin{defi}
	Soit $t$ un terme, $\phi$ une formule et $x$ une variable. La substitution $\phi[t / x]$ de $x$ par $t$ dans $\phi$ est la formule obtenue par le processus suivant :
	\begin{itemize}
		\item on renomme les variables liées de $\phi$ pour que $\mathcal{B}(\phi) \cap \mathcal{V}(t) = \emptyset$,
		\item on remplace ensuite toutes les occurrences libres de $x$ dans $\phi$ par $t$.
	\end{itemize}
\end{defi}

\textbf{Remarque :} On trouve d'autres notations : $\phi[x \leftarrow t], \phi^{\{x \leftarrow t\}}$...

\begin{exemple}
	Pour comprendre pourquoi le renommage est nécessaire, considérons la formule suivante : $\phi = \forall x \exists y \ f(x+y) > z$.
	\begin{itemize}
		\item Si l'on prend $t = g(c, w)$ où $c$ est une constante et $w$ est une variable, on obtient $\phi[t / z] = \forall x \exists y \  f(x+y) > g(c, w)$. Il n'y a pas de problème.
		\item Pour $t = g(x)$, une substitution directe donnerait $\forall x \exists y \ f(x+y) > g(x)$. Ca ne correspond pas à ce que l'on voulait, le $x$ apparaissant dans $t$ est devenu lié (il a été capturé). En réalité, on commence par faire un renommage : $\phi = \forall w \exists y \ f(w+y) > z$. On obtient alors $\phi[g(x) / z] = \forall w \exists y \ f(w+y) > g(x)$.
	\end{itemize}
\end{exemple}

\section{Déduction naturelle pour le calcul des prédicats}

Pour adapter la déduction naturelle au calcul des prédicats, on traite essentiellement les quantificateurs comme les autres connecteurs : règles d'introduction et d'élimination.

\subsection{Quantificateur universel}

On veut traduire deux choses :
\begin{itemize}
	\item si l'on sait prouver une propriété $P(x)$ dépendant d'une variable $x$ sans aucune hypothèse sur $x$, alors on sait prouver $\forall x \ P(x)$,
	\item inversement, pour prouver une propriété $\forall x \ P(x)$, on se donne un $x$ sans faire aucune hypothèse dessus ("soit $x$") et on montre que l'on a $P(x)$,
	\item si l'on sait prouver $\forall x \ P(x)$, alors on peut remplacer $x$ par un terme quelconque $t$ et obtenir $P(t)$,
	\item inversement, pour prouver une propriété de la forme $P(t)$, il suffit de montrer que l'on a plus généralement $\forall x \ P(x)$ et conclure avec le cas particulier $t$.
\end{itemize}

La seule difficulté est de réfléchir aux bonnes restrictions à poser.

\medskip

\textbf{Introduction :} La traduction formelle de "aucune hypothèse n'a été faite sur $x$" est que $x$ n'apparait pas libre dans $\Gamma$ (c'est-à-dire que $x$ n'apparait libre dans aucune formule de $\Gamma$). Elle signifie que l'on a rien supposé sur $x$ puisque $x$ n'a pas le droit d'apparaître dans les hypothèses. Cette condition est cruciale, et l'oubli de cette vérification est souvent source d'erreurs.

\begin{figure*}[!h]
	\centering
	\begin{prooftree}
		\hypo{\hyp \montre \phi}
		\hypo{x \not\in \mathcal{F}(\hyp)}
		\infer2[($\forall_i)$]{\hyp \montre \forall x \ \phi}
	\end{prooftree}
\end{figure*}

\textbf{Élimination :} Ici, il faut juste comprendre que "remplacer $x$ par un terme $t$ quelconque" signifie faire la substitution de $x$ par $t$ comme défini ci-dessus (en évitant donc les captures).

\begin{figure*}[!h]
	\centering
	\begin{prooftree}
		\hypo{\hyp \montre \forall x \ \phi}
		\hypo{\mathcal{B}(\phi) \cap \mathcal{V}(t) = \emptyset}
		\infer2[($\forall_e$)]{\hyp \montre \phi[t / x]}
	\end{prooftree}
\end{figure*}

\remarklist{
	\item Avec notre définition de la substitution (qui inclut le renommage), la condition $\mathcal{B}(\phi) \cap \mathcal{V}(t) = \emptyset$ est redondante. Cependant, la remettre explicitement permet de se rappeler que ce renommage est indispensable.
	\item On parle aussi de règle de généralisation pour l'introduction de $\forall$, et d'instanciation pour l'élimination de $\forall$.
}

\subsection{Quantificateur existentiel}

\textbf{Introduction :} Pour prouver $\exists x \ \phi$, il faut fournir un témoin : un terme $t$ tel que $\phi[t / x]$ soit vrai.
\begin{figure*}[!h]
	\centering
	\begin{prooftree}
		\hypo{\hyp \montre \phi[t/x]}
		\infer1[($\exists_i$)]{\hyp \montre \exists x \ \phi}
	\end{prooftree}
\end{figure*}
On indiquera toujours explicitement le témoin utilisé.

\medskip

\textbf{Élimination :} Quand on fait une démonstration (au sens usuel et non formel), la manière d'utiliser une hypothèse de type $\exists x \in E \ \phi(x)$ est la suivante : on introduit un témoin (que l'on nomme généralement $x$, mais qui en tout cas doit porter un nom "frais") et l'on continue la démonstration avec comme seule hypothèse supplémentaire que $\phi(x)$ est vraie. Si l'on arrive à tirer une conclusion, qui ne fait pas intervenir le témoin, alors cette conclusion est valide :
\begin{figure*}[!h]
	\centering
	\begin{prooftree}
		\hypo{\hyp \montre \exists x \ \phi}
		\hypo{\hyp, \phi \montre \psi}
		\hypo{x \not\in \mathcal{F}(\hyp) \cup \mathcal{F}(\psi)}
		\infer3[($\exists_e$)]{\hyp \montre \psi}
	\end{prooftree}
\end{figure*}

Ici, on sait qu'il existe un $x$ tel que $\psi$. On prend un tel $x$, c'est-à-dire que l'on considère $\phi$ avec un $x$ sans hypothèses (c'est-à-dire non libre dans $\Gamma$, ni dans $\psi$) et on montre $\psi$ en utilisant $\phi$ pour ce $x$.

\medskip

\textbf{Remarque :} La condition sur la variable dans les règles $\forall_i$ et $\exists_e$ peut s'écrire dans les deux cas "$x$ n'est pas libre dans le séquent conclusion de la règle".