\chaptr{Arbres couvrants}{0.6}

\setcounter{figure}{17}

Dans ce chapitre, on ne considère que des graphes non-orientés et sans boucles.

\index{graphe!pondéré}
\begin{defi}
	Un graphe pondéré est un triplet $G = (S, A, p)$ avec $(S, A)$ un graphe et $p : A \to \KK$ ($\KK = \RR, \CC, \NN, \ZZ$, ...) une fonction de pondération des arrêtes. Si $a \in A$, $p(a)$ est le poids de l'arête $a$.
\end{defi}

\section{Arbres couvrants}

\textbf{Rappel :} Un arbre est un graphe connexe acyclique. 

\begin{prop}
	Soit $G = (S, A)$ un graphe (non-orienté). Alors, \begin{itemize}
		\item Si $G$ est connexe, $\abs{A} \geq \abs{S} - 1$.
		\item Si $G$ est acyclique, $\abs{A} \leq \abs{S} - 1$.
	\end{itemize}
\end{prop}

\textbf{Remarque :} Les arbres correspondent à la rencontre entre ces deux mondes et on a $\abs{A} = \abs{S} - 1$.

\begin{prop}
	Si $G = (S, A)$ est un graphe non orienté, alors les propositions suivantes sont équivalentes : 
	\begin{itemize}
		\item $G$ est un arbre ($G$ est connexe acyclique).
		\item $G$ est connexe et $\abs{A} \leq \abs{S} - 1$.
		\item $G$ est acyclique et $\abs{A} \geq \abs{S} - 1$.
	\end{itemize}
\end{prop}

\index{arbre!couvrant}
\begin{defi}
	Un arbre couvrant d'un graphe $G = (S, A)$ (pondéré ou non) est un sous-graphe $T = (S, A')$ avec $A' \subseteq A$ qui est un arbre. 
	
	\medskip
	
	C'est donc un sous-graphe connexe acyclique qui possède tous les sommets. Un arbre couvrant a les mêmes sommets.
\end{defi}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (-3,3) {A};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (-0.5,3) {C};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (2,3) {E};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (-3,0.5) {B};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v5) at (-0.5,0.5) {D};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v4) at (2,0.5) {F};
		\draw  (v1) edge (v2);
		\draw  (v2) edge (v3);
		\draw  (v3) edge (v4);
		\draw  (v4) edge (v5);
		\draw  (v5) edge (v2);
		\draw  (v2) edge (v4);
		\draw  (v6) edge (v5);
		\draw  (v6) edge (v1);
		\draw  (v1) edge (v5);
		\draw  (v2) edge (v6);
		\node at (-1.75,3.25) {6};
		\node at (0.75,3.25) {5};
		\node at (1,2) {3};
		\node at (2.5,1.75) {4};
		\node at (0.75,0.25) {4};
		\node at (-0.25,1.5) {2};
		
		\node at (-1.75,0.25) {2};
		\node at (-3.25,1.75) {5};
		\node at (-2.25,1.5) {4};
		\node at (-2,2.25) {1};
		
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v8) at (-3,-1) {A};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (-0.5,-1) {C};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v12) at (2,-1) {E};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v10) at (2,-3.5) {F};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v11) at (-0.5,-3.5) {D};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v7) at (-3,-3.5) {B};
		\draw  (v7) edge (v8);
		\draw  (v8) edge (v9);
		\draw  (v9) edge (v10);
		\draw  (v10) edge (v11);
		\draw  (v9) edge (v12);
	\end{tikzpicture}
	\caption[Arbre couvrant d'un graphe]{Le sous-graphe est un arbre couvrant du graphe.}
\end{figure}

\remarklist{
	\item Un arbre couvrant d'un graphe $G = (S, A)$ correspond donc à la donnée d'un ensemble d'arêtes $A' \subseteq A$. On a alors tout sommet de $S$ est incident à une arête de $A'$. On dit que $A'$ couvre les sommets du graphe.
	\item $(x, y)$ est une arête incidente à $x$ et $y$ qui lui sont incident.
	\item Un arbre couvrant est un sous-graphe connexe minimal qui couvre tous les sommets.
	\item Les arbres couvrants ont de nombreuses applications : construction de réseaux électriques, téléphoniques, etc... Les sommets correspondent à des villes et le poids des arêtes correspondent au coût de construction d'une ligne entre ces villes. On veut tout interconnecter au plus bas prix.
}

\begin{prop}
	Un graphe (non orienté, non nécessairement connexe) est connexe si et seulement s'il possède au moins un arbre couvrant.
\end{prop}

\begin{proof}
	Si le graphe $G = (S, A)$ possède un arbre couvrant $T = (S, A')$, alors tout sommet $x, y \in S$ sont reliés dans $T$ donc dans $G$. Donc $G$ est connexe. L'arborescence de parcours d'un graphe connexe est un arbre couvrant de ce graphe.
\end{proof}

\textbf{Remarque :} Si le graphe n'est pas connexe. On travaille sur les composantes connexes. On peut parler de forêt couvrante. Ici, on va supposer les graphes connexes.

\index{poids}
\begin{defi}
	Soit $G = (S, A, p)$ un graphe (non-orienté) pondéré connexe et $T = (S, A')$ un arbre couvrant de $G$. Le poids de $T$, noté $p(T)$ est la quantité $$p(T) = \summ_{a \in A} p(a)$$
\end{defi}

\newpage

\index{arbre!couvrant de poids minimal}
\begin{defi}
	Un arbre couvrant de poids minimal d'un graphe $G$ est un arbre couvrant de $G$ donc le poids est plus petit ou égal au poids de tout autre arbre couvrant de $G$.
\end{defi}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (-3,3) {A};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (-0.5,3) {C};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (2,3) {E};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (-3,0.5) {B};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v5) at (-0.5,0.5) {D};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v4) at (2,0.5) {F};
		\draw  (v3) edge (v4);
		\draw  (v5) edge (v2);
		\draw  (v2) edge (v4);
		\draw  (v1) edge (v5);
		\draw  (v2) edge (v6);
		\node at (1,2) {3};
		\node at (2.5,1.75) {4};
		\node at (-0.25,1.5) {2};
		\node at (-2.25,1.5) {4};
		\node at (-2.25,2.5) {1};
	\end{tikzpicture}
	\caption[Arbre couvrant de poids minimal]{L'arbre couvrant de poids minimal du graphe de la figure $7$.}
\end{figure}

\begin{exemple}
	Le poids de l'arbre couvrant de la figure $7$ est de $5 + 6 + 5 + 3 + 4 = 23$. Mais est-ce que Figure $8$ l'est ?
\end{exemple}

\remarklist{
	\item Il n'y a pas unicité a priori de l'arbre couvrant de poids minimal (cf TD : c'est le cas si toutes les arêtes ont un poids différent.)
	\item Ici encore les applications sont nombreuses. 
}

Mais comment faire pour trouver un algorithme qui trouve un arbre couvrant de poids minimal.

\section{Algorithme de Kruskal}

\index{{\scshape Algorithme}!de Kruskal}
L'algorithme de Kruskal est un algorithme glouton qui permet de résoudre ce problème.

\medskip

\textbf{Rappel :} Intuitivement, un algorithme glouton effectue à chaque étape le choix optimal à cette étape, sans remettre en question ses choix passés.

\medskip

\textbf{Remarque :} Un algorithme glouton doit toujours être accompagné d'une preuve de sa correction (si on veut l'utiliser pour trouver une solution ; sinon il s'agit le plus souvent d'un algorithme d'approximation).

\medskip

On crée un arbre $T$ initialement vide. A chaque étape, on prend la plus petit arête disponible admissible (\cad qui ne rée pas de cycle dans $T$). Autrement dit, on ajoute si possible toutes les arêtes dans l'ordre de poids croissant.

\medskip

On propose donc l'algorithme de Kruskal suivant :

\begin{algorithm}
	\centering
	\caption*{Une première partie de Kruskal}
	\begin{algorithmic}
		\Function{Kruskal}{G = (S, A, p)}
		\State Trier A selon p
		\State T $\leftarrow$ (S, $\emptyset$)
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\remarklist{
	\item On peut commencer par trier la liste. Ceci permet ensuite simplement de considérer les arêtes par poids croissant. 
	\item Comment savoir si une arête est admissible, \cad si elle créé un cycle dans $T$.
	\item On peut faire un parcours en profondeur pour détecter si le graphe devient cyclique. Cependant, cette méthode est couteuse. 
}

\begin{prop}
	L'algorithme de Kruskal construit un arbre couvrant (plus tard, de poids minimal).
\end{prop}

Au départ, \codeCaml{T = (S, $\emptyset$)}.

\medskip

\textbf{Invariant :} \codeCaml{T = (S, A')} est un sous-graphe acyclique. L'invariant est bien conservé car on n'ajoute une arête seulement si elle ne créé pas de cycle. 

\medskip

Après $\abs{A}$ ajouts, comme le graphe est connexe, il y a eu au moins $\abs{S} - 1$ ajouts. Donc, $T$ est connexe car acyclique avec au moins $\abs{S} - 1$ arêtes.

\medskip

\textbf{Remarque :} On peut s'arrêter après $\abs{S} - 1$ ajouts.

\medskip

Pour savoir efficacement que la prochaine arête candidate est admissible, il suffit de vérifier que les deux sommets qu'elle relie ne sont pas dans une même composante connexe de $T$. (le sous-graphe acyclique en construction). L'ensemble des composantes connexes de $T$ est une partition des sommets de $T$, c'est une collection d'ensemble disjoints dynamique. On peut alors utiliser l'algorithme Unir et Trouver.

\medskip

Pour savoir s'il faut ajouter une arête $(x, y) \in A$, on vérifie que \codeCaml{trouver(x)} $\ne$ \codeCaml{trouver(y)}. Si c'est le cas, on l'ajoute à $T$ et on fusionne les composantes connexes avec \codeCaml{unir(x, y)}.

\begin{algorithm}
	\centering
	\caption*{Algorithme de Kruskal}
	\begin{algorithmic}
		\Function{Kruskal}{G = (S, A, p)}
		\State Trier A selon p
		\State T $\leftarrow$ (S, $\emptyset$)
		\State C = creer\_partition($\abs{S}$)
		\For{chaque {x, y} $\in$ A trié par ordre de poids croissant}
		\If{trouver(x) != trouver(y)}
		\State Ajouter({x, y}, T)
		\State unir(x, y)
		\EndIf
		\EndFor
		\State \Return T
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\textbf{Remarque :} On peut faire un arrêt anticipé si on a fait $\abs{S} - 1$ ajouts (et dans ce cas, la partition comporte une unique composante).

\medskip

\textbf{Analyse de la complexité :} Le tri des arêtes se fait en $O(\abs{A}\log \abs{A})$ avec un tri semi-linéaire (tri par tas, tri fusion, tri rapide), en moyenne. Il y a ensuite :
\begin{itemize}
	\item $\abs{A}$ étapes pour la boucle, \cad $O(\abs{A})$.
	\item Un appel à \codeCaml{creer_partition}, \cad $O(\abs{S})$.
	\item Au plus $2\abs{A}$ appels à trouver, \cad $O(2\abs{A}\log\abs{S})$.
	\item Exactement $\abs{S} - 1$ appels à unir, \cad $O(\abs{S}\log\abs{S})$
\end{itemize}

Par conséquent, la complexité totale est $O(\abs{A} \log \abs{S})$. En réalité, \codeCaml{unir} et \codeCaml{trouver} sont en $O(\log*\abs{S}))$ en amorti.  

\medskip

On a donc une complexité plutôt de l'ordre de $O(\abs{A} \log \abs{S}) + O(\abs{A})$. Le premier terme est pour le tri.

\medskip

La complexité est donc ici dominée par le tri. Si les arêtes sont déjà triées, on a une complexité en $O(\abs{A})$. Sinon, avec un tri radix en $O(\abs{A})$, on a aussi une complexité en $O(\abs{A})$.

\medskip

\textbf{Tri radix :} On trie par chiffre des puissances décroissantes avec un tri stable (qui ne change pas l'ordre relatif de ce qui est fait avant). 

\medskip

\remarklist{
	\item Si $T = (S, B)$ est un arbre couvrant de $G = (S, A)$ et si $a \in A \backslash B$, alors, $T + a$ contient un cycle. Enlever une arête de ce cycle donne un arbre couvrant $T'$.
	\item Il existe toujours un arbre couvrant minimal : en effet, il existe un nombre fini d'arbres couvrants.
}

\section{Correction de l'algorithme de Kruskal}

Une idée réside dans l'invariant suivant : à chaque étape, le sous-graphe en cours de construction est un sous-graphe d'un arbre couvrant de poids minimal. Il est toujours possible de compléter le sous-graphe en cours de construction en un arbre de poids minimal.

\medskip

\textbf{Remarque :} L'algorithme glouton ne fait jamais de choix aux conséquences irrémédiables.

\begin{prop}
	L'algorithme de Kruskal appliqué à un graphe non-orienté connexe $G = (S, A)$ renvoie un arbre couvrant $T = (S, B)$ de poids minimal.
\end{prop}

\begin{proof}
	Montrons tout d'abord que $T$ est bien un arbre couvrant. On a $T = (S, B)$ donc $T$ est couvrant. On a pour invariant que le sous-graphe en construction est acyclique. Si on s'arrête en atteignant $\abs{S} - 1$ étapes, on a un sous-graphe acyclique à $\abs{S} - 1$ arêtes qui est donc connexe. Il s'agit d'un arbre.
	
	\medskip
	
	Montrons que ceci est toujours le cas. Supposons que l'on a considéré toutes les arêtes et que le sous-graphe n'est pas connexe. Il comporte au moins deux composantes connexes. Comme $G$ est connexe et ne comporte qu'une seule composante connexe, deux de ces composantes connexes sont reliés dans $G$ pour une arête $\{x, y\}$.
	
	\medskip
	
	Or, cette arête $\{x, y\}$ a été envisagée dans l'algorithme de Kruskal. A ce moment, elle ne reliait pas les composantes connexes qui contenaient $x$ et $y$, sinon, ce serait toujours le cas. Elle a donc due être ajoutée. Absurde.
	
	\medskip
	
	Montrons maintenant que la propriété suivante est un invariant en notant $\underline{T} = (S, \underline{B})$ le sous-graphe en cours de construction : 
	\begin{center}
		$\underline{T} = (S, \underline{B})$ est un sous-arbre d'un graphe $T^* = (S, B^*)$ de poids minimal de $G$.
	\end{center}
	
	\textbf{Initialisation :} Avant le premier tour de boucle, on a $\underline{T} = (S, \emptyset)$. Il existe donc un arbre couvrant $T^*$ de poids minimal (car l'ensemble des arbres couvrants est fini non vide) et $\underline{T}$ est bien sous-graphe de $T^*$.
	
	\medskip
	
	\textbf{Hérédité :} Supposons la propriété vraie au début d'un tour de boucle. On a donc $\underline{T} = (S, \underline{B})$ sous-graphe d'un arbre couvrant $\underline{T^*} = (S, \underline{B^*})$ de poids minimal de $G$. Dans un passage dans la boucle, distinguons trois cas :
	\begin{enumerate}
		\item L'arête $\{x, y\}$ n'est pas ajoutée à $\underline{T}$, on a $\overline{T} = \underline{T}$. Et la propriété reste vraie avec $\overline{T^*} = \underline{T^*}$.
		\item L'arête $\{x, y\} \in \underline{B^*}$, dans ce cas, $\overline{T} = (S, \underline{B} \cup \{\{x, y\}\})$. On a encore $\overline{T}$ sous graphe de $\overline{T^*} = \underline{T^*}$ arbre couvrant de poids minimal.
		\item Si $\{x, y\} \in A \backslash B^*$, on a $\overline{T} = (S, \underline{B} \cup \{\{x, y\}\})$ qui n'est pas un sous-graphe de $\underline{T^*}$. Le sous-graphe $T^* + \{x,y\}$ comporte un cycle (contenant $\{x,y\}$). Comme $x$ et $y$ ne sont pas dans une même composante connexe de $T$, le chemin de $x$ à $y$ dans $\underline{T^*}$ qui utilise le reste du cycle, ne peut pas être en entier dans $\underline{T}$. Donc il existe une arête $a \in \underline{B*} \backslash \underline{B}$ sur ce cycle. On ne peut pas encore avoir rencontré $a$, sinon on aurait ajouté $a$. En effet, comme $\underline{T}$ est sous-graphe de $T^*$, il ne peut pas y avoir d'autre chemin reliant dans $\underline{T}$ les extrémités de $a$ (sinon cela  crée un cycle dans $\underline{T^*}$).
		
		\medskip
		
		Comme on considère les arêtes dans l'ordre croissant, on a donc $p(\{x,y\}) \leq p(a)$. On considère $\overline{T*}= \underline{T^*} - a + \{x,y\}$ qui est encore un arbre couvrant. $p(\overline{T^*}) = p(\underline{T^{*}}) - p(a) + p(\{x,y\}) \leq p(\underline{T^*})$. Donc $\overline{T^*}$ est aussi de poids minimal (et $p(\overline{T^*}) = p(\underline{T^*})$).
	\end{enumerate}
	
	L'invariant est conservée $\overline{T}$ est un sous-graphe de $\overline{T^*}$ qui est un arbre couvrant de poids minimal. 
	
	\medskip
	
	\textbf{Correction :} A la fin de la boucle $T=(S,B)$ est un arbre couvrant, sous-graphe d'un arbre couvrant de poids minimal, donc égal à cet arbre de poids minimal.
\end{proof}