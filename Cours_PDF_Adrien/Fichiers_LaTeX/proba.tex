\chaptr{Algorithmes probabilistes}{1}

Un algorithme probabiliste est un algorithme qui effectue des choix aléatoires pendant son exécution. Sa mise en œuvre implique l'utilisation d'un générateur de nombres pseudo-aléatoires fourni par le système ou par la bibliothèque standard. Dès lors, deux exécutions différentes ne donnent pas forcément les mêmes résultats, les mêmes temps d'exécution ou le même usage de la mémoire. Par opposition, un algorithme ou un programme qui ne fait pas usage de choix aléatoires est qualifié de déterministe.

\medskip

\index{algorithme!probabiliste}
Il existe de multiples sortes d'algorithmes probabilistes.

\begin{itemize}
	\item On peut utiliser un algorithme probabiliste pour mélanger des données ou encore réaliser un échantillonnage, c'est-à-dire sélectionner un sous-ensemble des données. Dans le premier cas, on peut souhaiter que toutes les permutations soient équiprobables. Dans le second cas, on peut souhaiter un tirage équitable entre tous les sous-ensembles possibles. Par exemple, tirer aléatoirement un arbre binaire de taille $n$, avec équiprobabilité parmi tous les arbres binaires de taille $n$.
	\item On peut utiliser un algorithme probabiliste pour améliorer les performances d'un programme, comme par exemple mélanger un tableau avant de lui appliquer un tri rapide. Le choix du pivot à chaque étape n'est plus imposé par la forme initiale du tableau. On évite ainsi des situations pathologiques, comme un tableau qui serait trié en ordre croissant ou décroissant. On montre alors que l'espérance du nombre de comparaisons est $O(n \log n)$ pour trier un tableau de taille $n$, ce qui est optimal.
	\item Un algorithme probabiliste peut être utilisé pour obtenir une solution approchée à un problème difficile, dont une résolution exacte demanderait trop de temps. Par exemple, il existe des algorithmes probabilistes qui permettent de vérifier très efficacement qu'un entier est probablement premier avec une très faible probabilité de se tromper.
	\item Une structure de données probabiliste utilise le hasard pour améliorer ses performances en temps ou en espace. Le filtre de Bloom en est un exemple où  une structure d'ensemble permet d'ajouter des éléments et de tester la présence d'un élément. Cette seconde opération renvoie toujours vrai pour un élément de l'ensemble mais elle peut parfois renvoyer également vrai pour un élément qui n'est pas dans l'ensemble. Sous certaines conditions, on peut limiter la probabilité de ces faux positifs.
	\item Il existe également des structures de données probabilistes où les résultats sont toujours corrects, le hasard n'étant utilisé que pour améliorer statistiquement les performances.
\end{itemize}

Ces exemples montrent qu'il n'est pas aisé de classifier précisément les algorithmes probabilistes.

\medskip

\index{algorithme!Las Vegas}
\index{algorithme!Monte Carlo}
Néanmoins, deux grandes familles se dégagent :
\begin{itemize}
	\item Les algorithmes de type \textit{Las Vegas} donnent toujours un résultat correct. Le hasard influence ici le temps de calcul. Il est petit avec une forte probabilité. Un exemple de cette catégorie est le tri rapide.
	\item Les algorithmes de type \textit{Monte Carlo} ne donnent pas forcément un résultat correct. Ici, c'est la probabilité de la correction qui nous intéresse. Un exemple de cette catégorie est le test de primalité.
\end{itemize}

\section{Tirage aléatoire et pseudo-aléatoire}

\index{tirage!aléatoire}
\index{tirage!pseudo-aléatoire}
On étend notre modèle de calcul en le dotant d'une opération supplémentaire \lcaps{GetRandomBit()} permettant de tirer au hasard un bit, en temps unitaire. Ce tirage se fait de manière uniforme et les différents tirages sont mutuellement indépendants.

\begin{itemize}
	\item Tirer aléatoirement un entier $0$ et $n-1$ prend donc un temps proportionnel à $\log n$. Dans le cas d'un entier machine qui est bien sûr de loin le plus fréquent en pratique, il s'agit d'une opération élémentaire ($\log n$ est majorée par la taille d'un mot machine.)
	\item Il faut bien distinguer le choix non-déterministe que l'on autorise dans la classe \cNP (où, selon le poids de vue, l'algorithme devine le bit ou se sépare en deux et continue à s'exécuter pour les deux valeurs) et ce choix aléatoire (où l'on tire au hasard le bit et ne continue l'exécution qu'avec la valeur choisie).
\end{itemize}

En pratique, on utilise le plus souvent des générateurs pseudo-aléatoires. Un tel générateur possède un état interne $S$ (de $N$ bits avec $N$ fixé), et à chaque fois qu'on l'appelle :
\begin{itemize}
	\item renvoie un bit $g(S)$, où $g$ est une fonction parfaitement déterministe
	\item remplace son état $S$ par un état $S' = h(S)$ à nouveau de manière parfaitement déterministe.
\end{itemize}

La valeur initiale de $S$ est appelée graine : si l'on part de la même graine, on obtiendra les mêmes bits aléatoires, dans le même ordre.

\medskip

Voici par exemple une implémentation naïve en C. La fonction \codeCc{srand} initialise la graine et la fonction \codeCc{rand} renvoie un entier tiré uniformément entre $0$ et \codeCc{RAND_MAX} inclus.

\medskip

\begin{listingsbox}{myc}{C}
	uint64_t next = 1;
	
	void srand(int seed) {
		next = seed;
	}
	
	int rand(void) {
		// RAND_MAX asumed to be 2147483647
		next = next * 1103515245 + 12345;
		return (next / 65536) % 2147483647
	}
\end{listingsbox}

\medskip

Il est très délicat de définir précisément quelles propriétés la suite de bits renvoyés doit vérifier pour qu'on la considère comme pseudo-aléatoire. Dans le cas d'un algorithme probabiliste, la qualité de l'aléatoire n'a pas une grande importance : on fera les démonstrations en supposant qu'on a de vrais triages indépendants, mais en pratique, même un générateur de mauvaise qualité fera l'affaire. Il en va autrement si l'on est en train de choisir une clé de chiffrement...

\medskip

En OCaml, la fonction \codeCaml{Random.init} permet de choisir la graine (de fixer S). Si on ne l'appelle pas, une graine par défaut est utilisée. Un appel à la fonction \codeCaml{Random.int n} permet de générer de manière uniforme un entier entre $0$ inclus et $n$ exclus.

\medskip

\begin{minipage}{0.48\textwidth}
	\begin{listingsbox}{myocaml}{OCaml}
	let () = 
		for i = 0 to 5 do
			Printf.printf "d "
				(Random.int 100)
		done;
		print_newline ()
	\end{listingsbox}
\end{minipage} 
\hfill
\begin{minipage}{0.48\textwidth}
	\begin{listingsbox}{mybash}{Bash}
	$ ./alea
	44 85 82 41 39 0
	$ ./alea
	44 85 82 41 39 0
	$ ./alea
	44 85 82 41 39 0
	$ ./alea
	44 85 82 41 39 0
	\end{listingsbox}
\end{minipage}

\section{Échantillonnage}

Supposons que l'on veuille choisir $k$ valeurs parmi $n$, sous l'hypothèse $1 \leq  k \leq n$. Pour fixer les idées, les valeurs sont données dans un tableau $a$ de taille $n$ et on souhaite renvoyer un tableau de taille $k$ avec les valeurs sélectionnés. Il convient d'être précis quant à la spécification du problème.
\begin{itemize}
	\item L'ordre dans le tableau renvoyé n'est pas significatif. En particulier, les éléments peuvent ne pas être ordonnés comme dans le tableau initial.
	\item Le tableau $a$ peut contenir des doublons mais on considère pour autant tous les éléments de $a$ comme distincts. Dit autrement, tout se passe comme si on sélectionnait $k$ indices parmi $n$ pour renvoyer ensuite les éléments de $a$ situés à ces indices. Si par exemple, $a = [1, 1, 2]$, alors on renvoie $[1, 1]$ une fois sur trois et $[1, 2]$ deux fois sur trois. 
\end{itemize}

Pour $k = 1$, le problème est plus simple : il suffit de tirer un indice dans $[0, n[$. Pour $k \geq 2$, le problème est plus complexe. Après avoir choisi un premier élément, le choix devient plus délicat. Que faire si on retombe sur un élément déjà choisi ? L'ignorer et recommencer ? Cela va-t-il terminer dans un temps raisonnable ? Et quand bien même cela terminerait rapidement, obtient-on un tirage  équitable ? Fort heureusement, un algorithme simple et efficace résout ce problème. Il utilise un tableau $r$ de taille $k$ qui contiendra le résultat final.
\begin{itemize}
	\item On initialise le tableau $r$ avec les $k$ premières valeurs de $a$, c'est-à-dire qu'on pose $r = [a_0, a_1, \ldots, a_{k-1}]$.
	\item Pour $i$ de $k$ à $n$, on tire au hasard un entier entier $j$ entre $0$ et $i$ inclus. Si $j < k$, on remplacera $r_j$ par $a_i$.
\end{itemize}

Le programme suivant implémente cet algorithme : la fonction \codeCaml{sampling} est de complexité $\Theta(n)$.

\medskip

\begin{listingsbox}{myocaml}{OCaml}
	let sampling k a =
		if k < 0 || k > Array.length a then
			invalid_arg "sampling";
		let r = Array.sub a 0 k in
		for i = k to Array.length a - 1 do
			let j = Random.int (i+1) in
			if j < k then r.(j) <- a.(i)
		done;
		r
\end{listingsbox}

\medskip

On peut commencer par examiner le comportement du programme sur des cas limites. Pour $k = 1$ et $n = 2$, on initialise $r = [a_0]$ puis on effectue une itération pour $i = 1$ en tirant $j$ dans $[0, 2[$. On a donc bien une chance sur deux de remplacer $a_0$ par $a_1$ (cas $j < 1$).

\medskip

Plus généralement, montrons que tous les éléments de $a$ ont la même probabilité d'être sélectionnés. La preuve repose sur un invariant pour la boucle \codeCaml{for} du programme, à savoir :
\begin{center}
	pour tout $0 \leq l < i, \PP(a_l \text{ est sélectionné}) = \frac{k}{i}$. 
\end{center}

Initialement, $i = k$ et l'invariant est trivialement établi car les $k$ premières valeurs sont sélectionnées avec probabilité $1$. Supposons l'invariant établi pour $ i \geq k$ et considérons l'itération $i$ de l'algorithme. Soit $0 \leq l < i + 1$. 
\begin{itemize}
	\item Pour $l = i$, \[\PP(a_i \text{ est sélectionné}) = \frac{k}{i}\] car on a tiré $j$ dans $[0, i]$ et on a conservé $a_i$ si et seulement si $i < k$.
	\item Pour $l < i$, on a \[\PP(a_l \text{ est sélectionné}) = \PP(a_l \text{ était sélectionné}) \times \PP(a_l \text{ pas écrasé}) = \frac{k}{i+1}\] car pour écraser $a_l$ il faut tirer exactement l'indice où se trouve $a_l$ actuellement, parmi $i+1$ valeurs.
\end{itemize}

L'invariant est donc bien préservé pour $i+1$. A l'issue de l'algorithme, c'est-à-dire $i = n$ on en déduit que chaque élément de $a$ est sélectionné avec probabilité $\frac{k}{n}$, ce qui est bien le résultat attendu.

\medskip

Une autre solution pour l'échantillonnage consiste à mélanger aléatoirement le tableau, par exemple avec le mélange de Knuth, puis à prendre les $k$ premières valeurs. Mais cette solution a pour effet de modifier le tableau, à moins de commencer par le recopier.

\medskip

Enfin, notons que la fonction \codeCaml{sampling} n'a nul besoin de connaître tous les éléments à l'avance, puisqu'elle ne fait que parcourir le tableau une seule fois et qu'elle n'utilise pas la valeur $n$, mais uniquement la valeur $i$. Dès lors, les valeurs pourraient être lues dans un fichier ou encore produites par un autre algorithme, et le même algorithme pourrait être utilisé pour réaliser un échantillonnage. Lorsqu'un algorithme reçoit des données en continu, sans même en connaître le nombre total, on parle d'algorithme en ligne.

\section{Le problème des $n$-reines}

\index{reines}
Le problème des $n$ reines a été proposé par Max Bezzel, un jour d'échec allemand, en 1848 pour l'échiquier standard $8 \times 8$ puis généralisé par François-Joseph Eustache Lionnet en 1869 pour un échiquier $n \times n$. Le problème est le suivant : est-il possible de placer $n$ reines sur un échiquier de taille $n \times n$ sans qu'aucune reine ne soit en prise, c'est-à-dire ne soit attaquée par une autre. Autrement dit, est-il possible de placer $n$ reines sur un échiquier $n \times n$, sans que deux reines ne se retrouvent sur une même ligne, colonne ou diagonale.

\medskip

Carl Friedrich Gauss étudia le problème et le résolut partiellement en proposant 72 solutions avec pour chacune, $8$ reines sur l'échiquier de $64$ cases. Cependant, il avait oublié certaines solutions. La première solution complète fut proposée en $1850$ par Franz Nauck : il existe $92$ dispositions convenables pour l'échiquier $8 \times 8$.

\begin{figure}[!h]
	\centering
	\def\mylist{Qa8, Qb4, Qc1, Qd3, Qe6, Qf2, Qg7, Qh5}
	\setchessboard{setpieces=\mylist, showmover=false}
	\chessboard[pgfstyle=straightmove, shortenstart=1ex, markmoves={d3-a3, d3-a6, d3-b1, d3-d1, d3-d8, d3-f1, d3-h3, d3-h7}]
	\caption{Problème des $n$-reines pour $n=8$.}
\end{figure}

On vérifie sans difficulté que ce problème admet exactement une solution pour $n=1$, n'a pas de solutions pour $n=2$ ou $n=3$ et deux solutions distinctes pour $n=4$. Un exemple de résolution complète pour $n=4$ est illustré par la seconde figure de la section.

\begin{figure}[!h]
	\centering
	\resizebox{4\totalheight}{!}{
		\def\mylist{Qa2, Qb4, Qc1, Qd3}
		\setchessboard{setpieces=\mylist, showmover=false}
		\chessboard[tinyboard, maxfield=d4]
		\def\mylist{Qa3, Qb1, Qc4, Qd2}
		\setchessboard{setpieces=\mylist, showmover=false}
		\chessboard[tinyboard, maxfield=d4]
	}
	\caption{Les deux seules solutions pour $n=4$.}
\end{figure}

La technique du retour sur trace résout efficacement ce problème pour de petites valeurs de $n$, par exemple $n \leq 20$. Mais si on cherche à résoudre le problème pour une valeur relativement grande de $n$, par exemple $n=1000$, alors l'algorithme de retour sur trace va tourner très longtemps avant de trouver une solution.

\medskip

On peut alors adopter une approche probabiliste de la recherche d'une solution. Comme dans le retour sur trace, on place les reines successivement sur chaque ligne, en vérifiant à chaque fois que la reine placée sur la ligne $k$ n'est pas en conflit avec les reines placées sur les lignes précédentes. Mais à la différence du retour sur trace, on n'explore pas systématiquement toutes les possibilités pour placer une reine sur la ligne $k$. On en choisit une seule, au hasard ! Bien entendu, il est tout à fait possible que cela mène à une impasse, c'est-à-dire à une ligne sur laquelle il n'est plus possible de placer une seule reine. Dans ce cas, on recommence depuis le début.

\medskip

Le programme ci-dessous met en oeuvre cet algorithme. La fonction \codeCaml{solve_prob} reçoit en paramètre une solution partielle pour les $k$ premières lignes, dans \codeCaml{sol[0..k[}. Elle choisit alors au hasard une colonne \codeCaml{c} pour la reine de la ligne $k$. Le code utilise la fonction \codeCaml{check} qui vérifie si le choix pour la ligne $k$ est compatible avec les lignes précédentes. S'il n'y a aucun choix possible, il renvoie \codeCaml{false}. Sinon, il continue avec la ligne suivante. La fonction \codeCaml{solve_prob} renvoie \codeCaml{true} si le tableau \codeCaml{sol} a pu être complété avec une solution. Si en revanche elle renvoie \codeCaml{false}, cela veut dire qu'on a échoué à compléter \codeCaml{sol} en une solution, mais cela ne veut pas dire que cela n'était pas possible. C'est là la différence avec le retour sur trace.

\medskip

Le lecteur attentif aura noté comment la fonction \codeCaml{solve_prob} choisit une colonne au hasard parmi les colonnes pour lesquelles \codeCaml{check} renvoie vrai.

\medskip

\begin{listingsbox}{myc}{C}
	bool check(int n, int sol[], int k) {
		for (int i = 0 ; i < k ; i++) {
			if (sol[i] == sol[k] || abs(sol[i] - sol[k]) == abs(i - k) ) {
				return false;
			}
		}
		return true;
	}
	
	bool solve_prob(int n, int sol[], int k) {
		if (k == n) {return true; }
		int c = 0; // colonne candidate
		int t = 0; // nombre de candidats
		for (int v = 0; v < n; v++) {
			sol[k] = v;
			if (check(n, sol, k)) {
				t++;
				if (rand () % t == 0) {
					c = v;
				}
			}
		}
		if (t == 0) {return false; }
		sol[k] = c;
		return solve_prob(n, sol, k + 1);
	}
	
	void queens_prob(int n, int sol[]) {
		while (true) {
			if (solve_prob(n, sol, 0)) {
				return;
			}
		}
	}
\end{listingsbox}

\medskip

Cette version probabiliste des $n$ reines est étonnamment efficace. Pour $n = 100$, elle trouve une solution en un quart de seconde après seulement $344$ descentes. Il est difficile d'expliquer précisément ce résultat, la structure du problème des $n$ reines étant encore mal connue. Mais il est frappant de voir à quel point cette approche probabiliste est efficace.

\section{Test de primalité}

\index{primalité}
Dans cette section, nous étudierons le test de primalité de Fermat. Il s'agit d'un algorithme probabiliste de type Monte Carlo. S'il affirme qu'un nombre est composé, alors il l'est effectivement. Mais s'il affirme qu'un nombre est premier, alors il se peut que ce ne soit pas le cas. Cependant, la probabilité d'un faux positif est inférieure à $\frac{1}{2}$. Dès lors, il suffit de répéter le test $k$ fois pour diminuer la probabilité d'erreur à moins de $\frac{1}{2^k}$.

\medskip

Le test de primalité de Fermat repose sur le petit théorème de Fermat, qui indique que si un entier $n$ est premier alors, pour tout entier naturel $a \in \brak{1, n-1}$, on a : 
\[a^{n-1} \equiv 1 \pmod n.\]
L'algorithme consiste à choisir un entier $a$ au hasard dans l'intervalle $[2, n-1]$ puis à calculer $a^{n-1} \pmod n$. Si le résultat n'est pas égal à $1$, on est certain que le nombre $n$ est composé. Dans le cas contraire, le nombre est possiblement premier. On peut répéter le test un certain nombre de fois.

\medskip

La fonction \codeCc{pow_mod} ci-dessous réalise une exponentiation rapide $x^n$ modulo $m$ dans les entiers $64$ bits de C.

\medskip

\begin{listingsbox}{myc}{C}
	uint64_t pow_mod(uint64_t x, uint64_t n, uint64_t m) {
		uint64_t y = 1;
		while (n > 0) {
			if (n % 2 == 1) {
				y = (y * x) % m;
			}
			x = (x * x) % m;
			n = n / 2;
		}
		return y;
	}
\end{listingsbox}

\medskip

Le programme suivant réalise cette idée. Les premières lignes évacuent des cas triviaux. Ensuite, une boucle \codeCc{for} répète le test $k$ fois pour une valeur $k$ fournie par l'utilisateur. Si tous les tests passent avec succès, on renvoie \codeCc{true} en présupposant que $n$ est premier.

\medskip

\begin{listingsbox}{myc}{C}
	bool premier(uint64_t n, int k) {
		if (n <= 1) {return false;}
		if (n == 2 || n == 3) {return true;}
		if (n % 2 == 0) {return false;}
		for (int i = 1; i <= k ; i++) {
			// a dans [2, n-1]
			int a = 2 + (rand() % (n-2));
			if (pow_mod(a, n-1, n) != 1) {
				return false; // n est composé
			}
		}
		return true; // n est probablement premier
	}
\end{listingsbox}

\medskip

Il est bien sûr possible que $n$ soit composé mais passe le test pour certaines valeurs de $a$, c'est-à-dire que $a^n \equiv 1 \pmod n$ alors que $n$ est composé. Par exemple, $341 = 11 \times 31$ n'est pas premier mais $2^{340} \equiv 1 \pmod{341}$. Mais on peut espérer que pour la plupart des valeurs de $a$, le test pour un nombre composé va rater. On verra en effet que, sauf cas particulier, c'est le cas pour environ la moitié des choix de $a$. C'est ceci qui nous motive à choisir $a$ de manière aléatoire.

\medskip

Il existe cependant des nombres composés particuliers, appelés nombre de Carmichael, qui passent le test pour tous les choix de $a$. Bien entendu, notre algorithme se trompera systématiquement pour ces nombres (avec probabilité $1$). Cependant, ces nombres deviennent rapidement extrêmement rares. De plus, il existe des généralisations de la méthode présentée ici, comme le test de Miller-Rabin, qui savent gérer correctement ces cas pathologiques. Pour simplifier, on va désormais supposer que ces nombres n'existent pas et se placer dans un monde imaginaire sans nombres de Carmichael.

\medskip

Montrons que la probabilité qu'un nombre composé $n$ passe le test de Fermat avec succès est inférieure à $\frac{1}{2}$. Considérons un nombre composé $n > 3$ et considérons le sous-groupe multiplicatif $(\znz)^\times = \{a \in \brak{0, n-1} \mid a \wedge n = 1\}$ de $(\znz)$ formé de ses éléments inversibles, c'est-à-dire des entiers premiers avec $n$. On a $\abs{(\znz)^\times} < n-1$ puisque $n$ est composé. Considérons maintenant l'ensemble $A = \{a \in (\znz)^\times \mid a^{n-1} \equiv 1 \pmod n\}$ le sous ensemble de $(\znz)^\times$ des éléments qui passent indûment le test. Il n'est pas difficile de vérifier que $A$ est un sous-groupe de $(\znz)^\times$, et donc, par le théorème de Lagrange, que son ordre $\abs{A}$ divise celui de $(\znz)^\times$. Ainsi, soit $\abs{A} = \abs{(\znz)^\times}$. Soit $A \leq \frac{\abs{(\znz)^\times}}{2}$.

\medskip

Comme nous avons mis de côté les nombres de Carmichael, si $n$ est composé, il existe au moins un nombre $a \in \brak{2, n-1}$ qui ne passe pas le test et ainsi $\abs{A} < \frac{n-1}{2}$. Dit autrement, moins de la moitié des entiers $a$ dans $[2, n-1]$ passent le test avec succès.

\medskip

Comme on répète $k$ fois le test, on en déduit que la probabilité que la fonction  \codeCaml{premier} déclare incorrectement un nombre composé comme étant premier est inférieure à $\frac{1}{2^k}$.

\medskip

Un autre test de primalité probabiliste, du même genre mais un peu plus sophistiqué, est celui de Miller-Rabin.

\medskip

Il est important de comprendre que les entiers $64$ bits ne nous permettent pas d'aller très loin. En effet, il faut pouvoir calculer un produit sans débordement arithmétique, ce qui nous limite de fait à $n < 2^{32}$. Or, tester la primalité d'un entier $32$ bits de façon exacte se fait facilement. Pour que le test de Fermat ou de Miller-Rabin devienne intéressant, il faut de grands entiers, comme par exemple ceux de la bibliothèque GMP. Cette bibliothèque logicielle de calcul multiprécision sur les nombres entiers, rationnels et en virgule flottante offre justement un test de primalité probabiliste, qui utilise en particulier l'algorithme de Miller-Rabin.