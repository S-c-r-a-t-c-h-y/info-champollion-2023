\chaptr{Introduction à la théorie de la complexité}{1}

\section{Problèmes de décision et d'optimisation}

\subsection{Problèmes de décision}

\begin{defi}
	On appelle problème de décision la donnée d'une instance et d'une question dichotomique :
	\begin{itemize}
		\item \textit{INSTANCE :} Un objet possédant certaines caractéristiques.
		\item \textit{QUESTION :} Une question sur cet objet dont la réponse est oui ou non.
	\end{itemize}
\end{defi}

Formellement, un problème correspond à une fonction totale $f : \Sigma^* \to \mathbb{B}$ avec $u \in \Sigma^*$ l'encodage d'une instance du problème et $f(u)$ la réponse pour cette instance.

\medskip

Cela correspond au langage $\{u \in \Sigma^* \mid f(u)\}$ des instances positives. Un problème de décision est donc un langage $L \subseteq \Sigma^*$.

\medskip

\textbf{Remarque :} si $f(u) = F$, alors soit $u$ n'est pas une instance positive, soit $u$ ne représente âs une instance. En général, déterminer si un mot $u \in \Sigma^*$ est l'encodage d'une instance d'un problème est facile (c'est d'ailleurs un problème de décision que l'on peut résoudre au préalable).

\begin{exemple}[Exercice 1, TD 16]
	PREMIER $= \{10, 11, 101, ...\}$ (sans $0$ inutile). Le nombre d'instance pour PREMIER est infini, et la taille de l'entrée est $\ceil{\log_2(n)}$.
	
	\medskip
	
	TRIÉE : taille de l'instance : $n \ceil{\log_2(k)}$ avec $n$ la taille de la liste et $k$ le maximum des valeurs des éléments.
	
	\medskip
	
	CONNEXE : $O(\abs{S} + \abs{A}\log(\abs{S})$.
\end{exemple}

Un algorithme $A : \Sigma^* \to \mathbb{B}$ résout le problème $L \subseteq \Sigma^*$ \ssi $$L = \{u \in \Sigma^* \mid A(u)\}.$$

\subsection{Problème de recherche}

\index{problème!de recherche}
\begin{defi}
	On appelle problème de recherche la donnée d'une instance et la description des solutions possibles pour cette instance : 
	\begin{itemize}
		\item \textit{INSTANCE :} un objet possédant certaines caractéristiques.
		\item \textit{SOLUTION :} la structure d'une solution possible.
	\end{itemize}
\end{defi}

Il s'agit de trouver explicitement une solution pour l'instance. Formellement, il s'agit d'une relation binaire sur $\Sigma^*$ : $\mathcal{R} \subseteq \Sigma^* \times \Sigma^*$. L'ensemble des solutions associées à une instance $u \in \Sigma^*$ est l'image de $u$ par $\mathcal{R}$ : $$\{v \in \Sigma^* \mid u \mathcal{R} v\}.$$ Un algorithme $A$ résout un problème de recherche $\mathcal{R}$ si pour tout mot d'entrée $u \in \Sigma^*$, $A(u)$ calcule un mot $v$ tel que $u \mathcal{R} v$ (\cad trouve une solution $v$) s'il en existe une et indique qu'il n'en existe pas sinon.

\medskip

\textbf{Remarque :} Ceci convient qu'il y ait une seule solution possible ou plusieurs.

\medskip

A un problème de recherche, on peut associer plusieurs problèmes de décision :

\begin{itemize}
	\item Problème de l'existence d'une solution : Soit $\mathcal{R} \subseteq \Sigma^* \times \Sigma^*$ un problème de recherche, le problème de décision existentiel associé est le problème définit par $f_\exists : \Sigma^* \to \mathbb{B}$ telle que $\forall u \in \Sigma^*, f_\exists(u) = V \iff \exists v \in \Sigma^*, u \mathcal{R} v$.
	\item Problème de vérification d'une solution : Soit $\mathcal{R} \subseteq \Sigma^* \times \Sigma^*$ un problème de recherche. Le problème de vérification associé est $f_{\checked} : \Sigma^* \times \Sigma^* \to \mathbb{B}$ telle que $\forall (u, v) \in \Sigma^* \times \Sigma^*$, $f_{\checked}(u, v) = V \iff u \mathcal{R} v$ (est ce que $v$ est bien solution pour $u$).
\end{itemize}

\textbf{Remarque :} pour avoir un vrai problème de décision, il faut se donner une bijection calculable entre $\Sigma^* \times \Sigma^*$ et $\Sigma^*$.

\subsection{Problème d'optimisation}

\index{problème!d'optimisation}
\begin{defi}
	Dans un problème de recherche, lorsqu'il y a plusieurs solutions, on peut s'intéresser à choisir "la meilleure" pour un certain critère. Un problème d'optimisation est un problème de recherche auquel on ajoute une fonction de coût que l'on cherche à minimiser.
	\begin{itemize}
		\item \textit{INSTANCE :} un objet avec certaines caractéristiques.
		\item \textit{SOLUTION :} la structure des solutions possibles pour cette instance.
		\item \textit{OPTIMISATION :} une fonction qui à une instance $x$ et une solution $y$ associe un coût $c(x, y)$que l'on cherche à minimiser.
	\end{itemize}
\end{defi} 

Formellement, un problème d'optimisation est un problème de recherche $\mathcal{R} \subseteq \Sigma^* \times \Sigma^*$ et une fonction de cout $c : \Sigma^* \times \Sigma^* \to \mathbf{R}$. On cherche pour $u \in \Sigma^*$ fixe un $v \in \{w \in \Sigma^* \mid u \mathcal{R} w\}$ qui minimise $c(u, v)$. Un algorithme $A : \Sigma^* \to \Sigma^*$ résout ce problème d'optimisation si pour tout $u \in \Sigma^*$, $A(u) = \argmin c(u, v)$, avec $v \in \{w \in \Sigma^* \mid u \mathcal{R} w\}$ si un minimum existe, et signale que non sinon.

\medskip

\remarklist{
	\item On peut encoder $\mathcal{R}$ dans $c$ par exemple en posant $c(u,v) = +\infty$ si $(u, v) \not\in \RR$.
	\item On formule ici comme un problème de minimisation, mais on peut aussi maximiser.
}

\index{problème!de seuil}
On introduit le \textbf{problème de seuil.} Considérons un problème d'optimisation $\mathcal{R} \subseteq \Sigma^* \times \Sigma^*$ et $c : \Sigma^* \times \Sigma^* \to \RR$. Le problème de décision associé à ce problème d'optimisation est le problème de décision :
\begin{itemize}
	\item \textit{INSTANCE :} une instance $u \in \Sigma^*$ du problème et un seuil $s \in \RR$.
	\item \textit{QUESTION :} existe-t-il une solution $v \in \Sigma^*$ au problème de recherche $\mathcal{R}$ pour $u$ telle que $c(u, v) \leq s$.
\end{itemize}

\index{{\scshape Problème}!SAC-A-DOS}
\begin{exemple}[Exercice 3, TD 16]
	PGCD : \begin{itemize}
		\item \textit{INSTANCE :} $a, b \in \NN$ et $k \in \NN$ (seuil).
		\item \textit{QUESTION :} existe-t-il $d \in \NN$ avec $d \mid a \wedge d \mid b \wedge d \geq k$.
	\end{itemize}
	SAC A DOS : \begin{itemize}
		\item \textit{TAILLE DES ENTREES :} $n(\log P' + \log V')$ avec $P' = \underset{\max}{i \in \brak{1, n}} (P, p_i)$ et $V' = \underset{\max}{i \in \brak{1, n}}(v_i)$. 
	\end{itemize}
\end{exemple}

\begin{exemple}[Exercice 4, TD 16]
	Trouver le minimum d'un tableau d'entiers : c'est un problème d'optimisation ou de recherche.
	
	\medskip
	
	Trier un tableau en place : ce n'est pas un problème formalisé dans le cours car on doit interagir sur l'instance.
	
	\medskip
	
	Le problème SAT est un problème de décision (associé au problème de valuation / l'existence).
\end{exemple}

\section{La classe \cNP}

\textbf{Attention :} La classe \cNP ne s'applique qu'aux problèmes de décisions. (Il n'est pas impossible que le prof demande si un certain problème est \cNP : il faut donc penser à vérifier que c'est un problème de décision).

\medskip

Pour un problème d'optimisation, il faut considérer le problème de décision associé.

\begin{defi}
	On dit $\cNP$ pour Non-deterministic polynomial. Intuitivement, la classe \cNP correspond aux problèmes que l'on peut résoudre en temps polynomial si on sait deviner une solution. Il suffit juste de vérifier que notre candidat est bien solution. On devine et on vérifie.
\end{defi}

\textbf{Remarque :} On appelle cela un oracle.

\medskip

De manière générale, il s'agit de vérifier (en temps polynomial) qu'une instance est positive étant donnée une preuve sous la forme d'un certificat (en temps polynomial). Dans 99\% du temps, le problème de décision est un problème associé à un problème de recherche ou d'optimisation et le certificat est la solution candidate.

\medskip

\textbf{Mais attention :} cette solution doit être de taille polynomiale en les entrée et vérifier que c'est bien une solution doit se faire en temps polynomial. En DS, il faut bien penser à vérifier cette condition !!

\begin{defi}
	Un problème de décision $L \subseteq \Sigma^*$ est dans la classe \cNP \ssi il existe un algorithme $\mathcal{V} : \Sigma^* \times \Sigma^* \to \mathbb{B}$ appelé vérificateur, de complexité polynomiale en la taille de ses entrées, tel que :
	\begin{center}
		$u \in L$ \ssi $\exists v \in \Sigma^*$ de taille polynomiale en $u$ tel que $\mathcal{V}(u, v) = V$.
	\end{center}
	Un $v \in \Sigma^*$ de taille polynomial en $u$ est appelé un certificat en $u$.
\end{defi}

\textbf{Remarque (à oublier) :} Certains auteurs (profs) supposent que $(u, v) \to \mathcal{V}(u, v)$ est polynomiale en $\abs{u}$. Dans ce cas, le certificat est nécessairement polynomial.

\begin{exemple}
	Le problème SAT est \cNP. Un certificat pour une instance $\phi \in F$ est une solution $\nu : \mathbb{V}_{\phi} \to \mathbb{B}$. La taille de $\nu$ est linéaire en le nombre de variables de $\phi$ donc polynomiale en $\abs{\phi}$ et on sait vérifier que $\nu \vDash \phi$ en $O(\abs{\phi})$ polynomiale en $\phi$.
\end{exemple}

\textbf{Remarque :} Le vérificateur est la fonction d'évaluation $(\nu, \phi) \to \hat{\nu}(\phi)$.

\begin{exemple}
	Pour les échecs généralisés, on voit mal un certificat polynomial. Une stratégie gagnante va être de taille vraiment grande.
\end{exemple}

\begin{prop}
	On a $\cP \subseteq \cNP$.
\end{prop}

\begin{proof}
	
	Soit $A : \Sigma^* \to \mathbb{B}$ qui résout un problème $L \in \Sigma^*$. Construisons $\mathcal{V} : \Sigma^* \times \Sigma^* \to \mathbb{B}$ polynomial tel que $\forall v \in \Sigma^*, A(u) \iff \exists v \in \Sigma^*$ polynomial en $u$ tel que $\mathcal{V}(u, v) = V$. $\mathcal{V}$ procède ainsi pour $(u, v)$ : 
	\begin{itemize}
		\item jette $v$
		\item renvoie $A(u)$.
	\end{itemize}
	
	Pour $u \in \Sigma^*$, si $A(u) = V$, alors $\mathcal{V}(u, \epsilon) = V$. Inversement, s'il existe $v \in \Sigma^*$ (polynomial) tel que $\mathcal{V}(u, v) = V$, alors $A(u) = V$. Donc, $\mathcal{V}$ vérifie bien $L$ avec par exemple le certificat $\epsilon$.
\end{proof}

\begin{prop}
	On a $\cNP \subseteq \cEXP$.
\end{prop}

\textbf{Remarque :} Dans la définition de problème \cNP, il faut bien sûr que tous les certificats soient en temps polynomial en la taille de l'instance pour le même polynôme. Si $L \in \cNP$, il existe un vérificateur $\mathcal{V} : \Sigma^* \times \Sigma^* \to \mathbb{B}$ polynomial en la taille de ses entrées, \cad qu'il existe $f \in \KK[X]$ tel que pour tout $u, v \in \Sigma^*$, $\mathcal{V}(u, v)$ est en temps $O(f(\max(\abs{u}, \abs{v})))$ et un polynôme $g \in \KK[X]$ tel que $u \in L \iff \exists c \in \Sigma^*$ avec $\abs{c} \leq g(\abs{u})$ et $\mathcal{V}(u, c) = V$.

\begin{proof}
	Si $L \in \cNP$, il existe un vérificateur $\mathcal{V}$ de complexité polynomiale pour un polynôme $f \in \KK[X]$ et un polynôme $g \in \KK[X]$ tel que pour tout $u \in \Sigma^*, u \in L \iff \exists c \in \Sigma^*, \abs{c} \leq g(\abs{u})$ et $\mathcal{V}(u, c) = V$.
	
	\medskip
	
	Construisons un algorithme $A$ de complexité au pire exponentielle qui détermine si $u \in L$.
	
	\begin{algorithm}
		\begin{algorithmic}
			\Ensure Énumère tous les mots $v \in \Sigma^*$ avec $\abs{v} \leq g(\abs{u})$ et pour chacun, on teste $\mathcal{V}(u, v)$.
			\If{on trouve $v \in \Sigma^*$ avec $\abs{v} \leq g(\abs{u})$ tel que $\mathcal{V}(u, v) = V$}
			\State On renvoie correctement vrai.
			\Else \State $\forall v \in \Sigma^*$ avec $\abs{v} \leq g(\abs{u})$, $\mathcal{V}(u, v) = F$.
			\State On renvoie correctement faux.
			\EndIf
		\end{algorithmic}
	\end{algorithm}
\end{proof}

Une grande question de l'informatique est : $\cP \overset{?}{=} \cNP$.

\section{Réductions et problèmes \cNP-complets}

\subsection{Réductions polynomiales}

La définition suivante permet de montrer qu'un problème est "plus facile qu'un autre en terme de complexité.

\index{réduction}
\begin{defi}
	Soit $A \subseteq \Sigma^*$ et $B \subseteq \Sigma^*$ deux problèmes de décision. On dit que $A$ se réduit en temps polynomial à $B$ ce que l'on note $A \red B$, s'il existe une fonction $f : \Sigma^* \to \Sigma^*$, qui peut se calculer en temps polynomial, telle que pour tout $u \in \Sigma^*, u \in A \iff f(u) \in B$.
	
	\medskip
	
	Autrement dit, $u$ est une instance positive pour $A$ \ssi $f(u)$ est une instance positive.
\end{defi}

\textbf{Remarque :} Puisque $f$ est de complexité polynomiale en $\abs{u}$, $\abs{f(u)}$ est polynomial en $\abs{u}$. Intuitivement, $A$ se réduit à $B$ veut dire que si on sait résoudre $B$, il ne faut pas qu'un temps polynomial supplémentaire pour résoudre $A$.

\begin{prop}
	Soit $A \subseteq \Sigma^*$ deux problèmes de décision tels que $A \red B$. Alors ;
	\begin{itemize}
		\item Si $B \in \cP$, alors $A \in \cP$.
		\item Si $A \in \cNP$, alors $B \in \cNP$.
	\end{itemize}
\end{prop}

\begin{proof}
	Le deuxième point est à montrer pour demain.
\end{proof}

\begin{exemple}
	Montrer que pour $m \leq n$, $m-$Sat $\red$ $n$-SAT.
\end{exemple}

\index{{\scshape Problème}!MAX-CLIQUE}
\index{{\scshape Problème}!CLIQUE}
\index{{\scshape Problème}!MAX-STABLE}
\index{{\scshape Problème}!STABLE}
\begin{exemple}
	MAX-CLIQUE :
	\begin{itemize}
		\item \textit{Entrée :} Un graphe $G = (S, A)$
		\item \textit{Solutions :} Sous-graphes complets de $G$
		\item \textit{Optimisation :} Nombre de sommets du sous-graphe (maximiser)
	\end{itemize}
	
	CLIQUE :
	\begin{itemize}
		\item \textit{Entrée :} Un graphe $G = (S, A)$
		\item \textit{Question :} Existe-t-il un sous-graphe complet de $G$ avec au moins $K$ sommets ?
	\end{itemize}
	
	MAX-STABLE :
	\begin{itemize}
		\item \textit{Entrée :} Un graphe $G = (S, A)$
		\item \textit{Solutions :} Un ensemble de sommets indépendants
		\item \textit{Optimisation :} Taille de l'ensemble (maximiser)
	\end{itemize}
	
	STABLE :
	\begin{itemize}
		\item \textit{Entrée :} Un graphe $G = (S, A)$, un seuil $k \in \NN$
		\item \textit{Question :} Existe-t-il un ensemble indépendant de $G$ avec au moins $K$ sommets.
	\end{itemize}
\end{exemple}


Montrons que STABLE $\red$ CLIQUE.

\medskip


\textbf{Remarque :} Si $X \subseteq S$ est un stable de $G = (S, A)$, alors $X$ est une clique de $\overline{G} = (S, \overline{A})$. 

\medskip

Calculer $\overline{G} = (S, \overline{A})$ se fait en temps polynomial à partir de $G = (S, A)$ et les stables de $G$ sont les cliques de $\overline{G}$ donc il existe un stable de $G$ de taille au moins $k$ \ssi il existe une clique de $G$ de taille au moins $k$.

\begin{exemple}
	Montrer que CLIQUE $\red$ STABLE.
\end{exemple}

\subsection{\cNP-complétude}

\index{classe!\cNP!difficile}
\index{classe!\cNP!complet}
\begin{defi}
	Soit $A \subseteq \Sigma^*$ un problème de décision, on dit que :
	\begin{itemize}
		\item $A$ est \cNP-difficile (\cNP-hard en anglais) si pour tout $B \in \cNP$, $B \red A$.
		\item $A$ est \cNP-complet si $A$ est $\cNP$ et \cNP-difficile.
	\end{itemize}
\end{defi}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\clip  (-3.8,4.4) rectangle (3.6,-2.8);
		\draw  (0,0) ellipse (2.6 and 2.6);
		\draw  (0,5.8) ellipse (3.4 and 5);
		\draw  (0,-1.4) ellipse (1.4 and 1);
		\node [text=red, outer sep=0,inner sep=0,minimum size=0] at (0,3.4) {NP-difficile};
		\node [text=orange, outer sep=0,inner sep=0,minimum size=0] at (0,1.6) {NP-complet};
		\node [text=yellow!20!black, outer sep=0,inner sep=0,minimum size=0] at (0,0) {NP};
		\node [text=green, outer sep=0,inner sep=0,minimum size=0] at (0,-1.4) {P};
	\end{tikzpicture}
	\caption{Classes de complexité, en supposant $\cP$ différent de $\cNP$.}
\end{figure}

Un problème est \cNP-difficile s'il est plus difficile que tous les problèmes \cNP.

\medskip

\textbf{Remarque :} On peut étendre la notion de \cNP-difficile aux problèmes d'optimisation

\begin{prop}
	Si $A$ est \cNP-complet et si $A \in P$, alors $P = NP$.
\end{prop}

\begin{proof}
	On sait que $\cP \subseteq \cNP$. Si $B \in \cNP$, alors comme $A$ est \cNP-difficile, on a $B \red A$. De plus, comme $A \in \cP$, on a $B \in \cP$ et donc $\cNP \subseteq \cP$.
\end{proof}

\begin{prop}
	SI $A$ est \cNP-difficile et si $A \red B$, alors $B$ est \cNP-difficile.
\end{prop}

\begin{proof}
	Soit $C \in \cNP$, donc $C \red A$ et par transitivité, $C \red B$.
\end{proof}

Pour montrer qu'un problème $A$ est \cNP-complet : 
\begin{itemize}
	\item On montre que $A \in \cNP$.
	\item On trouve un problème \cNP-difficile (en fait \cNP-complet) tel que $B \red A$.
\end{itemize}

\textbf{Attention au sens :} On réduit un problème \cNP-complet connu à ce problème. Si $B$ est connu pour être \cNP-complet, et que $B \red A$, alors $A$ est \cNP-difficile.

\begin{exemple}
	En admettant que $3$-SAT est $\cNP$-complet, on a montré que $3$-COLOR est \cNP difficile car $3$-SAT se réduit en temps polynomial à $3$-COLOR. Comme $3$-COLOR est \cNP, $3$-COLOR est \cNP-complet.
\end{exemple}

\textbf{Problème :} Il nous faut un premier problème \cNP-complet

\index{{\scshape Problème}!SAT}
\index{{\scshape Théorème}!de Cook-Levin}
\begin{prop}[Cook-Levin, 1971]
	SAT est \cNP-complet.
\end{prop}

\begin{proof}
	Admise.
\end{proof}

\textbf{Rappel :} un problème est $\cNP$-difficile s'il est plus difficile que tous les problèmes $\cNP$, \cad que tout problème $\cNP$ s'y réduit en temps polynomial.

\begin{prop}
	Si $A \in \cNP$-complet $\cap \cP$, alors $\cP = \cNP$.
\end{prop}

Pour montrer qu'un problème est $\cNP$-complet :
\begin{itemize}
	\item on montre que $A$ est $\cNP$. On explicite un vérificateur $V$ polynomial pour lequel il existe un certificat polynomial exactement pour les instances positives.
	\item On montre $A$ est $\cNP$-difficile. Pour cela, on propose un problème $\cNP$-complet connu et on montre $B \red A$.
\end{itemize}

On trouve $f : \Sigma^* \to \Sigma^*$ polynomial telle que pour tout $u \in \Sigma^*$, $u \in B$ \ssi $f(u) \in A$. 

\medskip

\textbf{Remarque :} Ce n'est pas symétrique. A une instance quelconque de $B$, on associe une instance spécifique, choisie, de $A$.

\index{{\scshape Problème}!CNF-SAT}
\begin{prop}
	CNF-SAT est $\cNP$-complet, où CNF désigne la forme normale conjonctive.
\end{prop}

\begin{proof}
	CNF-SAT $\in \cNP$, toujours avec comme certificat une valuation et comme vérificateur la fonction d'évaluation (polynomiale). 
	
	\medskip
	
	Montrons que SAT $\red$ CNF-SAT. A une instance $\phi \in F$ de SAT et on lui associe sa FNC avec l'algorithme du TP :
	\begin{itemize}
		\item Éliminer les $x \implique y \equiv \non x \ou y$.
		\item Faire descendre les $\non$.
		\item Faire descendre les $\ou$.
	\end{itemize}
	On obtient une formule $\overline{\phi}$ en FNC équivalente à $\phi$, donc $\phi \in $ SAT $\iff \overline{\phi} \in$ CNF-SAT.
	
	\textbf{Mais... la transformation $\phi \to \overline{\phi}$ est exponentielle dans le pire cas.} Il existe des formules $\phi \in F$ telles que toute FNC équivalente à $\phi$ est de taille exponentielle en $\phi$.
	
	\medskip
	
	On introduit donc une nouvelle idée : \begin{itemize}
		\item à une formule $\phi$, on associe la formule $T(\phi)$ la transformée de Tsechin de $\phi$.
	\end{itemize}
	
	D'après le DM, cette transformation est polynomiale en $\abs{\phi}$ et produit une formule $T(\phi)$ de taille polynomiale.
\end{proof}

\begin{prop}	
	La réduction est correcte car si $\phi$ satisfiable $T(\phi)$ satisfiable.
\end{prop}

\textbf{Remarque :} Pour $n \geq 0$, $n$-SAT est le problème de savoir si une formule $\phi$ en FNC avec des classes de taille au plus $n$ est satisfaite.

\begin{prop}
	Pour $n \geq 3$, $n$-SAT est $\cNP$-complet.
\end{prop}

\begin{proof}
	Si $n \geq 3$, on a $3$-SAT $\red$ $n$-SAT (cas particulier). Il suffit de montrer que $3$-SAT est $\cNP$-difficile.
	
	\medskip 
	
	A noter que pour tout $n \in \NN$, $n$-SAT est dans $\cNP$.
	
	\medskip
	
	Montrons que $3$-SAT est $\cNP$-difficile : CNF-SAT $\red$ $3$-SAT. Si $\phi$ est une formule en FNC. Soit $c$ une classe de $\phi$ de taille strictement plus que $3$ (sinon on la garde).
	
	\medskip
	
	$c = (l_1 \ou l_2 \ou l_3 \ou l_4 \ou \dots \ou l_k)$ avec $k \geq 3$. On introduit des variables $y_2, \dots, y_{K-1}$ et on pose $\overline{c}$ la formule $c = (l_1 \ou l_2 \ou y_2)) \et (\overline{y_2} \ou l_3 \ou y_3) \et (\overline{y_3} \ou l_4 \ou y_4) \et \dots \et (\overline{y_{k-1}} \ou l_{k-1} \ou l_k)$. On fait alors pour chaque clause (avec des $y$ différents pour chaque clause) pour construire une formule $\overline{\phi}$. On obtient une formule de taille polynomiale et cette transformation est polynomiale.
\end{proof}

\textbf{Remarque :} Si une clause $c$ est satisfiable par une valuation $\nu$, on construit une valuation $\overline{\nu}$ qui prolonge $\nu$ de la manière suivante :
\begin{itemize}
	\item Comme $c$ est satisfiable, il existe $i \in \brak{1, k}$ tel que $\nu(l_i) = V$.
	\item On pose pour $j \in \brak{2, i-1}, \overline{\nu}(\mu_j) = V$ et pour $j \in \brak{i, k-1}, \overline{\nu}(\mu_j) = F$.
\end{itemize}
Toute clause de $\overline{c}$ est satisfaite par $\overline{\nu}$. On prolonge ainsi $\overline{\nu}$ sur toutes les clauses et on obtient un modèle $\overline{\phi}$. 

\medskip

Réciproquement, si $\overline{\nu}$ est un modèle de $\overline{\phi}$, on pose $\nu = \overline{\nu}_{\nu(\phi)}$ (la restriction de $\overline{\nu}$ aux variables de $\phi$). Montrons que $\nu$ est un modèle de $\phi$. Soit $c$ une clause de $\phi$. Si tous les littéraux de $\phi$ sont évalués à $F$ alors pour $\overline{\nu}$, on doit avoir $\overline{\nu}(\mu_2) = V$ puis $\overline{\nu}(\mu_3) = V$, etc... jusqu'à $\overline{\nu}(\mu_{k-1}) = V$ et la dernière clause n'est plus satisfaite. Donc $\nu$ est un modèle de $\phi$.

\begin{exemple}
	A titre d'exercice, on peut montrer que l'on peut de plus supposer que toutes les clauses sont de taille exactement $3$. L'idée réside dans le fait de remplacer chaque occurrence d'une variable $x$ par de nouvelles variables $x_1, \dots, x_{\abs{\phi}_x}$ et on force ces variables à avoir la même valuation.
\end{exemple}

\begin{exemple}
	On peut également montrer que l'on peut supposer que chaque variable apparaît au plus trois fois.
\end{exemple}

\textbf{Remarque :} pour $n \leq 2$, $n$-SAT est dans $\cP$.
\begin{proof}
	Pour $n = 0$, une formule $0$-SAT $\bigwedge_{i=1}^k \bot$ est satisfiable \ssi $k = 0$. $1$-SAT est satisfiable \ssi aucun littéral n'apparaît avec sa négation. Et $2$-SAT a été étudié dans le DM $5$.
\end{proof}