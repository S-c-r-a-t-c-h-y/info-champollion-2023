\chaptr{Langages formels}{1}

On va formaliser la notion de langage. Il faut être capable d'exprimer formellement les problèmes informatiques pour les résoudre dans un certain langage. 

\medskip

Un langage est un ensemble de mots avec une syntaxe, une sémantique et une grammaire.

\medskip

On a déjà étudié plusieurs exemples : la compilation, la bio-informatique, la recherche de motifs, etc...

\section{Mots}

\subsection{Alphabets et mots}

\index{alphabet}
\begin{defi}
	Un alphabet est un ensemble fini non-vide. Ses éléments sont appelés symboles, lettres ou caractères. On le note souvent $\Sigma$.
\end{defi}

\begin{exemple}
	$\Sigma = \{a\}, \Sigma = \{a, b\},  \Sigma = \{a, b, c, ..., z\}$\\
	$\Sigma$ l'ensemble des caractères du code ASCII.\\
	$\Sigma = \{A, G, T, C\}, \Sigma = \{\triangle, \square\}$ 
\end{exemple}

\index{mot}
\index{longueur!d'un mot}
\begin{defi}
	Un mot sur un alphabet $\Sigma$ est une suite finie de symboles. Le mot $u = (u_1, ..., u_n) \in \Sigma^n$ est noté $u = u_1...u_n$. Sa longueur que l'on note $\abs{u}$ est $n$. Le mot vide de longueur nulle est noté $\epsilon$ ou $\Lambda$.
\end{defi}

\begin{exemple}
	Les suites suivantes sont des mots : $\epsilon$, $aaaa$, $abbabaa$, $ceciestunmot$.\\
	Si $\Sigma = \{a, b, ..., z, \underbrace{\_}_\text{espace}\}$, alors $ceci \_ est \_ un \_ mot$. \\
	Finalement, $u_1...u_n = \epsilon$ \ssi $n = 0$.
\end{exemple}

\textbf{Notation : } L'ensemble des mots de longueur $k \in \NN$ est noté $\Sigma^k$. L'ensemble de tous les mots est noté $\Sigma^*$. L'ensemble des mots non-vides est noté $\Sigma^+$. 
On a $\Sigma^* = \bigcup_{k \in \NN} \Sigma^k$ et $\Sigma^+ = \Sigma^* \backslash \{ \epsilon \} = \bigcup_{k \in \NN^*} \Sigma^k$.

\remarklist{
	\item On peut considérer des mots infinis, on note $\Sigma^\omega$.
	\item $\Sigma^*$ est toujours de cardinal infini, même si $\abs{\Sigma} = 1$. Il est dénombrable.
	\item On énumère tous les mots par ordre de grandeur croissante, puis pour une même longueur, par ordre lexicographique.
	\item On identifie abusivement $\Sigma$, l'ensemble des caractères, avec $\Sigma^1$, les mots d'une lettre. 
}

\index{longueur!d'un mot en $a$}
\begin{defi}
	Pour $a \in \Sigma$ et $u \in \Sigma^*$, la longueur de $u$ en $a$ est le nombre $\abs{u}_a$ d'occurrences de la lettre $a$ dans le mot $u$. Pour $a = u_1...u_n$, $\abs{u}_a = \abs{\{i \in \brak{1, n} \mid u_i = a\}}$.
\end{defi}

\begin{exemple}
	$\abs{abbab}_a = 2$.
\end{exemple}

\subsection{Concaténation}

\index{concaténation}
\begin{defi}
	Soient $u, v \in \Sigma^*$ deux mots avec $u = u_1...u_n$ et $v = v_1...v_m$. La concaténation de $u$ et $v$, notée $u \cdot v$ (ou plus simplement $uv$) est le mot $u_1...u_nv_1...v_m$ formé des lettres de $u$ suivies des lettres de $v$. Il est de longueur $n + m$.
\end{defi}

\begin{exemple}
	$aba \cdot bab = ababab$.
\end{exemple}

\remarklist{
	\item Pour tout $a \in \Sigma$, pour tout $u, v \in \Sigma^*$, $\abs{u \cdot v} = \abs{u} + \abs{v}$ et $\abs{u \cdot v}_a = \abs{u}_a + \abs{v}_a$
	\item On trouve parfois $u + v$ pour $u \cdot v$.
	\item Pour tout $u, v \in \Sigma^*$, $\epsilon \cdot u = u \cdot \epsilon = u$.
	\item Pour tout $u, v, w \in \Sigma^*$, $(u \cdot v) \cdot w = u \cdot (v \cdot w)$.
}

\index{monoïde}
\begin{prop}
	$(\Sigma^*, \cdot, \epsilon)$ est un monoïde, \cad que la LCI est associative et possède un élément neutre.
\end{prop}

\textbf{Attention : } Si on écrit $u = \overbrace{\underbrace{u_1}_{\in \Sigma \text{?}}}^{\in \Sigma^* \text{?}}u_2...u_n$, il faut faire attention à l'ambiguïté : il faut préciser.

\remarklist{
	\item $\abs{\circ} : \Sigma^* \to \NN$ est un morphisme de monoïde.
	\item $\epsilon$ est le seul mot inversible pour $(\Sigma^*, \cdot, \epsilon)$ qui n'est donc pas un groupe. On peut cependant simplifier à droite et à gauche : si $u, v, w \in \Sigma^*$ avec $uv = uw$, alors, $v = w$ et si $vu = wu$ alors $v = w$.
	\item $(\Sigma^*, \cdot, \epsilon)$ est commutative \ssi $\abs{\Sigma} = 1$. 
	\item Pour $u, v \in \Sigma^*, uv = \epsilon$, alors $u = v = \epsilon$. Pour $u = \Sigma^*$ et $k \in \NN^*$, on pose $u^0 = \epsilon$ et $u^k = uu^{k-1} = u^{k-1}u$. Et pour tout $k, l \in \NN$, on a $u^{k+l} = u^ku^l$.
}

\textbf{Notation : } $aaaabbaaaaabbba$ se note $a^4b^2a^5b^3a$.

\section{Préfixes, suffixes et sous-mots}

\index{mot!préfixe d'un}
\index{mot!suffixe d'un}
\index{mot!facteur d'un}
\begin{defi}
	Si $u, v \in \Sigma^*$, on dit que :
	\begin{itemize}
		\item $u$ est préfixe de $v$ s'il existe $w \in \Sigma^*$ tel que $v = uw$.
		\item $u$ est suffixe de $v$ s'il existe $w \in \Sigma^*$ tel que $v = wu$.
		\item $u$ est facteur de $v$ s'il existe $w_1, w_2 \in \Sigma^*$ tel que $v = w_1uw_2$.
	\end{itemize}
	Le préfixe / suffixe / facteur $u$ est propre si $u \ne v$ et est non-trivial si de plus, $u \ne \epsilon$.
\end{defi}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\begin{scope}
			\draw (0,1) -- (2,1) -- (2,0) -- (0,0) -- cycle;
			\draw[dashed] (2.5,1) -- (4.5,1) -- (4.5,0) -- (2.5,0) -- cycle;
			\draw (0, -0.5) -- (4.5, -0.5) -- (4.5, -1.5) -- (0, -1.5) -- cycle;
			
			\node [outer sep=0,inner sep=0,minimum size=0] at (1,0.5) {u};
			\node [outer sep=0,inner sep=0,minimum size=0] at (2.25,-1) {v};
			\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,0.5) {w};
		\end{scope}
		
		\begin{scope}[xshift=200, yshift=0]
			\draw[dashed] (0,1) -- (2,1) -- (2,0) -- (0,0) -- cycle;
			\draw (2.5,1) -- (4.5,1) -- (4.5,0) -- (2.5,0) -- cycle;
			\draw (0, -0.5) -- (4.5, -0.5) -- (4.5, -1.5) -- (0, -1.5) -- cycle;
			
			\node [outer sep=0,inner sep=0,minimum size=0] at (1,0.5) {w};
			\node [outer sep=0,inner sep=0,minimum size=0] at (2.25,-1) {v};
			\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,0.5) {u};
		\end{scope}
		
		\begin{scope}[xshift=75, yshift=-100]
			\draw[dashed] (0,1) -- (2,1) -- (2,0) -- (0,0) -- cycle;
			\draw (2.5,1) -- (4.5,1) -- (4.5,0) -- (2.5,0) -- cycle;
			\draw[dashed] (5,1) -- (7,1) -- (7,0) -- (5,0) -- cycle;
			\draw (0, -0.5) -- (7, -0.5) -- (7, -1.5) -- (0, -1.5) -- cycle;
			
			\node [outer sep=0,inner sep=0,minimum size=0] at (1,0.5) {$w_1$};
			\node [outer sep=0,inner sep=0,minimum size=0] at (6,0.5) {$w_2$};
			\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,-1) {v};
			\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,0.5) {u};
		\end{scope}
	\end{tikzpicture}
	\caption{Représentation avec des blocs des préfixes, suffixes et facteurs.}
\end{figure}

\remarklist{
	\item $u$ est un préfixe, suffixe et facteur de lui-même.
	\item Un mot de longueur n possède exactement $n+1$ suffixes et préfixes et au plus $\binom{n}{2} + 1$ facteurs.
}

\begin{exemple}
	Les préfixes de $baobab$ sont $\{\epsilon, b, ba, bao, baob, baoba, baobab\}$.\\
	Les suffixes de $baobab$ sont $\{\epsilon, b, ab, bab, obab, aobab, baobab\}$. On détermine de même les facteurs.
\end{exemple}

\index{mot!sous-}
\begin{defi}
	On dit que $u = u_1...u_n$ avec pour tout $i \in \brak{1, n}, u_i \in \Sigma$ est un sous-mot de $v$ s'il existe des mots $w_0, w_1, ..., w_n \in \Sigma^*$ tel que $v = w_0u_1w_1u_2...w_{n-1}u_nw_n$. Intuitivement, les lettres de $u$ apparaissent  dans $v$ dans le bon ordre.
\end{defi}

\remarklist{
	\item On ne demande à un sous-mot que d'avoir les mêmes lettres dans le même ordre, tandis qu'un facteur doit être une suite consécutive de lettres du mot. Un facteur est un sous-mot. La réciproque est fausse.
	\item Certains sujets utilisent le terme sous-mot pour facteur.
}

\begin{exemple}
	Les sous-mots de $abcb$ sont $\{\epsilon, a, b, c, ab, ac, bc, bb, cb, abc, abb, acb, bcb, abcb\}$.
\end{exemple}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\draw (1.5,1) -- (2.5,1) -- (2.5, 0) -- (1.5, 0) -- cycle;
		\draw[dashed] (0,1) -- (1,1) -- (1, 0) -- (0, 0) -- cycle;
		\draw (4.5,1) -- (5.5,1) -- (5.5, 0) -- (4.5, 0) -- cycle;
		\draw[dashed] (3,1) -- (4,1) -- (4, 0) -- (3, 0) -- cycle;
		\draw (7.5,1) -- (8.5,1) -- (8.5, 0) -- (7.5, 0) -- cycle;
		\draw[dashed] (9,1) -- (10,1) -- (10, 0) -- (9, 0) -- cycle;
		\draw (10.5,1) -- (11.5,1) -- (11.5, 0) -- (10.5, 0) -- cycle;
		\draw[dashed] (12,1) -- (13,1) -- (13, 0) -- (12, 0) -- cycle;
		
		\draw (0,-0.5) -- (13,-0.5) -- (13, -1.5) -- (0, -1.5) -- cycle;
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (6.5,0.5) {...};
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (2,0.5) {$u_1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5,0.5) {$u_2$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (8,0.5) {$u_{n-1}$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (11,0.5) {$u_n$};
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (6.5,-1) {$v$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (0.5,0.5) {$w_0$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,0.5) {$w_1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (9.5,0.5) {$w_{n-1}$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (12.5,0.5) {$w_n$};
	\end{tikzpicture}
	\caption{Représentation des sous-mots avec des blocs.}
\end{figure}

\section{Ordre}

On suppose que $\Sigma$ est muni d'un ordre total noté $\leq$ qui est un ordre sur les symboles.

\subsection{Ordre lexicographique}

L'ordre lexicographique est l'ordre usuel sur les mots d'un dictionnaire. C'est aussi celui par défaut en Ocaml. 

\begin{exemple}
	On a donc "caml" < "python" qui est évalué à vrai.\\
	De plus, ("caml", 3) < ("caml", 1) est une expression évaluée à faux.
\end{exemple}

\index{ordre!lexicographique}
\begin{defi}
	On définit l'ordre lexicographique sur les mots, noté $\lex$ de la manière suivante : pour $u, v \in \Sigma^*$, on a $u \lex v$ si, soit $u$ est préfixe de $v$, soit il existe $a, b \in \Sigma$ avec $a < b$ et il existe $w, w', w'' \in \Sigma^*$ tel que $u = waw'$ et $v = wbw''$.
	
	\medskip
	
	En ce cas, le mot $w$ est le plus long préfixe commun à $u$ et à $v$, noté $w = u \wedge v$ et $a$ et $b$ sont les premières lettres qui diffèrent.
\end{defi}

\remarklist{
	\item Dans le premier cas, si $u$ est préfixe de $v$, alors $u \wedge v = u$.
	\item L'ordre lexicographique prolonge l'ordre $(\Sigma, \leq)$ en un ordre total $(\Sigma^*, \lex)$ (les deux ordres coïncident sur $\Sigma = \Sigma^*$).
	\item Si $\Sigma$ est de cardinal au moins deux, $(a^nb)_{n \in \NN}$ est une suite (infinie) strictement décroissante. $\{a^nb, n \in \NN\}$ est une partie non-vide qui n'admet pas de plus petit élément. En effet, $ab \gex aab \geq aaab \gex ...$. Cet ordre n'est pas bien fondé sur $\Sigma^*$.
}

\subsection{Ordre hiérarchique}

On compare d'abord par longueur, puis par ordre lexicographique.

\index{ordre!hiérarchique}
\begin{defi}
	On définit l'ordre hiérarchique pour $u, v \in \Sigma^*$ par $u \leq v$ \ssi soit $\abs{u} < \abs{v}$, soit $\abs{u} = \abs{v}$ et $u \lex v$. C'est un ordre total bien fondé. C'est un ordre également très utile pour énumérer les mots sur $\Sigma$.
\end{defi}

\section{Langages}

\index{langage}
\begin{defi}
	On appelle langage sur $\Sigma$ une partie de $\Sigma^*$, \cad un ensemble fini ou non de mots.
\end{defi}

\begin{exemple}
	On trouve plusieurs langages sur $\Sigma$ ; par exemple, $\emptyset, \Sigma^*, \Sigma, \Sigma^4, \{\epsilon\}$. Ce sont tous des langages différents.
	
	\medskip
	
	Si $\Sigma = \{a, b\}$, on a par exemple les langages \begin{eqnarray*}
		L_1 =& \{a, ab, aba, abab\} \\
		L_2 =& \{u \in \Sigma^* \mid \abs{u}_a = \abs{u}_b\}\\
		L_3 =& \{a^nb^n, n \in \NN\}\\
		L_4 =& \{(ab)^n, n \in \NN\}
	\end{eqnarray*}
	
	A noter que $L_3 \subsetneq L_2$ et $L_4 \subsetneq L_2$. Et $ba \in L_2 \backslash (L_3 \cup L_4)$.
	
	\medskip 
	
	Avec $I$ l'ensemble des caractères ASCII, un langage $L$ peut être constitué des programmes Ocaml systématiquement corrects, des programmes bien types, ou ceux qui terminent toujours.
\end{exemple}

\textbf{Opérations sur les langages : } Les langages étant des ensembles, on peut leur appliquer toutes les opérations ensemblistes : la réunion, l'intersection, la différence, la différence symétrique, le complémentaire, etc...

\textbf{Remarque : } On note souvent l'union par $+$, \cad $L_1 + L_2 \to L_1 \cup L_2$.

\index{concaténation!de langages}
\begin{defi}
	Soit $L, M \subseteq \Sigma^*$ deux langages. On définit la concaténation de $L$ et $M$, notée $L \cdot M$ (ou plus simplement $LM$) par $L \cdot M = \{u \cdot v \mid u \in L, v \in M\}$.
\end{defi}

\begin{exemple}
	$L = \{a, ab, aba\}$, alors $M = \{\epsilon, a\}$.\\
	$LM = \{a, ab, aba, a^2, aba^2\}$. Mais $aab \not\in LM$.
\end{exemple}

\remarklist{
	\item Cette opération n'est pas commutative.
	\item $(\mathcal{P}(\Sigma^*), +, \emptyset)$ est un monoïde commutatif.
	\item $(\mathcal{P}(\Sigma^*), \cdot, \{\epsilon\})$ est un monoïde non commutatif en général.
	\item $(\mathcal{P}(\Sigma^*), +, \cdot, \emptyset, \{\epsilon\})$ est un semi-anneau non-commutatif, \cad qu'il n'y a pas d'inverse pour la loi $+$.
	\item Comme pour les mots, $M \subseteq \Sigma^*$ et $k \in \NN$, on définit $M^0 = \{\epsilon\}$ et $M^{k+1} = M^k \cdot M = M \cdot M^k$.
}

Les mots de $L^n$ sont exactement les mots qui s'écrivent $u = u_1u_2...u_n$ avec pour tout $i \in \brak{1, n}, u_i \in L$.

\textbf{Attention : } $L^2 \ne \{u^2, u \in L\}$ mais $L^2 = \{uv, u \in L, v \in L\}$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\draw (0,1) -- (6,1) -- (6,0) -- (0,0) node (v1) {} -- cycle;
		\draw (1,1) -- (1,0);\draw (2,1) -- (2,0);\draw (3,1) -- (3,0) node (v2) {};\draw (5,1) -- (5,0);
		
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (0.5,0.5) {$u_1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.5,0.5) {$u_2$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (2.5,0.5) {$u_3$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5.5,0.5) {$u_n$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (4,0.5) {...};
	\end{tikzpicture}
	\caption[Un mot de $L^n$]{Un mot de $L^n$ : les $u_i$ sont tous des mots de $L$.}
\end{figure}

\index{étoile de Kleene}
\begin{defi}
	L'étoile de Kleene de $L$ est le langage $L^* = \bigcup_{n \in \NN} L^n$. Le langage $L^+ = \bigcup_{n \in \NN^*} L^n$ est appelé étoile de Kleene stricte.
\end{defi}

\begin{exemple}
	Avec $L = \{a, ab\}$, $L^* \neq \{a^n, n \in \NN\} \cup \{(ab)^n, n \in \NN\}$ mais $L^* = \{\epsilon, a, ab, a^3, a^2b, aba, a^3, a^3b, a^2ba, ...\}$.
\end{exemple}

\remarklist{
	\item $\underbrace{\Sigma^*}_\text{étoile de Kleene} = \bigcup_{n \in \NN} \Sigma^n = \underbrace{\Sigma^*}_\text{tous les mots}$.
	\item On a toujours $\epsilon \in L^*$ quelque soit $L$.
	\item $L^* = \bigcup_{n \in \NN} L^n = L^0 \cup \bigcup_{n \in \NN^*} L^n$. On a $L^* = L^+ \cup \{\epsilon\}$.
	\item $\{\epsilon\} \in L^+$ \ssi $\epsilon \in L$. On a toujours $L^+ \subseteq L^*$.
}

\textbf{Attention : } $\emptyset^* = \{\epsilon\}$.

Pour $L \subseteq \Sigma$ et $(M_i)_{i \in \NN} \in \mathcal{P}(\Sigma^*)^\NN$, on a :
\begin{itemize}
	\item $L \cdot \bigcup_{i \in I} M_i = \bigcup_{i \in I}(L \cdot M_i)$
	\item $\bigcup_{i \in I} M_i \cdot L = \bigcup_{i \in I} (M_i \cdot L)$
	\item $L \bigcap_{i \in I}M_i = \bigcap_{i \in I} (L \cdot M_i)$.
\end{itemize}

\begin{exemple}
	Si $L = \{a, a^2\}, M_1 = \{a\}, M_2 = \{a^2\}$, on a $LM_1 = \{a^2, a^3\}$ et $LM_2 = \{a^3, a^4\}$.
\end{exemple}

\textbf{Notation : } L'ensemble des mots dont $u$ est préfixe est $\{u\} \cdot \Sigma^*$ que l'on note $u\Sigma^*$. De même, l'ensemble des mots dont $u$ est suffixe est noté $\Sigma^*u$. Finalement, l'ensemble des mots dont $u$ est facteur est noté $\Sigma^*u\Sigma^*$.

\textbf{Attention : } $u\Sigma^*u$ ne désigne pas l'ensemble des mots qui débutent par $u$ et finissent par $u$. Ce langage est noté $u\Sigma^* \cap \Sigma^*u$. En effet, $u \in u\Sigma^* \cap \Sigma^*u$ mais $u \not\in u\Sigma^*u$.

\textbf{Remarque : } $u\Sigma^*u \subseteq u\Sigma^* \cap \Sigma^*u$. L'inclusion réciproque est fausse sauf si $u = \epsilon$.

\section{Lemme de Levi}

\index{{\scshape Lemme}!de Levi}
\begin{lem}
	Soit $x, y, z, t \in \Sigma^*$ tel que $xy=zt$. Alors, il existe un unique $u \in \Sigma^*$ tel que ($x=zu$ et $t=uy$) ou ($z=xu$ et $y=ut$)
\end{lem}

Deux mots $x, y \in \Sigma^*$ commutent si $xy = yx$. Deux mots $u, v \in \Sigma^*$ sont conjugués s'il existe $x, y \in \Sigma^*$ tels que $u = xy$ et $v = yx$ (on dit aussi qu'ils sont rotation l'un de l'autre).

\begin{proof}
	On commence par donner une preuve de l'unicité, puis une preuve accélérée de l'existence.
	 
	\medskip
	
	\textbf{Unicité : } Si $u$ et $u'$ sont deux solutions, alors ($x = zu$ et $x = zu'$) ou ($z=xu$ et $z=xu'$). En particulier, si $x=zu$ et $x = zu'$, alors $u = u'$ après simplification par $z$ à gauche. De même, on montre le second cas.
	
	\textbf{Existence : } Si $\abs{x} \geq \abs{z}$, alors $zt=xy$ : on a donc $z$ préfixe de $x$. Donc, il existe $u \in \Sigma^*$ tel que $x = zu$. On a donc $zuy = zt$ donc $uy = t$. Et de même, si $\abs{z} \geq \abs{x}$, on se retrouve dans l'autre cas.
\end{proof}

\textbf{Remarque : } Pour simplifier à gauche, on procède formellement par récurrence. Si $z = \epsilon$ et $zu = zv$, alors $u = v$. Sinon, $z = az'$ avec $az'u = az'v$ et donc $z'u = z'v$. Par récurrence, $u = v$.


\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\draw (0,1) -- (3.5,1) -- (3.5,0) -- (0,0) -- cycle;
		\draw (4,1) -- (6,1) -- (6,0) -- (4,0) -- cycle;
		\draw (0,-0.5) -- (2,-0.5) -- (2,-1.5) -- (0,-1.5) -- cycle;
		\draw (2.5,-0.5) -- (6,-0.5) -- (6,-1.5) -- (2.5,-1.5) -- cycle;
		\draw[dashed] (2.5, 1) -- (2.5, 0);
		\draw[dashed] (3.5, -0.5) -- (3.5, -1.5);
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (1,0.5) {x};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1,-1) {z};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3,0.5) {u};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3,-1) {u};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5,-1) {t};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5,0.5) {y};
	\end{tikzpicture}
	\caption{Représentation du lemme de Levi avec des blocs}
\end{figure}


\begin{proof}
	On finit en donnant une preuve formelle de l'existence dans le lemme de Levi.
	
	\medskip
	
	On peut poser $x = x_1x_2...x_m$ avec pour tout $i \in \brak{1, m}, x_i \in \Sigma$ et $z = z_1...z_n$ avec pour tout $i \in \brak{1, n}, z_i \in \Sigma$. 
	
	\medskip
	
	Si $\abs{x} \geq \abs{z}$, alors $m \geq n$. On pose en ce cas $u = x_{n+1}...x_m$.
	
	Montrons par récurrence forte sur $n \in \NN$ que si $x, y, z, t \in \Sigma^*$ avec $xy = zt$ et $\abs{xy} = n$, alors il existe $u \in \Sigma^*$ tel que ($x = zu$ et $x = zu'$) ou ($z=xu$ et $z=xu'$).
	
	\textbf{Initialisation : } Pour $n = 0$, on a $\abs{xy} = 0$ et donc $x=y=z=t=\epsilon$ donc $u = \epsilon$.
	
	\textbf{Hérédité : } On suppose le résultat vrai jusqu'au rang $n$ et soient $x, y, z, t \in \Sigma^*$ avec $xy=zt$ et $\abs{xy} = n+1$.
	\begin{itemize}
		\item Si $x = \epsilon$, alors $u = z$ convient. On a bien $z = xu = \epsilon z$ et $y = ut = zt = xy = \epsilon y$.
		\item De même si $z = \epsilon$ par symétrie.
		\item Il existe $a, a' \in \Sigma$ et il existe $x', z' \in \Sigma^*$ tel que $x = ax'$ et $z = az'$. On a donc $ax'y = a'z't$ donc $a=a'$ et $x'y = z't$. Or $\abs{xy} = n$ et $x'y = z't$. Donc par hypothèse de récurrence, il existe $u' \in \Sigma^*$ tel que ($x' = z'u'$ et $t = u'y$) ou ($z'=x'u'$ et $y=u't$). On choisit $u = u'$ et on a donc ($ax' = az'u$ et $t = u'y$) ou ($az=ax'$ et $y=ut$).
	\end{itemize}
	La propriété reste vraie au rang $n+1$, le lemme est prouvé.
\end{proof}