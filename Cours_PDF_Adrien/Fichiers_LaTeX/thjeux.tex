\chaptr{Théorie des jeux}{1}

On s'intéresse à l'étude théorique et algorithmique de jeux à deux joueurs et en particulier à l'élaboration de stratégies.

\medskip

Quelques dates :
\begin{itemize}
	\item 1952 : premier jeu Tic Tac Toe;
	\item 1997 : Deep Blue bat le champion d'échecs Garry Kasparov;
	\item 2017 : Alpha Go bat le champion du monde de Go Ke Jie;
	\item 2019 : Alpha Star bat un champion du monde Serial à Starcraft.
\end{itemize}

On ne va considérer que des jeux à deux joueurs, qui jouent alternativement à information parfaite : à tout moment les deux joueurs ont la connaissance complète et exhaustive du jeu. On ne considère que des jeux à somme nulle : la somme des gains est nulle et même de type victoire / échec / match nul.

\section{Jeux à deux joueurs}

On trouve par exemple le Tic-Tac-Toe. On peut modéliser ce jeu par un arbre des configurations possibles. Chaque noeud représente un état qui comporte toute l'information ainsi que le joueur à qui c'est le tour de jouer (on dit que le joueur contrôle cet état). Les fils d'un état sont les états on peut accéder en jouant un coup.

\begin{exemple}
	Pour le Tic-Tac-Toe, il y a $549 946$ noeuds dans l'arbre.
\end{exemple}

On va plutôt représenter un jeu par un graphe biparti (en général acyclique). Il y a alors $5478$ sommets et $16 167$ arcs.

\textbf{Remarque :} Plusieurs chemins peuvent mener d'une configuration à une autre. Un état est contrôlé par l'un des deux joueurs. Un arc $x \to y$ entre un sommet contrôlé par $X$ et un sommet contrôlé par $Y$ correspond à un coup possible à partir de la configuration $x$. Les puits du graphe (sommets sans successeurs) sont appelés états terminaux. Ce sont les états gagnants pour un jour, pour l'autre, ou les états de match nul. 

\begin{exemple}
	Pour le Tic-Tac-Toe, il y a $958$ états terminaux dont $626$ gagnants  par $X$, $316$ gagnants par $Y$ et $16$ de match nul.
\end{exemple}

\newpage

\index{jeu}
\begin{defi}
	Un jeu à deux joueurs est un quadruplet $\jeu$ avec :
	\begin{itemize}
		\item $G = (S = S_1 \coprod S_2, A)$ un graphe biparti orienté (acyclique),
		\item $s_0 \in G$ est appelé état initial,
		\item $T_1 \subseteq S$ et $T_2 \subseteq S$ et $T_1 \cap T_2 \ne \emptyset$ appelés états gagnants pour le joueur $1$ et le joueur $2$ respectivement des puits de $G$.
	\end{itemize}
\end{defi}

\index{état!du joueur}
\begin{defi}
	Les sommets de $S_1$ sont appelés états du joueur $1$ (ou contrôlés par le joueur $1$), \cad que c'est à $1$ de jouer, et de même pour $2$.
	
	\medskip
	
	Les puits de $G$ sont appelés états terminaux. On note $T$ les puits de $G$. On a $T = T_1 \coprod T_2 \coprod T_=$ avec $T_=$ les états terminaux correspondant à des matchs nuls. On note $X_1 = S_1 \backslash T$ l'ensemble des sommets contrôlés par le joueur $1$ non terminaux.
\end{defi}

\textbf{Remarque :} Si le graphe est acyclique, il y a au moins un puits.

\index{victoire}
\begin{defi}
	Une partie d'un jeu $\jeu$ avec $G = (S = S_1 \coprod S_2, A)$ est un chemin fini du graphe $(S , A)$ qui part de $s_0$ et qui arrive sur un sommet $t \in T$. La partie est gagnée par le joueur $1$ si $t \in T_1$ gagné par le joueur $2$ si $t \in T_2$ et nulle si $t \in T_=$.
\end{defi}

\index{stratégie}
\begin{defi}
	Une stratégie pour le joueur $i \in \{1, 2\}$ est une fonction $f : X_i \to X_{3 - i}$ telle que pour tout $x \in X_i$ non-terminal contrôlé par le joueur $i$, $f(x) \in S_{3 - i}$ indique le coup à jouer à partir du sommet $x$, $(x, f(x)) \in A$.
\end{defi}

\textbf{Remarque :} Ici, une stratégie ne considère que l'état actuel. On parle de stratégie sans mémoire.

\begin{defi}
	Une partie $s_0 \to s_1 \to \dots \to s_n$ est jouée suivant une stratégie $f$ pour le joueur $i$ si pour tout $K \in \brak{0, n-1}$, si $s_K \in X_i$, alors $f(s_K) = s_{K+1}$, \cad si tout coup pour le joueur $i$ est joué en utilisant $f$.
\end{defi}

\index{stratégie!gagnante}
\begin{defi}
	On dit qu'une stratégie $f$ pour le joueur $i \in \{1, 2\}$ est gagnante pour le joueur $i$ à partir d'une position $s \in S$ si toute partie de $(G, s, T_1, T_2)$ qui commence en ce sommet $s \in S$ jouée suivant $f$ est gagnante. Un tel sommet $s$ est appelé position gagnante.
\end{defi}

\textbf{Remarque :} $f$ est une stratégie gagnante à partir de $s$ si quel que soit l'adversaire, jouer en suivant $f$ permet de gagner à coup sûr.

\index{position gagnante}
\begin{defi}
	Un sommet $s \in S$ est une position gagnante s'il existe une stratégie pour laquelle cette position est gagnante. 
	
	\medskip
	
	Une stratégie gagnante pour le joueur $i \in \{1, 2\}$ pour un jeu $\jeu$ est une stratégie gagnante pour ce joueur à partir de la position initiale $s_0$. Elle existe \ssi $s_0$ est une position gagnante pour ce joueur.
\end{defi}

\textbf{Remarque :} L'ensemble des sommets se partitionne en :
\begin{itemize}
	\item les positions gagnantes pour $1$,
	\item les positions gagnantes pour $2$, 
	\item les autres.
\end{itemize}

Une position contrôlée par le joueur $i$ peut-être gagnante pour $i$ ou pour l'autre ou pour aucun des deux. Ainsi, si $s \in T$, alors si $s \in T_1$, $s$ est une position gagnante pour $1$. De même, si $s \in T_2$, $s$ est une position gagnante pour $2$. Finalement, si $s \in T_=$, alors $s$ n'est une position gagnante pour aucun des deux.

\medskip

Soit $x \in X_i$ et notons $V_x$ ses successeurs. S'il existe $y \in V_x$, une position gagnante pour $i$, alors $x$ est une position gagnante pour $i$. A l'inverse, si pour tout $y \in V_x$, $y$ est une position gagnante gagnante pour $3 - i$, alors $x$ est une position gagnante pour $3 - i$.

\section{Calcul des attracteurs}

\index{attracteur}
\begin{defi}
	L'attracteur d'un joueur $i$ est l'ensemble des positions gagnantes pour $i$. On le note $A(i) \subseteq S$.
\end{defi}

On vient de voir un moyen de calculer les attracteurs pour les deux joueurs. Pour $i \in \{1, 2\}$, et pour $n \in \NN$, on définit par récurrence la suite $A_n(i) \subseteq S$ une suite d'ensembles de positions qui gagnent en au plus $n$ étapes.

\medskip

Pour $n = 0$ et $i \in \{1, 2\}$, $A_0(i) = T_i$. Pour $n \in \NN$, $$A_{n+1}(i) = A_n(i) \cup \{x \in X_i \mid \exists y \in A_n(i), (x, y) \in A\} \cup \{x \in X_{3-1} \mid \forall y \in V(x), y \in A_n(i)\}$$.

\begin{prop}
	$A(i) = \bigcup_{n \in \NN} A_n(i)$ est l'attracteur du joueur $i$ et pour tout $x \in A(i)$, il existe $n \in \NN$ tel que $x < A_n(i)$.
\end{prop}

En pratique, pour calculer les attracteurs, on calcule le graphe des configurations et on effectue un parcours en largeur à partir de $T_i$ et $T_{3 - i}$, dans le graphe miroir.

\medskip

\textbf{Remarque :} A deux stratégies $f_1$ et $f_2$ pour les joueurs $1$ et $2$ correspond à une unique partie $s_0 \to f_1(s_0) \to f_2(f_1(s_0)) \to \dots$ si $s_0 \in S_1$.

\medskip

\index{stratégie!optimale}
\textbf{Construction d'une stratégie "optimale" :} On parle de stratégie optimale dans le sens d'une stratégie qui gagne le plus vite possible, ou perd le moins vite possible. Construisons $f_i : X_i \to X_{3 - i}$. Pour $x \in X_i$ :
\begin{itemize}
	\item Si $x \in A(i)$, soit $n \in \NN$ le plus petit $n \in \NN$ tel que $x \in A_n(i)$. On a $n > 0$ car $x \not\in T$. Par définition de $A_n(i)$, puisque $x \not\in A_{n-1}(i)$, il existe $y \in A_{n-1}(i)$ tel que $(x, y) \in A$. On pose $f(x) = y$.
	\item Si $x \in A(3-i)$, on considère $n \in \NN$ le plus petit tel que $x \in A_n(3-i)$. On a $n > 0$. Par construction, pour tout $y \in V(x)$, $y \in A_{n-1}(i)$. Il existe donc $y \in V(x)$ tel que $y \not\in A_{n-2}(3-i)$ (si $n \geq 2$). Sinon, on fait $x \in A_{n-1}(3-i)$ et ce serait absurde par choix de $n$. Soit $y$ un tel voisin. On pose $f(x) = y$.
	\item Si $x \not\in A(i) \cup A(3-i)$, alors il existe $y \in S_{3 - i} \backslash A(3 - i)$ et on pose $f(x) = y$.
\end{itemize}

\section{Min-Max et heuristique}

\subsection{Algorithme Min-Max}

\index{{\scshape Algorithme}!Min-Max}
Une autre manière équivalente de déterminer les positions gagnantes est d'associer n score à chaque état :
\begin{itemize}
	\item $+\infty$ si la position est gagnante pour le joueur $1$,
	\item $-\infty$ si la position est gagnante pour le joueur $2$,
	\item $0$ si la position n'est gagnante par aucun des deux.
\end{itemize}

L'attracteur du joueur $1$ est alors les positions de score $+\infty$. Déterminer si une position est gagnante revient à déterminer son score. Si le graphe est acyclique, cela peut se faire de la manière suivante récursive :
\begin{itemize}
	\item Si $x$ est terminal, son score est $+\infty, 0, -\infty$ suivant que la configuration est gagnante, nulle, ou perdante.
	\item Si $s$ est contrôlé par $1$, alors son score est $\max\{\text{score}(y) \mid y \in V(s)\}$.
	\item Si $s$ est contrôlé par $2$, alors son score est $\min\{\text{score}(y) \mid y \in V(s)\}$.
\end{itemize}

Le joueur $1$ est souvent appelé le joueur $\max$ et le joueur $2$ le joueur $\min$. Pour que le calcul soit efficace, il faut mémoriser les configurations pour ne pas recalculer plusieurs fois la même chose. On utilise une technique dite de mémoïsation, même si cette méthode est rarement appliquée en pratique.

\subsection{Heuristiques}

Dans la très grande majorité des cas, les configurations possibles sont trop nombreuses pour calculer les attracteurs (ou effectuer MinMax en entier). 

\begin{exemple}
	Au Domineering, il y a $4632$ états pour une grille $4 \times 4$. Un million pour $5 \times 5$ et $40$ billiards pour le jeu classique $8 \times 8$. Au Go, c'est encore pire ; on estime le nombre de configurations à $10^{130}$.
\end{exemple}

Pour parer ce problème, on va introduire la notion d'heuristique (un guide, une aide).

\begin{defi}
	Une heuristique évalue la qualité d'une configuration. C'est une fonction de $S \to \overline{\RR}$. En général, on impose $h(e) = +\infty$ quand $e \in A(1)$ et $h(e) = -\infty$ quand $e \in A(2)$.
\end{defi}

\remarklist{
	\item Une heuristique doit être une bonne estimation de la situation.
	\item De plus, elle doit être facile à calculer.
}

\begin{exemple}
	On peut citer deux exemples : 
	\begin{itemize}
		\item Le nombre de coups possibles pour le joueur $1$ moins le nombre de coups possibles pour le joueur $2$ est une bonne heuristique dans des jeux comme le Stevia (voir TP).
		\item Jouer $10^6$ parties aléatoires à partir de cette configuration et renvoyer la différence du nombre de parties gagnées est également une bonne heuristique (Monte Carlo MinMax).
	\end{itemize}
\end{exemple}

\index{{\scshape Algorithme}!Min-Max!avec heuristique}
\textbf{Algorithme MinMax avec heuristique :} \begin{enumerate}
	\item On choisit une profondeur maximale d'exploration $p \in \NN$, \cad que l'on ne considère pas le sous-graphe des configurations à distance plus grande que $p$ de $s_0$.
	\item On applique l'algorithme MinMax à ceci près que si on atteint la profondeur maximale, on traite les états comme terminaux en utilisant l'heuristique comme score.
\end{enumerate}

Bien entendu, l'exploration étant limitée et l'heuristique imparfaite, rien ne garantit que l'on prédit correctement une bonne stratégie. La qualité de l'algorithme dépend de la qualité de l'heuristique et de la profondeur d'exploration.

\subsection{Élagage $\alpha$-$\beta$}

\index{{\scshape Algorithme}!Min-Max!avec élagage $\alpha-\beta$}
Comme dans un algorithme de retour sur trace (backtracking) l'algorithme Min-Max explore l'intégralité de l'arbre jusqu'à une profondeur $p$. Dans certaines situations, il est possible d'élaguer. On va garder en mémoire une plage $[\alpha, \beta]$ des scores "utiles". Si on a la garantie que le score d'un noeud sort de cette plage, alors il est inutile et poursuivre son exploration. 

\medskip

L'élagage $\alpha$ permet de couper des branches lorsque l'on cherche à calculer le score d'un noeud Min. Réciproquement, l'élagage $\beta$ permet de couper des branches lorsque l'on cherche à calculer le score d'un noeud Max.

\medskip

\remarklist{
	\item Les scores obtenus peuvent différer avec ceux de l'approche Min-Max sans élagage mais celui de la racine sera le même.
	\item L'ordre dans lequel on parcourt les fils a une importance. Il vaut mieux explorer les noeuds les plus prometteurs en premier. Cela permet d'élaguer davantage.
}

On propose l'implémentation suivante de l'élagage $\alpha-\beta$ (voir page suivante).

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\fill[red, opacity=0.1] (-3.6,-0.4) rectangle (6.4,-1.4);
		\fill[red, opacity=0.1] (-3.6,2.4) rectangle (6.4,1.2);
		\fill[blue, opacity=0.1] (-3.6,1.2) rectangle (6.4,-0.4);
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (1,2) {$5$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (1,0.5) {$4$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v10) at (4,0.5) {$5$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (-2,0.5) {$1$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v8) at (-2,-1) {$12$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (-1,-1) {$7$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v7) at (-3,-1) {$4$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (0,-1) {$10$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v4) at (1,-1) {$5$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v5) at (2,-1) {$6$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v11) at (3,-1) {$1$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v12) at (4,-1) {$2$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v13) at (5,-1) {$3$};
		\draw  (v1) edge (v2);
		\draw  (v2) edge (v3);
		\draw  (v2) edge (v4);
		\draw  (v2) edge (v5);
		\draw  (v1) edge (v6);
		\draw  (v6) edge (v7);
		\draw  (v6) edge (v8);
		\draw  (v6) edge (v9);
		\draw  (v1) edge (v10);
		\draw  (v10) edge (v11);
		\draw  (v10) edge (v12);
		\draw  (v10) edge (v13);
		
		\draw [red](3.8,-0.3) -- (4.2,-0.3);
		\draw [red](3.8,-0.35) -- (4.2,-0.35);
		\draw [red](4.35,-0.45) -- (4.65,-0.15);
		\draw [red](4.4,-0.5) -- (4.7,-0.2);
		\node [outer sep=0,inner sep=0,minimum size=0] at (5.8,-1) {MAX};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5.8,0.5) {MIN};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5.8,2) {MAX};
	\end{tikzpicture}
	\caption{Exemple d'un élagage $\alpha-\beta$}
\end{figure}

\newpage 

\begin{listingsbox}{myocaml}{OCaml}
	(* Pour un noeud Max `etat` avec une profondeur d'exploration `p`. *)
	let rec max_min p etat =
		if p = 0 || terminal etat then heuristique etat
		else
			(* On cherche le maximum des scores des fils *)
			let rec maxi = function
				| [] -> neg_infinity
				| x :: xs -> max (min_max (p - 1) x) (maxi xs)
			in
			maxi (coups etat)
	(* On procede mutatis mutandis pour un noeud Min. *)
	and min_max p etat =
		if p = 0 || terminal etat then heuristique etat
		else
			let rec mini = function
				| [] -> infinity
				| x :: xs -> min (max_min (p - 1) x) (mini xs)
			in
			mini (coups etat)
	
	(* Pour un noeud Max `etat` avec une profondeur d'exploration `p`. On
	sait que seul un score dans [alpha, beta] sera utile par la suite.
	Pour un noeud Max, si on trouve une valeur supérieur a beta on
	peut s'arreter et renvoyer beta. Inversement, inutile de renvoyer
	une valeur inferieure a alpha. *)
	let rec max_min p alpha beta etat =
		if p = 0 || terminal etat then heuristique etat
		else
			(* On cherche le maximum des scores des fils. La meilleure valeur
			trouvée jusqu'ici est alpha. *)
			let rec maxi alpha beta = function
				| [] -> alpha
				| x :: xs ->
					let score_x = min_max (p - 1) alpha beta x in
					(* Elagage beta : si on trouve un fils de score au moins beta,
					le score de ce noeud Max sera donc au moins beta. Cela ne
					sert plus a rien de continuer et on peut prendre beta. *)
					if score_x >= beta then beta
					else
					(* Sinon, le score de ce neoud Max sera au moins score_x. On
					met donc a jour alpha pour indiquer qu'il ne sert a rien de
					trouver une valeur plus faible que score_x. *)
						maxi (max alpha score_x) beta xs
			in
			maxi alpha beta (coups etat)
	(* On procede mutatis mutandis pour un noeud Min. *)
	and min_max p alpha beta etat =
		if p = 0 || terminal etat then heuristique etat
		else
			let rec mini alpha beta = function
				| [] -> beta
				| x :: xs ->
					let score_x = max_min (p - 1) alpha beta x in
					if score_x <= alpha then alpha
					else
						mini alpha (min score_x beta) xs
		in
		mini alpha beta (coups etat)
	
\end{listingsbox}