%! TEX root = InformatiqueMPI.tex

\chaptr{Séparation et évaluation}{1}

La technique de "séparation et évaluation" (branch and bound) est une technique qui permet d'accoler la recherche exhaustive (brute force) (par élagage d'une partie de l'espace de recherche) lors de l'application d'une stratégie de retour sur trace (backtracking) pour la résolution d'un problème d'optimisation.

\medskip 

\index{backtracking}
\textbf{Backtracking :} (pour des problèmes de recherche / décision), on structure l'espace de recherche pour une recherche exhaustive avec élagages (décomposition du problème en solutions partielles que l'on complète peu à peu avec arrêt si impossible)

\medskip

\textbf{Branch and bound :} même chose mais pour des problèmes d'optimisation.

\section{Problèmes d'optimisation}

On considère un problème d'optimisation $P$ pour lequel une instance  $x \in X$, on cherche à minimiser une quantité $f(x, y)$ pour les solutions $y \in  Y(x)$ possibles à l'instance $x$. On cherche  $\underset{y \in Y(x)}{\min} f(x,y)$.

\medskip

\textbf{Remarque :} Le cas de maximisation se traite de manière symétrique.

\medskip

\index{{\scshape Problème}!MINUNSAT}
\index{{\scshape Problème}!MAXSAT}
\begin{exemple}
    MAXSAT est un problème d'optimisation relié à SAT où l'on cherche à maximiser le nombre de clauses satisfaites. On va plutôt considérer MINUNSAT : 
    \begin{itemize}
        \item Étant donné une formule $x$ sous forme normale conjonctive, on cherche une valuation $y$ sur les variables de $x$ qui ne satisfait pas un nombre minimal de clauses.
            \begin{center}
                 $f(x,y)$ : le nombre de clauses de $x$ non satisfaites par $y$.
            \end{center}
        \item $\phi = (x_1 \vee x_2 \vee x_3) \wedge (x_1 \vee \non x_3 \vee x_4) \wedge (x_1 \vee \non x_4) \wedge (\non x_1 \vee x_2 \vee x_3) \wedge (\non x_1 \vee \non x_2) \wedge (\non x_1 \vee \non x_3) \wedge (x_2 \vee \non x_3) \wedge (\non x_2 \vee x_3)$
    \end{itemize}
\end{exemple}

\textbf{Remarque :} Si on sait résoudre efficacement MAXSAT, on sait résoudre efficacement CNF-SAT (et SAT).

\medskip

\begin{exemple}
    Dans le problème du voyageur de commerce à un graphe $G = (S, A, \omega)$ non orienté, pondéré, (souvent complet, à poids strictement positifs), on cherche un cycle hamiltonien de poids minimal dans $G, f(x, y) = \omega(y)$.
\end{exemple}

\textbf{Remarque :} Si on sait résoudre le problème du voyageur de commerce efficacement, on sait résoudre le problème du cycle hamiltonien.

\section{Séparation}

La première étape de l'approche par backtracking et par branch and bound est la séparation de l'espace de recherche (branch). Elle consiste à donner à l'espace des solutions une structure arborescente adaptée. On cherche à séparer récursivement (branch) l'espace des solutions en plusieurs sous-espaces.

\medskip

[FIGURE]

\medskip

On obtient alors des solutions partielles qu'il reste à compléter. 

\begin{exemple}
    Pour SAT et MAXSAT, l'ensemble des solutions est l'ensemble des valuations à $n$ variables (celles de $x$ ). On peut représenter cet ensemble sous la forme d'arbre binaire en associant à chaque nœud une variable et en séparant les solutions suivant que cette variable vaut $V$ ou $P$. La valuation initiale est $\emptyset$. A chaque nœud de l'arbre correspond une valuation partielle et le sous arbre enraciné en ce nœud correspond à toutes les manières de la prolonger.
\end{exemple}

En exercice, on pourra dessiner l'arbre complet correspondant à $\phi$, évaluer les feuilles et donner une solution optimale pour MAXSAT.

\medskip

De plus, pour le voyageur de commerce, pour un graphe $G = (S, A, \omega)$ une solution partielle est un chemin élémentaire de $a \in  S$ à $b \in S$ passant par les sommets $X \subseteq S$. On note cela $a \overset{X}{\leadsto} b$. On cherche alors à compléter ce chemin par un voisin $x$ de $b$ qui n'est pas dans $X$ (sauf si $X = S$ dans ce cas, on cherche à revenir en $a$). La racine est $a \overset{\{a\}}{\leadsto} a$ pour un sommet $a \in S$ arbitraire.

\medskip

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v7) at (0.5,2) {$F$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v8) at (3,2) {$E$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v1) at (-1,0.5) {$G$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v2) at (4.5,0.5) {$D$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v4) at (-1,-2) {$H$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v6) at (0.5,-3.5) {$A$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v5) at (3,-3.5) {$B$};
		\node [circle,fill=blue!20,draw,minimum size=0.5cm,inner sep=0pt] (v3) at (4.5,-2) {$C$};
		\draw  (v1) edge (v2);
		\draw  (v3) edge (v4);
		\draw  (v4) edge (v1);
		\draw  (v2) edge (v3);
		\draw  (v5) edge (v6);
		\draw  (v6) edge (v7);
		\draw  (v7) edge (v8);
		\draw  (v8) edge (v5);
		\draw  (v6) edge (v4);
		\draw  (v1) edge (v7);
		\draw  (v8) edge (v2);
		\draw  (v3) edge (v5);
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.75,-3.75) {$2$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.75,-2.25) {$5$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.75,0.75) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1.75,2.25) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-1.25,-0.75) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (0.25,-0.75) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3.25,-0.75) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (4.75,-0.75) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (4,-3) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,1.5) {$2$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,-3) {$1$};
		\node [outer sep=0,inner sep=0,minimum size=0] at (4,1.5) {$2$};
		
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (8.5,1) {$A$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v11) at (8.5,0) {$F$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v10) at (9.5,0) {$H$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v12) at (7.5,0) {$B$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v14) at (7.5,-1) {$E$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v13) at (6.5,-1) {$C$};
		\node [outer sep=0,inner sep=0,minimum size=0] (v16) at (8,-2) {};
		\node [outer sep=0,inner sep=0,minimum size=0] (v15) at (7,-2) {};
		\draw  (v9) edge (v10);
		\draw  (v9) edge (v11);
		\draw  (v9) edge (v12);
		\draw  (v12) edge (v13);
		\draw  (v12) edge (v14);
		\draw[dotted]  (v14) edge (v15);
		\draw[dotted]  (v14) edge (v16);
	\end{tikzpicture}
	\caption{Exemple de séparation, une solution partielle serait $a \to b \to e$.}
\end{figure}

\medskip

Sans élagage, on effectue un parcours en profondeur dans l'arborescence : il s'agit d'une recherche exhaustive.

\section{Retour sur trace}

L'idée est d'améliorer la recherche exhaustive en élagant de grandes parties de l'espace de recherche ($99,999999\%$, mais attention, la complexité sera exponentielle. Si pour un nœud on est capable de déterminer qu'il est impossible de compléter la solution partielle en une solution totale, on peut s'arrêter là (élagage).

\medskip

Pour un problème d'optimisation, on va faire la même chose, mais il faut être capable de déterminer si pour un nœud, il n'y a plus d'espoir de trouver une meilleur solution qu'un déjà trouvée.

\section{Évaluation}

On peut souvent déjà calculer une évaluation partielle pour une solution partielle $\hat{f}\left(x, \hat{y}  \right)$.

\begin{exemple}
    \textit{MAXSAT :} pour une solution partielle $\hat{y}$, les clauses déjà entièrement non satisfaite par $\hat{y}$ si elles ne seront pas non plus un prolongement de $\hat{y}$.
\end{exemple}

Pour le voyageur de commerce, une solution partielle est le poids de la solution partielle.

On suppose donc disposer d'une heuristique $h$ qui évalue pour une solution partielle $\hat{y}$ le cout $h(\hat{y})$ qui cherche à approcher le cout minimal de toute complétion de $\hat{y}$ en une solution totale $y$ (ou $+\infty$ si aucune solution n'est possible).

\begin{defi}
    Pour élaguer sans erreur, une partie de l'espace de recherche, il est nécessaire que cette heuristique sous-estime ce coût. Une telle heuristique est dite admissible.
\end{defi}

\begin{prop}
	Une telle heuristique est admissible si pour tout $x \in Y$ et pour toute solution partielle $\hat{y}$ pour $x$, pour toute solution totale $y$ de $x$ qui prolonge  $\hat{y}$, on a $h(\hat{y}) \le  f(x, y)$.
\end{prop}

\begin{exemple}
    \textit{MAXSAT :} Pour une valuation partielle $\hat{v}$, pour une formule  $\phi$, on peut prendre comme heuristique le nombre de clause déjà non satisfaite par $\hat{v}$.
\end{exemple}

\textbf{Remarque :} Deux clauses $c_1, c_2$ comportant un littéral $x$ pour l'un et $\non x$ pour l'autre, le reste des littéraux étant déjà déterminés par $\hat{v}$ et ces clauses n'étant pas déjà satisfaites. Alors, tout prolongement de $\hat{v}$ aura une des deux clauses non satisfaites.

\begin{exemple}
    Voyageur de commerce : quelle heuristique pour une solution partielle $a \overset{\alpha}{\to} b$ :
    \begin{itemize}
        \item le poids de $\omega(a \to b)$ 
        \item le poids d'un arbre couvrant de poids minimal de $S \backslash X$ 
        \item le minimum des arêtes de $a$ à $S \backslash X$
        \item le minimum des arêtes de $b$ à $S \backslash X$
    \end{itemize}
    On minore le coût de toute solution qui prolonge $a \overset{\alpha}{\to} b$.
\end{exemple}

\section{Élagage}

\index{élagage}
\begin{defi}
    L'élagage consiste à couper des branches lors de l'exploration lorsqu'on est certain que le nœud ne peut pas donner une meilleur solution que celle déjà obtenue.
\end{defi}

La structure de l'algorithme est donc la suivante :
\begin{itemize}
    \item Explorer l'ensemble des solutions, structuré sous la forme d'un arbre.
    \item Mémoriser la valeur de la meilleur solution.
    \item Pour un nœud, calculer une borne minimale (heuristique) de toute complétion de la solution partielle. Si on trouve plus que la solution mémorisée, on élague.
\end{itemize}

\index{{\scshape Algorithme}!Branch and Bound}
\begin{breakablealgorithm}
    \caption{Élagage}
    \begin{algorithmic}[1]
        \Require Une instance $x$ de $P$ 
        \Ensure La valeur exacte d'une solution minimale
        \State $f_{min} \leftarrow +\infty$ 
        \State $y_{min} \leftarrow \emptyset$ 
        \Function{BnB}{$\hat{y}$} // On complète la solution partielle de $y$ 
        \If{$\hat{y}$ est totale}
        \If{$f(x, \hat{y}) < f_{min}$}
        \State $f_{min} \leftarrow f(x, \hat{y})$ 
        \State $y_{min} \leftarrow y$ 
        \EndIf
        \Else \If{$h(\hat{y}) < f_{min}$}
        \For{chaque manière $z$ de compléter $\hat{y}$ en une étape}
        \State BnB($\hat{y}z$)
        \EndFor
        \EndIf
        \EndIf
        \EndFunction
        \State BnB($\emptyset$)
        \State \Return  $y_{min}$
    \end{algorithmic}
\end{breakablealgorithm}

\remarklist{
    \item Au lieu d'initialiser $f_{min}$ à $+\infty$, on peut prendre une solution approchée (algorithme glouton, approximation, algorithme probabilisé, etc...)
    \item Il convient souvent de bien choisir l'ordre des branches.
}

A titre d'exercice, on pourra appliquer $BnB$ avec l'heuristique du cours et l'ordre lexicographique (sur le "graphe octogone").
