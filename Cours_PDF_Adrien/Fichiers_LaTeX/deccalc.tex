\chaptr{Décidabilité et calculabilité}{1}

Il faut commencer par formaliser la notion de problème et de procédure effective. On se donne un alphabet $\Sigma$ contenant au moins deux symboles. On peut prendre par exemple :
\begin{itemize}
	\item $\Sigma = \{0, 1\}$ correspondant à la représentation binaires de données informatique,
	\item $\Sigma = \{0, \dots, 255\}$ correspondant à la représentation textuelle de données informatique en utilisant par exemple le code ASCII étendu.
\end{itemize}

\textbf{Remarque :} On évite en général de prendre un alphabet $\Sigma$ unaire avec $\abs{\Sigma} = 1$.

\section{Représentation des données}

Pour que l'on puisse traiter un problème avec un ordinateur, il faut que les données soient représentables.

\begin{exemple}
	On peut considérer des problèmes sur des nombres, des textes, des images, des sons, mais pas sur des concepts, sauf à modéliser ceux-ci par une représentation abstraite.
\end{exemple}

On se convainc assez facilement que toute donnée informatique, pour être effectivement utilisable, doit être représentée en machine par un mot sur $\Sigma$. De la même manière, remarquons qu'un algorithme ou un programme informatique dans un langage quelconque est toujours la donnée d'une suite de symboles, c'est-à-dire un mot sur $\Sigma$.

\begin{exemple}
	Un programme OCaml est la donnée de son fichier source .ml qui est une chaine de caractères.
\end{exemple}

\index{algorithme}
\index{programme!informatique}
\begin{defi}
	Un algorithme ou un programme informatique est une donnée textuelle. On peut donc en particulier écrire des algorithmes qui prennent en entrée ou renvoient en sortie des algorithmes.
\end{defi}

\textbf{Remarque :} On identifie ici un algorithme au texte qui le définit. Deux algorithmes peuvent avoir exactement et rigoureusement le même comportement. Par exemple, deux programme OCaml dont on change simplement le nom de toutes les occurrences d'une variable auront exactement le même comportement. On considèrera tout de même qu'il s'agit de deux algorithmes différents, même si intuitivement on a envie de dire qu'il s'agit du même algorithme. Ils seront cependant sémantiquement équivalents.

\section{Problèmes de décision}

\subsection{Définition intuitive}

Un problème de décision est une question à laquelle on peut répondre par oui ou par non.

\index{problème!de décision}
\begin{defi}
	On appelle problème de décision la donnée d'une instante et d'une question dichotomique :
	\begin{itemize}
		\item \textbf{Instance :} un objet possédant certaines caractéristiques.
		\item \textbf{Question :} une question à laquelle on ne peut répondre que "oui" ou "non".
	\end{itemize}
	On appelle instance positive (resp. négative) une instance pour laquelle la réponse à la question est "oui" (resp. "non").
\end{defi}

\begin{exemple}
	Les problèmes suivants sont des problèmes de décision :
	\begin{itemize}
		\item \textit{PREMIER :}
		\begin{itemize}
			\item[$\star$] \textbf{Instance :} un entier naturel $n$.
			\item[$\star$] \textbf{Question :} $n$ est-il premier ?
		\end{itemize}
		\item \textit{TRIEE :}
		\begin{itemize}
			\item[$\star$] \textbf{Instance :} une liste $l$.
			\item[$\star$] \textbf{Question :} les éléments de $l$ sont-ils ordonnées par ordre croissant ?
		\end{itemize}
		\item \textit{CONNEXE :}
		\begin{itemize}
			\item[$\star$] \textbf{Instance :} un graphe $G$ non orienté.
			\item[$\star$] \textbf{Question :} $G$ est-il un graphe connexe ?
		\end{itemize}
	\end{itemize}
\end{exemple}

\subsection{Formalisation}

Puisque l'on suppose donc que l'on peut représenter les données sous la forme d'un mot sur $\Sigma$, une instance d'un problème est donc un mot de $\Sigma^*$.

\index{instance!positives}
\index{instance!négatives}
\index{instance!non-valables}
\begin{defi}
	Un problème de décision est la donnée d'une partition de $\Sigma^*$ par trois langages $L_+ \coprod L_- \coprod L_\# = \Sigma^*$.
	\begin{itemize}
		\item $L_+ = \{u \in \Sigma^*, \text{ la réponse au problème sur } u \text{ est oui}\}$
		\item $L_- = \{u \in \Sigma^*, \text{ la réponse au problème sur } u \text{ est non}\}$
		\item $L_\# = \{u \in \Sigma^*, u \text{ n'est pas la représentation d'une instance du problème}\}$
	\end{itemize}
	$L_+$ est l'ensemble des instances positives du problème, $L_-$ est l'ensemble des instances négatives du problème et $L_\#$ est l'ensemble des mots qui ne forment pas une instance valable du problème.
\end{defi}

\begin{exemple}
	Par exemple si $\Sigma^*$ est l'ensemble des chaînes de caractères, on peut considérer, $L_\# \subseteq \Sigma^*$ l'ensemble des chaînes de caractères qui ne représentent pas un programme OCaml qui compile sans erreurs, $L_+ \subset \Sigma^*$ ceux qui correspondent à un programme OCaml qui se termine sur toute entrée et $L_- \subset \Sigma^*$ les autres mots. On sait décider si un mot est dans $L_\#$ (c'est un travail pour un compilateur). Sait-on décider si un mot est dans $L_+$.
\end{exemple}

Déterminer si un mot $u \in L_\#$ est en général facile et on fusionne souvent $L_-$ et $L_\#$. Dans ce cas un problème est un langage $L \subseteq \Sigma^*$ correspondant à l'ensemble de ses instances positives. Un mot qui n'est pas dans $L$ est une instance négative ou ne représente pas une instance valable.

\begin{prop}
	Un problème de décision correspond au langage $L \subseteq \Sigma^*$ des mots qui sont la représentation de ses instances positives.
\end{prop}

\textbf{Remarque :} On sait donc résoudre tous les problèmes si on est capable de déterminer si un mot $u \in \Sigma^*$ appartient à un langage $L \subseteq \Sigma^*$ pour un mot $u \in \Sigma^*$ et un langage $L \subseteq \Sigma^*$ quelconques. Tous les problèmes se réduisent donc à ce problème.

\section{Procédure effective}

Il faudrait également formaliser la notion de "machine" et de définir précisément ce qu'est un algorithme "valable" : vraisemblablement il ne s'agit pas de n'importe quelle chaine de caractères. Intuitivement, il doit s'agit d'une "procédure effective".

\subsection{Thèse de Church-Turing (Hors-Programme)}

\index{thèse de Church-Turing}

Dans les années $30$, Church et Turing ont proposé différents modèles de calculs et ont montré que ceux-ci étaient en réalité équivalents et pouvaient s'exprimer les uns en fonction des autres. La thèse de Church-Turing conjecture que tout modèle raisonnable de procédure effective est équivalent à l'un de ces modèles. 

\medskip

On peut essayer de donner une description universelle de la notion de calculabilité et d'algorithme. Les points communs sont les suivants :
\begin{itemize}
	\item on dispose d'un nombre fini et fixe de commandes élémentaires définies par le modèle de calcul,
	\item un programme est un enchaînement fini (mais pouvant être arbitrairement grand) de ces commandes, 
	\item le programme peut être exécuté sur une machine à mémoire potentiellement infinie.
\end{itemize}

On peut citer comme modèles de calcul les machines à registres, les machines de Turing, le lambda-calcul ou encore les fonctions récursives. D'autres modèles de calcul plus surprenant ont été démontrés comme étant équivalents aux modèles précédents (on parle de modèle Turing-complet), comme par exemple le jeu de cartes \textit{Magic : The Gathering} ou le jeu de la vie de Conway.

\textbf{Remarque :} La plupart des langages de programme sont Turing-complets et sont donc équivalents; On peut toujours réécrire ce que réalise un programme dans un langage de programme A dans un langage de programmation B.

\subsection{Modèle de calcul retenu}

On en va pas chercher à formaliser la notion de Turing, qui correspond intuitivement à un automate auquel on ajoute une bande de mémoire infinie. On se contentera du modèle de calcul théorique intuitif suivant.

\index{procédure}
\begin{defi}
	Une procédure effective correspond à la réalisation d'un programme, qui est une chaîne de caractères dans un langage de programmation (par exemple OCaml ou C) sur un ordinateur idéal, déterministe possédant une mémoire infinie.
\end{defi}

Si $A \in \Sigma^*$ est la description d'un algorithme (ou un programme en OCaml ou C), on note $\tilde{A}$ la procédure effective correspondant à cet algorithme, \cad son exécution sur une machine idéale avec une mémoire arbitraire.

\index{programme!source}
\begin{defi}
	$A \in \Sigma^*$ correspond au programme source d'un algorithme et $\tilde{A}$ à son exécution dans le modèle de calcul.
\end{defi}

\textbf{Remarque :} On identifie souvent la description d'un algorithme qui est un mot $A \in \Sigma^*$ et sa réalisation $\tilde{A}$.

\medskip

Pour simplifier, on suppose les programmes sans effets de bord (on peut toujours passer comme argument l'état complet de la mémoire pour simuler les effets de bord) et sans exceptions. Ceci se fait sans perte de généralité mais en tenir compte compliquerait toute la présentation.

\subsection{Machine universelle}

\index{machine universelle}

Une propriété intéressante de tout ces modèles de calcul est l'existence d'une machine universelle, \cad un programme qui peut simuler l'exécution de n'importe quel autre programme (avec éventuellement n'importe quel argument).

\medskip

Il existe un algorithme $\mathcal{U} \in \Sigma^*$ tel que pour tout algorithme $A \in \Sigma^*, \tilde{\mathcal{U}}(A)$ se comporte exactement comme $\tilde{A}$.

\medskip

Par exemple, PyPy est un interpréteur Python écrit en Python. C'est donc une machine universelle pour le modèle de calcul Python. CPython, qui est un interpréteur Python écrit en C n'est pas une machine universelle (car ce n'est pas le même modèle de calcul).

\medskip

Dans le modèle de calcul des langages de programmation, cela se traduit par l'existence d'un programme \codeCaml{universel} qui prendrait comme argument le code source d'un autre programme p et qui simulerait ce programme et qui renverrait son résultat.

\medskip

En OCaml, cela correspond à une fonction \codeCaml{universel : string -> string} telle que \codeCaml{universel source} correspond à l'exécution du programme décrit par la chaîne de caractère \codeCaml{source}. Cette fonction existe en Python, il s'agit de la fonction \codeCaml{eval} (pour évaluer une expression) ou de la fonction \codeCaml{exec} (pour exécuter une suite d'instructions).

\medskip

On s'intéresse souvent à un cas particulier d'une telle machine universelle qui exécute un algorithme sur une entrée particulière. En OCaml, ce serait une fonction \codeCaml{eval : string -> string -> string} telle que \codeCaml{eval f_s x_s} se comporte exactement comme \codeCaml{f x} où \codeCaml{f} est la fonction décrite par son code source \codeCaml{f_s} et \codeCaml{x_s} la représentation de la valeur \codeCaml{x}. En particulier, \codeCaml{eval f_s x_s} termine \ssi \codeCaml{f x} termine et le cas échéant renvoie la même chaîne de caractères \codeCaml{y}.

\textbf{Remarque :} Attention, même si l'idée est similaire, l'écriture OCaml suivante \codeCaml{let eval f x = f x} ne décrit pas vraiment une machine universelle car ici, ce n'est pas le code source de \codeCaml{f} qui est donné en argument. On se permettra cependant parfois l'abus.

\section{Décidabilité}

Nous avons étudié différents algorithmes répondant à certains problèmes et nous nous sommes interrogés sur leur complexité et sur les améliorations possibles. Mais lors de l'étude d'un problème, avant ces questions, il est important de se demander s'il est possible de trouver une solution à ce problème. C'est la question de la décidabilité.

\medskip

Un problème de décision est décidable s'il existe un algorithme (ou programme ou fonction) dans le modèle de calcul choisi permettant de le résoudre, \cad un algorithme qui termine toujours en temps fini et répond correctement à la question posée. Il est indécidable sinon.

\index{décidabilité}
\begin{defi}
	Un problème de décision $L \subseteq \Sigma^*$ est décidable s'il existe un algorithme $A$ qui prend en entrée un mot $u \in \Sigma^*$, qui termine toujours et qui renvoie "vrai" si $u \in L$ et "faux" sinon. L'algorithme $A$ est appelé un décideur pour $L$.
\end{defi}

\remarklist{
	\item Il peut exister plusieurs décideurs pour un langage décidable.
	\item En OCaml, ceci se traduit par l'existence d'une fonction \codeCaml{f : string -> bool} telle que \codeCaml{f x} s'évalue à \codeCaml{true} \ssi $x \in L$.
	\item Sans la notion de décidabilité, on ne s'intéresse pas à la complexité de l'algorithme éventuel résolvant le problème, elle peut être arbitrairement grande.
}

\index{fonction!totale}
\begin{defi}
	Une fonction totale $f : \Sigma^* \to \Sigma^*$ est calculable s'il existe un algorithme $A$ qui prend en entrée un mot $u \in \Sigma^*$ qui termine toujours et qui renvoie en sortie le mot $f(u)$.
\end{defi}

\textbf{Remarque :} En OCaml, ceci se traduit par une fonction \codeCaml{f : string -> string} qui calcule \codeCaml{f}.

\subsection{Problème de l'arrêt}

Le premier exemple très important de problème indécidable est celui du problème de l'arrêt. Il s'énonce de la manière suivante :

\index{{\scshape Problème}!ARRET}
\textit{ARRET :}
\begin{itemize}
	\item \textbf{Instance :} le code source d'une fonction $f$ et un argument $x$.
	\item \textbf{Question :} est-ce que l'exécution de $f(x)$ termine en temps fini.
\end{itemize}

En OCaml, cela revient à chercher une fonction \codeCaml{arret : string -> string -> bool} telle que \codeCaml{arret source_f source_x}, où \codeCaml{source_f} est le code source d'une fonction \codeCaml{f} et \codeCaml{source_x} la représentation de son entrée \codeCaml{x} termine toujours et renvoie "vrai" \ssi \codeCaml{f x} termine.

\textbf{Remarque :} La fonction \codeCaml{arret} doit donc elle-même toujours terminer.

\begin{prop}
	Le problème de l'arrêt n'est pas décidable.
\end{prop}

\begin{proof}
	Supposons que l'on dispose d'une fonction \codeCaml{arret : string -> string -> bool} qui décide le problème de l'arrêt. Considérons la fonction \codeCaml{paradoxe : string -> string -> unit} suivante qui prend en paramètre un code source de fonction \codeCaml{f_s} correspondant à une fonction \codeCaml{f} et qui termine \ssi l'appel de \codeCaml{f} sur son propre code source \codeCaml{f_s} ne termine pas, \cad qui termine \ssi \codeCaml{eval f_s f_s} ne termine pas.
	
	\medskip
	
	\begin{listingsbox}{myocaml}{OCaml}
	let rec loop () = loop ()

	(* S'arrete si et seulement si 'f' s'arrete sur 'f_s'. *)
	let paradoxe f_s = 
		if arret f_s f_s then loop ()
		else ()
	\end{listingsbox}
	
	\medskip
	
	Considérons le code source de cette fonction :
	\begin{center}
		\codeCaml{let paradoxe_s = "let paradoxe f_s = \\n if arret f_s f_s then loop ()\\n else ()\\n"}
	\end{center}
	Considérons maintenant l'exécution de la fonction \codeCaml{paradoxe} sur son propre code source :
	\begin{center}
		\codeCaml{let () = eval paradoxe_s paradoxe_s}
	\end{center}
	
	Le comportement de \codeCaml{eval paradoxe_s paradoxe_s} est le même que \codeCaml{paradoxe paradoxe_s} qui termine \ssi \codeCaml{paradoxe paradoxe_f} ne termine pas. Ceci est absurde et la fonction \codeCaml{arret} ne peut pas exister.
\end{proof}

\subsection{Réductions}

On traite l'exemple de

\medskip

\textit{ARRET\_VIDE :}
\begin{itemize}
	\item \textbf{Instance :} un code source \codeCaml{f_s} d'une fonction $f : \Sigma^* \to \Sigma^*$
	\item \textbf{Question :} $f(\epsilon)$ se termine-t-il ?
\end{itemize}

Montrons que \textit{ARRET\_VIDE} n'est pas décidable. On suppose que l'on dispose d'une fonction \codeCaml{arret_vide : string -> string -> bool} qui décide ce problème.

\medskip

\begin{listingsbox}{myocaml}{OCaml}
	let arret f_s x = 
		let g_s = "let g y = eval" ^ f_s ^ " " ^ x in
		arret_vide g_s
\end{listingsbox}

\medskip

\codeCaml{arret_est_vide g_s} termine \ssi \codeCaml{g} termine sur \codeCaml{" "} \ssi \codeCaml{eval f_s x} termine \ssi \codeCaml{f x} termine. Absurde donc \codeCaml{arret_vide} n'existe pas car arrêt n'existe pas. On a montré que le problème de l'\textit{ARRET} se réduit au problème de l'\textit{ARRET\_VIDE} : 
\begin{center}
	\textit{ARRET} $\preccurlyeq_T$ \textit{ARRET\_VIDE}
\end{center}