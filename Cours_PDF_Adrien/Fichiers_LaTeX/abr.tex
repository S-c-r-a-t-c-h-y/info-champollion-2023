\chaptr{Arbres binaires de recherche équilibrés}{1}

\sectionbypart

\section{Rappels}

On définit un arbre binaire par induction :

\smallskip 

\emph{Avec un arbre vide :} Dans ce cas, un arbre est soit un arbre vide, soit un noeud constitué de deux sous-arbres gauche et droit. Une feuille est alors un noeud avec deux fils vide.

\smallskip 

\emph{Avec une définition feuille-noeud :} Dans ce cas, un arbre est soit une feuille, soit un noeud avec deux fils gauche et droit.

\smallskip

\remarklist{
	\item La définition par induction considère des cas de base et des règles inductives et constitue le plus petit ensemble compatible avec cela.
	\item On peut ajouter des étiquettes, ou plein d'informations supplémentaires sur les noeuds.
}

\section{Arbres binaires de recherche (ABR)}

\index{arbre!binaire de recherche (ABR)}
\begin{defi}
	Un arbre binaire de recherche est un arbre binaire étiqueté sur un ensemble muni d'un ordre total, qui vérifie de plus la propriété suivante :
	\begin{center}
		L'étiquette de tout noeud est inférieure (ou égale) à celle de tous les noeuds de son sous-arbre droit, et supérieure (ou égale) à celle de tous les noeuds de son sous-arbre gauche
	\end{center}
\end{defi}

On peut facilement construire des contre-exemples où la propriété est vérifiée localement, mais pas globalement.

\begin{prop}
	Un arbre binaire et un arbre binaire de recherche si et seulement si la liste des étiquettes du parcours infixe est croissante.
\end{prop}

\begin{proof}
	La preuve se fait par induction.
\end{proof}

\textbf{Remarque : } On suppose en général que tous les éléments sont distincts.

\smallskip

L'intérêt d'une telle construction est multiple : il permet une recherche, une insertion et une suppression d'éléments efficace, en $\Theta(h)$, avec $h$, la hauteur de l'arbre.

\begin{itemize}
	\item Dans le meilleur cas, l'arbre est équilibré et la hauteur est logarithmique en le nombre de noeuds. Les opérations sont en $O(\log n)$.
	\item Mais dans le pire cas, on peut avoir $h = n - 1$. C'est le cas si on insère des éléments triés.
\end{itemize}

On peut montrer que si les données sont insérées dans un ordre aléatoire, alors, en espérance, la hauteur de l'arbre est $O(\log n)$.

\section{Arbres équilibrés et rotations}

\index{arbre!équilibré}
\begin{defi}
	Soit $A$ un ensemble ABR et $C > 0$. On dit que $A$ est $C$-équilibré si pour tout $a \in A$, $h(a) \leq C \log(n(a))$.
\end{defi}

On cherche donc à construire des familles d'arbres équilibrés pour garantir des opérations en $O(\log(n))$. Ainsi, on cherche un mécanisme pour équilibrer les arbres avec un petit surcout (idéalement en temps constant).

\index{rotation}
\begin{defi}
	Une rotation est une opération sur un ABR qui permet de réduire un déséquilibre tout en maintenant les propriétés des ABR. Elles sont utilisées par les structures de données d'arbre équilibrés.
\end{defi}

\medskip

\begin{figure}[!h]
	\hspace*{1cm}
	\resizebox{2\totalheight}{!}{
	\begin{tikzpicture}
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (-2,5) {$x$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (-0.5,3.5) {$y$};
		\fill[blue] (-2,2) node (v4) {}--(-1,0)--(-3,0)--cycle;
		\fill[red] (1,2) node (v5) {}--(2,0)--(0,0)--cycle;
		\fill[green] (-3.5,3.5) node (v2) {}--(-2.5,1.5)--(-4.5,1.5)--cycle;
		
		\draw  (v1) edge (v2);
		\draw  (v1) edge (v3);
		\draw  (v3) edge (v4);
		\draw  (v3) edge (v5);
		
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (-3.5,1) {a};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-2,-0.5) {b};
		\node [outer sep=0,inner sep=0,minimum size=0] at (1,-0.5) {c};
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v9) at (8,5) {y};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v6) at (6.5,3.5) {x};
		\fill[red] (8.5,1.5)--(9.5,3.5) node (v10) {}--(10.5,1.5)--cycle;
		\fill[green] (5,2) node (v7) {}--(4,0)--(6,0)--cycle;
		\fill[blue] (8,2) node (v8) {}--(9,0)--(7,0)--cycle;
		
		\draw (v6) edge (v7);
		
		\draw (v9) edge (v6);
		\draw (v9) edge (v10);
		\node at (5,-0.5) {a};
		
		\node at (9.5,1) {c};
		\draw [-latex] plot[smooth, tension=.7] coordinates {(0.5,5.5) (3,6) (5.5,5.5)};
		\node at (3,6.5) {rotation à gauche};
		\draw  (v6) edge (v8);
		\node [outer sep=0,inner sep=0,minimum size=0] at (8,-0.5) {b};
	\end{tikzpicture}
	}
	\caption{Rotation à gauche}
\end{figure}

On a bien conservé le caractère ABR. Le cout de la rotation est $\Theta(1)$ (en fonction de l'implémentation). On peut définir de même les rotations à droite. On a l'implémentation suivante :

\medskip

\begin{listingsbox}{myc}{C}
	typedef struct noeud {
		int cle;
		struct noeud* gauche;
		struct noeud* droit;
		struct noeud* parent;
	} noeud;
	
	void rotation_gauche(noeud* x) {
		noeud* y = x->droit;
		x->droit = y->gauche;
		y->gauche = x;
	}
\end{listingsbox}

\medskip

Finalement, pour les arbres de la forme suivante, on définit des opérations gauche-droite et droite gauche

\medskip

\begin{figure}[!h]
	\begin{tikzpicture}
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (-2,5) {$z$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (-3.5,3.5) {$x$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (-2,1.5) {$y$};
		\fill[blue] (-5,2) node (v4) {}--(-4,0)--(-6,0)--cycle;
		\fill[red] (-0.5,3.5) node (v5) {}--(0.5,1.5)--(-1.5,1.5)--cycle;
		\fill[green] (-0.5,0) node (v6) {}--(0.5,-2)--(-1.5,-2)--cycle;
		\fill[orange] (-3.5,0) node (v7) {}--(-2.5,-2)--(-4.5,-2)--cycle;
		
		\draw  (v1) edge (v2);
		\draw  (v2) edge (v3);
		\draw  (v2) edge (v4);
		\draw  (v1) edge (v5);
		\draw  (v3) edge (v6);
		\draw  (v3) edge (v7);
		
		\draw [-latex] plot[smooth, tension=.7] coordinates {(1.5,2.5) (3,3) (4.5,2.5)};
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (8,5) {$z$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (5,2) {$x$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (6.5,3.5) {$y$};
		\fill[blue] (3.5,0) node (v4) {}--(4.5,-2)--(2.5,-2)--cycle;
		\fill[red] (9.5,3.5) node (v5) {}--(10.5,1.5)--(8.5,1.5)--cycle;
		\fill[green] (8,2) node (v6) {}--(9,0)--(7,0)--cycle;
		\fill[orange] (6.5,0) node (v7) {}--(7.5,-2)--(5.5,-2)--cycle;
		
		
		\draw  (v1) edge (v5);
		\draw  (v1) edge (v3);
		\draw  (v3) edge (v6);
		\draw  (v2) edge (v7);
		\draw  (v2) edge (v4);
		\draw  (v3) edge (v2);
		
		\draw [-latex] plot[smooth, tension=.7] coordinates {(7.5,-3) (7,-4) (5.5,-4.5)};
		
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v1) at (3,-5.5) {$z$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v2) at (0,-5.5) {$x$};
		\node [draw,outer sep=0,inner sep=1,minimum size=10] (v3) at (1.5,-4) {$y$};
		\fill[blue] (-2.5,-7) node (v4) {}--(-1.5,-9)--(-3.5,-9)--cycle;
		\fill[red] (5.5,-7) node (v5) {}--(6.5,-9)--(4.5,-9)--cycle;
		\fill[green] (3,-7) node (v6) {}--(4,-9)--(2,-9)--cycle;
		\fill[orange] (0,-7) node (v7) {}--(1,-9)--(-1,-9)--cycle;
		
		
		\draw  (v1) edge (v5);
		\draw  (v1) edge (v3);
		
		\draw  (v2) edge (v7);
		\draw  (v2) edge (v4);
		\draw  (v3) edge (v2);
		\draw  (v1) edge (v6);
		
		
		\node [outer sep=0,inner sep=0,minimum size=0] at (-5,-0.5) {a};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-3.5,-2.5) {b};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,-2.5) {c};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-0.5,1) {d};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3.5,-2.5) {a};
		\node [outer sep=0,inner sep=0,minimum size=0] at (6.5,-2.5) {b};
		\node [outer sep=0,inner sep=0,minimum size=0] at (8,-0.5) {c};
		\node [outer sep=0,inner sep=0,minimum size=0] at (9.5,1) {d};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-2.5,-9.5) {a};
		\node [outer sep=0,inner sep=0,minimum size=0] at (0,-9.5) {b};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3,-9.5) {c};
		\node [outer sep=0,inner sep=0,minimum size=0] at (5.5,-9.5) {d};
		\node [outer sep=0,inner sep=0,minimum size=0] at (3,3.5) {rotation gauche};
		\node [outer sep=0,inner sep=0,minimum size=0] at (6.5,-5) {rotation droite};
		\draw [-latex] plot[smooth, tension=.7] coordinates {(-4.5,-3) (-4,-4) (-2.5,-4.5)};
		\node [outer sep=0,inner sep=0,minimum size=0] at (-3.5,-5) {rotation gauche-droite};
	\end{tikzpicture}
	\caption{Rotation gauche-droite}
\end{figure}

On propose les implémentations suivantes pour les rotations. Le type défini pour représenter un arbre sera souvent utilisé. Il est pratique pour faire des preuves par induction, pour stocker des données, etc...

\medskip

\begin{listingsbox}{myocaml}{OCaml}
	type 'a arbre = 
		| V
		| N of 'a arbre * 'a * 'a arbre
	
	let rotation_gauche t = 
		match t with 
		| N(a, x, N(b, y, c)) -> N(N(a, x, b), y, c)
		| _ -> t
		
	let rotation_gauche_droite a =
		match a with
			| N(l, z, r) -> rotation_droite (rotation_gauche N(l, z, r))
			| _ -> a
	
	let rotation_gauche_droite a =
		match a with 
			| N(N(N(a,x,b),y,c),z,d) -> N(N(a, x, b), y, N(c, z, d))
			| _ -> a
\end{listingsbox}

\section{Arbres AVL}

On essaye donc de se ramener à des arbres assez équilibrés, de sorte à ne pas avoir une branche bien plus grande qu'une autre (et donc, un mauvais stockage des données).

\index{arbre!bien équilibré (AVL)}
\begin{defi}
	Les arbres AVL sont des arbres binaires de recherche "bien équilibrés", c'est-à-dire que la différence de hauteur des fils en tout noeud est au plus $1$.
\end{defi}

Ces arbres sont donc équilibrés, et on a, en notant $h$ la hauteur de l'arbre, $h = O(\log n)$.