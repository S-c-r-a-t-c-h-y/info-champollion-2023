# Chapitre 2 - Langages formels

> [!note] On va formaliser la notion de **langage**.
Il faut être capable d'exprimer formellement les problèmes informatiques pour les résoudre dans un certain langage $\rightarrow$ ensemble de mot $\rightarrow$ syntaxe, sémantique, grammaire

> [!example] Exemples
> - compilation (ex: code C $\rightarrow$ code machine)
> - bio informatique
> - recherche de motif
## I - Mots

### A - Alphabet et mot

> [!def] Définition
> Un **alphabet** est un ensemble *fini* non vide. Les éléments sont appelés **symboles**, **lettres**, ou **caractères**. On le note souvent $\Sigma$ ou $A$.

> [!example] Exemples
> $\Sigma = \lbrace a \rbrace$
> $\Sigma = \lbrace a, b, \cdots, z \rbrace$
> $\Sigma =$ l'ensemble des caractères ASCII ou de l'Unicode
> $\Sigma = \lbrace A, G, T, C \rbrace$

> [!rem] Remarque
> La seule chose qui impacte est le cardinal.

> [!def] Définition
> Un **mot** sur un alphabet $\Sigma$ est une suite finie de symboles. Le mot $u = (u_{1}, u_{2}, \cdots, u_{n}) \in \Sigma^{n}$ est noté $u = u_{1}u_{2}\cdots u_{n}$. La longueur que l'on note $|u|$ est $n$. Le **mot vide** de longueur nulle est noté $\varepsilon$ ou $\Lambda$.

> [!rem] Remarque
> $u_{1}u_{2}\cdots u_{n} = \varepsilon$ si $n=0$

> [!tldr] Notation
> L'ensemble des mots de longueur $k \in \mathbb{N}$ est noté $\Sigma^{k}$. L'ensemble de tous les mots est noté $\Sigma^{*}$. L'ensemble des mots non vides est noté $\Sigma^+$.
> On a $\Sigma^{*} = \bigcup_\limits{k\in \mathbb{N}}\Sigma^{k}$ et $\Sigma^{+} = \Sigma^{*}\backslash\lbrace \varepsilon\rbrace = \bigcup_\limits{k\in \mathbb{N^{*}}}\Sigma^{k}$ 

> [!rem] Remarque
> On peut consiéder des mots infinis (X-ENS), on note $\Sigma^{\omega}$

> [!rem] Remarque
> $\Sigma^{*}$ est toujours de cardinal infini, même si $|\Sigma| = 1$. Il est dénombrable.
> On énumère tous les mots par ordre de longueur croissante puis pour une même longueur pas ordre lexicographique

> [!rem] Remarque
> On identifie abusivement $\Sigma$ et $\Sigma^{1}$

> [!def] Définition
> Pour $a \in \Sigma$ et $u \in \Sigma^{*}$, la longueur de $u$ en $a$ est le nombre $|u|_{a}$ d'occurences
 de la lettre $a$ en $u$.

> [!example] Exemple
> $|abbab|_{a}= 2$

### B - Concaténation

> [!def] Définition
> Soit $u, v \in \Sigma^{*}$ deux mots avec $u = u_{1}\cdots u_{n}$, $v = v_{1}\cdots v_{m}$. La concaténation de $u$ et $v$, notée $u \bullet v$ (ou plus simplement $uv$) est le mot $u_{1}\cdots u_{n}v_{1}\cdots v_{m}$ formé des lettres de $u$ suivies des lettres de $v$. Il est de longueur $n + m$

> [!example] Exemple
> $aba \bullet bab = ababab$

> [!rem] Remarque
> $\forall a \in \Sigma, \forall u, v \in \Sigma^{*}, |u \bullet v| = |u| + |v|$ et $|u \bullet v|_a = |u|_a + |v|_a$

> [!rem] Remarque
> $\forall u \in \Sigma^{*}, \varepsilon \bullet u = u \bullet \varepsilon = u$

> [!rem] Remarque
> $\forall u, v, w \in \Sigma^{*}, (u \bullet v) \bullet w = u \bullet (v \bullet w)$

> [!tip] Proposition
> $(\Sigma^{*}, \bullet, \varepsilon)$ est un monoïde, c'est à dire la loi de composition interne est associative et possède un élément neutre.

> [!rem] Remarque
> $|\cdot| : \Sigma^{*} \to \mathbb{N}$ est un morphisme de monoïde.

> [!rem] Remarque
> $\varepsilon$ est le seul mot inversible pour $(\Sigma^{*}, \bullet, \varepsilon)$ qui n'est donc pas un groupe.
> On peut cependant simplifier à droite et à gauche.
> Si $u, v, w \in \Sigma^{*}$ avec $uv = uw$ alors $v = w$ et si $vu = wu$ alors $v = w$

> [!rem] Remarque
> $(\Sigma^{*}, \bullet, \varepsilon)$ est commutatif si et seulement si $|\Sigma| = 1$

> [!rem] Remarque
> Pour $u, v \in \Sigma^{*}, uv = \varepsilon \Leftrightarrow u = v = \varepsilon$
> Pour $u \in \Sigma^{*}$ et $k \in \mathbb{N}^{*}$, on pose
> $\begin{cases} u^{0}= \varepsilon \\ u^{k} = u\bullet u^{k-1}= u^{k-1}\bullet u \end{cases}$
> $\forall k, l \in \mathbb{N}$ on a $u^{k}u^{l}= u^{k+l}$

## II - Préfixes, suffixes, facteurs et sous-mots

> [!def] Définition
> Si $u, v \in \Sigma^{*}$, on dit que :
> - $u$ est **préfixe** de $v$ s'il existe $w \in \Sigma^{*}$ tel que $v = uw$
> - $u$ est **suffixe** de $v$ s'il existe $w \in \Sigma^{*}$ tel que $v = wu$
> - $u$ est **facteur** de $v$ s'il existe $w_{1}, w_{2} \in \Sigma^{*}$ tel que $v = w_{1}uw_{2}$
> Le préfixe/suffixe/facteur $u$ est **propre** si $u \neq v$ et **non trivial** si de plus $u \neq \varepsilon$

> [!rem] Remarque
> $u$ est un préfixe/suffixe/facteur de lui-même

> [!rem] Remarque
> Un mot de longueur $n$ possède exactement $n+1$ suffixe et préfixe, et au plus $\binom{n}{2} +1$ facteurs.

>[!def] Définition 
>On dit que $u = u_1 u_2 u_3 \cdots u_n$ avec $\forall i \in [|1,n|],\ u_i \in \Sigma$ est un sous mot de $v$ ($k=0$ si $n = \epsilon$) s'il existe des mots $w_0,w_{1}, \cdots, w_n \in \Sigma^*$ tels que $v = w_0u_1w_{1}...u_nw_n$
>Intuitivement les lettres de $u$ apparaissent dans v dans le bon ordre.

> [!bug] Note à moi-même : ajouter def dans Anki

> [!warning] Attention
> sous-mots $\neq$ facteurs

> [!rem] Remarque
> Un facteur est un sous-mot mais la réciproque est fausse.

> [!warning] Attention
> Certains sujets utilisent le terme sous-mot pour facteur.

## III - Ordres

On suppose que $\Sigma$ est muni d'un ordre total noté $<$, qui est un ordre sur les symboles.
### 1 - Ordre lexicographique

> [!def] Définition
> L'ordre lexicographique est l'ordre usuel sur les mots dans le dictionnaire.
> On le définit de la manière suivante :
> Pour $u, v \in \Sigma^{*}$ on a $u \leq_{lex}v$ si :
> - soit $u$ est un préfixe de $v$
> - soit il existe $a, b \in \Sigma$ avec $a < b$, il existe $w, w', w'' \in \Sigma^{*}$ tels que $u = waw'$ et $v = wbw''$
> Le mot $w$ est le plus long préfixe commun à $u$ et $v$, noté $w = u \wedge v$ et $a, b$ sont les premières lettres qui diffèrent.

> [!rem] Remarque
> C'est aussi celui par défaut en OCaml
> ```ocaml
> "caml" < "python";;
> true
> ```

> [!rem] Remarque
> Dans le premier cas, si $u$ est préfixe de $v$, alors $u \wedge v = u$

> [!example] Exemple
> caml < camlia
> camol < camul

> [!tip]
L'ordre lexicographique prolonge l'ordre $(\Sigma, \leq)$ en un ordre total $(\Sigma^{*}, \leq_{lex})$

> [!rem] Remarque
Si $\Sigma$ est de cardinal au moins deux, $(a^{n}b)_{n\in \mathbb{N}}$ infinie strictement décroissante. $\lbrace a^{n}b, n\in \mathbb{N}\rbrace$ est une partie non vide qui n'admet pas de plus petit élément.
Cet ordre n'est pas bien fondé sur $\Sigma^{*}$

### 2 - Ordre hiérarchique

On compare d'abord par longueur puis par ordre lexicographique.

> [!def] Définition
> On définit l'ordre hiérarchique pour $u, v \in \Sigma^{*},\ u \leq v$ si :
> - soit $|u| < |v|$
> - soit $|u| = |v|$ et $u \leq_{lex} v$
> C'est un ordre total bien fondé.

> [!rem] Remarque
> C'est un ordre très utile pour énumérer les mots sur $\Sigma$.

## IV - Langages

> [!def] Définition
> On appelle langage sur $\Sigma$ une partie de $\Sigma^{*}$, c'est à dire un ensemble (fini ou non) de mots.

> [!example] Exemple
> Langages sur $\Sigma$ : $\varnothing, \Sigma^{*}, \Sigma, \Sigma^{+}, \lbrace \varepsilon \rbrace$

> [!example] Exemple
> Avec $\Sigma$ l'ensemble des caractères ASCII
> $L$ constitué des programmes OCaml syntaxiquement corrects.
> $L$ constitué des programmes OCaml syntaxiquement corrects bien typés.
> $L$ constitué des programmes OCaml syntaxiquement corrects bien typés qui terminent.

> [!tip] Opérations sur les langages
> Les langages étant des ensembles, on peut leur appliquer toutes les opérations ensemblistes :
> - la réunion
> - l'intersection
> - la différence
> - la différence symétrique
> - le complémentaire
> - etc...

> [!rem] Remarque
> On note souvent l'union par $+$ : $L_{1}+ L_{2} = L_{1}\cup L_{2}$

> [!def] Définition
> Soit $L, M \subseteq \Sigma^{*}$ deux langages. On définit la concaténation de $L$ et $M$, notée $L \bullet M$ ou plus simplement $LM$ par $L\bullet M = \lbrace u \bullet v\ |\ u \in L, v \in M \rbrace$

> [!example] Exemple
> $L = \lbrace a, ab, aba \rbrace$
> $M = \lbrace \varepsilon, a \rbrace$
> $LM = \lbrace a, ab, aba, a^{2}, aba^{2} \rbrace$

> [!rem] Remarque
> Cette opération n'est pas commutative

> [!rem] Remarque
> $(P(\Sigma^{*}), +, \varnothing)$ est un monoïde commutatif.
> $(P(\Sigma^{*}), \bullet, \lbrace \varepsilon \rbrace)$ est un monoïde (non commutatif en général)
> $(P(\Sigma^{*}), +, \bullet, \varnothing, \lbrace \varepsilon \rbrace)$ et un semi-anneau (non commutatif). On a $+$ distributive sur $\bullet$ et $\varnothing$ est absorbant pour $\bullet$
> On a donc $\varnothing \bullet M = \varnothing$ pour tout $M \subseteq \Sigma^{*}$

> [!rem] Remarque
> Comme pour les mots on définit une notion de puissance.
> Pour $M \subseteq \Sigma^{*}$ et $k \in \mathbb{N}^{*}$, on pose
> $\begin{cases} M^{0}= \lbrace\varepsilon\rbrace \\ M^{k} = M\bullet M^{k-1}= M^{k-1}\bullet M \end{cases}$
> $\forall k, l \in \mathbb{N}$ on a $M^{k}M^{l}= M^{k+l}$

> [!fail] Attention
> $L^{2}\neq \lbrace u^{2}, u \in L \rbrace$

> [!def] Définition
> L'**étoile de Kleene** de $L$ est le langage
> $L^{*} = \bigcup\limits_{n\in \mathbb{N}} L^{n}$
> Le langage $L^{+} = \bigcup\limits_{n\in \mathbb{N}^{*}} L^{n}$ est appelé **étoile de Kleene strict**.

> [!rem] Remarque
> On a ***toujours*** $\varepsilon \in L^{*}$ quel que soit $L$
> On a $L^{*} = L^{+} \cup \lbrace \varepsilon \rbrace$

> [!rem] Remarque
> $\varepsilon \in L^{+}$ ssi $\varepsilon \in L$
> On a toujours $L^{+}\subseteq L^{*}$

> [!warning] Attention
> $\varnothing^{*} = \lbrace \varepsilon \rbrace$

> [!rem] Remarque
> Pour $L \subseteq$ et $(M_{i})_{i\in \mathbb{N}} \in P(\Sigma^{*})^{\mathbb{N}}$
> $L\bullet \bigcup\limits_{i \in I}M_{i}= \bigcup\limits_{i \in I} (L \bullet M_{i})$
> $\bigcup\limits_{i \in I}M_{i} \bullet L = \bigcup\limits_{i \in I} (M_i \bullet L)$
> $L\bullet \bigcap\limits_{i \in I}M_{i} \subseteq \bigcap\limits_{i \in I} (L \bullet M_{i})$

> [!tip]
> L'ensemble des mots dont $u$ est préfixe est $\lbrace u \rbrace\bullet \Sigma^{*}$ ou $u\Sigma^{*}$
> L'ensemble des mots dont $u$ est suffixe est $\Sigma^{*} \bullet \lbrace u \rbrace$ ou $\Sigma^{*}u$
> L'ensemble des mots dont $u$ est facteur est $\Sigma^{*} \bullet \lbrace u \rbrace \bullet \Sigma^{*}$ ou $\Sigma^{*}u\Sigma^{*}$

> [!rem] Remarque
> $u\Sigma^{*}u \subseteq u\Sigma^{*}\cap \Sigma^{*}u$
> L'inclusion réciproque est fausse sauf si $u = \varepsilon$

> [!example] Contre-exemple
> Si $u \neq \varepsilon$ alors $u \in u\Sigma^{*} \cap \Sigma^{*}u$ mais $u \not\in u\Sigma^{*}u$

## V - Lemme de Levi

> [!tip] Lemme
> Soient $x, y, z, t \in \Sigma^{*}$ tels que $xy = zt$. Alors, $\exists!\ u \in \Sigma^{*}$ tel que $\begin{cases} x = zu \text{ et } t = uy \\ \text{ou} \\ z = xu \text{ et } y = ut \end{cases}$

> [!example] Application
> - Deux mots $x, y \in \Sigma^*$ commutent si $xy = yx$ (cf. exo 7)
> - Deux mots $u, v \in \Sigma^{*}$ sont conjugués s'il existe $x, y \in \Sigma^{*}$ tels que $u = xy$ et $v = yx$ (on dit aussi qu'ils sont rotation l'un de l'autre)

> [!tldr] Démonstration rapide
> - **unicité**
> Si $u$ et $u'$ sont deux solutions, alors $\begin{cases} x = zu \text{ et } x = zu' \\ \text{ou} \\ z = xu \text{ et } z = xu'\end{cases}$
> En particulier si $x = zu$ et $x = zu'$ alors $u = u'$ (simplification par $z$ à gauche)
> De même dans l'autre cas.
> - **existence**
> Si $|x| \geq |z|$, comme $zt = xy$, on a $z$ préfixe de $x$
> Donc $\exists\ u \in \Sigma^{*}$ tel que $x = zu$
> On a donc $zuy = zt$ donc $uy = t$
> Et de même si $|x| \leq |z|$ on se retrouve dans l'autre cas

> [!rem] Remarque
> **Simplication à gauche :**
> Preuve par récurrence.
> Si $z = \varepsilon$ et $zu = zv$ alors $u = v$
> Sinon $z = az'$ avec $az'u = az'v$ donc $z'u = z'v$ et par récurrence $u = v$

> [!tldr] Démonstration rigoureuse
>On peut poser $x = x_1x_{2}\cdots x_n$ avec $\forall i \in [[1,n]],\ x_i \in \Sigma$ et $z = z_1 \cdots z_n$ avec $\forall i \in [[1,n]],\ z_i \in \Sigma$
>Si |x| >= |z| alors m >= n 
>On pose u = x_n+1...x_ù
> Preuve par récurrence sur |xy|
> Montrons par récurrence forte sur n in N que si x,y,z,t in Sigma^* avec xy = zt et |xy| = n alors il existe u in Sigma^* tel que $\left\{x = zu\ \text{et}\ t = uy \right\}$
>$\left\{z = xu\ \text{et}\ y = ut \right\}$
>
>*Initialisation*
>Pour n =0, on a |xy| = 0 donc x=y=z=y=epsilon et u = epsilon convient
>
>*Hérédité* : On suppose le résultat vrai jusqu'au rang n et soit x,y,z,t in Sigma^* avec xy=zt et |xy|=n+1
>
>1er cas : si x = epsilon alors u = z convient et y=ut=zt=xy=epsilon y
>
>2ème cas : de même si z = epsilon par symétrie
>
>3ème cas : exists a, a' in Sigma, exists x',z' in Sigma^* tels que 
>x=ax'
>z=a'z'
>On a donc ax'y=a'z't donc a=a' et x'y = z't
>or |x'y|=x et x'y=z't
>Donc par HR, exists u' in Sigma^* tel que x'=z'u' et t =u'y
>ou z' = x'u' et y = u't
>
>On choisit u = u', donc on a 
>ax' = az'u et t = u'y
>az'=ax' et y=ut
