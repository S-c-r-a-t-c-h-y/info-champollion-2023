# Chapitre 13 - Introduction à la théorie de la complexité

> [!success] Info
> Ce chapitre est un complément du poly sur les classes de complexité P et NP

## I - Problèmes de décision et d'optimisation

### 1) Problème de décision

> [!def] Définition
> On appelle **problème de décision** la donnée d'une **instance** et d'une **question** dichotomique.
> *INSTANCE* : un objet possédant certaines caractéristiques
> *QUESTION* : une question sur cet objet dont la réponse est oui ou non
> 
>Formellement, un problème correspond à une fonction totale $f : \Sigma^* \to \mathbb B$ avec $u \in \Sigma^*$ l'**encodage** d'une instance du problème et $f(u)$ la **réponse** pour $u$ à cette instance.
>Cela correspond au langage $\{u \in \Sigma^* \mid f(u)\}$ des *instances positives*.
>Un problème de décision est donc un langage $L \subseteq \Sigma^*$

> [!rem] Remarque
> Si $f(u) = F$, alors soit $u$ n'est pas une instance positive, soit $u$ ne représente même pas une instance. En général, déterminer si un mot $u \in \Sigma^*$ est l'encodage d'une instance d'un problème est facile (c'est d'ailleurs un problème de décision que l'on peut résoudre au préalable).

> [!danger] TD 16-1
> PREMIER = $\{10, 11, 101, 111, \cdots\}$
> Taille de l'entrée : $\lceil \log_2 n\rceil$
> 
> TRIEE
> Taille de l'instance : $n \lceil \log_2 k \rceil$ avec $n$ la taille de la liste et $k$ le max de valeurs des éléments

> [!def] Définition
> Un algorithme $A : \Sigma^* \to \mathbb B$ **résout** le problème $L \subseteq \Sigma^*$ ssi $L = \{u \in \Sigma^* \mid A(u)\}$

### 2) Problème de recherche

> [!def] Définition
> On appelle **problème de recherche** la donnée d'une **instance** et la description des **solutions** possibles pour cette instance.
> *INSTANCE* : un objet possédant certaines caractéristiques
> *SOLUTION* : la structure d'une solution possible
> Il s'agit de trouver explicitement une solution pour l'instance.
>
> Formellement, il s'agit d'une relation binaire sur $\Sigma^*$ $\mathcal R \subseteq \Sigma^* \times \Sigma^*$
> L'ensemble des solutions associées à une instance $u \in \Sigma^*$ est l'image de $u$ par $\mathcal R$ : $\{v \in \Sigma^* \mid u \mathcal R v\}$
> Un algorithme $A : \Sigma^* \to \Sigma^*$ **résout** un problème de recherche $\mathcal R$ si pour tout mot d'entrée $u \in \Sigma^*, A(u)$ calcule un mot $v$ tel que $u \mathcal R v$ (c'est-à-dire trouve une solution $v$) s'il en existe une et indique qu'il n'en existe pas sinon.

> [!rem] Remarque
> Ceci convient qu'il y ait une seule solution possible ou plusieurs.

> [!info]
> A un problème de recherche on peut associer plusieurs problèmes de décision :

> [!def] Problème de l'existence d'une solution
Soit $\mathcal R \subseteq \Sigma^* \times \Sigma^*$ un problème de recherche. Le problème de décision existentiel associé est le problème définit par $f_\exists : \Sigma^* \to \mathbb B$ telle que $\forall u \in \Sigma^*, f_\exists(u) = V \iff \exists v \in \Sigma^*, u \mathcal R v$

> [!def] Problème de vérification d'une solution
Soit $\mathcal R \subseteq \Sigma^* \times \Sigma^*$ un problème de recherche. Le problème de vérification associé est $f_V : \Sigma^* \times \Sigma^* \to \mathbb B$ telle que $\forall (u, v) \in \Sigma^* \times \Sigma^*, f_V(u, v) = V \iff u \mathcal R v$
(est-ce que $v$ est bien solution pour $u$)

> [!rem] Remarque
> Pour avoir un vrai problème de décision, il faut trouver une bijection calculable entre $\Sigma^* \times \Sigma^*$ et $\Sigma^*$

### 3) Problèmes d'optimisation

> [!info] Motivation
Dans un problème de recherche, lorsqu'il y a plusieurs solutions, on peut s'intéresser à choisir "la meilleure" pour un certain critère.

> [!def] Définition
Un **problème d'optimisation** est un problème de recherche auquel on ajoute une fonction de coût que l'on cherche à minimiser.
*INSTANCE* : un objet avec certaine caractéristiques
*SOLUTION* : la structure des solutions possibles pour cette instance
*OPTIMISATION* : une fonction qui à une instance $x$ et une solution $y$ associe un coût $c(x, y)$ que l'on cherche à minimiser
>
Formellement, un problème d'optimisation est un problème de recherche $\mathcal R \subseteq \Sigma^* \times \Sigma^*$ et un fonction de coût $c : \Sigma^* \times \Sigma^* \to \mathbb R$
On cherche, pour $u \in \Sigma^*$ fixe un $v \in \{w \in \Sigma^* \mid u \mathcal R w\}$ qui minimise $c(u, v)$.
Un algorithme $A : \Sigma^* \to \Sigma^*$ **résout** ce problème d'optimisation si $\forall u \in \Sigma^*, A(u) = \underset{v \in \{w \in \Sigma^* \mid u \mathcal R w\}}{\text{argmin }} c(u, v)$ si un minimum existe et signale que non sinon.

> [!rem] Remarque
> - On peut encoder $\mathcal R$ dans $c$, par exemple en posant $c(u, v) = +\infty$ si $(u, v) \not \in \mathcal R$
> - On formule ici comme un problème de minimisation mais on peut aussi maximiser

> [!def] Problème de seuil
Considérons un problème d'optimisation $\mathcal R \subseteq \Sigma^* \times \Sigma^*$ et $c : \Sigma^* \times \Sigma^* \to \mathbb R$. Le problème de décision associé à ce problème d'optimisation est le problème de décision :
INSTANCE : une instance $u \in \Sigma^*$ du problème et un seuil $s \in \mathbb R$
QUESTION : existe-t-il une solution $v \in \Sigma^*$ au problème de recherche $\mathcal R$ pour $u$ telle que $c(u, v) \leq s$

> [!danger] TD 16-4
> Trouver le minimum d'un tableau d'entiers :
> problème d'optimisation (ou de recherche)
>
> Trier un tableau en place :
> pas un problème formalisé dans ce cours
>
> Le problème SAT :
> problème de décision

## II - La classe NP

> [!warning] Attention
> La classe NP ne s'applique qu'aux problèmes de décisions.
> Pour un problème d'optimisation, il faut considérer le problème de décision associé.

> [!def] Définition
> NP : Non-deterministic Polynomial
> Intuitivement, la **classe NP** correspond aux problèmes que l'on peut résoudre en temps polynomiale <u>si</u> on sait deviner une solution. Il suffit juste de vérifier que notre candidat est bien solution -> algorithmes **deviner et vérifier**

> [!rem] Remarque
> On appelle cela un **oracle**.

> [!rem] Remarque
De manière générale, il s'agit de vérifier (en temps polynomiale) qu'une solution qu'une instance est positive étant donné une preuve sous la forme d'un certificat polynomial (en temps polynomial).

> [!rem] Remarque
Dans 99% du temps, le problème de décision est un problème associé à un problème de recherche ou d'optimisation et le certificat est la solution candidate.

> [!warning] Attention
> Cette solution doit être de taille polynomiale en les entrées et vérifier que c'est bien une solution doit se faire en temps polynomial.

> [!def] Définition
> Un problème de décision $L \subseteq \Sigma^*$ est dans la classe NP si et seulement si il existe un algorithme $\mathcal V : \Sigma^* \times \Sigma^* \to \mathbb V$ appelé vérifieur, de complexité polynomiale en la taille de ses entrées tel que "$u \in L$ ssi $\exists v \in \Sigma^*$ de taille polynomiale en $u$ tel que $\mathcal V(u, v) = V$"
> Un $v \in \Sigma^*$ de taille polynomiale en $u$ est appelé un **certificat** pour $u$.
> 

> [!rem] Remarque
> Certains cours supposent que $(u, v) \to \mathcal V(u,v)$ est polynomiale en $|u|$. Dans ce cas le certificat est nécessairement polynomiale.

> [!example] Exemple
> SAT $\in$ NP
> Un certificat pour une instance $\varphi \in F$ est une solution $\nu : \mathbb V_\varphi \to \mathbb B$. La taille de $\nu$ est linéaire en le nombre de variables de $\varphi$ donc polynomiale en $|\varphi|$ et on sait vérifier que $\nu \models \varphi$ en $O(|\varphi|)$ polynomiale en $\varphi$.

> [!rem] Remarque
> Le vérificateur est la fonction d'évaluation $(\nu, \varphi) \to \hat \nu (\varphi)$

> [!example] Exemple
> Les échecs généralisés.
> On voit mal un certificat polynomial. Une stratégie gagnante va être de taille vraiment grande.


> [!tip] Proposition
> P $\subseteq$ NP

> [!tldr] Démonstration
> Soit $A : \Sigma^* \to \mathbb B$ qui résout un problème $L \in \Sigma^*$. Construisons $\mathcal V : \Sigma^* \times \Sigma^* \to \mathbb B$ polynomial tel que $\forall v \in \Sigma^*, A(u) \iff \exists v \in \Sigma^*$ polynomial en $u$ tel que $\mathcal V(u, v) = V$
> $\mathcal V$ procède ainsi pour $(u, v)$ :
> - jette $v$
> - renvoie $A(u)$
> 
> Pour $u \in \Sigma^*$, si $A(u) = V$ alors $\mathcal V(u, \varepsilon) = V$
> Inversement, s'il existe $v \in \Sigma^*$ (polynomial) tel que $\mathcal V(u, v) = V$ alors $A(u) = V$
> Donc $\mathcal V$ vérifie bien $L$ avec par exemple le certificat $\varepsilon$.


> [!rem] Remarque
> Dans la définition de problème NP, il faut bien sûr que tous les certificats soient en temps polynomial en la taille de l'instance pour le même polynôme.
> $L \in$ NP ssi il existe un vérificateur $\mathcal V : \Sigma^* \times \Sigma^* \to \mathbb B$ polynomial en la taille de ses entrées.
> C'est-à-dire, $\exists f \in \mathbb N[X]$ tel que $\forall u, v \in \Sigma^*, \mathcal V(u, v)$ est en temps $O(f(\max(|u|, |v|)))$ <u>et</u> un même polynôme $g \in \mathbb N[X]$ tel que $u \in L \iff \exists c \in \Sigma^*$ avec $|c| \leq q(|u|)$ et $\mathcal V(u, v) = V$


> [!tip] Proposition
> NP $\subseteq$ Exp

> [!tldr] Démonstration
> Soit $L \in$ NP, il existe un vérificateur $\mathcal V$ de complexité polynomiale pour un polynôme $f \in \mathbb N[X]$ et un polynôme $g \in \mathbb N[X]$ tel que $\forall u \in \Sigma^*, u \in L \iff \exists c \in \Sigma^*, |c| \leq g(|u|)$ et $\mathcal V(u, c) = V$
> Construisons un algorithme $A$, de complexité au pire exponentielle, qui détermine si $u \in L$ :
> énumère tous le mots $v \in \Sigma^*$ avec $|v| \leq g(|u|)$ et pour chacun on teste $\mathcal V(u, v)$.
> si on trouve $v \in \Sigma^*$ avec $|v| \leq g(|u|)$ tel que $\mathcal V(u, v) = V$ alors on renvoie correctement, sinon c'est que $\forall v \in \Sigma^*$, avec $|v| \leq g(|u|)$, $\mathcal V(u, v) = F$ et on renvoie correctement faux.


> [!help] Grande question de l'informatique :
P = NP ?

## III - Réduction et problèmes NP-complets

### 1) Réductions polynomiales

La définition suivante permet de montrer qu'un problème est "plus faible" qu'un autre en terme de complexité.

> [!def] Définition
> Soit $A\subseteq \Sigma^*$ et $B \subseteq \Sigma^*$ deux problèmes de décision. On dit que $A$ se **réduit en temps polynomial** à $B$, ce que l'on note $A \leq_p B$ s'il existe une fonction $f : \Sigma^* \to \Sigma^*$, qui peut se calculer en temps polynomial, telle que pour tout $u \in \Sigma^*$, $u \in A \iff f(u) \in B$.
> Autrement dit, $u$ est une instance positive pour $A$ si et seulement si $f(u)$ est une instance positive pour $B$.
> 

> [!rem] Remarque
> Puisque $f$ est de complexité polynomiale en $|u|$, $|f(u)|$ est polynomiale en $|u|$.

> [!info] Intuition
Intuitivement, $A$ se réduit veut dire que si on sait résoudre $B$, il ne faut qu'un temps polynomial supplémentaire pour résoudre $A$.

> [!tip] Proposition
> Soit $A, B \subseteq \Sigma^*$ deux problèmes de décision tels que $A \leq_p B$, alors :
> - si $B \in$ P, alors $A \in P$
> - si $B \in$ NP, alors $A \in NP$

> [!tldr] Démonstration
> Supposons $A \leq_p B$ et soit $f : \Sigma^* \to \Sigma^*$ telle que $\forall u \in \Sigma^*, u \in A \iff f(u) \in B$
> Supposons $B \in P$ et $Y$ un algorithme polynomial pour $B$.
> On peut proposer un algorithme $X$ qui résout $A$ en temps polynomial : $X(u)$ :
> calculer $f(u)$
> renvoyer $Y(f(u))$
> On a bien $X(u) = Y(f(u))$ car $A \leq_p B$

> [!example] Exemple
> Montrer que pour $m \leq n$, $m-SAT \leq_p n-SAT$ (réduction "cas particulier") -> $f = id$

### 3) NP-complétude

> [!def] Définition
> Soit $A \subseteq \Sigma^*$ un problème de décision, on dit que :
> - $A$ est **NP-difficile** (ou *NP-hard*) si pour tout $B \in NP, B \leq_p A$
> - $A$ est **NP-complet** si $A$ et NP et NP-difficile

![[np_graphe.png]]

> [!def] Définition
> Un problème est NP-difficile s'il est plus "difficile" que <u>tous</u> les problèmes NP.

> [!rem] Remarque
> On peut étendre la notion de NP-difficile aux algorithmes d'optimisation
> 


> [!tip] Proposition
> Si $A$ est NP-complet et si $A \in P$, alors $P = NP$

> [!tldr] Démonstration
> On sait que $P \subseteq NP$
> Si $B \in$ NP, alors comme $A$ est NP-difficile, on a $B \leq_p A$. De plus comme $A \in P$, on a $B \in P$ donc $NP \subseteq P$


> [!tip] Proposition
> Si $A$ est NP-difficile et si $A \leq_p B$, alors $B$ est NP-difficile.

> [!tldr] Démonstration
> Soit $C \in$ NP, on a $C \leq_p A$ et par transitivité $C \leq_p B$


> [!info]
> Pour montrer qu'un problème $A$ est NP-complet :
> - on montre que $A\in NP$
> - on trouve un problème $B$ NP-difficile tel que $B \leq_p A$

> [!help] Problème 
> Il nous faut un premier problème NP-complet


> [!tip] Théorème de Cook-Levin 1971
> SAT est NP-complet

> [!tldr] Démonstration
> Admis.
> On représente tout ordinateur par un énorme circuit, puis ce circuit par une grosse formule satisfiable si et seulement si le calcul vérifie certaines propriétés.


> [!def] Définition
> Un problème est **NP-difficile** s'il est plus difficile que tous les problèmes NP, c'est-à-dire que tout problème NP s'y réduit en temps polynomial.

> [!tip] Proposition
> Si $A \in$ NP-complet $\cap$ P alors P = NP

> [!info] Méthode
> Pour montrer qu'un problème $A$ est NP-complet
> - On montre que $A$ est NP
> 	On explicite une vérification $\mathcal V$ polynomial pour lequel il existe un certificat polynomial exactement pour les instances positives
> - On montre que $A$ est NP-difficile
> 	Pour cela on propose un problème $B$ NP-difficile connu et on montre que $B \leq_p A$

> [!tip] Proposition
> CNF-SAT est NP-complet
> (CNF = forme normale conjonctive)

> [!tldr] Démonstration
> - CNF-SAT $\in$ NP toujours comme certificat une valuation (polynomial) et comme vérificateur la fonction d'évaluation (polynomial)
>-  Montrons que $SAT \leq_p CNF-SAT$
>
> Idée qui ne marche **pas** :
> A une instance $\varphi \in F$ de SAT, on lui associe sa FNC avec l'algorithme du TP n° ? :
> - éliminer les implications et les équivalences
> - faire descendre les négations
> - faire descendre les disjonctions
> 
> On obtient une formule $\bar \varphi$ en FNC équivalente à $\varphi$, or la transformation $\varphi \to \bar \varphi$ est exponentielle dans le pire cas.
>
>Bonne idée :
>A une formule $\varphi$ on associe la formule $T(\varphi)$ la transformée de Tsechin de $\varphi$.
>D'après le DM 4, cette transformation est polynomiale en la taille de la formule et produit une formule $T(\varphi)$ de taille polynomiale équisatisfiable

> [!tip] Proposition
> La réduction est correcte car $\varphi$ satisfiable ssi $T(\varphi)$ satisfiable.

> [!tip] Proposition
> Pour $n \geq 3$, $n-$SAT est NP-complet

> [!tldr] Démonstration
> Si $n \geq 3$, on a $3-$SAT $\leq$ $n-$SAT (cas particulier). Il suffit de montrer que $3-$SAT est NP-difficile.
> Montrons que $3-$SAT est NP-difficile, pour cela montrons que CNF-SAT $\leq_p$ $3-$SAT
> Si $\varphi$ est une formule en FNC. Soit $c$ une clause de $\varphi$ de taille strictement plus que $3$.
> $c=(l_1 \lor l_2 \lor \cdots \lor l_k)$ (avec $k \geq 3$) : on introduit de nouvelles variables $y_2, \cdots, y_{k-1}$ (utilisées nul part ailleurs) et on pose $\bar c$ la formule $\bar c = (l_1 \lor l_2 \lor y_2) \land (\bar y_2 \lor l_2 \lor y_3) \land \cdots \land (\bar y_{k-2} \lor l_{k-1} \lor l_k)$
> On fait cela pour chaque clause (avec des $y$ différents pour chaque clause) pour construire une formule $\bar \varphi$.
> On obtient une formule de taille polynomiale et cette transformation est polynomiale.

> [!rem] Remarque
> Si une clause $c$ est satisfaite par une valuation $v$, on construit une valuation $\bar v$ qui prolonge $v$ de la manière suivante : 
> comme $c$ est satisfiable, $\exists i \in [[1, k]], v(l_i) = V$. On pose pour $j \in [[2, k-1]], \bar v(y_i) = V$ et $\bar v (y_j) = F$.
> Toute clause de $\bar c$ est satisfaite par $\bar v$

