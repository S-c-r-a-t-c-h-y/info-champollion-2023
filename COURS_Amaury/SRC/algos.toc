\contentsline {section}{\numberline {1}Algorithmes classiques}{3}{section.0.1}%
\contentsline {subsection}{\numberline {}Recherche du minimum d'un tableau}{3}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {}Algorithme d'Euclide}{3}{subsection.0.1.2}%
\contentsline {subsection}{\numberline {}Exponentiation rapide}{3}{subsection.0.1.3}%
\contentsline {subsection}{\numberline {}Insertion d'un élément dans une liste triée}{4}{subsection.0.1.4}%
\contentsline {subsection}{\numberline {}Produit matriciel naïf}{4}{subsection.0.1.5}%
\contentsline {subsection}{\numberline {}Evaluation de polynôme via la méthode de Horner}{4}{subsection.0.1.6}%
\contentsline {subsection}{\numberline {}Décomposition en base $b$}{4}{subsection.0.1.7}%
\contentsline {subsection}{\numberline {}Recherche par dichotomie dans un tableau trié}{5}{subsection.0.1.8}%
\contentsline {subsection}{\numberline {}Calcul du miroir d'une liste en temps linéaire en sa taille}{5}{subsection.0.1.9}%
\contentsline {subsection}{\numberline {}MinMax}{6}{subsection.0.1.10}%
\contentsline {subsection}{\numberline {}Quine}{8}{subsection.0.1.11}%
\contentsline {section}{\numberline {2}Algorithmes de tri}{10}{section.0.2}%
\contentsline {subsection}{\numberline {}Tri par insertion}{10}{subsection.0.2.1}%
\contentsline {subsection}{\numberline {}Tri par sélection}{11}{subsection.0.2.2}%
\contentsline {subsection}{\numberline {}Tri fusion}{12}{subsection.0.2.3}%
\contentsline {subsection}{\numberline {}Tri par tas}{13}{subsection.0.2.4}%
\contentsline {subsection}{\numberline {}Tri rapide}{14}{subsection.0.2.5}%
\contentsline {section}{\numberline {3}Algorithmes sur les graphes}{15}{section.0.3}%
\contentsline {subsection}{\numberline {}Parcours en profondeur}{15}{subsection.0.3.1}%
\contentsline {subsection}{\numberline {}Parcours en largeur}{16}{subsection.0.3.2}%
\contentsline {subsection}{\numberline {}Dijkstra}{17}{subsection.0.3.3}%
\contentsline {subsection}{\numberline {}A*}{18}{subsection.0.3.4}%
\contentsline {subsection}{\numberline {}Floyd-Warshall}{20}{subsection.0.3.5}%
\contentsline {subsection}{\numberline {}Kruskal}{21}{subsection.0.3.6}%
\contentsline {subsection}{\numberline {}Tri topologique}{23}{subsection.0.3.7}%
\contentsline {subsection}{\numberline {}Détection de cycle dans un graphe orienté}{24}{subsection.0.3.8}%
\contentsline {subsection}{\numberline {}Kosaraju}{25}{subsection.0.3.9}%
\contentsline {subsection}{\numberline {}Calcul de couplage maximal dans un graphe biparti}{27}{subsection.0.3.10}%
\contentsline {section}{\numberline {4}Algorithmes sur les automates}{28}{section.0.4}%
\contentsline {subsection}{\numberline {}Elimination des états}{28}{subsection.0.4.1}%
\contentsline {subsection}{\numberline {}Berry-Sethi}{30}{subsection.0.4.2}%
\contentsline {section}{\numberline {5}Algorithmes textuels}{31}{section.0.5}%
\contentsline {subsection}{\numberline {}Huffman}{31}{subsection.0.5.1}%
\contentsline {subsection}{\numberline {}Boyer Moore}{33}{subsection.0.5.2}%
\contentsline {subsection}{\numberline {}Rabin-Karp}{35}{subsection.0.5.3}%
\contentsline {subsection}{\numberline {}LZW}{37}{subsection.0.5.4}%
\contentsline {section}{\numberline {6}Algorithmes de synchronisation}{39}{section.0.6}%
\contentsline {subsection}{\numberline {}Peterson}{39}{subsection.0.6.1}%
\contentsline {subsection}{\numberline {}Boulangerie de Lamport}{39}{subsection.0.6.2}%
\contentsline {section}{\numberline {7}Algorithmes d'apprentissage automatique}{41}{section.0.7}%
\contentsline {subsection}{\numberline {}$k$ plus proches voisins}{41}{subsection.0.7.1}%
\contentsline {subsection}{\numberline {}ID3}{42}{subsection.0.7.2}%
\contentsline {subsection}{\numberline {}$k$ moyennes}{43}{subsection.0.7.3}%
\contentsline {subsection}{\numberline {}Classification hiérarchique ascendante}{44}{subsection.0.7.4}%
\contentsline {section}{\numberline {8}Structures de données}{45}{section.0.8}%
\contentsline {subsection}{\numberline {}Implémentation d'une pile par liste chaînée}{45}{subsection.0.8.1}%
\contentsline {subsection}{\numberline {}Implémentation d'une file par liste chaînée}{46}{subsection.0.8.2}%
\contentsline {subsection}{\numberline {}Implémentation d'une file par tableau circulaire}{47}{subsection.0.8.3}%
\contentsline {subsection}{\numberline {}Unir et trouver}{48}{subsection.0.8.4}%
\contentsline {subsection}{\numberline {}Arbre binaire de recherche}{50}{subsection.0.8.5}%
\contentsline {subsection}{\numberline {}Arbre AVL}{52}{subsection.0.8.6}%
\contentsline {subsection}{\numberline {}Arbres rouge-noir}{54}{subsection.0.8.7}%
\contentsline {subsection}{\numberline {}Tas min}{55}{subsection.0.8.8}%
\contentsfinish 
