# Chapitre 10 - Théorie des jeux

> [!success] Info
> Ceci est une toute petite introduction au grand domaine de la théorie des jeux.
> On s'intéresse à l'étude théorique et algorithmique de jeux à deux joueurs, et en particulier à l'élaboration de stratégies.

> [!tldr] Quelques dates
> - 1952 : premier jeu -> Tic Tac Toe
> - 1997 : Deep Blue bat le champion d'échecs Garry Kasparov
> - 2017 : AlphaGo bat le champion du monde de Go Ke Jic
> - 2019 : Alpha Star bat un champion du monde de Starcraft 2

> [!info] Cadre d'étude
> On ne va considérer que des jeux à deux joueurs, qui jouent alternativement à **information parfaite** : à tout moment les deux joueurs ont la connaissance complète et exhaustive du jeu. On ne vas considérer que des jeux à somme nulle : la somme des gains est nulle et même de type victoire/échec et match nul

## I - Jeux à deux joueurs

> [!example] Exemple : Tic Tac Toe

On peut modéliser ce jeu par un **arbre des configurations possibles**.
Chaque noeud représente un état qui comporte toute l'information ainsi que le joueur à qui c'est le tour de jouer (on dit que le joueur contrôle l'état)

> [!def] Définition
> Les fils d'un état sont les états auxquels on peut accéder en jouant un coup.

> [!example] Tic Tac Toe
> Pour le Tic Tac Toe, il y a 549 946 noeuds dans l'arbre.

On va plutôt représenter un jeu par un graphe biparti (en général acyclique).
Il y a alors 5478 sommets et 16167 arcs.

> [!rem] Remarque
> Plusieurs chemins peuvent mener d'une configuration à une autre.

Un état est contrôlé par l'un des deux joueurs.
Un arc $x \to y$ entre un sommet contrôlé par $X$ et un sommet contrôlé par $Y$ correspond à un coup possible à partir de la configuration $x$.

> [!def] Définition
Les puits du graphe sont appelés **états terminaux**. Ce sont les état gagnants pour un joueur, pour l'autre ou match nul.

> [!example] Exemple
> Pour le Tic Tac Toe, il y a 968 états terminaux dont 626 gagnants pour $X$, 316 gagnants pour $Y$ et 16 matchs nuls.

> [!def] Définition
> Un jeu à deux joueur est un quadruplet $J = (G, s_0, T_1, T_2)$ avec :
> - $G = (S = S_1 \sqcup S_2, A)$ un graphe biparti orienté (acyclique)
> - $s_0 \in G$ appelé état initial
> - $T_1 \subseteq S$ et $T_2 \subseteq S$ appelés états gagnants pour le joueur 1 et le joueur 2 respectivement des puits de $G$
>
> Les sommets de $S_1$ sont appelés **états** du joueur 1 (ou contôlés par le joueur 1), c'est à dire que c'est à 1 de jouer, et de même pour 2.
> Les puits de $G$ sont appelés **états terminaux**. On note $T$ les puits de $G$. On a $T = T_1 \sqcup T_2 \sqcup T_=$ avec $T_=$ les états terminaux correspondant à des matchs nuls.
> On note $X_1 = S_1 \setminus T$ l'ensemble des sommets contrôlés par le joueur 1 non terminaux.

> [!rem] Remarque
> Si le graphe est acyclique, il y a au moins un puits

> [!def] Définition
> Une **partie** d'un jeu $J = ((S = S_1 \sqcup S_2, A), s_0, T_1, T_2)$ est un chemin fini du graphe $(S = S_1 \sqcup S_2, A)$ qui part de $s_0$ et qui arrive sur un sommet $t \in T$.
> La partie est gagnée par le joueur 1 si $t \in T_1$, gagné par le joueur 2 si $t \in T_2$ et nulle si $t \in T_=$

> [!def] Définition
> Une **stratégie** pour le joueur $i \in \{1, 2\}$ est une fonction $f : X_i \to X_{3-i}$  telle que pour tout $x \in X_i$ non terminal contrôlé par le joueur $i, f(x) \in S_{3 - i}$, indique le coup à jouer à partir du sommet $x$. $(x, f(x)) \in A$

> [!rem] Remarque
> Ici une stratégie ne considère que l'état actuel. On parle de stratégie **sans mémoire**.

> [!def] Définition
> Une partie $s_0 \to s_1 \to \cdots \to s_n$ est jouée suivant une stratégie $f$ pour le joueur $i$ si pour tout $K \in [[0, n-1]]$, si $s_K \in X_i$ alors $f(s_K) = s_{K+1}$.
> C'est à dire si tout coups pour le joueur $i$ est joué en utilisant $f$.

> [!def] Définition
> On dit qu'une stratégie $f$ pour le joueur $i \in \{1, 2\}$ est **gagnante** pour le joueur $i$ à partir d'une position $s \in S$ si toute partie de $G = (S, s, T_1, T_2)$ qui commence en ce sommet $s \in S$ jouée suivant $f$ est gagnante.
> Un tel sommet $s$ est appelé **position gagnante**.

> [!rem] Remarque
> $f$ est une stratégie gagnante à partir de $s$ si quel que soit l'adversaire, jouer en suivant $f$ permet de gagner à coup sûr.

> [!def] Définition
> Un sommet $s \in S$ est une **position gagnante** s'il existe une stratégie pour laquelle cette position est gagnante.

> [!def] Définition
> Une **stratégie gagnante** pour le joueur $i \in \{1, 2\}$ pour un jeu $J = (G, s_0, T_1, T_2)$ est une stratégie gagnante pour ce joueur à partir de la position initiale $s_0$. Elle existe si et seulement si $s_0$ est une position gagnante pour ce joueur.

> [!rem] Remarque
> L'ensemble des sommets se partitionne en :
> - les positions gagnantes pour 1
> - les positions gagnantes pour 2
> - les autres

## II - Calcul des attracteurs

> [!def] Définition
> L'**attracteur** d'un joueur $i$ est l'ensemble des positions gagnantes pour $i$. On le note $A(i) \subseteq S$.

On vient de voir un moyen de calculer les attracteurs pour les deux joueurs.

> [!def] Définition
Pour $i \in \{1, 2\}$ et pour $n \in \mathbb N$ on définit par récurrence la suite $A_n(i) \subseteq S$ une suite d'ensembles de positions qui gagnent en au plus $n$ étapes.
Pour $n=0$ et $i \in \{1, 2\}$, $A_0(i) = T_i$
Pour $n \in \mathbb N, A_{n+1}(i) = A_n(i) \cup \{y \in S_{3-1} | \exists x \in A_n(i), (x, y) \in A\} \cup \{x \in X_{3-i}| \forall y \in S, (x, y) \in A \Rightarrow y \in A_n(i)\}$

> [!tip] Proposition
> $A(i) = \bigcup\limits_{n \in \mathbb N}A_n(i)$ et $\forall x \in A(i),\exists n \in \mathbb N, x \in A_n(i)$

> [!rem] Remarque
> A deux stratégies $f_1$ et $f_2$ pour les joueurs 1 et 2 correspond une unique partie $s_0 \to f_1(s_0) \to f_2(f_1(s_0)) \to \cdots$

> [!tldr] Construction d'une stratégie "optimale" (au sens où elle gagne le plus vite possible ou perd le moins vite possible) :
> Construisons $f_i : X_i \to S_{3-1}$
> Pour $x \in X_i$
> - Si $x \in A(i)$, soit $n \in \mathbb N$ le plus petit $n$ tel que $x \in A_n(i)$. On a $n > 0$ car $x \not\in T$. Par définition de $A_n(i)$, pour que $x \not \in A_{n-1}(i)$, $\exists y \in A_{n-1}(i)$ tel que $(x, y) \in A$. On pose $f(x) = y$
> - Si $x \in A(3-1)$, on considère $n \in \mathbb N$ le plus petit $n$ tel que $x \in A_n(3-i)$. On a $n > 0$. Par construction, $\forall y \in V(x), y \in A_{n-1}(i)$. $\exists y \in V(x)$ tel que $y \not \in A_{n-2}(3-i)$ sinon on aurait $x \in A_{n-1}(3-i)$ absurde par choix de $n$. Soit $y$ un tel voisin, on pose $f(x)=y$
> - Si $x \not\in A(i) \cup A(3-i)$ alors $\exists y \in S_{3-i}\setminus A(3-i)$ et on pose $f(x) = y$

## III - MinMax et heuristiques

### 1) Algorithme MinMax

Une autre manière équivalente de déterminer les positions gagnantes est d'associer un score à chaque état : 
- $+\infty$ si la position est gagnante pour le joueur 1
- $- \infty$ si la position est gagnante pour le joueur 2
- $0$ si la position n'est gagnante pour aucun des deux
L'attracteur du joueur 1 est alors l'ensemble des positions de score $+ \infty$

Déterminer si une position est gagnante revient à déterminer son score.
Si le graphe est acyclique cela peut se faire de la manière suivante récursive :
- si $s$ est terminal son score est $+ \infty$, $0$ ou $- \infty$ suivant que la configuration est gagnante, nulle ou perdante.
- si $s$ est contrôlé par 1 alors son score est $\max\{\textrm{score(y)} \mid y \in V(s)\}$
- si $s$ est contrôlé par 2 alors son score est $\min\{\textrm{score(y)} \mid y \in V(s)\}$

> [!rem] Remarque
> Le joueur 1 est souvent appelé le joueur max et le joueur 2 le joueur min

> [!rem] Remarque
> Pour que le calcul soit efficace il faut mémoriser les configurations pour ne par recalculer plusieurs fois la même chose : mémoïsation

> [!rem] Remarque
> En pratique on le fait rarement.

### 2) Heuristique

> [!info] Info
Dans la majorité des cas, les configurations possibles sont trop nombreuses pour calculer les attracteurs ou effecteur un MinMax en entier.

> [!example] Exemple
> Au Domineering, il y a 4632 états dans une grille 4x4, un million pour une grille 5x5, ($40 \times 10^{16}$) pour le jeu classique 8x8.
> Au Go on estime à $10^{130}$ le nombre de configurations.

> [!def] Définition
> On va introduire la notion d'**heuristique**. Une heuristique évalue la "qualité" d'une configuration. C'est une fonction de $S \to \overline{\mathbb R}$. On impose en général que si $h(e) = +\infty$ alors $e \in A(1)$ et si $h(e) = - \infty$ alors $e \in A(2)$
> Une heuristique doit être une estimation de la situation facile à calculer.

> [!example] Exemples d'heuristiques
> - Le nombre de coups possibles pour le joueur 1 moins le nombre de coups possibles pour le joueur 2.
> - On joue 10^6 parties aléatoires à partir de cette configuration et on renvoie la différence du nombre de parties gagnées. (méthode de Monte-Carlo)

- On choisit une profondeur maximale d'exploration $p \in \mathbb N$ (on ne considère que le sous-graphe des configurations de distance au plus $p$ de $s_0$)
- On applique l'algorithme MinMax à ceci près que si on atteint la profondeur maximale on traite les états comme terminaux en utilisant l'heuristique comme score.

### 3) Elagage alpha beta

> [!help] Principe
> Comme dans un algorithme de retour sur trace (backtracking), l'algorithme MinMax explore l'intégralité de l'arbre jusqu'à une profondeur $p$. Dans certaines situations,, il est possible d'élaguer. On va garder en mémoire une plage $[\alpha, \beta]$ des scores "utiles". Si on a la garantie que le score d'un nœud sort de cette plage, alors il est inutile de poursuivre son exploration.

> [!def] Définition
> L'**élagage** $\alpha$ permet de couper des branches lorsqu'on cherche à calculer le score d'un noeud Min.
> L'**élagage** $\beta$ permet de couper des branches lorsqu'on cherche à calculer le score d'un noeud Max.

> [!rem] Remarque
> Les score obtenus peuvent différer avec ceux de l'approche MinMax sans élagage mais celui de la racine reste le même.

> [!rem] Remarque
> L'ordre dans lequel on parcourt les fils a une importance. Il vaut mieux explorer les noeuds les plus prometteurs en premier. Cela permet d'élaguer davantage.

