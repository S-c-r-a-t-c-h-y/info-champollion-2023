# Chapitre 3 - Langages réguliers

> [!note] Lien utile
> [Regex 101](https://regex101.com/)

## I - Motivation

> [!example] Motivation 1
> - Mon collègue de maths m'a envoyé un mail en début d'année => Ctrl+F Quibel
> - Ma collègue de physique => Ctrl+F C----ier
> On fait un Ctrl+F amélioré : C.\*(o|l)\*.\*ier

> [!example] Motivation 2
> ACGGTACTGACGTC
> Quelles sont les sous-séquences de la forme AGT???GAC?

> [!example] Motivation 3
> - Trouver tous les `print_int` qui sont imbriqués dans au moins une boucle et pas les autres
> - Trouver les liens hypertexte dans un fichier pour les colorer

## II - Opérations régulières

> [!def] Définition
> Les trois opérations **réunion**, **concaténation** et **étoile de Kleene** sont appelés **opérations régulières**.

> [!help] Question
>On peut se poser la question de la classe de langage que l'on peut obtenir à partir de langages tout simple et uniquement de ces trois générateurs.
>Cela donne naissance à ce que l'on appelle les **langages réguliers.**

> [!def] Définition
> Soit $\Sigma$ un alphabet. La **classe des langages** <u>réguliers</u> ou <u>rationnels</u>, notée $Reg(\Sigma)$ est défini par induction par :
> $\begin{cases} \varnothing \in Reg(\Sigma) \\ \lbrace \varepsilon \rbrace \in Reg(\Sigma) \\ \lbrace a \rbrace \in Reg(\Sigma) \text{ pour } a \in \Sigma \\ L_{1}, L_{2} \in Reg(\Sigma) \Rightarrow L_{1}\cup L_{2} \in Reg(\Sigma) \\ L_{1}, L_{2} \in Reg(\Sigma) \Rightarrow L_{1}\bullet L_{2} \in Reg(\Sigma) \\ L \in Reg(\Sigma) \Rightarrow L^{*} \in Reg(\Sigma) \end{cases}$
> Les langages réguliers sont donc exactement les langages que l'on peut construire à partir des langages $\varnothing,\ \lbrace \varepsilon \rbrace,\ \lbrace a \rbrace$ pour $a \in \Sigma$ et par opérations régulières.

> [!rem] Rappel
> "par induction" veut dire que $Reg(\Sigma)$ est *la plus petite classe de langages* qui contient $\varnothing,\ \lbrace \varepsilon \rbrace$, tous les langages $\lbrace a \rbrace$ pour $a \in \Sigma$ et qui soit *stable* par opérations régulières.

> [!rem] Remarque
> $\lbrace \varepsilon \rbrace$ est redondant car $\varnothing^{*} = \lbrace \varepsilon \rbrace$

> [!tip] Propriété
> $\Sigma^{*}$ est régulier.

> [!tip] Propriété
> Tout langage fini est régulier.

> [!rem] Remarque
> Si toute réunion (même infinie) de langages réguliers est un langage régulier, alors tous les langages seraient réguliers.

> [!warning] Attention
> Les langages réguliers ne sont pas stables par réunion infinie, seulement finie.

> [!rem] Remarque
> <u>régulier</u> : nouveau programme
> <u>rationnel</u> : ancien programme

## III - Expressions régulières

> [!success] Motivation
> On va se donner une *notation* pour dénoter/décrire simplement les langages réguliers ainsi que pour spécifier des *motifs* à recherche dans un texte.

> [!rem] Remarque
> En anglais : regular expression (regexp ou regex ou re)

> [!def] Définition
> Soit $\Sigma$ un alphabet. On définit inductivement les expressions régulières, notées $\mathcal{E}(\Sigma)$ par :
> $\begin{cases} \varnothing \in \mathcal{E}(\Sigma) \\ \epsilon \in \mathcal{E}(\Sigma) \\ a \in \mathcal{E}(\Sigma)\ \forall a \in \Sigma \\ e_{1}, e_{2} \in \mathcal{E}(\Sigma) \Rightarrow (e_{1}| e_{2}) \in \mathcal{E}(\Sigma) \\ e_{1}, e_{2} \in \mathcal{E}(\Sigma) \Rightarrow (e_{1} \bullet e_{2}) \in \mathcal{E}(\Sigma) \\ e \in \mathcal{E}(\Sigma) \Rightarrow e^{\star} \in \mathcal{E}(\Sigma) \end{cases}$
> Une expression régulière est donc une formule construite à partir des cas de bases et des opérations $|,\ \bullet$ et $\star$

> [!rem] Remarque
> $(e_{1}| e_{2})$ se lit $e_{1}$ ou $e_{2}$
> > [!warning] Attention
> > On trouve souvent $e_{1}$ ☩ $e_{2}$

> [!example] En OCaml
> ```ocaml
> type regexp =
> 	| Empty
> 	| Epsilon
> 	| Lettre of char
> 	| Or of regexp * regexp
> 	| Concat of regexp * regexp
> 	| Star of regexp
> ```

> [!example] Exemple
> $((a^{\star}\bullet b)|c)$ sur $\Sigma = \lbrace a, b, c \rbrace$
> ```ocaml
> let e = Or (Concat (Star (Lettre 'a'), Lettre 'b'), Lettre 'c')
> ```

> [!rem] Remarque
> Arbre associé à une expression régulière :
> ```mermaid
> flowchart TD
> A["|"] --> B["."] --> C["*"] --> D["a"] 
> B --> E["b"]
> A --> F["c"]
> ```

> [!danger] Exercice
> Ecrire une fonction `taille : regexp -> int` et une fonction `hauteur : regexp -> int` qui calcule respectivement la taille et la hauteur d'une expression régulière.

> [!rem] Remarque
> Le parenthésage donne une syntaxe très lourde.

> [!tldr] Règles d'abus
> - On enlève les $\bullet$ concaténation implicites : $(a \bullet b) \to ab$
> - On enlève les parenthèses par associativité de $|$ et $\bullet$ : $(a\bullet b) \bullet a \to aba$
> - On utilise les exposants : $((a\bullet a) \bullet a) \to a^{3}$
> - On définit des priorités : $\star$ prioritaire sur $\bullet$ prioritaire sur $|$ : $((a \bullet b) | c) | a^{\star} \to ab | c | a^{\star}$
> - On introduit l'exposant $+$ : $(a\bullet a^{\star}) \to a^{+}$ et  $(a^{\star}\bullet a) \to a^{+}$

> [!def] Définition
> Une expression régulière est **linéaire** si chaque caractère de $\Sigma$ apparaît au plus une fois. On a donc $e \in \mathcal{E}(\Sigma)$ linéaire ssi $\forall\ a \in \Sigma, |e|_{a} \leq 1$

> [!warning] Attention
> $a^{+}$ n'est pas linéaire
> Les abus ne doivent être utilisés que pour simplifier des écritures lorsqu'il n'y a pas ambiguïté.
> Toujours revenir à la formalisation si on étudie des propriétés.

## IV - Des expressions aux langages réguliers

> [!def] Définition
> A toute expression régulière $e \in \mathcal{E}(\Sigma)$, on associe un langage $L(e)$ appelé **langage dénoté par e**, par induction :
> - Si $e = \varnothing$ alors $L(e) = \varnothing$
> - Si $e = \epsilon$ alors $L(e) = \lbrace \varepsilon \rbrace$
> - Si $e = a$ avec $a \in \Sigma$ alors $L(e) = \lbrace a \rbrace$
> - Si $e = (e_{1}|e_{2})$ avec $e_{1}, e_{2}\in \mathcal{E}(\Sigma)$ alors $L(e) = L(e_{1}) \cup L(e_{2})$
> - Si $e = (e_{1}\bullet e_{2})$ avec $e_{1}, e_{2}\in \mathcal{E}(\Sigma)$ alors $L(e) = L(e_{1}) \bullet L(e_{2})$
> - Si $e = e_{1}^{\star}$ avec $e_{1}\in\mathcal{E}(\Sigma)$ alors $L(e) = L(e_{1})^{*}$

> [!rem] Remarque : abus de notation
> $L(ab^{*}|b) = ab^{*}+b$

> [!tip] Proposition
> Un langage $L \subseteq \Sigma^{*}$ est une langage régulier ssi il est dénoté par une expression régulière, c'est à dire ssi il existe une expression régulière $e \in \mathcal{E}(\Sigma)$ tel que $L = L(e)$.
> On dit que $e$ dénote $L$ et que $L$ est dénoté par $e$.

> [!rem] Remarque
> Attention $e$ n'est pas unique
> > [!example] Exemple
> > $L((a|b)) = L((b|a))$

> [!rem] Remarque
> C'est souvent la définition donnée pour les langages réguliers (définition alternative).

Par induction
Dans un sens si un langage est régulier alors il existe une expression régulière qui la dénote
Par induction :
- Si $L = \varnothing$, alors $L = L(e)$
- Si $L = \lbrace \varepsilon \rbrace$, alors $L = L(\epsilon)$
- Si $L = \lbrace a \rbrace$, alors $L = L(a)$
- Si $L = L_{1}\cup L_{2}$, avec $L_{1}, L_{2}$ réguliers, alors par induction $\exists\ e_{1}, e_{2}\in \mathcal{E}(\Sigma)$ telles que$L_{1} = L(e_{1})$ et $L_{2} = L(e_{2})$ et $L = L((e_{1}|e_{2}))$
- Si $L = L_{1} \bullet L_{2}$, avec $L_{1}, L_{2}$ réguliers, alors par induction $\exists\ e_{1}, e_{2}\in \mathcal{E}(\Sigma)$ telles que$L_{1} = L(e_{1})$ et $L_{2} = L(e_{2})$ et $L = L((e_{1}\bullet e_{2}))$
- Si $L = L_{1}^{*}$, avec $L_1$ régulier, alors par induction $\exists\ e \in \mathcal{E}(\Sigma)$ telle que $L_{1} = L(e)$ et $L = L(e^{\star})$


> [!def] Définition
> On dit que deux expressions régulières $e_{1},e_{2}\in \mathcal{E}(\Sigma)$ son **équivalentes**, ce que l'on note $e_{1}\equiv e_{2}$ si elles dénotent le même langage, c'est à dire  si $L(e_{1}) = L(e_{2})$


>[!rem] Remarque
>On cherche souvent une expression régulière la plus simple possible à équivalence près.

> [!rem] Remarque
> $(a|b) \neq (b|a)$ mais $(a|b) \equiv (b|a)$

> [!rem] Remarque
> Ceci justifie les abus de notation

