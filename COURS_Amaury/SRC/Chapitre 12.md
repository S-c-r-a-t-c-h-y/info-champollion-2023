# Chapitre 12 - Couplages

> [!def] Définition
> Un **couplage** est un ensemble d'arêtes indépendants, c'ets-à-dire sans extrémités communes.

> [!info]
Cette notion apparaît naturellement lorsqu'il s'agit de coupler deux à deux des objets, certains couplages, certaines associations seulement étant possibles (celles des arêtes du graphe).

> [!example] Exemple
> - ensemble de personnes à marier (sans polygamie)
> - une assemblée de personnes parlant diverses langues. On veut des groupes de 2 qui peuvent se comprendre. On modélise alors par un graphe avec un arête entre deux personnes (sommets) si elles parlent la même langue.

> [!info]
Le problème en général est posé comme un problème d'optimisation. Plus il y a d'arêtes, mieux c'est. On recherche à maximiser le nombre de couplages.

## I - Couplage dans un graphe

> [!def] Définition
> Soit $G = (S, A)$ un graphe (non orienté). Un **couplage** de $G$ est un ensemble $C \subseteq A$ d'arêtes indépendantes. Un sommet $s \in S$ est incident à au plus une arête de $C$.
> Un couplage est dit :
> - **maximal** (ou maximal au sens de l'inclusion) s'il n'est pas strictement inclus dans un couplage de $G$ (on ne peut plus ajouter d'arêtes).
> - **maximum** (ou *optimal* ou maximal pour le cardinal) si son cardinal est plus grand ou égal que celui de tout les couplages de $G$.
> - **parfait** si tout sommet de $G$ est incident à une arête du couplage.

> [!def] Définition
> Soit $G = (S, A)$ un graphe et $C \subseteq A$ un couplage de $G$.
> On dit que $s \in S$ est **couvert** par $C$ si $s$ est extrémité d'une arête de $C$ et **libre** sinon.

> [!rem] Remarques
> - Comme un couplage peut se contenir aucune arête ($C = \varnothing$) l'ensemble des couplages d'un graphe est non vide et fini (car $A$ est fini). Donc tout graphe possède un couplage optimal.
> - Toute arête d'un graphe est un couplage de ce graphe de cardinal $1$.
> - Un couplage est maximal ssi il n'existe par deux sommets libres incidents (sinon il suffirait d'ajouter cette arête)
> - Les couplages maximum n'ont pas nécessairement tous le même cardinal
> - Un couplage maximal n'est pas toujours optimal, mais la réciproque est vraie
> - Le cardinal d'un couplage est inférieur à $|S| / 2$
> - Un couplage est parfait ssi son cardinal est $|S| / 2$. Le graphe doit alors être d'ordre pair
> - Un graphe complet d'ordre pair admet un couplage parfait

> [!help] Comment obtenir un couplage maximal ?
> On ajoute les arêtes si on peut.

> [!help] Comment trouver un couplage optimal ?

> [!def] Définition
> Soit $G = (S, A)$ un graphe et $C$ un couplage de $G$.
> - Un **chemin alternant** de $G$ pour $C$ est un chemin élémentaire dont les arêtes sont alternativement dans $C$ et dans $A \setminus C$ 
> - Un **chemin améliorant** (ou augmentant) est un chemin alternant d'extremités libres

> [!rem] Remarque
> La longueur d'un chemin améliorant est impar et les arêtes à ses extrémités sont dans $A \setminus C$

> [!tip] Lemme du chemin améliorant
> Soit $C$ un couplage de cardinal $K$ d'un graphe $G = (S, A)$ et $D$ un chemin améliorant pour $C$ dans $G$.
> On note $C \Delta D = C \setminus D \cup D \setminus C$ l'ensemble des arêtes qui sont dans le couplage ou exclusif dans le chemin.
> Alors $C \Delta D$ est un couplage de $G$ de cardinal $K+1$.

> [!tldr] Démonstration
> Une arête de $D$ à $A \setminus D$ n'est pas dans le couplage. En effet, les extremités de $D$ sont libres donc ne sont pas incidentes à une arête du couplage. Les autres sont couvertes par une arête de $D$. Donc elles ne peuvent pas non plus être incidentes à une arête de $C \setminus D$. $C \setminus D$ reste un couplage.
> $D \setminus C$ est un couplage puisque l'on prendre une arête sur deux dans un chemin dont les sommets sont tous deux à deux distincts. Donc toutes ces arêtes sont indépendants. De plus aucun sommet n'est incident à la fois à une arête de $C \setminus D$ et de $D \setminus C$ qui sont donc indépendantes donc $C \Delta D$ est un couplage.
> 

> [!tip] Théorème de Berge 1957
> Soit $G = (S, A)$ un graphe et $C$ un couplage de $G$. Le couplage $C$ est optimal si et seulement si il n'admet aucun chemin améliorant pour $C$.

> [!tldr] Démonstration
> Le lemme ci-dessus établi la condition nécessaire.
> Pour la condition suffisante, on procède par contraposée.
> Soit $C$ un couplage non optimal, montrons que $C$ possède un chemin améliorant.
> On considère un couplage optimal $C' \subseteq A$, on a donc $|C| < |C'|$.
> On considère $C \Delta C'$ et notons $H = (S, C \Delta C')$
> Considérons un sommet de $H$, il est incident à au plus une arête de $C$ et au plus une arête provenant de $C'$, donc dans $H$ tous les sommets ont un degré dans $\{0, 1, 2\}$.
> Le long d'une chaîne ou d'un cycle de $H$ lest arêtes alternent entre celles ed $C$ et de $C'$.
> Donc les cycles sont de longueur paire et contiennent autant d'arêtes de $C$ et de $C'$.
> Comme $|C'| > |C|$, au moins une composant connexe de $H$ contient plus d'arêtes de $C'$ que de $C$. Ce n'est pas un cycle donc c'est une chaîne. Cette chaîne est améliorante car elle alterne des arêtes de $C$ et de $C' \setminus C$ donc de $A \setminus C$ avec plus d'arêtes de $C' \setminus C$ que de $C$.

> [!tip] Prop (exo)
> Un graphe dont tous les sommets sont de degré inférieur ou égal à 2 est une réunion de chaînes et de cycles.

On en déduit un algorithme pour trouver un couplage optimal :
Entrée : un graphe $G = (S, A)$
Sortie : Un couplage $C$ optimal

## II - Graphes bipartis

> [!def] Définition
> Un **stable** est ou *ensemble indépendant* est un sous-ensemble $X \subseteq S$ de sommets deux à deux non adjacents. Autrement dit aucune arête de $G$ n'a ses deux extrémités dans $X$.

> [!def] Définition
> Un graphe est **biparti** si on peut partitionner $S = X \sqcup Y$ en deux ensembles $X$ et $Y$ indépendants. Autrement dit une arête a une extrémité dans $X$ et l'autre dans $Y$.
> On note $G = (X, Y, A)$.

> [!info]
Le cas des graphes bipartis est celui où la notion de couplage est la plus naturelle. Typiquement, on a deux ensembles d'objets $X$ et $Y$ ($X$ les candidats, $Y$ les écoles) et un ensemble d'arêtes entre des sommets de $X$ et des sommets de $Y$.

> [!example] Exemple
> auteurs - articles

> [!success] Avantage
Un avantage des graphes bipartis (outre le fait que de nombreux problèmes de couplage se modélisent bien par des graphes bipartis) est que l'on sait déterminer s'il existe des chemins augmentant relativement facilement et les trouver le cas échéant.

> [!rem] Remarques
> - Un chemin augmentant dans un graphe biparti commence par un sommet de $X$, alterne entre des sommets de $X$ et $Y$ et se termine en un sommet libre de $Y$.
> - Quand on va de $X$ à $Y$, on prend une arête de $A\setminus C$ et quand on va de $Y$ à $X$ on prend une arête de $C$.

> [!tip] Algorithme
Pour déterminer si un graphe biparti avec un couplage $C$ possède un chemin augmentant, il suffit de faire un parcours en profondeur à partir des sommets libres de $X$ jusqu'à arriver à un sommet libre de $Y$. 
En utilisant uniquement les arêtes de $A \setminus C$ entre $X$ et $Y$, et de $C$ entre $Y$ et $X$, un tel chemin sera bien augmentant et s'il existe un chemin augmentant, il s'agit d'un tel chemin.
> > [!danger] Complexité
> > $O(|S| + |A|)$

> [!tip] Autre présentation possible
> A un graphe $G = (S, A)$ et un couplage $C$ on peut associer un graphe orienté $G_C = (S', A')$ comme suit :
> - on se donne deux nouveaux sommets $s, f \not \in S$ et $S' = S \cup \{s, f\}$
> - il y a un arc entre $s$ et tout sommet libre de $X$ 
> - il y a un arc entre tout sommet libre de $Y$ et $f$
> - il y a un arc $x \to y$ pour $x \in X, y \in Y$ si $\{x, y\} \in A \setminus C$
> - il y a un arc $y \to x$ pour tout $x \in X, y \in Y, \{x, y\} \in C$

> [!tip] Proposition
> Les chemins augmentant de $G$ pour le couplage $C$ son en bijection avec les chemins élémentaires de $s$ vers $f$ dans $G_C$.

On a donc un algorithme pour trouver un couplage optimal dans un graphe biparti en $O(|S|(|S| + |A|))$

A chaque fois que l'on trouve un chemin augmentant, on ajoute au moins une arête au couplage et deux nouveaux sommets couverts. Il y a donc au plus $|S| / 2$ recherches de chemin augmentant, chacune étant en complexité $O(|S| + |A|)$.

