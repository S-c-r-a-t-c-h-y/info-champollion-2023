# Chapitre 17 - Séparation et évaluation

La technique de "séparation et évaluation" (branch and bound) est une technique qui permet d'accélerer la recherche exhaustive (brute force) (par élagage d'une partie de l'espace de recherche) lors de l'applicaiton d'une stratégie de recherche (backtracking) pour la résolution d'un problème d'optimisation.

Backtracking (pour un problème de recherche / décision) : structurer l'espace de recherche pour une recherche exhaustive avec élagages (décomposition du problème en solutions)

Branch and bound : même chose mais pour des problèmes d'optimisation

## I - Problèmes d'optimisation

On considère un problème d'optimisation $P$ pour lequel une instance $x \in X$ on cherche à minimiser une quantité $f(x,y)$ pour les solutions $y \in Y(x)$ possibles à l'instance $x$. On cherche donc $\underset{y \in Y(x)}{\min}f(x, y)$

> [!rem] Remarque
> Le cas de maximisation se traite de manière symétrique.

> [!example] Exemple 1
> MAXSAT est un problème d'optimisation relié à SAT où l'on cherche à maximiser le nombre de clauses satisfaites.
> On va plutôt considérer MINUNSAT :
> étant donné une formule $x$ sous forme normale conjonctive, on cherche une valuation $y$ qui ne satisfait pas un nombre minimal de clauses.
> - $f(x, y)$ le nombre de clauses de $x$ non satisfaites par $y$.

> [!rem] Remarque
> Si on sait résoudre efficacement MAXSAT, on sait résoudre efficacement CNF-SAT (et SAT).

> [!example] Exemple 2
> Dans le problème de voyageur de commerce à un graphe $G = (S, A, w)$ non orienté pondéré (souvent complet) (souvent à poids strictement positifs) on cherche un cycle hamiltonien de poids minimal dans $G$. $f(x, y) = w(y)$

> [!rem] Remarque
> Si on sait résoudre le problème du voyageur de commerce efficacement, on sait résoudre le problème du cycle hamiltonien.

## II - Séparation

La première étape de l'approche par backtracking et par branch and bound est la séparation de l'espace de recherche (branch). Elle consiste à donner à l'espace des solutions une structure arborescente adaptée. On cherche à séparer récursivement (branch) l'espace des solutions en plusieurs sous-espaces. On obtient alors des solutions partielles qu'il reste à compléter.

> [!example] Exemple
> Pour SAT et MAXSAT, l'ensemble des solutions est l'ensemble des valuations à $n$ variables (celles de $x$). On peut représenter cet ensemble sous la forme d'arbre binaire en associant à chaque noeud une variable et en séparant les solutions suivant que cette variable vaut $V$ ou $F$. La valuation initiale est $\varnothing$. A chaque noeud de l'arbre correspond une valuation partielle et le sous-arbre enraciné en ce noeud correspond à toutes les manières de la prolonger.

> [!danger] Exercice
> Dessiner l'arbre complète correspondant à $\varphi$. Evaluer les feuilles et donner une solution optimale pour MAXSAT.
> 

> [!example] Exemple 2
> Pour le voyageur de commerce, pour un graphe $G = (S, A, w)$, une solution partielle est un chemin élémentaire de $a \in S$ à $b \in S$ passant par les sommets $X \subseteq S$. On note cela $a \overset{X}{\leadsto} b$. On cherche alors à compléter ce chemin par un voisin $x$ de $b$ qui n'est pas dans $X$ (sauf si $X = S$, dans ce cas on cherche à revenir en $a$). La racine est $a \overset{\{a\}}{\leadsto} a$ pour un sommet $a \in S$ arbitraire.
> Sans élagage on effectue un parcours en profondeur dans l'arborescence : il s'agit d'une recherche exhaustive.

## III - Retour sur trace

L'idée est d'améliorer la recherche exhaustive en élaguant de grandes parties de l'espace de recherche (99.9999%) (mais attention la complexité reste exponentielle).
Si pour un nœud on est capable de déterminer qu'il est impossible de compléter la solution partielle en une solution totale, on peut s'arrêter là (élagage).
Pour un problème d'optimisation, on va faire la même chose, mais il faut être capable de déterminer si pour un noeud il n'y a plus d'espoir de trouver une meilleure solution qu'une déjà trouvée.

## IV - Evaluation

On peut souvent déjà calculer une évaluation partielle pour une solution partielle $\tilde{f}(x, \tilde{y})$.

> [!example] Exemple
> Pour MAXSAT : pour une solution partielle $\tilde{y}$, les clauses déjà entièrement non satisfaites par $\tilde{y}$ ne le seront pas non plus par un prolongement de $\tilde{y}$.

> [!example] Exemple 2
> Pour le voyageur de commerce, $f(x, \tilde{y}) = u(\tilde{y})$.

On suppose donc disposer d'une **heuristique** $h$ qui évalue pour une solution partielle $\tilde{y}$ le coût $h(\tilde y)$ qui cherche à approcher le coût minimal de toute complétion de $\tilde y$ en une solution totale $y$ (ou $+ \infty$ si aucune solution n'est possible).
Pour élaguer une partie de l'espace de recherche, il est nécessaire que cette heuristique sous-estime ce coût. Une telle heuristique est dite **admissible**. Une heuristique est admissible si pour tout $x \in Y$ et pour toute solution partielle $\tilde y$ pour $x$ et pour toute solution totale $y$ de $x$ qui prolonge $\tilde y$, $h(\tilde y) \leq f(x, y)$

> [!example] Exemple
> Pour MAXSAT pour une valuation partielle $\tilde v$ pour une formule $\varphi$ on peut prendre comme heuristique :
> - le nombre de clause que l'on satisfait par $\tilde v$


## V - Elagage

L'élagage consiste à couper des branches lors de l'exploration lorsque l'on est certain que le noeud ne peut pas donner une meilleure solution que celle qu'on a déjà obtenue.

La structure de l'algorithme est donc la suivante :
- explorer l'ensemble des solutions vu comme un arbre
- mémoriser la valeur de la meilleure solution
- pour un noeud, calculer une borne minimale (heuristique) de toute complétion de la solution partielle. Si on trouve plus que la solution mémorisée, on élague.

Entrée : une instance $x$ de $P$
Sortie : la valeur exacte d'une solution minimale
Début :
$f_{\min} \leftarrow +\infty$
$y_{\min} \leftarrow \varnothing$
$BnB(\tilde y) :$
si $\tilde y$ est totale : (
	si $f(x, \tilde y) < f_{\min}$ : (
		$f_{\min} \leftarrow$ $f(x, \tilde y)$
		$y_{\min} \leftarrow \tilde y$
	)
)
sinon : (
	si $h(\tilde y) < f_{\min}$ : (
		pour chaque manière $z$ de compléter $\tilde y$ en une étape : (
			BnB($\tilde y z$)
		)
	)
)

BnB($\varnothing$)
renvoyer $y_{\min}$

> [!rem] Remarque
> Au lieu d'initialiser $f_{\min}$ à $+\infty$ on peut prendre une solution approchée (algorithme glouton, approximation, algorithme probabiliste ...)

> [!rem] Remarque
> Il convient souvent de bien choisir l'ordre des branches.


