# Arbres binaires de recherche équilibrés

## I - Rappels

> [!info] Question de cours (au concours)
> On définit un arbre binaire par induction
> 
> > [!warning] Deux grandes définitions possibles (et autant de possibles que de sujets de concours)
> 
> **Avec arbre vide :**
> Un arbre est soit un arbre vide, soit un noeud constitué de deux sous-arbres gauche et droit.
> Une feuille est alors un noeud avec deux fils vide.
>
> **Arbre avec définition feuille-noeud :**
> Un arbre est soit une feuille, soit un noeud avec deux fils gauche et droit.
> *Remarque :* pas d'arbre vide dans ce cas

> [!note] Rappel
> La définition par induction considère des cas de bases et des règles inductives et constituant le plus petit ensemble compatible avec cela.
> $\rightarrow$ preuve par induction

> [!tip]
> On peut ajouter des étiquettes, ou plein d'informations supplémentaires sur les noeuds.

## II - Arbres binaires de recherche

> [!def] Définition
> Un **arbre binaire de recherche** est un arbre binaire étiqueté sur un ensemble muni d'un <u>ordre total</u> qui vérifie de plus la propriété suivante :
> l'étiquette de tout noeud est inférieure (ou égale) à celle de tous les noeuds de son sous-arbre droit et supérieure (ou égale) à celle de son sous-arbre gauche

> [!fail] Contre-exemple
> ```mermaid
> flowchart TD
> 3 --> 2 --> 0
> 2 --> 4
> 3 --> 8
> ```
> La propriété est vérifiée localement mais pas globalement

> [!tip] Proposition
> Un arbre binaire est un arbre binaire de recherche $\Leftrightarrow$ la liste des étiquettes du parcours infixe est croissante.

> [!tldr] Démonstration (par induction)

> [!rem] Remarque
> En général, on suppose tous les éléments distincts

> [!help] Intérêt
> Recherche, insertion et suppression efficace en $\Theta(h)$ avec $h$ la hauteur de l'arbre.
> - Dans le meilleur cas, "l'arbre est équilibré" et la hauteur est logarithmique en le nombre de noeuds. Les opérations sont en $O(\log n)$ avec $n$ le nombre de noeuds.
> - Mais dans le pire cas, on peut avoir $h = n-1$. C'est le cas si on insère des éléments triés.

> [!rem] Remarque
> On peut montrer que si les données sont insérées dans un ordre aléatoire alors en espérance la hauteur de l'arbre est en $O(\log n)$

## III - Arbres équilibrés et rotations

> [!def] Définition
> Soit $A$ un ensemble ABR et $C > 0$. On dit que $A$ est **$\textbf{C-}$équilibré** si pour tout $a \in A,\ h(a) \leqslant C \log(n(a))$
> On cherche donc à construire des familles d'arbres équilibrés pour garantir des opérations en $O(\log n)$.
> On cherche un mécanisme pour équilibrer les arbres avec un petit surcoût (idéalement en temps constant).

> [!tip] Rotations
> Une **rotation** est une opération sur un ABR qui permet de réduire un déséquilibre tout en maintenant les propriétés d'ABR. Elles sont utilisées par les structures de données d'arbre équilibrés.

> [!example] Exemple
> 
> ![[chap1_img1.png]]

> [!tldr] Implémentation
> ```c
> typedef struct noeud {
> 	int cle,
> 	struct noeud* gauche,
> 	struct noeud* droit,
> 	struct noeud* parent,
> } noeud;
> 
> void rotation_gauche(noeud* x) {
> 	noeud* y = x->droit;
> 	x->droit = y->gauche;
> 	y->gauche = x;
> }
> ```

> [!danger] Exercice
> Ecrire les programmes rotations droites en OCaml et en C

```ocaml
type 'a arbre =
| V
| N of 'a arbre * 'a * 'a arbre

let rotation_gauche a = match a with
	| N (a, x, N (c, y, d)) -> N (N (a, x, c), y, d)
	| _ -> a

let rotation_droite a = match a with
	| N (N (a, x, c), y, d) -> N (a, x, N (c, y, d))
	| _ -> a

let rotation_gauche_droite a = match a with
	| N (l, z, r) -> rotation_droite (N (rotation_gauche l, z, r))
	| _ -> a

let rotation_gauche_droite a = match a with
	| N (N (N (a, x, b), y, c)), z, d) -> N (N (a, x, b), y, N(x, z, d))
	| _ -> a
```

## IV - Arbres AVL

> [!def] Définition
> Les arbres AVL sont des arbres binaires de recherche "bien équilibrés" (la différence de hauteur des fils en tout noeud est au plus 1). 
> D'après la dernière question du DM 1, ces arbres sont donc équilibrés ($h = O(\log n)$)

