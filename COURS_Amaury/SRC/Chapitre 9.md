# Chapitre 9 - Automates finis

## I - Motivation

### 1) Modélisation d'un ordinateur très simplifié

> [!success] Motivation
> On a formalisé la notion de langage, il nous reste à modéliser la notion de machine.
> On va adopter un modèle pour un type de machine très simplifié.

> [!example] Exemple : Machine à café

### 2) Théorème de Kleene

Un des objectifs du chapitre est de nous intéresser au théorème suivant :

> [!tip] Théorème de Kleene
> Les langages reconnaissables par automates finis sont exactement les [[Chapitre 3|langages réguliers]].

> [!rem] Remarque
> On peut formaliser la notion de langage calculable (décidables). Il faut des <u>machines de Turing</u> (HP).

### 3) Recherche de motifs

Les expressions régulières permettent de noter des motifs à rechercher.

Langages réguliers $\longleftrightarrow$ notations, formulaire, théorie
Automates $\longleftrightarrow$ machine, réalisation, pratique

Comment trouver les motifs d'un texte ?

1) Spécifier le motif par une expression régulière
2) Traduire cette expression régulière en automate
3) Executer/Simuler l'automate

> [!rem] Remarque
> Beaucoup d'applications : correction orthographique, compilation, recherche etc...

 ## II - Automates finis déterministes

### 1) Formalisation

> [!def] Définition
> Un **automate fini déterministe** est un quintuplet $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ avec :
> - $Q$ un ensemble fini non vide. Les éléments de $Q$ sont appelés **états**
> - $\Sigma$ un alphabet
> - $q_{0}\in Q$ un état appelé **état initial**
> - $T \subseteq Q$ un ensemble d'état appelé **états terminaux** (pas de contraintes)
> - $\delta : D_{\delta}\subseteq (Q \times \Sigma) \to Q$ une application partielle appelée **fonction de transitions**. Si $q \in Q$ et $a \in \Sigma$, $\delta(q, a)$ est l'état d'arrivé en lisant la lettre $a$ depuis $q$

> [!example] Exemple pour la machine à café
> - $Q = \{\overline{0c}, \overline{10c}, \overline{20c}, \overline{café}\}$
> - $\Sigma = \{ 10c, 20c \}$
> - $q_{0}= \overline{0c}$
> - $T = \{\overline{café}\}$
> - $\delta$ déterminé par :

| $q/a$            | $10c$             | $20c$             |
| ---------------- | ----------------- | ----------------- |
| $\overline{0c}$  | $\overline{10c}$  | $\overline{20c}$  |
| $\overline{10c}$ | $\overline{20c}$  | $\overline{café}$ |
| $\overline{20c}$ | $\overline{café}$ |                   |
| $\overline{café}$                 |                   |                   |

> [!tldr] Vocabulaire
> - AFD pour Automate Fini Déterministe
> - DFA pour Deterministic Finite Automaton

> [!info] Représentation graphique
> On représente un automate par un multigraphe orienté étiqueté avec boucles $G = (Q, \{(q, a, \delta(q, a)), (q, a) \in D_{\delta}\})$
> Les états représentés par des cercles avec un flèche entrante pour l'état initial et un flèche sortante ou un double cercle pour un état terminal.
> 

> [!def] Définition
> Un **blocage** d'un automate $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$  est un couple $(q, a) \in Q \times D \setminus D_{\delta}$, c'est-à-dire une transition non définie par $\delta$.
> 

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ un AFD, on étends la fonction de transition $\delta : D_{\delta}\subseteq Q \times \Sigma \to Q$ en une fonction $\delta^* : D_{\delta^{*}}\subseteq Q \times \Sigma^* \to Q$ aux mots de la manière suivante :
> - $\forall q \in Q, \delta^{*}(q, \varepsilon) = q$
> - $\forall q \in Q, \forall a \in \Sigma, \forall u \in \Sigma^{*}, \delta^{*}(q, au) = \delta^{*}(\delta(q, a), u)$ si $(q, a) \in D_\delta$ sinon $(\delta(q, a), u) \in D_{\delta^*}$

> [!rem] Remarque
> On peut aussi définir $\delta^{*}$ dans l'autre sens :
> - $\forall q \in Q, \delta^{*(q, \varepsilon)}= q$
> - $\forall q \in Q, \forall u \in \Sigma^{*}, \forall a \in \Sigma, \delta^{*}(q, ua)= \delta(\delta^{*}(q, u), a)$ si par de blocages

> [!rem] Remarque
> $\delta^*(q, u)$ est l'état sur lequel on se arrive en lisant les lettres du mot $u$ dans l'ordre à partir de l'état $q$.

> [!tip] Proposition
> Soit $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ un AFD et $u, v \in \Sigma^{*}$ et $q \in Q$. $\delta^{*}(q, uv) = \delta^{*}(\delta^{*}(q, u), v)$

> [!tldr] Démonstration
> Par induction sur $u$ (ou par récurrence sur $|u|$)

> [!def] Définition
> Le **langage reconnu par un automate** est l'ensemble des mots reconnus. On le note $L(\mathcal A)$ si $\mathcal A$ est l'automate.

> [!def] Définition
> Un langage $L \subseteq \Sigma^{*}$ est **reconnaissable** (par automate déterministe fini) si il existe un automate $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ tel que $L(\mathcal A) = L$

> [!def] Définition
> Deux automates sont **équivalents** s'ils reconnaissent le même langage.

### 2) Automates particuliers

#### a) Automates complets

> [!def] Définition
> Un automate $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ est **complet** s'il n'a pas de blocage, c'est à dire si $D_{\delta}= Q \times \Sigma$ en entier.

> [!tip] Proposition
> Un langage reconnaissable est reconnu par un automate déterministe complet.

> [!rem] Remarque
> On peut donc toujours supposer l'automate complet, sans perte de généralité, il suffit de compléter l'automate avec un état puits (ou poubelle) absorbant.

> [!tldr] Preuve
> Soit $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ un AFD. On va construire un automate fini déterministe complet (AFDC) $\mathcal A'$ équivalent.
> Soit $q_{\bot}\not\in Q$ un nouvel état appelé état puits. On prolonge $\delta$ en $\eta$ sur $Q \cup \{q_\bot\}$ de la manière suivante :
> - $\forall (q, a) \in D_\delta$ on pose $\eta(q, a) = \delta(q, a)$
> - $\forall (q, a) \not \in D_\delta$, on pose $\eta(q, a) = q_\bot$
> - $\forall a \in \Sigma$, on pose $\eta(q_{\bot}, a) = q_\bot$

> [!tip] Lemme
> Pour tout $q \in Q$, $u=u_1u_{2}\cdots u_{n}\in \Sigma^{*}$, s'il existe $i \in [[1, n]]$ tel que $\eta(q, u_{i})= q_\bot$ alors $\eta^{*}(q, u) = q_\bot$

> [!rem] Remarque
> On a montré un peu plus fort : si il est reconnu par un AFD à $n$ états, alors il est reconnu par un AFDC à $n+1$ états

> [!rem] Remarque
> On suppose le plus souvent les automates complets même si on ne représente par l'état puits.

#### b) Accessibilité et émondage

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ un AFD et $q \in Q$. On dit que :
> - $q$ est **accessible** s'il existe $u \in \Sigma^*$ tel que $\delta^*(q_0, u)=q$
> - $q$ est **co-accessible** s'il existe $u \in \Sigma^{*}$ tel que $\delta^{*(q, u)}\in T$
> - $q$ est **utile** s'il est accessible et co-accessible
> - un automate est **émondé** si tous ses états sont utiles

> [!rem] Remarque
> Un état est accessible si on peut s'y rendre à partir de l'état de l'état initial, co-accessible si à partir de celui-ci on peut rejoindre un état terminal. Un état est inutile s'il sert à rien, on peut l'enlever sans changer le langage reconnu.

> [!tip] Proposition
> Un langage non vide reconnaissable est reconnu par un automate émondé.

> [!tldr] Preuve
> Il suffit d'enlever les états inutiles

> [!rem] Remarque
> On ne peut pas toujours compléter un automate en conservant le caractère émondé. on s'autorise en général au plus un état non co-accessible (état puits).

> [!rem] Remarque
> Comment déterminer les états accessibles ?
> -> parcours de graphe à partir de $q_0$
> Comment déterminer les états co-accessibles ?
> -> parcours de graphe à partir de $T$ sur le graphe miroir


> [!rem] Remarque
> Emondé ne veut pas forcément dire minimal en nombre d'états;

> [!rem] Remarque
> On peut montrer qu'il existe un unique AFDC minimal

### 3) Propriétés de clôtures

#### a) Clôture par complémentaire

> [!tip] Proposition
> Si $L$ est reconnaissable alors $\overline L = \Sigma^{*}\setminus L$ l'est aussi.

> [!tldr] Démonstration
> Soit $L$ un langage reconnaissable, il existe un automate $\mathcal A = (Q, \Sigma, q_{0},T,\delta)$ tel que $L(\mathcal A) = L$.
> On pose $\overline{\mathcal A} = (Q, \Sigma, q_{0}, Q\setminus T, \delta)$. Montrons que $L(\overline{\mathcal A}) = \Sigma^{*}\setminus L(\mathcal A)$
> $u \in L(\overline{\mathcal A}) \Leftrightarrow \overline\delta^{*}(\overline{q_{0}}, u) \in \overline T \Leftrightarrow \delta^{*(q_{0}, u)}\in Q\setminus T \Leftrightarrow \delta^*{(q_{0}, u)}\not \in T \Leftrightarrow u \not \in L(\mathcal A) \Leftrightarrow u \in \Sigma^{*}\setminus L$

> [!rem] Remarque
> Pour reconnaitre le langage complémentaire d'un langage reconnaissable par un automate, il suffit d'inverser les états terminaux et non terminaux d'un automate complet.

#### b) Automate produit

> [!def] Définition
> Soit $\mathcal A_1 = (Q_1, \Sigma, q_{0}^1,T_1,\delta_1)$ et $\mathcal A_2 = (Q_2, \Sigma, q_{0}^2,T_2,\delta_2)$ deux AFDC sur un même alphabet.
> Un **automate produit** est l'automate :
> $\mathcal A_{1}\times \mathcal A_{2} = (Q_{1}\times Q_2, \Sigma, (q_{0}^{1}, q_{0}^{2}),T_{1\times 2},\delta_{1 \times 2})$ avec $\delta_{1\times 2}$ défini par :
> $\forall q_{1}\in Q_{1}, \forall q_{2}\in Q_{2}, \forall a \in \Sigma, \delta_{1 \times 2}((q_{1}, q_{2}), a) = (\delta_{1}(q_{1},a), \delta_{2}(q_{2}, a))$

> [!rem] Remarque
> L'automate produit permet de simuler deux automates en parallèle.

> [!tip] Proposition
> $\forall q_{1}\in Q_{1}, \forall q_{2}\in Q_{2}, \forall u \in \Sigma^{*}, \delta_{1 \times 2}^{*}((q_{1}, q_{2}), a)= (\delta_{1}^{*}(q_{1}, u), \delta_{2}^{*}(q_{2}, u))$

> [!tldr] Démonstration
> Par induction (ou récurrence) sur $u$.
> - Si $u = \varepsilon$ alors $\delta_{1 \times 2}^{*}((q_{1},q_{2}), \varepsilon) = (q_{1},q_{2}) = (\delta_{1}^{*}(q_{1},\varepsilon), \delta_{2}^{*}(q_{2}, \varepsilon))$
> - Si $u = av$, avec $a \in \Sigma$, $v \in \Sigma^{*}$ alors $\delta_{1 \times 2}^{*}((q_{1},q_{2}), av) = \delta_{1 \times 2}^{*}(\delta_{1 \times 2}((q_{1},q_{2}), a), v) = \delta_{1 \times 2}^{*}((\delta_{1}(q_{1}, a), \delta_{2}(q_{2},a)), v) = (\delta_{1}^{*}(\delta_{1}(q_{1},a), v), \delta_{2}^{*}(q_{2}, a), v) = (\delta_{1}^{*}(q_{1}, u), \delta_{2}^{*}(q_{2}, u))$

> [!tip] Proposition
> Les langages reconnaissables sont clos par union et intersection finie.

> [!tldr] Démonstration
> Il suffit de le montrer pour l'union et l'intersection de deux automates.
> Soient $L_{1}$ et $L_2$ deux langages reconnaissables et $\mathcal A_1, \mathcal A_2$ deux AFDC qui les reconnaissent respectivement.
> On considère les automates produits $\mathcal A_1, \mathcal A_2$ avec $T_{1 \times 2} = T_1 \times T_2$
> Intuitivement on calcule en parallèle dans les deux automates et on valide si les deux automates valident.
> Montrons que $L(\mathcal A_{1 \times 2}) = L( \mathcal A_1) \cap L(\mathcal A_2)$.
> Soit $u \in \Sigma^*,$
> $u \in L(\mathcal{ A_{1 \times 2}}) \iff \delta_{1 \times 2}^*((q_0^1,q_0^2), u) \in T_1 \times T_2$
> $\iff (\delta_1^*(q_0^1, u), \delta_2^*(q_0^2, u)) \in T_1 \times T_2$
> $\iff \delta_1^*(q_0^1,u) \in T_1$ et $\delta_2^*(q_0^2, u) \in T_2$
> $\iff u \in L(\mathcal A_1)$ et $u \in L(\mathcal A_2)$
>
> De même pour reconnaître la réunion avec $T_{1 \times 2} = T_1 \times Q \cup Q \times T_2$

> [!rem] Remarque
> On peut montrer que les langages reconnaissables sont clos par concaténation et étoile de Kleene.

## III - Automates finis non déterministes

### 1) Définition

> [!def] Définition
> Un **automate fini non déterministe** est un quintuplet $\mathcal A = (Q, \Sigma, I, T, \Delta)$ avec :
> - $Q$ un ensemble non vide d'états
> - $\Sigma$ un alphabet
> - $I \subseteq Q$ l'ensemble des états initiaux
> - $T \subseteq Q$ l'ensemble des états terminaux
> - $\Delta \subseteq Q \times \Sigma \times Q$

> [!rem] Remarque
> Si $(q, a, q') \in \Delta$, on note
> ```mermaid
> flowchart LR
> q -- a --> q'
> ```

> [!rem] Remarque
> Non déterministe :
> - plusieurs états initiaux
> - plusieurs transitions à partir d'un état pour une même lettre

> [!rem] Remarque
> On peut voir un automate fini déterministe comme un cas particulier d'automate fini non déterministe.

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, I, T, \Delta), (q, q') \in Q^2$. On appelle **chemin** menant de $q$ à $q'$ dans $\mathcal A$ toute suite finie de transitions consécutives qui commence par $q$ et termine par $q'$.
> $(q_1, a_0, q_1), \cdots, (q_{n-1}, a_{n-1}, q_n)$ avec $q_0 = q, q_n = q'$ et $\forall i \in [[1, n-1]], (q_i, a_i, q_{i+1}) \in \Delta$
> Sa **longueur** est $n$ et son **étiquette** est le mot $u = a_0a_1\cdots a_{n-1}$

> [!rem] Remarque
> Pour $q \in Q$ le chemin vide mène de $q$ à $q$ d'étiquette $\varepsilon$.

> [!tldr] Notation
> Pour $q, q' \in Q$ et $u \in \Sigma^*$ on note $q \overset{u}{\leadsto}q'$ s'il existe au moins un chemin de $q$ à $q'$ d'étiquette $u$.

> [!rem] Remarque
> - $q \overset{\varepsilon}{\leadsto} q' \iff q=q'$
> - Si $p \overset{u}{\leadsto} q$ et $q \overset{v}{\leadsto}r$ alors $p \overset{uv}{\leadsto} r$ 
> - Si $p \overset{uv}{\leadsto} q$ alors $\exists z \in Q$ tel que $p \overset{u}{\leadsto}z$ et $z \overset{v}{\leadsto}q$

> [!rem] Remarque
> Si $p,q \in Q$ et $u \in \Sigma^1$, $q \overset{u}{\leadsto} p \iff (q, u, p) \in \Delta$

> [!def] Définition
> Soit $u \in\Sigma^*$ et $\mathcal A = (Q, \Sigma, I, T, \Delta)$ un automate fini non déterministe. On dit que $u$ est reconnu par $\mathcal A$ s'il existe $q \in I, q' \in T$ et un chemin de $q$ à $q'$ d'étiquette $u$.

> [!rem] Remarque
> $\varepsilon \in L(\mathcal A) \iff I \cap T \neq \varnothing$

> [!help] Question 
> Comment déterminer algorithmiquement si un mot $u$ est reconnu ?
> -> on teste tout (backtracking)

> [!def] Définition
> On étend directement les définitions d'états accessibles, co-accessibles et d'automates émondés et standards aux automates non déterministes.

### 2) Fonction de transition étendue

> [!info]
Plutôt que de définir un automate non déterministe par une relation de transition $\Delta \subseteq Q \times \Sigma \times Q$, on peut utiliser une fonction de transition $\delta : Q \times \Sigma \to P(Q)$
Il n'y a alors pas de blocages mais $\delta(q, a)$ peut être $\varnothing$.

> [!rem] Remarque
> Un AFND est un AFD si :
> - $|I| = 1$
> - $\forall (q, a) \in Q \times \Sigma, |\delta(q, a)| \neq 1$ ($=1$ pour un AFDC)

> [!def] Définition
> On peut étendre la fonction de transition $\delta$ en une fonction $\hat \delta$ sur les parties de $Q$ : $\hat \delta : P(Q) \times \Sigma \to P(Q)$ avec $\forall X \subseteq Q, \forall a \in \Sigma, \hat\delta(X,a) = \bigcup\limits_{q \in X}\delta(q, a)$
> Intuitivement $\hat\delta(X, a)$ est l'ensemble des états dans lesquelles on peut arriver en lisant la lettre $a$ à partir d'un état de $X$.

> [!def] Définition
> On étend $\hat \delta$ en une fonction $\hat\delta^* : P(Q) \times \Sigma^* \to P(Q)$ par : 
> $\forall X \subseteq Q, \forall a \in \Sigma, \forall u \in \Sigma^*, \hat\delta^*(X, \varepsilon) = X$ et $\hat\delta^*(X, au) = \hat\delta^*(\hat\delta(X, a), u)$
> Intuitivement $\hat\delta^*(X, u)$ est l'ensemble des états où on se trouve en lisant $u$ à partir d'un état de $X$.

> [!tip] Proposition
> Un mot $u \in \Sigma^*$ est reconnu par un AFND ssi $\hat\delta^*(I, u) \cap T \neq \varnothing$
> 

> [!rem] Remarque
> En pratique, il suffit donc de calculer $\hat\delta^*$ pour savoir si un mot est reconnu.
> Complexité $O(|Q|\cdot|u|)$ avec une représentation d'ensemble en $\Theta(1)$
> On maintient en tout temps une variable correspondant à l'ensemble des états en cours.

### 3) Déterminisation

> [!help] Questions
> Les automates non déterministes sont a priori plus expressifs. Mais est-ce le cas ?
>Existe-t-il des langages reconnus pour un automate fini non déterministe mais aucun déterministe ?
><u>Non</u>

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, I, T, \Delta)$ un AFND. **Déterminiser** $\mathcal A$ c'est trouver un AFD $\mathcal A' = (Q', \Sigma, q_0', T', \delta')$ tel que $L(\mathcal A) = L(\mathcal A')$

> [!rem] Remarque
> C'est toujours possible !

> [!success] Idée
> On crée un automate dont les états sont les parties de $Q$ (qui correspondent intuitivement aux états où on peut se trouver) et on simule toutes les exécutions possibles.

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, I, T, \Delta)$ un AFND. L'**automate des parties** de $\mathcal A$ est l'automate déterministe complet $\hat{\mathcal A} = (\hat Q, \Sigma, \hat{q_0}, \hat T,\hat\delta)$ avec :
> - $\hat Q = P(Q)$
> - $\hat{q_0} = I \in P(Q) = \hat Q$
> - $\hat T = \{X \subseteq Q\ |\ X \cap T \neq \varnothing \}$
> - $\hat\delta : \hat Q \times \Sigma \to \hat Q$ définie précédemment

> [!tip] Algorithme
> Pour construire l'automate des parties on cherche $\hat\delta$ sous la forme d'un tableau.
> On part de $\hat{q_0} = I$ et on construit les images par $\hat\delta$ des états de $\hat Q$ que l'on trouve. 
> On construire ainsi les états accessibles de l'automate des parties.

> [!rem] Remarque
> Il s'agit bien d'un automate fini déterministe complet.

> [!tip] Proposition
> Soit $\mathcal A = (Q, \Sigma, I, T, \Delta)$ un AFND. Il existe un AFDC qui reconnait le même langage.

> [!tldr] Démonstration
> On construit $\hat{\mathcal A} = (\hat Q, \Sigma, \hat{q_0}, \hat T,\hat\delta)$ l'automate des parties et on montre qu'il reconnait le même langage.
> On a pour $u \in \Sigma^*$
> $u \in L(\mathcal A) \iff \hat\delta(I, u) \cap T \neq \varnothing$
> $\iff \hat\delta(\hat{q_0}, u) \in \hat T$
> $\iff u \in L(\hat{\mathcal A})$

> [!rem] Remarque
> L'automate ainsi construit possède $2^n$ états (au pire). On ne peut pas faire mieux (cf TD).

#### $\varepsilon-$transitions

> [!def] Définition
> Un AFND avec $\varepsilon-$**transitions** (ou **transitions spontanées**) est un quintuplet $\mathcal A = (Q, \Sigma, I, T, \delta)$ où :
> - $I \subset Q$
> - $T \subset Q$
> - $\delta \subset Q \times (\Sigma \cup \{\varepsilon\}) \times Q$

> [!def] Définition
> Soit $\mathcal A = (Q, \Sigma, I, T, \delta)$ un $\varepsilon-$AFND. Pour un état $q \in Q$, l'$\varepsilon$-**fermeture** $E(q)$ de $q$ est la partie de $Q$ définie inductivement par :
> - $q \in E(q)$
> - si $q' \in E(q)$ et $q' \overset{\varepsilon}{\leadsto} q''$ alors $q'' \in E(q)$.
> On étend cette notion aux parties de $Q$ en posant $E(X) = \bigcup\limits_{q \in X}E(Q)$ si $X \subset Q$.

> [!rem] Remarques
> - $E(q)$ est le plus petit ensemble clos par $\varepsilon-$transition
> - $E(E(X)) = E(X)$

> [!def] Définition
> Un mot $u \in \Sigma^*$ est **reconnu** par un $\varepsilon-$AFND s'l existe un chemin d'un état initial à un état final d'étiquette $u$. L'étiquette d'un chemin étant la concaténation des lettres vues comme mots de taille $1$ et des $\varepsilon$ vus comme mots vides.

> [!def] Définition
> Pour un $\varepsilon-$AFND $\mathcal A = (Q, \Sigma, I, T, \delta)$, on définit la fonction de transition étendue $\delta^*$ par :
> - $\delta^*(q, \varepsilon) = E(q)$
> - pour $w \in \Sigma^*$ et $a \in \Sigma$, $\delta^*(q, wa) = \bigcup\limits_{q' \in \delta^*(q, w)}E(\delta(q', a))$

> [!rem] Remarque
> Si $u = u_1\cdots u_n \in \Sigma^*$ est reconnu par un $\varepsilon-$AFND alors $\exists q_0 \in I, \exists q_f \in T, q_0 \overset{u}{\leadsto}q_f$. On ne peut pas dire qu'il existe $q_1 \in Q$ tel que $q_0 \overset{u_1}{\leadsto}q_1$

> [!rem] Remarque
> $E(\delta^*(q, u)) = \delta^*(q, u)$ pour $q \in Q$ et $u \in \Sigma^*$

> [!tip] Proposition
> $\hat\delta^*(X, \varepsilon) = E(X)$
> $\hat\delta^*(wa) = E\left(\hat\delta\left(\hat\delta^*(X, w), a\right)\right)$

## V - Le théorème de Kleene

> [!def] Définition
Soit $\Sigma$ un alphabet. On note $Reg(\Sigma)$ l'ensemble des langages réguliers (rationnels) et $Rec(\Sigma)$ l'ensemble des langages reconnaissables par un automate.

> [!rem] Remarque
> On a vu que les de reconnaissable par un automate fini déterministe (complet, émondé si non vide, standard), par un automate non déterministe, par automate avec $\varepsilon-$transition, par un automate généralisé, coïncident et on peut juste parler des langages reconnaissables.

> [!prop] Propriété de clôture
> Soit $L, L' \in Rec(\Sigma)$.
> - $\Sigma^*\setminus L \in Rec(\Sigma)$
> - $L \cup L' \in Rec(\Sigma)$
> - $L \cap L' \in Rec(\Sigma)$
> - $L \bullet L' \in Rec(\Sigma)$
> - $L^* \in Rec(\Sigma)$

> [!rem] Remarque
> On peut généraliser à une union ou intersection finie.

> [!rem] Remarque
> $L = \bigcup\limits_{u \in L}\{u\}$ donc pas de généralisation à une union infinie.

> [!tip] Théorème de Kleene
> $Reg(\Sigma)=Rec(\Sigma)$

> [!tip] Conséquence
> Si $e \in \mathcal E (\Sigma)$ alors $\exists e' \in \mathcal E(\Sigma)$ tq $L(e') = \Sigma^*\setminus L(e)$ et si $e_1, e_2 \in \mathcal E(\Sigma), \exists e \in \mathcal E(\Sigma)$ tq $L(e) = L(e_1) \cap L(e_2)$

> [!rem] Remarque
> A partir de $e$ on construit un automate qui reconnait $L(e)$. On construit $\mathcal A'$ qui reconnait $\Sigma^* \setminus L(\mathcal A)$ et on construit $e'$ qui dénote $L(\mathcal A')$

## VI - Langages non reconnaissables

On sait montrer qu'un langage est reconnaissable si :
- on trouvee une expression régulière qui le dénote
- on trouve un automate (de tout type) qui le reconnait
- on utilise les propriétés de clôture

> [!tip] Lemme de l'étoile
> Soit $L$ un langage régulier. Il existe $N \in \mathbb N^*$ tel que pour tout mot $u \in L$, avec $|u| \geq N$, il existe une décomposition $x, y, z \in \Sigma^*$ avec :
> - $xyz = u$
> - $|xy| \leq N$
> - $y \neq \varepsilon$
> - $\forall k \in \mathbb N, xy^kz \in L$

> [!tldr] Démonstration
> Soit $\mathcal A = (Q, \Sigma, q_0, T, \delta)$ un AFDC. On note $N = |Q| \in \mathbb N^*$. Soit $u \in L \cap \Sigma^{\geq N}$. On pose $|u| = m$.
> Pour $i \in [[0, N]]$, on note $v_i = u_1\cdots u_i$ le préfixe de $u$ de longueur $i$. La fonction $\varphi : \begin{array}{rcl} [[0, N]] &\to& Q \\ i &\mapsto& \delta^*(q_0, u_i) \end{array}$ est non bijective.
> Donc $\exists i, j \in [[0, N]], i < j$ tels que $\delta^*(q_0, v_i) = \delta^*(q_0, v_j)$.
> On pose $x = u_1 \cdots u_i$, $y = u_{i+1} \cdots u_j$ et $z = u_{j+1} \cdots u_m$.
> On a bien :
> - $u = xyz$
> - $|xy| = j < N$
> - $y = u_1 \cdots u_j \neq \varepsilon$ car $j > i \geq 0$
> - Soit $k \in \mathbb N$, montrons que $xy^kz \in L$.
> On a $\delta^*(q_0, x) = \delta^*(q_0, v_i)$, $\delta^*(q_0, xy) = \delta^*(q_0, v_j)$.
> On montre par récurrence que $\forall k \in \mathbb N, \delta^*(q_0, xy^k) = \delta^*(q_0, xy) = \delta^*(q_0, x)$

> [!rem] Remarque
> $N$ est le nombre d'états d'un automate qui reconnait $L$.

> [!example] Exemple
> $L = \{a^nb^n, n \in \mathbb N\}$ n'est pas régulier.
> Supposons par l'absurde que $L$ est régulier, il vérifie le lemme de l'étoile, donc $\exists N \in \mathbb N$ une constante donnée par ce lemme.
> On considère $u = a^Nb^N$.
> Il existe $x, y, z \in \Sigma^*$ avec :
> - $xyz = u$
> - $|xy| \leq N$
> - $y \neq \varepsilon$
> - $xy^*z \subseteq L$
> On pose $k = 0$. Comme $xyz = a^Nb^N$ et que $|xy| \leq N$, $xy$ est préfixe de $a^N$ donc $\exists i, j, k \in \mathbb N$ tq :
> - $x = a^i$
> - $y = a^j$
> - $z = a^kb^N$
> - $j \neq 0$
> donc $xy^0z = a^{i+k}b^N \in L$ absurde car $i + k < i + j + k = N$.


> [!danger] Exercice
> $L = \{u \in \Sigma^* | |u|_a = |u|_b\}$ n'est pas régulier.
> Supposons par l'absurde que $L$ est régulier, il vérifie le lemme de l'étoile, donc $\exists N \in \mathbb N$ une constante donnée par ce lemme.
> On considère $u = a^Nb^N$.
> Il existe $x, y, z \in \Sigma^*$ avec :
> - $xyz = u$
> - $|xy| \leq N$
> - $y \neq \varepsilon$
> - $xy^*z \subseteq L$
> On pose $k = 0$. Comme $xyz = a^Nb^N$ et que $|xy| \leq N$, $xy$ est préfixe de $a^N$ donc $\exists i, j, k \in \mathbb N$ tq :
> - $x = a^i$
> - $y = b^j$
> - $z = a^kb^N$
> - $j \neq 0$
> donc $xy^0z = a^{i+k}b^N \in L$ absurde car $i + k < i + j + k = N$.

> [!tip] Proposition
> Soit $L, L'$ deux langages. Si $L$ est régulier et si $L \cup L'$ n'est pas régulier alors $L'$ n'est pas régulier

> [!rem] Remarque
> De même avec intersection, concaténation et étoile de Kleene

> [!rem] Remarque
> $L$ est régulier ssi $\Sigma^*\setminus L$ est régulier.

