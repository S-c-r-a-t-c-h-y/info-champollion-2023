# Chapitre 11 - Décidabilité et calculabilité

Il faut commencer par formaliser la notion de *problème* et de *procédure effective*. On se donne un alphabet $\Sigma$ contenant au moins deux symboles. On pourra prendre par exemple :
- $\Sigma = \{0, 1\}$, correspondant à la représentation binaires de données informatique
- $\Sigma = \{0, \cdots, 255\}$, correspondant à la représentation textuelle de données informatique en utilisant par exemple le code ASCII étendu

## I - Représentation des données

Pour que l'on puisse traiter un problème avec un ordinateur, il faut que les données soient représentables.

On se convainc assez facilement que toute donnée informatique, pour être effectivement utilisable, doit être *représenté* en machine par un mot $\Sigma$.

> [!def] Définition
> Un **algorithme** ou un **programme informatique** est une donnée textuelle. on peut donc en particulier écrire des algorithmes qui prennent en entrée ou renvoient en sortie des algorithmes.

## II - Problèmes de décision

> [!def] Définition
> On appelle **problème de décision** la donnée d'une instance et d'une question dichotomique :
> - **Instance :** un objet possédant certaines caractéristiques
> - **Question :** une question à laquelle on ne peut répondre que "oui" ou "non"

> [!def] Définition
> On appelle **instance positive** (resp. **négative**) une instance pour laquelle la réponse à la question est "oui" (resp. "non").

Puisque l'on suppose donc que l'on peut représenter les données sous la forme d'un mot sur $\Sigma$, une *instance* d'une problème est donc un mot de $\Sigma^*$

> [!def] Définition
> Un problème de décision est la donnée d'une partition de $\Sigma^*$ par trois langages $L_+ \sqcup L_- \sqcup L_{\#} = \Sigma^*$.
> - $L_+ = \{u \in \Sigma^* \mid \text{la réponse au problème } u \text{ est oui}\}$
> - $L_- = \{u \in \Sigma^* \mid \text{la réponse au problème } u \text{ est non}\}$
> - $L_{\#} = \{u \in \Sigma^* \mid u \text{ n'est pas une instance du problème}\}$
>
> $L_+$ est l'ensemble des *instances positives* du problème, $L_-$ est l'ensemble des *instances négatives* du problème et $L_{\#}$ est l'ensemble des mots qui ne forment pas une instance valable du problème.

Déterminer si un mot $u \in L_{\#}$ est en général facile et on fusionne souvent $L_-$ et $L_{\#}$. Dans ce cas un problème est un langage $L \subseteq \Sigma^*$ correspondant à l'ensemble de ses instances positives. Un mot qui n'est pas dans $L$ est une instance négative ou ne représente pas une instance valable.

> [!success]
> Un problème de décision correspond au langage $L \subseteq \Sigma^*$ des mots qui sont la représentation de ses instances positives. 

## III - Procédure effective

> [!def] Définition
> Une **procédure effective** correspond à la réalisation d'un programme, qui est une chaîne de caractères dans un langage de programmation (par exemple OCaml ou C) sur un ordinateur **idéal**, **déterministe** possédant une mémoire **infinie**.

