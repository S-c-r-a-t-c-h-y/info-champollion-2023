# Chapitre 4 - Langages locaux

> [!help] Cours
> Voir [TD 5](https://carnot.cpge.info/wp-content/uploads/2023/09/td05_locaux.pdf)

> [!def] Définition
> Soit $\Sigma$ un alphabet et $L \subseteq \Sigma^{*}$ un langage sur $\Sigma$. On définit les **ensembles de préfixes, suffixes, et facteurs de longueur 2** de $L$ de la manière suivante :
> - $P(L) = \{a \in \Sigma\ |\ a\Sigma^{*} \cap L \neq \varnothing\}$
> - $S(L) = \{a \in \Sigma\ |\ \Sigma^{*}a \cap L \neq \varnothing\}$
> - $F(L) = \{m \in \Sigma^{2}\ |\ \Sigma^{*}m\Sigma^{*} \cap L \neq \varnothing\}$
> - $N(L) = \Sigma^{2}\setminus F(L)$
> $P(L)$ est l’ensemble des premières lettres des mots de $L$, $S(L)$ celui des dernières lettres des mots de $L$, $F(L)$ l’ensemble des facteurs de longueur 2 que l’on trouve dans des mots de $L$ et $N$ celui des facteurs de longueur 2 qui sont évités par tous les mots de $L$ ($N$ pour non facteur).

> [!tip] Proposition
> Pour tout langage $L \subseteq \Sigma^{*}$ :
> $L \setminus \{\varepsilon\} \subseteq \left( P(L)\Sigma^{*} \cap \Sigma^{*}S(L) \right) \setminus \Sigma^{*}N(L)\Sigma^{*}$

> [!def] Définition
> Un langage $L \subseteq \Sigma^{*}$ est **local** s'il existe $P \subseteq \Sigma$, $S \subseteq \Sigma$ et $N \subseteq \Sigma^{2}$ tels que :
> $L \setminus \{\varepsilon\} = \left( P\Sigma^{*} \cap \Sigma^{*}S \right) \setminus \Sigma^{*}N\Sigma^{*}$
> On a alors $P(L) \subseteq P$, $S(L) \subseteq S$ et $N \subseteq N(L)$ et égalité dans la proposition ci-dessus.


