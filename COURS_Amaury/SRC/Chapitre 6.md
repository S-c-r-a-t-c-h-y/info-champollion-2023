# Chapitre 6 - Parcours en profondeur et applications

> [!rem] Remarque
> A graph $G = (V, E)$ has **vertices** V ( $v \in V$ is a **vertex**) and **edges** E ($e \in E$ is an **edge**)

## I - Représentation des graphes

> [!info]
Dans toute cette partie, pour simplifier, on suppose que $S = [[0, n-1]]$

> [!help] Prérequis
> Les algorithmes de parcours n'ont besoin que de deux primitives sur un graphe $G = (S, A)$. Une fonction `size : graph -> int` qui calcul $m = |S|$, l'**ordre** d'un graphe, c'est à dire le nombre de sommets et une fonction `neighbours : graph -> int -> int list` qui renvoie la liste des **successeurs** (ou **voisins**) d'un sommet du graphe.

> [!rem] Remarque
> Une représentation par liste d'adjacences permet de réaliser ces deux primitives en $\Theta(1)$

> [!example] Exemple d'implémentation
> ```ocaml
> type graph = int list array
> 
> let create n = Array.make n []
> let size = Array.length
> let has_edge g u v = List.mem v g.(u)
> 
> let add_edge g u v =
> 	if not @@ has_edge g u v then g.(u) <- v::g.(u)
> 	
> let neighbours g u = g.(u)
> ```

> [!rem] Remarque
> Pour un graphe non orienté, il y a juste à modifier la fonction `add_edge`.
> ```ocaml
> let add_edge g u v =
> 	if not @@ has_edge g u v then
> 	(g.(u) <- v::g.(u);
> 	g.(v) <- u::g.(v))
> ```

> [!rem] Remarque
> Ceci est un exemple d'implémentation, mais toute implémentation est possible puisqu'on utilise que l'interface.

## II - Parcours en profondeur

### A - Le squelette de l'algorithme

> [!info]
> Parcours en profondeur : depth-first-search (DFS)
> Il s'agit d'un algorithme fondamental.

> [!rem] Remarque
> Enormément d'algorithmes utilisent le parcours en profondeur ou en sont des adaptations / extensions. Il faut être absolument clair avec ce chapitre.

> [!success] Idée
> Explorer tant que c'est possible avant de faire marche-arrière (*backtracking*)
> On marque les sommets pour ne pas les reparcourir.

```pseudo
	\begin{algorithm}
	\caption{Parcours en profondeur}
	\begin{algorithmic}
		\Function{Explorer}{$u$}
			\If{$u$ non marqué}
				\State Marquer($u$)
				\For{chaque sommet $u->v$}
					\State Explorer($v$)
				\EndFor
			\EndIf
		\EndFunction
		\Function{DFS}{$g$}
			\State démarquer chaque sommet dans $S$
			\For{chaque sommet $u$ dans $S$}
				\State Explorer($u$)
			\EndFor
		\EndFunction
	\end{algorithmic}
	\end{algorithm}
```

> [!example] Implémentation en OCaml
> ```ocaml
> let dfs g =
> 	let n = size g in
> 	let vu = Array.make n false in
> 	let rec explorer u =
> 		if not vu.(u) then begin
> 			vu.(u) <- true
> 			List.iter explorer (neighbours g u)
> 		end
> 	in
> 	for i = 0 to n-1 do
> 		explorer i
> 	done
> ```
>
>
> > [!rem] Remarque
> > On peut aussi (en pratique cela accélère légèrement les choses) supposer dans explorer que le sommet n'est pas marqué et effectuer cette vérification avant tout appel à explorer
>
>
> ```ocaml
> let rec explorer g u vu =
> 	assert (not vu.(u));
> 	vu.(u) <- true;
> 	explorer_voisins g (neighbours g u) vu
> and explorer_voisins g voisins vu =
> 	match voisins with
> 	| [] -> ()
> 	| v::suite ->
> 		if not vu.(v) then explorer g v vu;
> 		explorer_voisins g suite vu
> 
> let dfs g =
> 	let n = size g in
> 	let vu = Array.make n false in
> 	for i = 0 to n-1 do
> 		if not vu.(i) then explorer g i vu
> 	done
> ```

> [!rem] Remarque
> Cet algorithme fonctionne à l'identique que le graphe soit non orienté ou non.

> [!def] Définition
> Pour $G = (S, A)$, $u \in S$, on note `accessible(u)` l'ensemble des sommets accessibles à partir de $u$ dans $G$ (c'est sa composante connexe si le graphe est non orienté)

> [!tip] Proposition
> En supposant qu'aucun sommet de `accessible(u)` n'est marqué, la fonction `explorer` parcours et marque chaque sommet de `accessible(u)` et exactement ceux là.

> [!danger] Complexité
> Avec `size` et `neighbours` en $\Theta(1)$, la complexité est $O(|S| + |A|)$ (à connaître **par coeur**)

### B - Forêt de parcours

> [!def] Définition
> On peut facilement modifier le parcours précédent pour obtenir les arcs qui nous ont permis de découvrir un sommet pour la première fois. Ces arcs sont appelés **arcs de parcours**.

> [!tip] Proposition
> L'ensemble des sommets et des arcs de parcours forment une forêt (c'est à dire un graphe acyclique) appelée **forêt de parcours**.

> [!info]
> Comme pour `unir` et `trouver`, on peut représenter cette forêt par un tableau père.

> [!rem] Remarque
> On peut se passer de `vu` avec la convention `pere.(u) = -1` si `u` n'est pas marqué.

> [!example]
> ```mermaid
> flowchart RL
> subgraph foret2
> direction LR
> I[A]
> J[C] --> K[D] --> L[B]
> end
> subgraph foret1
> direction LR
> E[A] --> F[B] --> G[C] --> H[D]
> end
> subgraph graphe
> direction LR
> A --> B --> C --> D --> B
> end
> ```

```ocaml
let dfs g =
	let n = size g in
	let pere = Arrat.make n (-1) in
	let rec explorer p u =
		if pere.(u) = -1 then begin
			pere.(u) <- p;
			List.iter (explorer u) (neighbours g u)
		end
	in
	for i = 0 to n-1 do
		explorer i i
	done;
	pere
```

### C - Prévisité et postvisité

> [!rem] Remarque
> En l'état, l'algorithme ne fais rien d'autre que parcourir et marquer les sommets.

> [!def] Définition
> On peut effectuer quelque chose lorsque l'on rencontre le sommet (**prévisite**) ou lorsque l'on en a terminé avec lui (**postvisite**). C'est à dire soit avant, soit après avoir exploré récursivement les sommets.

```ocaml
let rec explorer u =
	if not vu.(u) then begin
		vu.(u) <- true;
		previsiter u;
		List.iter explorer (neighbours u);
		postvisiter u
	end
```

> [!example] Exemple
> Les fonctions `previsiter` et `postvisiter` peuvent faire quelque chose d'intéressant.
> ```ocaml
> let previsiter u = Printf.printf "%d; " u
> let postvisiter u = ()
> ```

> [!rem] Remarque
> Un parcours dans l'ordre préfixe s'obtient avec `previsiter` et dans l'ordre postfixe (ou suffixe) avec `postvisiter`.

> [!info]
> Il peut être utile de retenir le temps de première découverte et de fin de traitement de chaque sommet.
> ```ocaml
> let horloge = ref 0 in
> let pre = Array.make n (-1) in
> let post = Array.make n (-1) in
> let previsiter u =
> 	incr horloge;
> 	pre.(u) <- !horloge;
> in
> let postvisiter u =
> 	incr horloge;
> 	post.(u) <- !horloge;
> ```

> [!tldr] Notation
> On va noter pre(u) et post(u) les temps de début et fin de traitement de u. Remarquons que l'on affecte la valeur pre(u) immédiatement après avoir ajouté u sur la pile (récursion) et que l'on affecte la valeur post(u) immédiatement avant d'enlever u de la pile. Donc l'intervalle de temps [pre(u), post(u)] correspond exactement à l'intevalle de présence de u sur la pile.

> [!def] Définition
> On dit que u est **actif** on **en cours de traitement** ou sur la pile, pendant l'intervalle $[\text{pre}(u), \text{post}(u)]$.
> On dit aussi que $u$ commence au temps $\text{pre}(u)$ et termine au temps $\text{post}(u)$.
> On note $\underset{u}{[} \quad \underset{u}{]}$ l'intervalle $[\text{pre}(u), \text{post}(u)]$

> [!rem] Remarque
> Il s'ensuit que pour deux sommets $u, v \in S$, les intervalles $[\text{pre}(u), \text{post}(u)]$ et $[\text{pre}(v), \text{post}(v)]$ sont disjoints ou l'un est inclus dans l'autre. De plus, $[\text{pre}(u), \text{post}(u)]$ contient $[\text{pre}(v), \text{post}(v)]$ si et seulement si $v$ a été découvert pendant l'exploration de $u$ : $\underset{u}{[}\quad \underset{v}{[} \quad \underset{v}{]}\quad \underset{u}{]}$

> [!tip] Proposition
> Notons $(u_{1},\cdots, u_{p})$ les sommets empilés. Alors pour tout $i \in [[1, p]]$, $u_{i}$ a été découvert par $u_{i-1}$. Donc $(u_{i-1}, u_{i})$ est un arc de parcours.

> [!tip] Proposition
> Si $u$ se trouve en même temps que $v$ sur la pile et si $u$ précède $v$ sur cette pile, alors $u$ est ancêtre de $v$ dans la forêt de parcours, $v$ a été découvert pendant l'exploration de $u$ et on a $\underset{u}{[}\quad \underset{v}{[} \quad \underset{v}{]}\quad \underset{u}{]}$

> [!def] Définition
> L'ordre total sur les sommets donné par pré s'appelle **ordre de prévisite** et celui donné par post s'appelle **ordre de postvisite**.
> A un moment $t$ de l'horloge pour un sommet $u \in S$ il y a trois possibilités :
> - $t < \text{pre}(u)$, on dit alors que $u$ est **vierge**. Le sommet n'a encore jamais été exploré. L'appel `explorer(u)` n'a pas eu lieu.
> - $t \in [\text{pre}(u), \text{post}(u)]$, on a alors $u$ est **actif**, en cours de traitement, sur la pile. L'appel à `explorer(u)` a eu lieu mais n'est pas terminé.
> - $t > \text{post}(u)$, on dit que $u$ est **terminé**. L'appel `explorer(u)` est terminé.

### D - Classification des arcs (cas orienté)

> [!def] Définition
> On peut distinguer quatre catégories possibles pour un arc $a \in A$ de $G = (S, A)$ après un parcours en profondeur :
> Les **arcs de parcours** (*tree edges*) qui font partie de l'arborescence de parcours
> Les **arcs arrières** (*back edges*) qui relient un sommet à un de ses ancêtres
> Les **arcs avant** (*forward edges*) qui relient un sommet à un de ses descendants propres
> Les **arcs transverses** (*cross edges*) qui sont les autres

> [!tip] Classification des arcs
> Considérons un arc $(u, v) \in A$. Sa catégorie dépend de l'état de $v$ lors du traitement de cet arc, qui a lieu lors de l'exploration de $u$ lors du traitement de son voisin $v$. A ce moment $u$ est actif.
> - Si $v$ est vierge. $u$ devient le père de $v$ et l'arc $(u,v)$ est un arc de parcours. On a l'imbrication $\underset{u}{[}\quad \underset{v}{[} \quad \underset{v}{]}\quad \underset{u}{]}$
> - Si $v$ est actif. Dans ce cas $v$ est sur la pile et puisque $u$ est au sommet, $u$ est un descendant de $v$ dans la forêt de parcours déjà construite. L'arc $(u, v)$ est alors un arc arrière. On a l'imbrication $\underset{v}{[}\quad \underset{u}{[} \quad \underset{u}{]}\quad \underset{v}{]}$
> - Si $v$ est terminé. Remarquons que $u$ n'est pas accessible à partir de $v$.
> 	- Si $v$ a été découvert avant $u$ ($\text{pre}(v) < \text{pre}(u)$) alors $v$ a entièrement terminé avant que $u$ ne commence (car $u$ non accessible à partir de $v$). On a donc un arc transverse et l'imbrication $\underset{v}{[}\quad \underset{v}{]} \quad \underset{u}{[}\quad \underset{u}{]}$
> 	- Si $v$ a été découvert après $u$, donc si $\text{pre}(u) < \text{pre}(v)$, alors $v$ est terminé et $u$ ne l'est pas. Il a donc été découvert pendant l'exploration des descendants de $u$. Donc $v$ est un descendant de $u$. On a un arc avant et l'imbrication $\underset{u}{[}\quad \underset{v}{[} \quad \underset{v}{]}\quad \underset{u}{]}$

> [!rem] Remarque
> Le cas $\underset{u}{[}\quad \underset{u}{]} \quad \underset{v}{[}\quad \underset{v}{]}$  est impossible en présence de l'arc $(u, v)$

> [!rem] Remarque
> On peut retrouver la classification des arêtes à posteriori grâce à la forêt de parcours et du tableau $\text{pre}$ et $\text{post}$.



```mermaid
flowchart TD
0 --- 1
0 --- 2
0 --- 3
0 --- 4
1 --- 4
2 --- 3
5 --- 7
6 --- 5
7 --- 6
```

```mermaid
flowchart LR
0 --> 1
0 --> 2 --> 3 --> 1
```
> [!tip] Proposition
> Considérons un parcours d'un graphe $G = (S, A)$ et deux sommets $u, v \in S$, les propositions suivantes sont équivalentes :
> - $u$ est un ancêtre de $v$ dans la forêt de parcours
> - $\underset{u}{[}\quad \underset{v}{[} \quad \underset{v}{]}\quad \underset{u}{]}$
> - au moment de l'appel à `explorer(v)`, $u$ est actif
> - juste avant l'appel `explorer(u)`, il existe un chemin de $u$ à $v$ dont tous les sommets sont vierges

> [!rem] Remarque
> Dans le cas d'un graphe non orienté, il n'y a que deux type d'arêtes : les arêtes de parcours et les autres

### E - Graphes orientés acycliques

Les graphes orientés acycliques (en anglais directed acyclic graph -> dag) jouent un rôle important en informatique et en particulier en programmation dynamique.

> [!def] Définition
> Un sommet sans prédécesseurs est appelé une **source**. Un sommet sans successeur est appelé un **puits**.

> [!rem] Remarque
> Un sommet isolé est à la fois une source et un puits.

> [!tip] Proposition
> Un graphe orienté acyclique comporte au moins une source et un puits.

> [!rem] Remarque
> Remarquons que si lors d'un parcours en profondeur on identifie un arc arrière $(u, v)$, autrement dit $\underset{v}{[}\quad \underset{u}{[} \quad \underset{u}{]}\quad \underset{v}{]}$ c'est à dire $v$ actif, alors $u$ est descendant de $v$ dans la forêt de parcours : il existe un chemin $\underbrace{x_{0}}_{=v}x_{1}\cdots\underbrace{x_{k}}_{=u}$ donc $x_{0}x_{1}\cdots x_{k}v$ est un cycle du graphe (ou du moins on peut en extraire un)
> Inversement, s'il existe un cycle dans le graphe, soit $v_{0}$ le premier sommet de $a$ cycle exploré par le parcours. Notons $v_{0}v_{1}\cdots v_{k}v_{0}$ le cycle, alors $(v_{k}, v_{0})$ sera un arc arrière car $v_{k}$ sera visité pendant l'exploration de $v_{0}$

> [!tip] Proposition
> Un graphe $G = (S, A)$ comporte un cycle si et seulement si pour tout parcours en profondeur de $a$ graphe ce parcours comporte un arc arrière.

En pratique, pour détecter un arc arrière on peut vérifier dans le traitement d'un sommet $u$ si on rencontre un sommet $v$ actif.
On peut utiliser les status

```ocaml
type statut = 
| Vierge
| Actif
| Fini
```

> [!rem] Remarque
> On peut aussi utiliser les tableaux pre et post pendant le parcours ou même à la fin, après le parcours. Il suffit, après le parcours, d'iterer sur les arcs à la recherche d'un arc $(u, v)$ avec $pre(v) < pre(u) < post(u) < post(v)$

> [!rem] Remarque
> Tout ceci est bien plus simple pour un graphe non orienté. Il y a un cycle ssi on retombe sur un sommet déjà vu (différent de son père). Il y a simplement besoin de deux status.

### F - Tri topologique

Les dags sont utiles pour modéliser des dépendances ou causalités. Chaque noeud est une tâche et il y a un arc entre deux tâches si l'une doit être effectuée avant l'autre.
La question est alors : dans quel ordre effectuer ces tâches.
S'il y a un cycle : peu d'espoir. Ce qui motive les dags.
La question est de linéariser ou trier topologiquement (intuitivement le représenter sur une ligne avec toutes les flèches qui vont de gauche à droite)

> [!def]
> Un **ordre topologique** (ou tri topologique) est une relation d'ordre totale $<$ sur $S^{2}$ compatible avec $A$, c'est à dire que si $(u, v) \in A$, alors $u < v$

> [!help] Question
> Quels sont les dags qui peuvent être linéarisés ? <u>Tous</u>

> [!tip] Proposition
> Dans un dag si $(u, v) \in A$ alors $post(u) > post(v)$

> [!tldr] Démontration
> Sinon on aurait un arc arrière donc un cycle absurde.

Donc la relation $u < v$ définie par $post(u) > post(v)$ est un ordre topologique. Il suffit donc de trier les sommets par ordre décroissant de fin de traitement.

> [!rem] Remarque
> Il suffit donc par exemple de les mettre dans une pile lorsque l'un en a terminé avec.

> [!rem] Remarque
> Trouver un ordre topologique, vérifier que l'on est dans un dag et absence d'arcs arrières ne sont qu'une et même choses.

Question :
comment trouver un puits ?
Il suffit de prendre le premier sommet terminé d'un parcours

comment trouver une source ?
Il suffit de prendre le dernier sommet terminé d'un parcours

## III - Composantes fortement connexes

### 1) Composante connexe d'un graphe non orienté

Il est facile de modifier le parcours en profondeur pour identifier les composantes connexes d'un graphe non orienté.

> [!rem] Remarque
> Si $u \in S$, alors `explorer(u)` visite $accessible(u)$ qui est la composante connexe de $u$

### 2) Composante fortement connexes pour un graphe orienté

Les choses sont un peu plus délicates


> [!tip] Proposition
> Soient $X$ et $Y$ deux composantes fortement connexes d'un graphe $G=(S, A)$, c'et à dire deux sommets du méta-graphe, avec un arête entre un sommet de $X$ et un sommet de $Y$, donc $X$ et $Y$ reliés dans le métagraphe.
> Alors le plus grand $post$ dans $X$ est plus grand que le plus grand $post$ de $Y$.

```mermaid
flowchart LR
X --> Y
```

> [!tldr] Preuve
> - Si le premier sommet de $X$ ou $Y$ visité est dans $X$, notons le $x_{0}$, alors `explorer(x0)` va explorer et donc affecter $post$ pour tous les sommets de $X$ et $Y$ avant d'affecter $post(x_0)$. Dans ce cas $post(x_0)$ est plus grand que tous les $post$ de $Y$.
> - Si le premier sommet visité est dans $Y$ alors on termine l'exploration de tout $Y$ avant de commencer $X$ et tous les $post$ de $X$ sont plus grands que ceux de $Y$.

Autrement dit, on peut linéariser le méta-graphe (que l'on ne connait pas) en utilisant l'ordre inverse d'un parcours postfixe (c'est à dire l'ordre de post-visite d'un parcours).
On ne cherche pas une source mais un puits. Il suffit de travailler dans le graphe miroir.

> [!tip] Proposition
> Le métagraphe du graphe miroir est le miroir du métagraphe.
> En particulier, les composantes fortement connexes du graphe miroir sont les même que celles du graphe et une source du métagraphe miroir est un puits du métagraphe.

> [!tldr] Preuve
> Il suffit de faire un parcours en profondeur du miroir du graphe, de prendre le sommet avec le $post$ le plus élevé (le dernier) et de relancer un parcours à partir de ce sommet (dans une composante puits donc) pour trouver exactement sa composante fortement connexe, que l'on enlève. Puis on recommence.

En réalité, un seul premier parcours dans le graphe miroir est nécessaire afin d'obtenir l'ordre (inverse de temps de fin de traitement) dans lequel considérer les sommets dans un seul deuxième parcours.

> [!rem] Remarque
> Enlever se fait par simple marquage en ignorant les sommets marqués.

> [!bug] Algorithme de Kosaraju-Sharir
> 1) On calcul le graphe miroir $\tilde{G}$ en $O(|S|+|A|)$
> 2) On effectue un parcours en profondeur de ce graphe $\tilde{G}$. On retient les temps de fin de parcours. On peut aussi mettre dans une pile les sommets rencontrés lorsque l'on en a terminé avec eux. Cette pile est bien triée dans l'ordre décroissant de fin de visite.
> 3) On effectue une recherche classique de composantes connexes (même algorithme que pour le cas non orienté) mais en considérant les sommets dans l'orde inverse du tableau $post$. Autrement dit, en les prenant dans la pile.

> [!info] Complexité
$O(3(|S|+|A|)) = O(|S|+|A|)$

```mermaid
flowchart LR
A --> B
B --> A
B --> C
C --> A
```
