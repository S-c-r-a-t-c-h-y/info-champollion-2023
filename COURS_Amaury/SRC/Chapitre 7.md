# Chapitre 7 - Déduction naturelle

package [EBproof](http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/ebproof/ebproof.pdf)

## Axiome

$\dfrac{}{\Gamma, A \vdash A}ax$

## Implication

$\dfrac{\Gamma, A \vdash B}{\Gamma \vdash A \to B}\to_i$


$\dfrac{\Gamma \vdash A \to B \qquad \Gamma \vdash A}{\Gamma \vdash B}\to_e$

## Conjonction

$\dfrac{\Gamma \vdash A \qquad \Gamma \vdash B}{\Gamma \vdash A \land B}\land_i$


$\dfrac{\Gamma \vdash A \land B}{\Gamma \vdash A}\land_{e}^{g} \qquad \dfrac{\Gamma \vdash A \land B}{\Gamma \vdash B}\land_e^d$

## Disjonction

$\dfrac{\Gamma \vdash A}{\Gamma \vdash A \lor B}\lor_{i}^{g} \qquad \dfrac{\Gamma \vdash B}{\Gamma \vdash A \lor B}\lor_{i}^{d}$


$\dfrac{\Gamma \vdash A \lor B \qquad \Gamma, A \vdash C \qquad \Gamma, B \vdash C}{\Gamma \vdash C}\lor_{e}$

## Négation

$\dfrac{\Gamma, A \vdash \bot}{\Gamma \vdash \lnot A}\lnot_{i}$


$\dfrac{\Gamma \vdash A \qquad \Gamma \vdash \lnot A}{\Gamma \vdash \bot}\lnot_{e}$

## Affaiblissement

$\dfrac{\Gamma \vdash B}{\Gamma, A \vdash B}\text{aff}$

## Introduction de $\top$

$\dfrac{}{\Gamma \vdash \top}\top_{i}$

## Elimination de l'absurde

$\dfrac{\Gamma \vdash \bot}{\Gamma \vdash A}\bot_{e}$

## Raisonnement par l'absurde

$\dfrac{\Gamma, \lnot A \vdash \bot}{\Gamma \vdash A}\text{raa}$

## Tiers exclu

$\dfrac{}{\Gamma \vdash A \lor \lnot A}t.e.$
