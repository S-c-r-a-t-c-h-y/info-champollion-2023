# Chapitre 15 - Grammaire et langages non contextuels

> [!success] Previously on "langages"
> On a déjà étudié la classe des langages réguliers (ou rationnels) qui est une classe de langages simples mais déjà expressifs. Ils correspondent :
> - aux langages dénotés par une expression régulière
> - la plus petite classe de langage contenant $\varnothing$, $\{\varepsilon\}$, les langages d'une lettre et stable par opérations régulières
> - les langages reconnaissables par automate fini

> [!warning] Attention
Mais nous avons vu que certains langages n'étaient pas réguliers : $\{a^nb^n \mid n \in \mathbb N\}$ n'est pas régulier

> [!info] Intuition
> Les langages réguliers sont ceux que l'on peut reconnaître avec un automate qui est à mémoire bornée (mémoire : dans les états et les transitions de l'automate -> le fameux lemme de l'étoile)

On a indiqué de manière informelle que si on ajoute une tranche de mémoire infinie et un curseur on obtient une machine de Turing.

La classe des langages reconnus par des machines de Turing qui s'arrêtent toujours sont les langages décidables.

> [!example] Exemple
> Le langage des mots qui représentent un programme OCaml qui s'arrête toujours n'est pas décidable.

Il existe de nombreuses classe intermédiaires et même une hiérarchie (hiérarchie de Chomsky HP).
Une classe intermédiaire intéressante et celle des langages non contextuels. Ce sont ceux reconnaissables par un automate à pile (HP).
Comme les langages réguliers, il existe de nombreuses caractérisations des langages
- non contextuels
- sans contexte
- hors-contexte
- algébrique
La caractérisation au programme est :
- langage généré par une grammaire non contextuelle

## I - Introduction informelle aux grammaires

> [!def] Définition
 Pour définir une **grammaire**, on se donne un ensemble de *règles de réécriture*, des *symboles terminaux* (un alphabet $\Sigma$), des *symboles non terminaux* (un autre alphabet $V$) et un symbole initial $S \in V$.

> [!example] Exemple
> $\Sigma = \{a, b\}, V = \{S\}, S \to aSb \mid \varepsilon$
> $S \Rightarrow \varepsilon$
> $S \Rightarrow aSb \Rightarrow aaSbb \Rightarrow aaaSbbb$
> Cette grammaire va générer $\{a^nb^n\mid n\in \mathbb N\}$

> [!success] Intérêt
On s'intéresse aux mots de $\Sigma^*$ que l'on peut obtenir à partir de $S$ par application des règles de production.

> [!info] Applications
> - Etude des langages naturelles 
> - Reformulation formelle de la syntaxe des langages (programmation, balisage, HTML, etc...)
> - Compilation
> - Analyse formelle de programmes
> - etc...

> [!danger] Exo 1
> $S \to aSa \mid aSb \mid \varepsilon$
> $\{a^n u \mid u \in \Sigma^n \}$
> 
> $S \to bSbb \mid A, A \to Aa \mid \varepsilon$
> $\{b^na^kb^{2n} \mid n, k \in \mathbb N\}$
>
> $S \to XY \mid a \mid b,\ X \to YS \mid a \mid b,\ Y \to a \mid b$
> $\Sigma^+$

> [!danger] Exo 2
> $S \to x \mid y \mid z \mid (S \lor S) \mid (S \land S) \mid \lnot S \mid (S \to S) \mid \bot \mid \top \mid S \lor S \mid S \land S \mid S \to S \mid (S)$

> [!danger] Exo 3
> 1. $S \to aS \mid bS \mid \varepsilon$
> 2. $S \to X \mid Y,\ X \to aX \mid \varepsilon,\ Y \to bY \mid \varepsilon$
> 3. $S \to XbX,\ X \to aX \mid \varepsilon$
> 4. $S \to XY, \ X \to aX \mid \varepsilon,\ Y \to bY \mid \varepsilon$
> 5. $S \to XS \mid \varepsilon, X \to aaa \mid aab \mid aba \mid abb \mid baa \mid bab \mid bba \mid bbb$
> 6. $S \to aS \mid bS \mid Sa \mid Sb \mid bab$
> 7. $S \to aSb \mid aSbb \mid \varepsilon$
> 8. $S \to aS \mid baS \mid B \mid \varepsilon,\ B \to bB \mid \varepsilon$
> 9. $S \to aSa \mid bSb \mid a \mid b \mid \varepsilon$ 
> 10. $S \to aSa \mid bSb \mid aXb \mid bXa,\ X \to aX \mid bX \mid \varepsilon$
> 11. $S \to aSb \mid bX \mid Xa,\ X \to XX \mid a \mid b \mid \varepsilon$

## II - Formalisation

> [!def] Définition
> Une **grammaire non contextuelle** est un quadruplet $G = (V, \Sigma, R, S)$ avec :
> - $V$ un alphabet des symboles non terminaux
> - $\Sigma$ un alphabet des symboles terminaux avec $V \cap \Sigma = \varnothing$
> - $R \subseteq V \times (V \cup \Sigma)^*$ un ensemble <u>fini</u> de *règles de production*. Si $(X, \alpha) \in R$, on note $X \to \alpha$ la règle
> - $S \in V$ le symbole initial 

> [!rem] Remarque
> Du côté gauche d'une règle, il y a **un** symbole non terminal (non contextuel).

> [!rem] Remarque
> Conventionnellement, on note les symboles de $V$ avec des majuscules et les mots de $(V \cup \Sigma)^*$ par des lettres grecques minuscules.
> 

> [!rem] Remarque
> Lorsque l'on dispose de plusieurs règles $X \to \alpha$ et $X \to \beta$ on note $X \to \alpha \mid \beta$

> [!rem] Remarque
> Toutes les grammaires au programme sont "non contextuelles". On peut juste dire "grammaire".

> [!def] Définition
> Soit $G = (V, \Sigma, R, S)$ une grammaire sans contexte et $\alpha, \beta \in (V \cup \Sigma)^*$.
> - On note $\alpha \Rightarrow \beta$ et on dit que $\alpha$ **se réduit** en $\beta$ si $\exists (X, \gamma) \in R$ un règle, $\alpha_1, \alpha_2 \in (V \cup \Sigma)^*$ tel que $\alpha = \alpha_1X\alpha_2$ et $\beta = \alpha_1\gamma\alpha_2$ ($\alpha_1X\alpha_2 \Rightarrow \alpha_1\gamma\alpha_2$)
> - Pour $n \in \mathbb N$, on note $\alpha \Rightarrow^n \beta$ et on dit que $\alpha$ **se dérive en $n$ étape** en $\beta$ si $\exists \gamma_0, \cdots,\gamma_n \in (V \cup \Sigma)^*$ tels que $\alpha = \gamma_0 \Rightarrow \gamma_1 \Rightarrow \cdots \Rightarrow \gamma_n = \beta$
> - On note $\alpha \Rightarrow^* \beta$ et on dit que $\alpha$ **se dérive** en $\beta$ s'il existe $n \in \mathbb N$ tel que $\alpha \Rightarrow^n \beta$

> [!def] Définition
> Soit $G = (V, \Sigma, R, S)$ une grammaire sans contexte et $u \in \Sigma^*$.
> On dit que $G$ **génère** $u$ si $S \Rightarrow^* u$.
> L'ensemble $L(G) = \{u \in \Sigma^* \mid S \Rightarrow^* u\}$ est appelé langage **engendré** par $G$.

> [!def] Définition
> Un langage $L \subseteq \Sigma^*$ est dit **algébrique** ou **non contextuel** s'il existe une grammaire non contextuelle $G$ telle que $L = L(G)$.

> [!example] Exemple
> $G = (\{S\}, \{a, b\}, \{(S, aSb), (S, \varepsilon)\}, S)$
> $L(G) = \{a^nb^n \mid n \in \mathbb N\} = L$
>
Montrons que $L \subseteq L(G)$
Soit $u \in L,\exists n \in \mathbb N, u = a^nb^n$.
Pour $k \in \mathbb N$, notons $\gamma_k = a^kSb^k \in (V \cup \Sigma)^*$
Montrons par récurrence que $S \Rightarrow^k \gamma_k$
<u>Initialisation :</u> $S \Rightarrow^0 S$
<u>Hérédité :</u> Supposons le résultat vrai pour $k \in \mathbb N$.
On a $S \Rightarrow^k \gamma_k = a^kSb^k \Rightarrow a^kaSbb^k$ avec la règle $S \to aSb$ d'où $S \Rightarrow^{k+1} a^{k+1}Sb^{k+1}$
Pour $k = n$ on a $S \Rightarrow^n a^nSb^n \Rightarrow a^n\varepsilon b^n = a^nb^n$ donc $a^nb^n \in L(G)$
>
Montrons que $L(G) \subseteq L$
Montrons par récurrence sur $n \in \mathbb N$ que si $S \Rightarrow^n \alpha$ alors $\begin{cases} a^{n-1}b^{n-1}\text{ ou}\\ \gamma_n\end{cases}$
<u>Initialisation</u> : Pour $n = 0$, si $S \Rightarrow^0 \alpha$ alors $\alpha = S = \gamma_0$ et la propriété est vérifiée
<u>Hérédité</u> : Supposons la propriété vraie au rang $n \in \mathbb N$ et supposons $S \Rightarrow^{n+1} \alpha$ avec $\alpha \in (V \cup \Sigma)^*$.
$\exists \beta \in (V \cup \Sigma)^*, S \Rightarrow^n \beta \Rightarrow \alpha$
Par HR, $\beta = \begin{cases} a^{n-1}b^{n-1}\text{ ou}\\ \gamma_n\end{cases}$. On a $\beta \Rightarrow \alpha$ donc
$\exists \beta_1, \beta_2 \in (V \cup \Sigma)^*, \beta = \beta_1S\beta_2$ car toutes les règles sont de la forme $S \to \delta$ et $\alpha = \beta_1\delta\beta_2$
Comme $a^{n-1}b^{n-1} \in \Sigma^*$ alors le cas (1) est impossible et comme $|\gamma_n|_S = 1$ la seule possibilité est $\beta_1 = a^n, \beta_2 = b^n$ et donc $\beta = a^nSb^n$ et $\begin{cases} a^{n+1}Sb^{n+1} = \gamma_{n+1}\text{avec la règle } S \to aSb \text{ ou}\\ a^nb^n \text{ avec la règle } S \to \varepsilon \end{cases}$ 
La propriété est donc vérifiée au rang $n+1$.

> [!tip] Proposition
> La classe des langages non contextuels est stable par opérations régulières (union, concaténation, étoile).

> [!tldr] Démonstration
>Soit $L_1$ et $L_2$ deux langages algébriques sur un même alphabet $\Sigma$
Soit $G_1 = (V_1, \Sigma, R_1, S_1)$ et $G_2 = (V_2, \Sigma, R_2, S_2)$ deux grammaires engendrant respectivement ces deux langages.
On peut supposer $V_1 \cap V_2 = \varnothing$, quitte à renommer, et $S \not \in V_1 \cup V_2$.
$L_1 \cup L_2$ est engendré par $G_\cup = (V_1 \sqcup V_2 \sqcup \{S\}, \Sigma, R_1 \sqcup R_2 \sqcup \{S \to S_1, S \to S_2\}, S)$
$L_1 \cap L_2$ est engendré par $G_\cap = (V_1 \sqcup V_2 \sqcup \{S\}, \Sigma, R_1 \sqcup R_2 \sqcup \{S \to S_1S_2\}, S)$
$L_1^*$ est engendré par $G_* = (V_1 \sqcup \{S\}, \Sigma, R_1 \sqcup \{S \to SS \mid S_1 \mid \varepsilon\}, S)$

> [!tip] Proposition
> Les langages réguliers sont algébriques.

> [!tldr] Démonstration
> $\varnothing$ est algébrique : $G = (V, \Sigma, \varnothing, S)$
> $\varepsilon$ est algébrique : $G = (\{S\}, \Sigma, \{S \to \varepsilon\}, S)$
> $\forall a \in \Sigma$, $\{a\}$ est algébrique : $G = (\{S\}, \Sigma, \{S \to a\}, S)$
> et on conclut avec la propriété précédente.

> [!rem] Remarque
> La classe des langages algébriques n'est pas close par complémentaire ni intersection.

> [!rem] Remarque
> L'inclusion est stricte.
> $\{a^nb^n \mid n \in \mathbb N\}$ est non contextuel et non régulier.

## III - Arbre syntaxique (ou arbre d'analyse d'après le programme) (ou *arbre de dérivation*)

> [!def] Définition
> Soit $G = (V, \Sigma, R, S)$ un grammaire et $u \in L(G)$.
> Un **arbre d'analyse syntaxique** (ou arbre de dérivation) est un arbre tel que :
> - la racine a pour étiquette $S$
> - chaque nœud interne a pour étiquette un non terminal de $V$
> - les feuilles sont étiquetées par $\Sigma \cup \varepsilon$
> - une feuille d'étiquette $\varepsilon$ est fille unique
> - les étiquettes des feuilles, lues de gauche à droite, forment le mot $u$
> - un noeud interne d'étiquette $\alpha$ a pour fils des nœuds d'étiquettes $\alpha_1, \alpha_2, \cdots, \alpha_n$, alors $X \to \alpha_1\cdots\alpha_n \in R$

> [!example] Exemple
> $S \to (S) \mid S + S \mid S * S \mid 0 \mid 1 \mid \cdots \mid 9$
> ```mermaid
> flowchart TD
> A[S] --> B[S] --> C[0]
> A --> D[*]
> A --> E[S] --> F[S] --> G[1]
> E --> H[+]
> E --> I[S] --> J[2]
> ```
> $0 * 1 + 2$

> [!def] Définition
> Soit $G$ une grammaire et $u \in L(G)$
> - On dit que $u$ est **ambigu** pour $G$ si il existe plusieurs arbres d'analyse syntaxique distincts pour $u$.
> - On dit que $G$ est **ambiguë** s'il existe au moins un mot ambigu pour $G$.

> [!example] Exemple
> $S \to aSb \mid aXb$
> $X \to AX \mid \varepsilon$
> $A \to a \mid b$
> $u = aababbb$

```mermaid
flowchart LR
subgraph one
	direction TD
	A[S] --> B[a]
	A --> C[X] --> D[A] --> E[a]
	C --> F[X] --> G[A] --> H[b]
	F --> I[X] --> J[A] --> K[a]
	I --> L[X] --> M[A] --> N[b]
	L --> O[X] --> P[A] --> Q[b]
	O --> R[X] --> S[epsilon]
	A --> T[b]
end

subgraph two
	direction TD
	a[S] --> b[a]
	a --> c[X] --> d[A] --> e[a]
	c --> f[X] --> g[A] --> h[b]
	f --> i[X] --> j[A] --> k[a]
	i --> l[X] --> m[A] --> n[b]
	l --> o[X] --> p[A] --> q[b]
	o --> r[X] --> s[epsilon]
	a --> t[b]
end
```

>[!warning] Attention
>L'ambiguïté est une propriété de la *grammaire* pas du langage. 
>On peut avoir pour un même langage, une grammaire ambiguë et une autre qui ne l'est pas.

>[!rem] Remarque
>Il existe des langages intrinsèquement ambigus : toute grammaire qui l'engendre est ambiguë.

>[!def] Définition
>Deux grammaires $G,G'$ sont **faiblement équivalentes** si $\mathcal{L}(G)=\mathcal{L}(G')$.

>[!rem] Remarque
>Faiblement car on n'impose rien sur la forme des dérivations.

>[!info] Dérivation Gauche
>Toujours prendre le symbole non terminal le plus à gauche.

> [!def] Définition
> Soit $G = (V, \Sigma, R, S)$ une grammaire.
> - une **dérivation immédiate à gauche** est une dérivation de la forme $uX\alpha \to u\beta\alpha$ avec $u \in \Sigma^*, (\alpha, \beta) \in R, \alpha \in(V \cup \Sigma)^*$
> - une **dérivation gauche** est une dérivation dont toutes les dérivations immédiates sont gauches.

> [!rem] Remarque
> De même à droite (au programme)

> [!tip] Proposition
> Les dérivations à gauche (ou à droite) sont en bijection avec les arbres d'analyse syntaxique (parcours préfixe)

> [!danger] Exo 7
> 1. $L(G) = \{u \in \Sigma^* \mid |u|_a = |u|_b, \forall v \in \text{pref}(u), |v|_a \geq |v|_b\}$
> 2. 
> ```mermaid
> flowchart TD
> A[S] --> B[S] --> C[epsilon]
> A --> D[S] --> E[epsilon]
> ```
> 
> ```mermaid
> flowchart TD
> A[S] --> B[epsilon]
> ```
> 3. $S \to aSbS \mid \varepsilon$

> [!danger] Exo 4
> Ma version : (marche peut-être pas)
$S \to NT \mid \varepsilon$
$T \to NTOT \mid S$
$N \to 0 \mid 1 \mid \cdots \mid 9 \mid x \mid y \mid " "$
$O \to + \mid * \mid / \mid -$
> Correction :
> $S \to SS+ \mid SS* \mid N \mid x \mid y$
> $N \to 1N' \mid 2N' \mid \cdots \mid 9N' \mid 0$
> $N' \to 0N' \mid 1N' \mid \cdots \mid 9N' \mid \varepsilon$

> [!danger] Exo 6

$a^ib^jc^k, i = j + k$

$S \to aSb \mid aXc \mid \varepsilon$
$X \to aXc \mid \varepsilon$

