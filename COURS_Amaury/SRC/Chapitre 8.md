# Chapitre 8 - Arbres couvrants

Dans ce chapitre on ne considère que des graphes non orientés, connexes et sans boucles.

> [!def] Définition
> Un graphe **pondéré** est un triplet $G = (S, A, P)$ avec $(S, A)$ un graphe et $P : A \to \mathbb R$ une fonction de pondération des arêtes.
> Si $a \in A$, $p(a)$ est le **poids** de l'arête $a$.

## I - Arbres couvrants

> [!note] Rappel
> Un **arbre** est un graphe connexe acyclique.

> [!tip] Propriété
> Soit $G = (S, A)$ un graphe (non orienté). Alors :
> - Si $G$ est connexe, $|A| \geqslant |S|-1$
> - Si $G$ est acyclique, $|A| \leqslant |S| - 1$

> [!rem] Remarque
> Les arbres correspondent à la rencontre entre ces deux mondes et on a $|A| = |S| - 1$

> [!tip] Propriété
> Si $G = (S, A)$ est un graphe non orienté alors les propositions suivantes sont équivalentes :
> - $G$ est un arbre ($G$ est connexe acyclique)
> - $G$ est connexe et $|A| \leqslant |S|-1$
> - $G$ est acyclique et $|A| \geqslant |S|-1$

> [!rem] Remarque
> C'est un très bon exercice de savoir redémontrer ces deux propriétés.

> [!def] Définition
> Un **arbre couvrant** d'un graphe $G = (S, A)$ (pondéré ou non) est un sous-graphe $T = (S, A')$ avec $A' \subseteq A$ qui est un arbre.
> C'est donc un sous-graphe connexe acyclique qui possède tous les sommets. Un arbre couvrant a les mêmes sommets.

> [!rem] Remarque
> Un arbre couvrant correspond donc à la donnée d'un ensemble d'arêtes $A' \subseteq A$ tel que tout sommet de $S$ est incident à une arête de $A'$. On dit que l'arbre couvre les arêtes du graphe.

> [!rem] Remarque
> Un arbre couvrant est un sous-graphe connexe minimal qui couvre tous les sommets.

> [!rem] Remarque
> Les arbres couvrants ont de nombreuses applications : construction de réseaux électriques, téléphoniques etc..
> Les sommets correspondent à des villes et le poids des arêtes au coût de construction d'une ligne entre ces villes. On veut tout interconnecter au plus bas prix.

> [!tip] Propriété
> Un graphe (non orienté, non nécessairement connexe) est connexe si et seulement s'il possède au moins un arbre couvrant.

> [!tldr] Démonstration
> - Si le graphe possède un arbre couvrant $T = (S, A')$ alors tout sommets $x, y \in S$ sont reliés dans $T$ donc dans $G$ donc $G$ est connexe
> - L'arborescence de parcours d'un graphe connexe est un arbre couvrant de ce graphe.

> [!rem] Remarque
> Si le graphe n'est pas connexe, on travaille sur les composantes connexes. On peut parler de **forêt couvrante**.
> Ici on va supposer les graphe connexes.

> [!def] Définition
> Soit $G = (S, A, p)$ un graphe (non orienté) pondéré connexe et $T = (S, A')$ un arbre couvrant e $G$.
> Le **poids de $T$**, noté $p(T)$ est la quantité $p(T) = \sum\limits_{a \in A'}p(A)$

> [!def] Définition
> Un **arbre couvrant de poids minimal** d'un graphe $G$ est un arbre couvrant de $G$ dont le poids est plus petit ou égal au poids de tous les autres arbres couvrants de $G$.

> [!rem] Remarque
> Il n'y a pas unicité a priori de l'arbre couvrant de poids minimal.
> (En TD : c'est la cas si toutes les arêtes ont un poids différent)

> [!rem] Remarque
> Ici encore les applications sont nombreuses.
> Mais comment faire pour trouver un algorithme qui trouve un arbre couvrant de poids minimal ?

> [!rem] Remarque
> Si $T = (S, B)$ est un arbre couvrant de $G = (S, A)$ et si $a \in A \setminus B$ alors $T + a$ contient un cycle. Enlever une arête de ce cycle donne un arbre couvrant $T'$.

> [!rem] Remarque
> Il existe toujours un arbre couvrant de poids minimal, le nombre d'arbre couvrant pour un graphe étant fini.

## II - Algorithme de KRUSKAL

L'algorithme de Kruskal est un algorithme glouton qui permet de résoudre ce problème.

> [!note] Rappel
> Un algorithme glouton effectue à chaque étape le choix optimal à cette étape sans remettre en question ses choix passés.

> [!rem] Remarque
> Un algorithme glouton doit toujours être accompagné d'une preuve de sa correction (si on veut l'utiliser pour trouver une solution optimale ; sinon il s'agit le plus souvent d'un algorithme d'approximation)

> [!info] Idée
On créé un arbre $T$ initialement vide. A chaque étape prendre la plus petite arête disponible admissible (c'est-à-dire qui ne créé pas de cycle dans $T$)
>
Autrement dit
Ajouter, si possible, toutes les arêtes dans l'ordre de poids croissant.

> [!bug] Algorithme de Kruskal
> ```pseudo
> \begin{algorithm} 
> \caption{Algorithme de Kruskal} 
> \begin{algorithmic} 
> \Function{Kruskal}{$G=(S,A,p)$} 
> 	\State Trier $A$ selon $p$ 
> 	\State $T \leftarrow (S, \emptyset)$ 
> 	\State $c$ = creer-partition($|S|$) 
> 	\For{chaque $(x,y) \in A$ trié par ordre de poids croissant}
> 		 \If{trouver($x$) $\neq$ trouver($y$)} 
> 			 \State ajouter($(x,y),T$) 
> 			 \State unir($x,y$) 
> 		\EndIf 
> 	\EndFor 
> 	\State Renvoyer T 
> \EndFunction 
> \end{algorithmic} 
> \end{algorithm} 
> ```

> [!rem] Remarque
> On peut commencer par trier la liste. Ceci permet ensuite de considérer les arêtes par ordre de poids croissant.

> [!help]
> Comment savoir si une arête est admissible, c'est-à-dire si elle créé un cycle dans $T$ ? 
> On peut faire un parcours en profondeur pour détecter si le graphe devient cyclique -> ok mais couteux

> [!tip] Propriété
> L'algorithme de Kruskal construit un arbre couvrant.

> [!tldr] Démonstration
> Au départ $T = (S, \varnothing)$
> <u>Invariant :</u> $T = (S, A')$ est un sous-graphe acyclique.
> Bien conservé car on ajoute une arête seulement si elle ne créé pas de cycle.
> Après $|A|$ ajouts, comme le graphe est connexe, il y a eu au moins $|S| - 1$ ajouts donc $T$ est connexe car acyclique avec au moins $|S| - 1$ arêtes.

> [!rem] Remarque
> On peut s'arêter après $|S| - 1$ ajouts.

> [!success] Principe
> Pour savoir efficacement que la prochaine arête est admissible il suffit de vérifier que les deux sommets qu'elle relie ne sont pas dans une même composante connexe de $T$ (le sous-graphe acyclique en construction). L'ensemble des composantes connexes de $T$ est une partition des sommets de $T$, c'est-à-dire une collection d'ensembles disjoints dynamique. On peut utiliser l'algorithme unir et trouver. Pour savoir s'il faut ajouter une arête $(x, y) \in A$, on vérifie que $trouver(x) \neq trouver(y)$. Si c'est le cas, on l'ajoute à $T$ et on fusionne les composantes connexes avec $unir(x, y)$

> [!rem] Remarque
> On peut aussi utiliser une file de priorité pour gérer l'ensemble $A$ et extraire à chaque étape l'arête minimale restante.

> [!rem] Remarque
> Il peut arriver aussi que les arêtes soient déjà triées.

> [!info] Analyse de la complexité
> Le trie des arêtes se fait en $O(|A|\log|A|)$ avec un tri semi-linéaire (tri par tas, tri fusion, tri rapide (en moyenne)).
> Il y a ensuite :
> - $|A|$ étapes pour la boucle $\rightarrow O(|A|)$
> - Un appel à `creer_partition` $\rightarrow O(|S|)$
> - Au plus $2|A|$ appels à `trouver` $\rightarrow O(2|A|\cdot\log|S|)$
> - Exactement $|S| - 1$ appels à  `unir` $\rightarrow O(|S| \cdot \log |S|)$
>
> Complexité totale :
> $O(|A|\cdot\log|S|)$
>
> En réalité `unir` et `trouver` sont en $O(\log^*|S|)$ en amorti
> On a donc une complexité plutôt de l'ordre de $\underbrace{O(|A|\cdot\log|S|)}_{tri} + O(|A|)$
>
> La complexité est ici dominée par le tri :
> - Si les arêtes sont déjà triées $\rightarrow O(|A|)$
> - Avec un tri radis en $O(|A|)$ on a une complexité de $O(|A|)$

tri radix:
on trie par chiffre des puissances croissantes avec un tri stable (qui ne change par l'ordre relatif de ce qui est fait avant)

Correction de l'algorithme de Kruskal :

Idée : 
Invariant : à chaque étape, le sous-graphe en cours de construction est un sous-graphe d'un arbre couvrant de poids minimal.

Il est toujours possible de compléter le sous-graphe en cours de construction en un arbre couvrant de poids minimal.

> [!rem] Remarque
> L'algorithme glouton ne fait jamais de choix aux conséquences irrémédiables

> [!tip] Proposition
> L'algorithme de Kruskal appliqué à un graphe non orienté $G = (S, A, p)$ renvoie un arbre couvrant de poids minimal

>[!tldr] Démo 
>Montrons tout d'abord que $T$ est bien un arbre couvrant. 
>On a $T=(S,B)$ avec $B\subseteq A$ donc les sommets de $T$ sont $S$. On a pour invariant que le sous-graphe en cours de construction est acyclique et est de la forme $S,B'$ avec $B' \subseteq A$. 
>Si on s'arrête en atteignant $|S|-1$ étapes, on a un sous-graphe acyclique à $|S|-1$ arêtes qui est donc connexe, il s'agit d'un arbre. 
>
>Montrons que ceci est toujours le cas. 
>Supposons que l'on a considérer toutes les arêtes et que le sous-graphe obtenu n'est pas connexe. Il comporte au moins deux composantes connexes. Comme $G$ est connexe et ne comporte qu'une seule composante connexe, deux de composantes connexes de $T$ sont reliées dans $G$ par arête $\left\{x,y\right\} \in A \backslash B$. 
>Or, cette arête $\left\{x,y\right\}$ a été envisagé dans l'algorithme de Kruskal. A ce moment, elle ne reliait pas les composantes connexe qui contenait $x$ et $y$, sinon ce serait toujours le cas. Elle a donc due être ajoutée. Absurde 
> 
> Montrons maintenant que la propriété suivante est un invariant en notant $\underline{T} = (S,\underline{B})$ le sous-graphe en cours de construction : "$\underline{T}=(S,\underline{B})$ est un sous-graphe d'un arbre couvrant $T^*=(S,B^*)$ de poids minimal de $G$". 
> ***Initialisation*** : 
> Avant le premier tour de boucle, on a $T=(S, \emptyset)$. Il existe un arbre couvrant $T^*$ de poids minimal (car l'ensemble des arbres couvrants est fini non vide) et $\underline{T}$ est bien un sous-graphe de $T^*$. 
> ***Hérédité*** : 
> Supposons la propriété vraie au début d'un tour de boucle. On a donc $\underline{T}=(S,\underline{B})$ sous graphe d'un arbre couvrant $\underline{T^*}=(S,\underline{B^*})$ de poids minimal de $G$. 
> Dans un passage dans la boucle, distinguons 3 cas : 
> - L'arête $\left\{x,y\right\}$ n'est pas ajouté à $I$, on a $\overline{T}=\underline{T}$ et la propriété reste vraie avec $\overline{T^*}=\underline{T^*}$ 
> - L'arête $\left\{x,y\right\} \in B^*$, dans ce cas $\overline{T}=(S,\underline{B} \cup \left\{x,y\right\})$. On a encore $\overline{T}$ sous-graphe de $\overline{T^*}=\underline{T^*}$ arbre couvrant de poids minimal. 
> - L'arête $\left\{x,y\right\} \in A\backslash B^*$. On $\overline{T}=(S,\underline{B} \cup \left\{x,y\right\})$ pas sous-graphe de $\overline{T^*}$. Le sous-graphe $T^{*}+\left\{x,y\right\}$ comporte un cycle (contenant $\left\{x,y\right\}$). Comme $x$ et $y$ ne sont pas dans une même composante connexe de $T$, le chemin de $x$ à $y$ dans $\underline{T^*}$ qui utilise le reste du cycle, ne peut pas être en entier dans $\underline{T}$. Donc il existe une arête $a \in \underline{B^{*}} \backslash \underline{B}$ sur ce cycle. On ne peut pas encore avoir rencontré $a$, sinon on aurait ajouté $a$. En effet, comme $\underline{T}$ est sous-graphe de $T^*$, il ne peut pas y avoir d'autre chemin reliant dans $\underline{T}$ les extrémités de $a$ (sinon cela crée un cycle dans $\underline{T^*}$). 
> 	Comme on considère les arêtes dans l'ordre croissant, on a donc $p(\left\{x,y\right\})\leq p(a)$. 
> 	On considère $\overline{T^{*}}= \underline{T^*}-a+\left\{x,y\right\}$ qui est encore un arbre couvrant. 
> 	$p(\overline{T^*})=p({\underline{T^{*}})-p(a)+p(\left\{x,y\right\})\leq}p(\underline{T^*})$. 
> 	Donc $\overline{T^*}$ est aussi de poids minimal (et $p(\overline{T^*})-p(\underline{T^*})$). 
> 
> L'invariant est conservée $\overline{T}$ est un sous-graphe de $\overline{T^*}$ qui est un arbre couvrant de poids minimal. 
> 
> ***Correction*** : 
> A la fin de la boucle $T=(S,B)$ est un arbre couvrant, sous-graphe d'un arbre couvrant de poids minimal, donc égal à cet arbre de poids minimal.

