# Chapitre 18 - Apprentissage automatique (IA)

## I - Motivations, formalisations, exemples

### 1) Introduction

En anglais *machine learning*. C'est un sous-domaine de l'"intelligence artificielle" de manière générale (mais c'est en fait difficile à définir).
On va ici se placer dans le cadre des **problèmes de prédiction**. On suppose avoir une entrée $x\in X$ et on cherche à **prédire** un certain $y\in Y$. 

> [!example] Exemple
> - Allo-ciné : note spectacteur, note critique
> - Détection de spam
> - Catégorisation d'image
> - Classification de messages courts
> - Traduction automatique

- Beaucoup de ces problèmes sont très complexes (font intervenir des capacités humaines, d'où le nom d'intelligence artificielle) : langues naturelles, vision, reconnaissance d'objets. Il ne peuvent pas (en général) simplement être résolus par des règles écrites "à la main".
- **Apprentissage automatique** : inférer automatiquement des règles à partir de données : on conçoit des algorithmes qui sont capable de trouver les "bonnes" règles de classification pour un problème donné (ils "apprennent" les règles).
- **Apprentissage statistique** : l'utilisation d'exemples (en grande quantité) pour connecter des statistiques pour choisir ensuite de manière probabiliste la "meilleure" solution.

*Idée générale* : identifier des "motifs" "automatiquement" et construire des modèles de ses données.

### 2) Formalisation

On considère un espace d'entrée $X$ et un espace de sortie $Y$. On cherche à construire une fonction $f : X \to Y$ qui à une entrée $x \in X$ associe une **prédiction** $f(x) = y \in Y$.
On parle de **classification binaire** si $|Y| = 2$ et de classification multi-classe si $Y$ est fini, on prend $Y = \{0, 1, \cdots, C-1\}$ avec $C = |Y|$.
On peut (et on cherche) à prendre $X = \mathbb R^d$, c'est à dire à représenter un objet par un vecteur de $\mathbb R^d$. C'est ce qu'on appelle **l'extraction de caractéristique**.

> [!rem] Remarque
> On va faire des dessins avec $d=2$ mais en pratique $d$ est très grand, plusieurs milliers/millions/milliards ou plus.

> [!danger] Problème
> Fléau des grandes dimensions (curse of dimension) : en grande dimension, le volume croit tellement vite que les objets se retrouvent "isolés".

En OCaml, on utilise donc les types suivants :
```ocaml
type data = float array
type label = int
```
L'objectif est de construire une fonction `classifiy : data -> label`

> [!rem] Remarque
> Dans les **modèles paramétriques** la fonction de prédiction $f_{\theta}$ dépend des paramètres $\theta \in H$. Ce sont ces paramètres que l'on cherche à apprendre.

On peut adopter des modèles très complexes mais il faut faire très attention à ne pas trop coller aux données d'apprentissage. Risque de **surapprentissage** (*overfitting*).

## II - Apprentissage supervisé

Dans l'apprentissage supervisé, on suppose que l'on a à notre disposition un **ensemble d'apprentissage** $D = \{(x_i, y_i), i \in [[1, n]]\}$.
En OCaml :
```ocaml
type dataset = (data * label) list
```

On peut utiliser cet ensemble pour construire notre fonction de classification. L'objectif est de construire une fonction qui classe déjà bien les exemples.

> [!warning] Attention
> Le véritable objectif est de bien classer de nouvelles données, pas d'apprendre de manière trop préciser les données $D$ (sinon surapprentissage).

Le but est de construire un modèle de $D$ qui  permet de généraliser au delà de $D$.

## III - Evaluation

### 1) Ensemble d'apprentissage et de test

Il faut prendre garde à ne pas évaluer les performances sur les mêmes données que celles d'apprentissage.
On suppose donc disposer d'un ensemble $T = ((x_i, y_i), i \in [[1, n]])$ "indépendant" de $D$ pour faire les tests.

> [!rem] Remarque
> Un ratio 90% - 10% fonctionne souvent.

### 2) Taux d'erreur

On peut mesurer le **taux d'erreur** sur l'ensemble d'apprentissage et de test. C'est le pourcentage de réponses fausses, c'est à dire $|\{i \in [[1, n]] \mid f(x_i) \neq y_i\}|$.

### 3) Surapprentissage

[schéma]

### 4) Matrice de confusion

On peut souhaiter comprendre de manière plus préciser quels sont les erreurs réalisées par notre classification grace à la **matrice de confusion**.

La matrice de confusion est une matrice $M$ de taille $C \times C$ telle que le coefficient $M_{i,j}$ correspond au nombre de fois où on a classé un exemple de vraie classe $i$ dans la classe $j$.

> [!example] Exemple
$$\begin{pmatrix} 6 & 0 & 2 \\ 0 & 5 & 0 \\ 1 & 1 & 5\end{pmatrix}$$

On trouve sur la diagonale les données correctement classées. Le taux d'erreur est dans le nombre d'éléments hors diagonale rapporté au nombre d'éléments total. (ici $\dfrac{4}{20} = 20\%$)

Mais on peut lire d'autre choses :
Ici toutes les données de la classe 2 ont été correctement classées.

> [!rem] Remarque
> Avec deux classes $\{V, F\}$ la matrice a cette forme :
> $\begin{pmatrix} VP & FN \\ FP & VN \end{pmatrix}$ avec $VP$ : Vrai Positif, $FN$ : Faux négatif ...

## IV - Algorithme des k-plus-proches-voisins

En anglais *k-nearest-neighbors*.

> [!info] Idée
L'idée est toute simple : pour prédire la classe d'une entrée $x$, on recherche les $k$ plus proches voisins de $x$ et on classe $x$ suivant la classe majoritaire de ses voisins.
On se donne un paramètre $k \geq 1$ et une "distance" $d : \mathbb R^d\times \mathbb R^d \to \mathbb R_+$

> [!example] Exemple
> Distance euclidienne (associée à la norme 2), distance de Manhattan (associée à la norme 1) ou autre chose

En cas d'égalité entre les classes, on peut choisir de manière aléatoire ou arbitraire.

> [!danger] TD Exo 1
> 1. $\dfrac{50}{100} = 50\%$
> 2. La classe 4 est la moins représentée avec seulement 20 éléments dans le jeu de test.
> 3. Le taux d'erreur est le plus faible pour la classe 3 (0%) et le plus fort pour la classe 2 (100%).
> 4. jsp

> [!danger] TD  Exo 2
> $d(bob, boris) = 11 + 5 + 13 + 11 = 40$
> $d(bob, marie) = 1 + 7 + 1 + 1 = 10$
> $d(bob, stéphanie) = 11 + 5 + 13 + 11 = 40$
> $d(bob, jean) = 9 + 15 + 7 + 9 = 40$
> $d(bob, lilly) = 1 + 5 + 3 + 1 = 10$
> $d(bob, annabelle) = 7 + 13 + 5 + 7 = 32$
> Pour $k \in [[1, 2]], G_1$ ou $G_2$, pour $k=3, G_2$, pour $k\in[[4, 6]], G_1$ ou $G_2$

> [!danger] TD Exo 3
```mermaid
flowchart TD
F --> A --> B
A --> E
F --> H --> C
H --> D --> G
```


> [!help] Comment choisir $k$ ?
On peut procéder par validation croisée : on considère une partie des exemples pour faire un petit ensemble de test, puis on entraîne sur le reste et on choisit les $k$ qui conviennent le mieux sur l'ensemble de test (celui qui réalise le plus petit taux d'erreur).

> [!help] Comment recherche les $k$ plus proches voisins ?
> - On peut trier les $n$ points par distance croissant à $p$ et prendre les $k$ premiers : complexité $O(d\cdot n\log n)$
> - On conserve les $k$ meilleurs voisins trouvés en parcourant les $n$ points pour les mettre à jour : complexité $\Theta(d\cdot n\cdot k)$
> - On peut mettre les $k$ plus proches voisins dans une file de priorité (inverse). Pour un nouveau point $x \in \mathbb R^d$ on calcule $d = d(p, x)$ et si $s$ est plus petite que le max des distances dans la file on insère dans la file : complexité $O(d\cdot n \cdot \log k)$
> 
> En pratique $n$ est très grand, même $n \log k$ est trop grand.

Autre idée / amélioration possible :
- on pourrait pondérer le vote par l'inverse de la distance (que l'on calcule de toute façon)
- d'autres distances que la distance euclidienne

## V - Arbres $d$-dimensionnels

> [!warning] Attention
> Le $k$ dans les arbres $k$-dimensionnels pour les $k$ plus proches voisins est le $d$ de dimension.

> [!info] Idée
> Stocker les $n$ points dans une structure de données adaptée pour accélérer la recherche des $k$ plus proches voisins sans avoir à systématiquement considérer les $n$ points en entier -> *élagage spatial*

> [!info] Idée
> Séparer l'espace en zones de recherche

> [!def] Définition
> Un **arbre $d$-dimensionnel** est un arbre binaire de recherche où les éléments sont comparés à chaque niveau selon une coordonnée différente (par exemple abscisse, ordonnée, abscisse, ...).
> A la profondeur $p$, on utilise la coordonnée $p \mod d$.

> [!rem] Remarque
> Pour construire cet arbre, on peut prendre le point d'abscisse médiane, séparer en deux les autres points et recommencer récursivement.

> [!help] Comment rechercher les $k$-plus-proches voisins dans un tel arbre ?

Considérons une étape où l'on recherche les $k$ plus proches voisins d'un point $x$ pour un noeud de l'arbre $y$ avec une comparaison suivant la dimension $s$ qui sépare l'espace par un hyperplan $H$ en $P_\leq$ et $P_>$ .
On suppose que $x \in P_\leq$.
On commence par chercher récursivement les $k$ plus proches voisins dans le plan $P_\leq$, notons les $(p_1, \cdots, p_k)$. On note $d = \underset{i \in [[1, k]]}{\max} d(x, p_i)$ et $d_H = dist(x, H)$.
- Si $d_H > d$, alors aucun points de $E \setminus P_\leq$ ne peut être plus proche de $x$ que $(p_1, \cdots, p_k)$. On a trouvé les $k$ plus proches voisins et on peut s'arrêter.
- Sinon, on recherche récursivement dans $P_>$, on trouve $\{p_1', \cdots, p_k'\}$ et les $k$ plus proches voisins sont dans $\{p_1, \cdots, p_k, y, p_1', \cdots, p_k'\}$.

> [!rem] Remarque
> Il manque un cas : s'il y a moins de $k$ points dans $P_\leq$, on prend $d = +\infty$ (et donc on explore à coup sûr l'autre côté).

## VI - Arbres de décision

On se place dans le cas où chaque coordonnée (attribut) peut prendre deux valeurs ($\{0, 1\}, \{V, F\}$ etc...)
On a donc $x \in X = \mathbb B^d$ (ou $\mathbb R^d$ avec seulement $0$ et $1$ possible). On parle **d'attribut**.

> [!example] Exemple
> On a présenté des livres informatique à des élèves et on leur a demandé s'ils les recommandent. On a quatre attributs :
> - E : contient des exercices
> - C : contient le corrigé des exercices
> - F : est en français
> - L : propose des exemples dans un vrai langage de programmation
I
> ll y a quatre attributs et deux classes possibles. Il s'agit d'un problème de classification linéaire.

On peut proposer une classification sous la forme d'un arbre de décision. A chaque noeud correspondant à un attribut, on va à gauche si cet attribut est F et à droite s'il est V. Ses feuilles contiennent le résultat de la classification si l'on suit cette branche.

> [!example] Qui-est-ce ?

Il peut manquer des informations (branche vide). On peut prendre la classe majoritaire du sous-arbre enraciné plus haut.

Il peut y avoir des contradictions dans les exemples -> on peut prendre la classe majoritaire.

Le choix des attributs à chaque étape peut avoir une influence sur la hauteur de l'arbre -> on va chercher à construire l'arbre de la manière la plus compacte possible.

> [!rem] Remarque
> On n'est pas obligé de choisir le même ordre pour les attributs des deux côtés.

> [!help] Comment choisir les attributs pour construire l'arbre à partir des données ?

ID3 : Iterative Dichotomiser 3
Algorithme pour sélectionner l'ordre du choix des attributs sur chaque branche.

Pour choisir le meilleur attribut, on va utiliser l'entropie de Shannon qui est une mesure de l'incertitude ou de l'information.
Pour un ensemble $S$ de données de classe + et -, on note 
$n_+$ le nombre de données de $S$ d'étiquette +
$n_-$ le nombre de données de $S$ d'étiquette -
$n = n_+ + n_-$ le nombre de données de $S$
$p_+ = \dfrac{n_+}{n}$
$p_- = \dfrac{n_-}{n}$
L'entropie de $S$ est $H(S) = -p_+\log_2 p_+ - p_- \log_2 p_-$
On convient que $0 \log_2 0 = 0$
Si toutes les classes sont les mêmes, alors $H(S) = 0$.
Si les classes sont réparties équitablement, $H(S) = 1$ (entropie maximale)



