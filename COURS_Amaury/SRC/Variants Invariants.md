
## I - Variants

$a^{n}=\underbrace{a \times \cdots \times a}_{n-1 \text{ fois}}$

```c
double expo(double a, int n)
{
	double res = a;
	for (int i = 1; i < n; i++)
	{
		res *= a;
	}
	return res;
}
```

> [!rem] Remarque
> Les "boucles for" n'existent pas en C -> boucle algorithmique sur les entiers qui termine toujours
> ```c
> for (int i = 0; i < n; i++) {*}
> // est équivalent à
> int i = 0;
> while (i < n)
> {
> 	*
> 	i++;
> }
> ```

> [!def] Définition
> Un **variant** est une quantité :
> - entière naturelle ($\in \mathbb{N}$)
> - strictement décroissante
> 
> De manière plus générale : une quantité strictement décroissante dans un ensemble bien fondé.

> [!rem] Remarque
> Par abus on accepte une quantité de $\mathbb{Z}$ strictement croissante majorée ou strictement décroissante et minorée.

## II - Invariants

> [!def] Définition
> Un **invariant** de boucle est une propriété qui est :
> - *vérifiée* avant le premier passage dans un tour de boucle
> - *conservée* par un tour de boucle

> [!example] Exemple
> La propriété "vrai" est un invariant.
> La propriété "42=42" est un invariant.
> "$res = a^i$ et $i \leq n$ et $n \geq 1$"

Initialisation:
Avant de rentrer dans le boucle, on a $res = a$ et $i=1$ donc $res = a^i$ vérifié et $i \leq n$ car $n \geq 1$ par hypothèse.

Conservation:
Si on a $\underline{res} = a^\underline{i}$ avant un passage dans la boucle, alors $\overline{res} = \underline{res} * a$ donc $\overline{res} = a^{\underline{i} + 1}$ puis $\overline{i} = \underline{i} + 1$  donc $\overline{res} = a^\overline{i}$ et l'invariant est conservé

Après le dernier tour de boucle on a $res = a^i$

Correction:
Il faut montrer que `expo(a, n)` calcule $a^n$ donc que $res = a^{n}$
Après la boucle (après le dernier tour) on a :
- $res = a^i$
- $i\geq n$

