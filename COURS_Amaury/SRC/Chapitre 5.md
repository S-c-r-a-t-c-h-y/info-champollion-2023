# Chapitre 5 - Structure de données pour ensembles disjoints

> [!help] Motivation
Dans de nombreuses applications, on peut avoir besoin de gérer une partition d'un ensemble à $n$ éléments.

> [!rem] Remarque
> En particulier, on peut représenter une relation d'équivalence. Deux éléments étant équivalents si et seulement si ils sont dans la même classe.

> [!success] Objectif
Dans ce chapitre on s'intéresse à la conception d'une structure de données pour gérer efficacement une collection dynamique d'ensembles (finis) disjoints.

> [!def] Définition
> $S = S_{1}\sqcup S_{2} \sqcup \cdots S_{c}$ de $c$ classes. Les ensembles $S_{i}$ sont appelés les **classes** de $S$ (penser classes d'équivalences).

> [!rem] Remarque
> Le mot dynamique veut dire "qui va évoluer au fil du temps" par opposition à statique qui veut dire "fixé une fois pour toutes au début"

> [!example] Exemple
> Représentation des graphes :
> Par matrice d'adjacence ou listes d'adjacences par du tout efficace pour un ensemble de sommet qui varie
> table de hachage de liste d'adjacence

> [!example] Application
> On cherche à maintenir les composantes connexes d'un graphe non orienté, mais avec des arêtes qui peuvent être ajoutées et/ou retirées.
> On peut rechercher les composantes connexes par un parcours en $O(|S| + |A|)$ mais il faudrait relancer à chaque fois ?

## I - Unir et trouver

### 1) Représentant

> [!def] Définition
> On peut représenter chaque classe par un **identifiant**, par exemple $i$ pour la class $S_{i}$
> On peut aussi choisir un **représentant** qui est un élément particulier de l'ensemble. Chaque classe est alors représentée par un unique représentant.

> [!example] Exemple
> Dans $\mathbb{Z}/n\mathbb{Z}$, on représente la classe de $i \in [[0, n-1]]$ par $i$

> [!rem] Remarque
> Dans certaines applications, on peut imposer des propriétés particulières sur ce représentant. (le plus petit par exemple).
> On demande en général que deux requêtes renvoient toujours le même représentant.

> [!tldr] Notation
> Si $x \in S$, on note $S_{x}$ la classe de $x$ et $\overline{x}$ le représentant de la classe $S_{x}$. On a donc $S_{x} = S_{\overline{x}}$

> [!rem] Remarque
> Pour déterminer si deux éléments sont équivalents, c'est à dire appartiennent à la même classe, il suffit de comparer leurs représentants $\overline{x} = \overline{y}$ ?
> On s'intéresse donc à savoir trouver efficacement le représentant d'une classe ou d'un élément de cette classe ($trouver : x \mapsto \overline{x}$)

> [!def] Définition
> La collection d'ensembles étant dynamique, on peut vouloir réaliser l'**union** ou la fusion de deux classes, par exemple réunir les classes des éléments $x$ et $y$ de $S$.
> On appelle cette opération : `unir` (pas `union` car c'est un mot clé en C).
> Ceci explique le nom de cette structure de données : **unir et trouver** (**union-find**)

### 2) Représentation par des nœuds (ici en C)

> [!tldr] Notation
Pour réaliser cette structure de données on peut représenter chaque élément de $S$ par un objet que l'on va appeler noeud qui comporte cet élément plus éventuellement d'autres informations utiles à la structure.

```c
struct noeud {
	int val; // ou autre chose
	...
};
typedef struct noeud noeud;
```

> [!tip] Spécifications
On souhaite obtenir les primitives suivantes:
> - `noeud* creer(int x)` qui permet de créer un noeud correspondant à un singleton $\{x\}$. Cela revient à ajouter une classe formée d'un élément à notre structure. Son représentant est $x$
> - `noeud* trouver(noeud* x)` qui détermine le représentant $\overline{x}$ associé à un élément $x$
> - `void unir(noeud* x, noeud*y)` qui réalise la fusion des classes $S_x$ et $S_y$, c'est à dire $\mathcal{P} \leftarrow \mathcal{P}\setminus\{S_{x}, S_{y}\} \cup \{S_{x}, S_{y}\}$. Les deux ensembles $S_{x}$ et $S_{y}$ sont supposés disjoints ou égaux avant l'opération. Après l'opération, $S_{x} = S_{y}$

> [!rem] Remarque
> En général le représentant de `unir(x, y)` sera $\overline{x}$ ou $\overline{y}$ (mais ce n'est pas obligatoire)

> [!rem] Remarque
> `unir(x, y) = unir(x^-, y^-)`
> Lors de la réunion on peut prendre n'importe quel élément des classes.
> `unir(x, y) = unir(trouver(x), trouver(y))`

> [!def] Définition
> On note $n$ le nombre d'éléments de $S$ et donc, ce qui revient au même, le nombre d'appels à créer. On note $m$ le nombre total d'appels à `créer`, `trouver` et `unir`.

> [!rem] Remarque
> On a $m \geqslant n$ et on a au plus $n-1$ appels à unir servent à quelque chose.

> [!rem] Remarque
> On peut toujours supposer que l'on a commencé par les $n$ appels à `créer`.

### 3) Représentation par une structure

> [!help] Conventions
> Sans perte de généralité on suppose $S = [[0, n-1]]$ (sinon on numérote les éléments et on utilise une table d'association)
> On représente l'information dans une structure à part.
> On suppose ici que l'on commence par créer les $n$ singletons.

> [!example] Exemple en OCaml
> ```ocaml
> type partition
> val creer : int -> partition
> (** créé la partition {{0}, {1}, ..., {n-1}} constitué de n singletons *)
> val trouver : partition -> int -> int
> (** trouve le représentant d'un élément *)
> val unir : partition -> int -> int -> unit
> ```

> [!rem] Remarque
> On peut faire en C ce que l'on fait en OCaml et vice versa.

## II - Représentation naïve

> [!tldr] Principe
> On implémente `partition` par tableau `id` tel que `id.(i)` contient le représentant de `i` qui est *toujours* le plus petit représentant de la classe.

> [!example] Implémentation
> ```ocaml
> type partition = int array
> let creer n = Array.init n (fun i -> i) (* complexité O(n) *)
> let trouver p i = p.(i) (* complexité O(1) *)
> let union p x y = (* complexité O(n) *)
> 	let rx = trouver x and ry = trouver y in
> 	let subsititute t a h =
> 		for i = 0 to Array.length t - 1 do
> 			if t.(i) = a then t.(i) <- h
> 		done
> 	in
> 	substitute p (max rx ry) (min rx ry)
> ```

> [!rem] Remarque
> On peut commencer la boule `for` à `a` car l'identifiant est toujours plus petit.

> [!rem] Remarque
> `if rx <> ry then ...`

> [!rem] Remarque
> `trouver` est efficace mais `unir` ne l'est pas.

> [!danger] Exercice
> Quelle est la complexité d'une séquence de $n-1$ `unir` dans le pire cas ?
> > [!cloze]
> > $O(n^2)$

## III - Représentation par une forêt

> [!tldr] Principe
> On peut organiser les noeuds de la manière suivante : chaque noeud pointe vers un autre noeud de sa classe. Le représentant pointe vers lui-même (et c'est le seul). On s'interdit les cycles. Ceci forme alors une forêt qui représente une partition. On trouve le représentant d'une classe en remontant les pointeurs.

> [!rem] Remarque
> On peut aussi faire pointer les représentants sur `NULL` en C.

> [!rem] Remarque
On obtient une forêt (graphe acyclique, réunion de graphes connexes acycliques -> arbres)

> [!rem] Remarque
> Ici, on ne représente que les pointeurs *parent*.

> [!example] Implémentation en C
> ```c
> typedef struct noeud
> {
> 	int val;
> 	struct noeud* parent;
> } noeud;
> 
> noeud* creer(int val) 
> {
> 	noeud* n = malloc(sizeof(noeud));
> 	n -> val = val;
> 	n -> parent = n;
> 	return n;
> } // O(1)
> 
> noeud* trouver(noeud* x)
> {
> 	while (x -> parent != x)
> 	{
> 		x = x->parent;
> 	}
> 	return x;
> } // linéaire en la profondeur du noeud dans l'arbre (O(n) en pire cas)
> 
> void unir(noeud* x, noeud* y)
> {
> 	noeud* rx = trouver(x);
> 	rx -> parent = y;
> } // O(1) + appel à trouver : O(n) dans le pire cas
> ```
> On peut aussi faire :
> ```c
> void unir(noeud* x, noeud* y)
> {
> 	noeud* rx = trouver(x);
> 	noeud* ry = trouver(y);
> 	rx->parent = ry;
> }
> ```
> qui à priori réduit la hauteur des arbres.

> [!danger] Exercice
> Trouve une suite d'opérations qui réalisent ce pire cas.

> [!example] Implémentation en OCaml
> ```ocaml
> type partition = int array
> 
> let creer n = Array.init n (fun i -> i)
> 
> let rec trouver p x =
> 	if p.(x) = x then x
> 	else trouver p p.(x)
> 
> let unir p x y = p.(trouver p x) <- y
> ```

> [!rem] Remarque
> Et si `rx = ry` ? C'est OK

> [!Info]
On va introduire deux heuristiques qui vont changer radicalement les choses

## IV - Union par rang

> [!tip]
> Il n'est pas très difficile de garantir une forme d'équilibrage des arbres pour obtenir `trouver` et `unir` en $O(\log n)$.
> Il faut veiller, lors de l'union, à choisir comme racine celle de l'arbre le plus haut. Ainsi la hauteur des arbres ne peut augmenter que lors de l'union de deux arbres de même hauteur.
> Bien sûr, il est hors de questions de recalculer la hauteur à chaque fois. On la stocke donc dans les noeuds.
> On va appeler cela le **rang** qui sera toujours un majorant de la hauteur.

> [!def] Définition
> On appelle **rang** d'un noeud un *majorant* de la hauteur.

```c
typedef struct noeud
{
	int val;
	int rang;
	struct noeud* parent;
} noeud;

noeud* creer(int val)
{
	noeud* n = malloc(sizeof(noeud));
	n->val = val;
	n->rang = 0;
	n->parent = n;
	return n;
}

// trouver est identique

void unir(noeud* x, noeud* y)
{
	noeud* rx = trouver(x);
	noeud* ry = trouver(y);
	if (rx == ry) {return; }
	if (rx->rang > ry->rang)
	{
		ry->parent = rx;
	} else 
	{
		rx->parent = ry;
		if (rx->rang == ry->rang)
		{
			ry->rang++;
		}
	}
}
```

## V - Compression de chemin

```c
noeud* trouver(noeud* x)
{
	if (x != x->parent)
	{
		x->parent = trouver(x->parent);
	}
	return x->parent;
}
```

## VI - Analyse de la complexité

> [!info]
On a vu que sans heuristiques la complexité de unir et trouver était en $O(n)$. En ajoutant l'heuristique d'union par rang (et éventuellement la compression de chemin).

> [!tip] Propriété
> Si $x$ n'est pas représentant, alors $rg(x) < rg(pere(x))$

> [!tldr] Démonstration
> Soit $y$ le père de $x$. Au moment où $y$ devient le père de $x$ (nécessairement dans `unir`) :
> - soit $rg(x) < rg(y)$
> - soit $rg(x) = rg(y)$ et alors on a incrémenté le rang de $y$
> 
> Dans les 2 cas, on a $rg(x) < rg(y)$ après l'appel `unir`.
> De plus le rang de $x$ est alors fixé pour toujours (le rang d'un non représentant ne peut pas changer et un non représentant ne peut pas le devenir).
> D'un autre côté le rang de $y$ ne peut que croître.
> Donc on a toujours $rg(x) < rg(y)$

> [!info] Conséquences
> - Les rangs sont strictement décroissants sur une branche quelconque.
> - Le rang d'un ancêtre propre de $x$ est strictement plus grand que le rang de $x$.
> - La propriété reste vrai même avec compression de chemin. Dans ce cas, on peut associer à un noeud $x$ un nouveau père qui est un ancêtre de $x$ donc de rang strictement supérieur.

> [!tip] Proposition
> Un noeud $x$ de rang $K$ est racine d'un arbre de hauteur au plus $K$. Autrement dit, le rang majore la hauteur.

> [!tldr] Démonstration
> Les noeuds sur un chemin de $x$ à une feuille sont de rang dans $[[0, K]]$ tous différents puisque le rang de $x$ est $K$, le rang d'une feuille est positif et strictement décroissant. Il y a donc au plus $K+1$ sommets sur ce chemin qui est donc de longueur au plus $K$.

> [!tip] Propriété
> Un noeud de rang $K$ est racine d'un arbre comportant au moins $2^K$ noeuds.

> [!tldr] Démonstration
> On procède par récurrence sur $K \in \mathbb{N}$.
> <u>Initialisation :</u> si $K = 0$, un noeud de rang $0$ est racine d'un arbre qui le contient au moins (et même exactement) lui même et donc de taille au moins $1$.
> <u>Hérédité :</u> On suppose que la propriété est vraie au rang $K \in \mathbb{N}$. Soit $x$ de rang $K+1$.
> Comme $x$ est de rang $K+1$ il est devenu père d'un noeud $y$ dans un appel à `unir` avec, au moment de cette liaison, $x$ de rang $K$ et $y$ de rang $K$ (seul moyen pour avoir un incrément de rang).
> Par hypothèse de récurrence, $x$ et $y$ sont racine d'arbres comportants au moins $2^{K}$ noeuds et donc $x$ comporte au moins $2^{K+1}$ noeuds.
> Même en cas de compression de chemin, comme $x$ est représentant, tous ses descendants restent ses descendants.
> Il est possible d'avoir d'autres appels à `unir` avec $x$ comme racine ce qui ne peut qu'augmenter le nombre de noeuds.

> [!tip] Proposition
> Il y a au plus $\dfrac{n}{2^K}$ noeuds représentants de rang $K$.

> [!tldr] Démonstration
> Notons $\{x_{1},\cdots, x_{p}\}$ les représentants de rang $K$ et $N_{x_i}$ les noeuds de l'arbre de racine $x_i$.
> On a $\forall\ i \in [[1, p]],\ |N_{x_{i}}\geq 2^{K}$, de plus les $N_{x_{i}}$ sont deux à deux disjoints car un noeud ne peut posséder qu'un seul ancêtre de rang $K$ (les rang sont strictement décroissants sur un branche).
> $\sum\limits_{k=1}^{p}2^{K}\leq \sum\limits_{i=1}^{p}|N_{x_{i}}| \leq n$ donc $p \leq \dfrac{n}{2^K}$

> [!info] Conséquence
> Il y a au plus $0$ noeuds de rang supérieur à $\lfloor \log_{2}n \rfloor + 1$
> On vient de voir que tous les arbres de la forêt ont une hauteur majorée par $\lfloor \log_{2}n \rfloor$. Ils sont équilibrés. On a donc `trouver` et `unir` en $O(\log n)$

> [!tip] Proposition (admis)
> En utilisant les deux heuristiques la complexité est en réalité excellente.
> Une série de $m$ opérations dont $n$ appells à `créer` est en complexité $O(m\alpha(n))$ où $\alpha$ est une forme de réciproque de la fonction d'Ackermann.
> On a $\alpha(n) \leq 4\ \forall\ n \in [[0, 10^{100}]]$.
> Autrement dit, la complexité amorti de cette structure est en $O(\alpha(n))$ (en gros $\Theta(1)$)


