# Chapitre 9 : Automates finis

```toc
```

## I - Motivation

### A - Modélisation d'un ordinateur simplifié

>[!note]
>On a formalisé la notion de **langage**, il nous reste à modéliser la notion de **machine**. 
>On va adopter un modèle pour un type de machines très simplifié.

>[!example] Exemple : machine à café
>![[chap9_img1.png]]

### B  - Théorème de Kleene

>[!note] 
>Un des objectif du chapitre est de nous intéresser au théorème suivant :
>**Théorème de Kleene**.

>[!tip] Théorème de Kleene
>Les langages reconnaissables par automates fini sont exactement les langages réguliers.

>[!info] Remarque
>On peut formaliser la notion de langage calculable (décidable).
>Il faut des **machines de Turing** (HP).

### C - Recherche de motif

>[!note]
>Les expressions régulières permettent de **noter** des motifs à rechercher.
>
>Les langages réguliers $\leftrightarrow$ notations formalisme théorie
>
>Automates $\leftrightarrow$ machine réalisation pratique

>[!question] Comment trouver les motifs d'un texte ?

>[!note]
>1. Spécifier le motif par une expression régulière.
>2. Traduire cette expression régulière par un automate.
>3. Exécuter l'automate.

>[!info] Remarque
>Beaucoup d'applications : correction orthographique, compilation, recherche, etc ...

## II - Automates finis déterministes

### A - Formalisation

>[!tip] Définition
>Un automate fini déterministe est un quadruplet $A = (Q, \Sigma,q_{0},T,\delta)$ avec :
>- $Q$ : un ensemble fini non vide. Les éléments de $Q$ sont appelés **états**.
>- $\Sigma$ : un alphabet
>- $q_{0} \in Q$ : un état appelé **état initial**.
>- $T \subseteq Q$ : un ensemble d'état appelé états terminaux (pas de contraintes).
>- $\delta$ : $D_{\delta} \subseteq Q \times \Sigma \rightarrow Q$ une application partielle de $Q \times \Sigma$ dans $Q$, appelée **fonctions de transitions**.
>
>Si $q \in Q$ et $a \in \Sigma, \delta(q,a)$ est l'état d'arrivé en lisant la lettre $a$ depuis $q$. 

>[!example] Exemple : machine à café
>$Q= \left\{\overline{0c},\overline{10c},\overline{20c},\overline{\text{café}}\right\}$
>$\Sigma = \left\{10c,20c\right\}$
>$q_{0}=\overline{0c}$
>$T=\left\{\overline{\text{café}}\right\}$

| $q/a$            | $10c$             | $20c$             |
| ---------------- | ----------------- | ----------------- |
| $\overline{0c}$  | $\overline{10c}$  | $\overline{20c}$  |
| $\overline{10c}$ | $\overline{20c}$  | $\overline{café}$ |
| $\overline{20c}$ | $\overline{café}$ |                   |
| $\overline{café}$                 |                   |                   |

>[!tip] Vocabulaire
>AFD pour automate fini déterministe
>DFA pour deterministe finite automaton

>[!info] Représentation graphique
>On représente un automate par un graphe orienté et étiqueté $G=(Q,\left\{(q,a,\delta(q,a)),(q,a)\in D_{\delta}\right\})$.
>Les états sont représentés par des cercles, avec une flèche pour l'état initial entrante et une flèche sortante ou un double cercle pour un état terminal.

>[!info] Remarque
>![[chap9_img2.png]]

>[!tip] Définition
>Un **blocage** d'un automate $A= (Q, \Sigma,q_{0},T,\delta)$ est un couple $(q,a)\in Q \times \Sigma \backslash D_{\delta}$, c'est-à-dire une transition non définie par $\delta$.

>[!tip] Définition
>Soit $A=(Q, \Sigma,q_{0},T,\delta)$ un AFD, on étend la fonction transition $\delta : D_{\delta} \subseteq Q \times \Sigma \rightarrow Q$ en fonction $\delta^{*} : D_{\delta^{*}} \subseteq Q \times \Sigma^{*} \rightarrow Q$ aux mot de la manière suivante :
>- $\forall q \in Q, \delta^{*}(q,\varepsilon)=q$
>- $\forall q \in Q, \forall a \in \Sigma, \forall u \in \Sigma^{*}, \delta^{*}(q,au)=\delta^{*}(\delta(q,a),u)$ si $(q,a)\in D_{\delta}$ et $(\delta(q,a),u) \in D_{\delta^{*}}$ sinon blocage

>[!info] Remarque
>On peut aussi définir $\delta^{*}$ dans l'autre sens :
>$\forall q \in Q, \delta^{*}(q,\varepsilon)=q$
>$\forall q \in  Q, \forall a \in \Sigma, \forall u \in \Sigma^{*}, \delta^{*} = \delta(\delta^{*}(q,u),a)$ si pas de blocages

>[!info] Remarque
>$\delta^{*}(q,u)$  est l'état sur lequel on arrive en lisant les lettres du mot $u$ dans l'ordre à partir de l'état $q$.

>[!tip] Propriété
>Soit $A= (Q, \Sigma,q_{0},T,\delta)$ un AFD et $u,v \in \Sigma^{*}$ et $q \in Q, \delta^{*}(q,uv)=\delta^{*}(\delta^{*}(q,u),v)$.

>[!note] Démo
>Par induction sur $u$ (ou par récurrence sur $|u|$).
>>[!bug] A faire

>[!tip] Définition
>Soit $A =(Q, \Sigma,q_{0},T,\delta)$ un AFD et $u \in \Sigma^{*}$ un mot. On dit que $u$ est reconnu par $A$ si et seulement $(q_{0},u)\in D_{\delta^{*}}$ et $\delta^{*}(q_{0},u) \in T$.

>[!tip] Définition
>Le langage reconnu par un automate est l'ensemble des mots reconnus. On le note $\mathcal{L}(A)$ si $A$ est l'automate.

>[!example] Exemple
>$\mathcal{L}(A) = L((a^{+}b|b^{+}a)\Sigma^{*})$

>[!warning] 
>Egalité et non inclusion : il faut les mots reconnus et seulement ceux là.

>[!tip] Définition
>Un langage $L \subseteq \Sigma^{*}$ est reconnaissable (par automate déterministe fini) s'il existe un automate $A= (Q, \Sigma,q_{0},T,\delta)$ tel que $\mathcal{L}(A)=L$.

>[!tip] Définition
>Deux automates sont **équivalents** s'ils reconnaissent le même langage.

>[!example] Exemple
>![[chap9_img3.png]]
>$\mathcal{L}(A)$ est l'ensemble des mots non vide sans deux lettres consécutives identiques.
>
>![[chap9_img4.jpg]]
>$\mathcal{L}(A)=\Sigma^{*}babb\Sigma^{*}$.

>[!bug] Exercice
>Construction des automates reconnaissant les langages suivant.
>$\Sigma^{*}ab\Sigma^{*}$
>$a(a^{2}|b)^{*}b$
>$\left\{u \in \Sigma^{*}\ |\ |u|_{a} \equiv 1 [2]\right\}$
>
![[chap9_img5.png]]

### B - Automates particuliers

#### 1 - Automates complets

>[!tip] Définition
>Un automate $A= (Q, \Sigma,q_{0},T,\delta)$ est **complet** s'il n'a pas de blocages, c'est-à-dire si $D_{\delta}=Q \times \Sigma$ en entier.

>[!tip] Proposition
>Un langage reconnaissable est reconnu par un automate fini déterministe complet.

>[!info] Remarque
>On peut toujours supposer l'automate complet, sans perte de généralité.

>[!note] Démo
>Il suffit de compléter l'automate avec un état **puits** (ou poubelle) absorbant.
>Soit $A= (Q, \Sigma,q_{0},T,\delta)$ un AFD. On va construire un automate fini déterministe  complet (AFDC) $A'$ équivalent, c'est-à-dire $\mathcal{L}(A)=\mathcal{L}(A')$.
>Soit $q_{\bot} \notin Q$ un nouvel état appelé puits. On prolonge $\delta$ en $\delta'$ sur $Q \cup \left\{q_{\bot}\right\}$ de la manière suivante.
>$\forall (q,a) \in D_{\delta}$ on pose $\eta(q,a)=\delta(q,a)$.
>$\forall (q,a) \in Q \times \Sigma \backslash D_{\delta}$ on pose $\eta(q,a)=q_{\bot}$.
>$\forall a \in \Sigma$, on pose $\eta(q_{\bot},a)=q_{\bot}$.
>
>>[!tip] Lemme
>>pour tout $q\in Q, u=u_{1}\dots u_{n} \in \Sigma^{*}$ s'il existe $i \in [|1,n|]$ tel que $\eta^{*}(q,u_{1},\dots ,u_{n})=q_{\bot}$ alors $\eta^{*}(q,u)=q_{\bot}$ (par récurrence)
>
>On pose $A'= (Q \cup \left\{q_{\bot}\right\}, \Sigma,q_{0},T,\eta)$, on a $\mathcal{L}(A)=\mathcal{L}(A')$.
>$L(A) \subseteq L(A')$. Soit $u = u_{1}\dots u_{n} \in \mathcal{L}(A)$
>on a $\delta^{*}(q_{0},u) \in T$
>On montre par récurrence que $\forall i \in [|1,n|]$ on a $\eta^{*}(q_{0},u_{1}\dots u_{i}) \in Q$ et que $delta^{*}(q_{0},u_{1}\dots u_{i}) = \eta^{*}(q_{0},u_{1}\dots u_{i})$.
>Donc $\eta^{*}(q_{0},u) = \delta^{*}(q_{0},u) \in T$ donc $u \in \mathcal{L}(A)$.
>
>Réciproque :
>si $u \in \mathcal{L}(A)$. On a $\eta^{*}(q_{0},u)\in T$.
>Montrons par récurrence que $\forall i \in [|1,n|], \eta^{*}(q_{0},u_{1}\dots u_{i})\neq q_{\bot}$ et $\eta^{*}(q_{0},u_{1}\dots u_{i})= \delta^{*}(q_{0},u_{1}\dots u_{i})$.
>Donc par récurrence et le lemme, 
>pour $i \in [|1,n|], \eta^{*}(q_{0},u_{1}\dots u_{i}) = q_{\bot}$ alors $\eta^{*}(q_{0},u) = q_{\bot} \notin T$ Absurde.


>[!info] Remarque
>On a toujours un peu plus fort :
>si $L$ est reconnu par un AFD à $n$ états, il est reconnu par un AFDC à $n+1$ états (au plus).

>[!info] Remarque
>On suppose, le plus souvent, les automates complets même si on le représente pas explicitement.

#### 2 - Accessibilité et émondage

>[!tip] Définition
>Soit $A= (Q, \Sigma,q_{0},T,\delta)$ un AFD et $q \in Q$. On dit que :
>-  q est accessible s'il existe $u \in \Sigma^{*}$ tel que $\delta^{*}(q_{0},u)=q$.
>- q est co-accessible s'il existe $u \in \Sigma^{*}$ tel que $\delta^{*}(q,u)\in T$.
>- q est utile s'il est accessible et co-accessible.
>- un automate est émondé si tous ses états sont utiles. 

>[!info] Remarque
>Un état est accessible si on peut s'y rendre à partir de l'état initial, co-accessible si à partir de celui-ci on peut rejoindre un état terminal. Un état inutile ne sert à rien, on peut l'enlever sans changer le langage reconnue.

>[!tip] Proposition
>Un langage non vide reconnaissable est reconnu par un automate émondé.

>[!note] Démo
>Il suffit d'enlever les états inutiles.

>[!info] Remarque
>On ne peut pas toujours compléter un automate en conservant le caractère émondé. On s'autorise en général, au plus un état non co-accessible (le puits).

>[!question] Comment déterminer les états accessibles ?
>$\rightarrow$ Parcours de graphe à partir de $q_{0}$.

>[!question] Comment déterminer les états co-accessibles ?
>$\rightarrow$ Parcours de graphe à partir de $T$ sur le graphe miroir.

>[!info] Remarque 
>émondé ne veut pas dire minimal pour le nombre d'états.

>[!info] Remarque
>On peut montrer qu'il existe un unique automate complet déterministe minimal.

### C - Propriétés de clôtures 

#### 1 - Clôture par compréhension

>[!tip] Proposition
>Si $L$ est reconnaissable alors $\overline{L}=\Sigma^{*} \backslash L$ l'est aussi.

>[!note] Démo
>Soit $L$ un langage reconnaissable, il existe un AFDC $A= (Q, \Sigma,q_{0},T,\delta)$ tel que $L = \mathcal{L}(A)$.
>On pose $\overline{A} = Q, \Sigma, q_{0}, Q \backslash T, \delta$.
>Montrons que $\mathcal{L}(\overline{A}) = \Sigma^{*} \backslash L$.
>$u \in \mathcal{L}(\overline{A}) \Leftrightarrow \overline{\delta^{*}}(\overline{q_{0}},u) \in \overline{T}$
>$\Leftrightarrow \delta^{*}(q_{0},u) \in Q \backslash T$
>$\Leftrightarrow \delta^{*}(q_{0},u) \notin T$
>$\Leftrightarrow u \notin \mathcal{L}(A)$
>$\Leftrightarrow u \in \Sigma^{*}\backslash L$

>[!note]
>Pour reconnaître le complémentaire de langage reconnu par un automate, il suffit d'inverser les états terminaux et non terminaux d'un automate complet.

#### 2 - Automate produit

>[!tip] Définition
>Soit $A_{1}= (Q_{1}, \Sigma,q_{0}^{1},T_{1},\delta_{1})$, $A_{2}= (Q_{2}, \Sigma,q_{0}^{2},T_{2},\delta_{2})$ deux AFDC (complets) sur un même alphabet.
>Un automate **produit** est l'automate $A_{1}\times A_{2}=(Q_{1}\times Q_{2}, \Sigma,(q_{0}^{1},q_{0}^{2}),T_{1\times 2},\delta_{1\times2})$ avec $\delta_{1\times2}$ définie par :
>$\forall q_{1}\in Q_{1}, \forall q_{2} \in Q_{2}, \forall a \in \Sigma$
>$\delta_{1\times2}((q_{1},q_{2}),a)=(\delta_{1}(q_{1},a),\delta_{2}(q_{2},a))$

>[!example] Exemple
>![[chap9_img6.png]]
>On simule les deux automates.

>[!tip] Proposition
>$\forall q_{1}\in Q_{1}, \forall q_{2} \in Q_{2}, \forall u \in \Sigma^{*}$
>$\delta_{1\times 2}^{*}((q_{1},q_{2}),u)= (\delta_{1}^{*}(q_{1},u),\delta_{2}^{*}(q_{2},u))$
>
>Intuitivement, $\delta_{1\times 2}^{*}$ permet de lire de manière parallèle dans les deux automates.

>[!note] Démo
>Par induction (ou récurrence) sur $u$.
>- Si $u = \varepsilon$ alors $\delta_{1 \times 2}^{*}((q_{1},q_{2}),\varepsilon) = (q_{1},q_{2})=(\delta_{1}^{*}(q_{1},\varepsilon),\delta_{2}^{*}(q_{2},\varepsilon))$
>- Si $u = av$ avec $a \in \Sigma, v \in \Sigma^{*}$
>  $\delta_{1\times 2}^{*}((q_{1},q_{2}),av)=\delta_{1\times 2}^{*}(\delta_{1 \times 2}((q_{1},q_{2}),a),v)$
>  $= \delta_{1 \times 2}^{*}((\delta_{1}(q_{1}),a),\delta_{2}(q_{2},a),v)$
>  $= (\delta_{1}^{*}(q_{1},av), \delta_{2}^{*}(q2,av))$

>[!tip] Proposition
>Les langages reconnaissables sont clos par union et intersection finie.

>[!note] Démo
>Il suffit de le montrer pour l'union et l'intersection de deux automates.
>Soit $L_{1},L_{2}$ deux langages reconnaissables par $A_{1},A_{2}$ deux AFDC qui les reconnaissent respectivement.
>On considère les automates produits $A_{1}\times A_{2}$ avec $T_{1 \times 2} = T_{1}\times T_{2}$. 
>Intuitivement, on calcule en parallèle dans les deux automates et on valide si les deux automates valident.
>Montrons que $\mathcal{L}(A_{1 \times 2}) = \mathcal{L}(A_{1})\cap \mathcal{L}(A_{2})$.
>Soit $u \in \Sigma^{*}$
>$u \in \mathcal{L}(A_{1\times 2}) \Leftrightarrow \delta_{1\times 2}^{*}((q_{0}^{1},q_{0}^{2}),u) \in T_{1}\times T_{2}$
>$\Leftrightarrow (\delta_{1}^{*}(q_{0}^{1},u), \delta_{2}^{*}(q_{0}^{2},u)) \in T_{1}\times T_{2}$
>$\Leftrightarrow \delta_{1}^{*}(q_{0}^{1},u) \in T_{1}$ et $\delta_{2}^{*}(q_{0}^{2},u) \in T_{2}$
>$\Leftrightarrow u \in \mathcal{L}(A_{1})$ et $u \in \mathcal{L}(A_{2}) \Leftrightarrow u \in \mathcal{L}(A_{1}) \cap \mathcal{L}(A_{2})$.
>De même pour reconnaître la réunion avec $T_{1\times 2} = T_{1}\times Q \cup Q \times T_{2}$.
>>[!bug] Faire la preuve sans regarder.

>[!info] Remarque
>On peut montrer que les langages reconnaissables sont clos par concaténations et étoile de Kleene.

>[!bug] Exercice
>Le montrer
>Indication : utiliser des automates standards (Ex 8 du TD)

## III - Automates finis non déterministes 

### A - Définitions

>[!tip] Définition
>Un automate fini non déterministe est un quadruplet $A=(Q,\Sigma,I,T,\Delta)$ avec : 
>- $Q$ un ensemble non vide d'états
>- $\Sigma$ un alphabet
>- $I \subseteq Q$ l'ensemble des états initiaux
>- $T \subseteq Q$ l'ensemble des états terminaux
>- $\Delta \subseteq Q \times \Sigma \times Q$ est la relation de transition

>[!example] Exemple 
>![[chap9_img7.png]]
>$A_{\text{ex}} = (\left\{q_0, q_1\right\}, \left\{a, b\right\}, \left\{q_0\right\}, \left\{q_1\right\}, \left\{(q_0, a, q_0),(q_0, b, q_0),(q_0, a, q_1),(q_1, a, q_1),(q_1, b, q_1)\right\})$

>[!info] Remarque
>Si $(q,a,q') \in \Delta$, on note 
>```mermaid
>flowchart LR
>q -- a --> q'
>```
 
>[!info] Remarque
>Non déterminisme : 
>- plusieurs états initiaux possibles
>- plusieurs transitions à partir d'un état pour une même lettre.

>[!info] Remarque
>On peut voir un automate finie déterministe comme un cas particulier d'automate fini non déterministe.
>Si $|I| =1$ et $\forall (q,a) \in Q \times \Sigma, \forall q',q'' \in Q, (q,a,q')\in \Delta$ et $(q,a,q'') \in \Delta$ alors $q'=q''$.

>[!info] Remarque 
>Non déterministe $\rightarrow$ non nécessairement déterministe.

>[!tip] Définition
>Soit $A=(Q,\Sigma,I,T,\Delta), (q,q')\in Q$. On appelle **chemin** menant de $q$ à $q'$ dans $A$ toute suite finie de transitions consécutives qui commence par en $q$ et termine en $q'$:
>$(q_{0},a_{0},q_{1})\cdots (q_{n-1},a_{n-1},q_{n})$
>avec $q_{0}=q, a_{n}=q'$ et $\forall i \in [|1,n-1|] (q_{i},a_{i},q_{i+1}) \in \Delta$
>Sa longueur est $n$ et son étiquette est le mot $u = a_{0}a_{1}\cdots a_{n}$.

>[!info] Remarque
>Pour $q\in Q$ le chemin vide mène de $q$ à $q'$ d'étiquette $\varepsilon$.
>

>[!info] Notation
>pour $q,q' \in Q^{2}$ et $u\in\Sigma^{*}$. On note
>```mermaid
>flowchart LR
>q--u-->q'
>```
>s'il existe au moins un chemin de $q$ à $q'$ d'étiquette $u$.

>[!info] Remarque
>```mermaid
>flowchart LR
>q--ε-->q'
>```
>si et seulement si $q=q'$

>[!info] Remarque
>si $p,q,r \in Q$ et $u,v \in \Sigma^{*}$
>si
>```mermaid
>flowchart LR
>p--u-->q
>```
>et 
>```mermaid
>flowchart LR
>q--v-->r
>```
>alors 
>```mermaid
>flowchart LR
>p--uv-->r
>```
>**Inversement**
>```mermaid
>flowchart LR
>p--uv-->r
>```
>alors $\exists z \in Q$
>```mermaid
>flowchart LR
>p--u-->z
>```
>et 
>```mermaid
>flowchart LR
>z--v-->q
>```

>[!info] Remarque
>Si $q,p \in Q$ et $u \in \Sigma^1$ 
>```mermaid
>flowchart LR
>q--u-->p
>```
>$\Leftrightarrow (q,u,p) \in \Delta$

>[!tip] Définition
>Soit $u \in \Sigma^*$ et $A = (Q,\Sigma,I,T,\Delta)$ un automate fini non (nécessairement) déterministe (AFND). On dit que $u$ est reconnu par $A$ s'il existe $q \in I, q' \in T$ et un chemin 
>```mermaid
>flowchart LR
>q--u-->q'
>```
>de $q$ à $q'$ d'étiquette $u$.
>On note alors $\mathcal{L}(A)$ le langage des mots reconnus par un AFND.

>[!info] Remarque
>$\varepsilon \in \mathcal{L}(A) \Leftrightarrow I \cap T \neq \varnothing$

>[!question] Comment déterminer algorithmiquement si un mot u est reconnu
>$\rightarrow$ on teste tout (backtracking).

>[!tip] Définition
>On étend directement les définitions d'états accessibles  et co-accessibles et d'automates émondés et standards au automates non déterministes.
>

### B - Fonctions de transitions étendues

>[!note]
>Plutôt que de définir un automate non déterministe pour une relation de transition $\Delta \subseteq Q \times \Sigma \times Q$. On peut utiliser une fonction de transition $\delta: Q\times \Sigma \rightarrow \mathcal{P}(Q)$.
>Il n'y alors pas de blocages mais $\delta(q,a)$ peut être $\varnothing$.

>[!info] Remarque
>Un AFND est un AFD si 
>- $|I|=1$
>- $\forall (q,a)\in Q \times \Sigma, |\delta(q,a)| \leq 1$ (et $=1$ pour AFDC)


>[!info] Remarque 
>On peut étendre la fonction de transition $\delta$ en une fonction $\hat{\delta}$ sur les parties de $Q$.
>$\hat{\delta}: \mathcal{P}(Q)\times \Sigma \rightarrow \mathcal{P}(Q)$
>avec $\forall X \subseteq Q, \forall a \in \Sigma, \hat{\delta}(X,a)=\bigcup_{q \in X}\delta(q,a)$
>![[chap9_img8.png]]
>Intuitivement $\hat{\delta}(X,a)$ est l'ensemble des états dans lesquels on peut arriver en lisant la lettre $a$ à partir d'un état de $X$.

>[!info] Remarque
>On étend $\hat{\delta} : \mathcal{P}(Q)\times \Sigma \rightarrow \mathcal{P}(Q)$ en une fonction $\hat{\delta}^{*} : \mathcal{P}(Q)\times \Sigma^{*} \rightarrow \mathcal{P}(Q)$ (qui est l'étoile de $\hat{\delta}$).
>$\forall X \subseteq Q, \forall a \in \Sigma, \forall u \in \Sigma^{*}, \hat{\delta}^{*}(X,\varepsilon)=X, \hat{\delta}^{*}(X,au)=\hat{\delta}^{*}(\hat{\delta}(X,a),u)$
>
>Intuitivement, $\hat{\delta}^{*}(X,u)$ est l'ensemble des états où on se trouve en lisant $u$ à partir d'un état de $X$.

>[!example] Exemple
>![[chap9_img9.png]]

>[!tip] Propostion
>Un mot $u \in \Sigma^{*}$ est reconnu par un AFND si et seulement si $\hat{\delta}^{*}(I,u)\cap T \neq \varnothing$.

>[!info] Remarque
>En pratique, il suffit donc de calculer $\hat{\delta}^{*}$ pour savoir si un mot est reconnu.
>Complexité en $O(|Q|\ |u|)$ avec une représentation d'ensemble en $O(1)$ et des voisins pour $\delta$.
>On maintient en tout temps un variable correspondant à l'ensemble des états en cours.

### C - Déterminisation

>[!Question] Les automates non déterministes sont a priori plus expressifs. Mais est-ce vraiment le cas ?

>[!question] Existe-t-il des langages reconnus par un automate fini non déterministe mais aucun déterministe ?
>$\rightarrow$ **NON**

>[!tip] Définition
>Soit $A= (Q,\Sigma,I,T,\Delta)$ un AFND.
>**Déterminiser** $A$ c'est trouver explicitement un AFD $A'=(Q',\Sigma,q_{0}',T',\delta')$ tel que $\mathcal{L}(A)=\mathcal{L}(A').$

>[!info] Remarque
>On crée un automate dont les états sont les parties de $Q$ (qui correspondent intuitivement au états où on peut se trouver) et on simule toutes les exécutions possibles.

>[!tip] Définition
>Sot $A= (Q,\Sigma,I,T,\Delta)$ un AFND. L'automate des partie de $A$ est l'automate déterministe complet $\hat{A}=(\hat{Q},\Sigma,\hat{q_{0}},\hat{T},\hat{\delta})$ avec 
>- $\hat{Q} = \mathcal{P}(Q)$
>- $\hat{q_{0}}=I \in \mathcal{P}(Q)=\hat{Q}$
>- $\hat{T}=\left\{X \in Q\ | \ X\cap T \neq \varnothing\right\}$
>- $\hat{\delta}: \hat{Q} \times \Sigma \rightarrow \hat{Q}$.

>[!example] Exemple
>![[chap9_img10.png]]

| $\hat{Q} \backslash \Sigma$         | a         | b     |
| ----------------------------------- | --------- | ----- |
| $\hat{q_{0}}= I = \left\{0\right\}$ | $\left\{0,1\right\}$     | $0$     |
| $\hat{q_{1}}=\left\{0,1\right\}$                 | $\left\{0,1\right\}$     | $\left\{0,1,2\right\}$ |
| $\hat{q2 }=\left\{0,1,2\right\}$                 | $\left\{0,1,2,3\right\}$ | $\left\{0,1,2\right\}$|
| $\hat{q_{3}}=\left\{0,1,2,3\right\}$            | $\left\{0,1,2,3\right\}$ | $\left\{0,1,2,3\right\}$      |
>[!example] Exemple suite
>![[chap9_img11.png]]

>[!tip] Algorithme 
>Pour construire l'automate des parties, on cherche $\hat{\delta}$ sous la forme d'un tableau.
>(cf exemple)
>On part de $\hat{q_{0}}=I$ et on construit les images par $\hat{\delta}$ des états de $\hat{Q}$ (ensemble des états de $Q$) que l'on trouve.
>On construit ainsi les états **accessibles** de l'automates des parties.

>[!info] Remarque
>Il s'agit bien d'un automate fini déterministe complet.

>[!tip] Proposition
>Soit $A = (Q,\Sigma, I, T, \Delta)$ un AFND, il existe un AFDC qui reconnait le même langage.

>[!note] Démo
>On construit $\hat{A}=(\hat{Q}, \Sigma, \hat{q_{0}},\hat{T}, \hat{\delta})$
>l'automate des parties.
>On a pour $u \in \Sigma^{*}$
>$u \in \mathcal{L}(A) \Leftrightarrow \hat{\delta}(I,u) \cap T \neq \varnothing$
>$\Leftrightarrow \hat{\delta} (\hat{q_0}, u) \in \hat{T}$
>$\Leftrightarrow u \in \mathcal{L}(\hat{A})$

>[!info] Remarque
>L'automate ainsi construit possède $2^n$ états (au pire). On ne peut pas faire mieux (cf TD).

### D - $\varepsilon$-transitions

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!tip] Définition
>Un mot $u \in \Sigma^{*}$ est reconnu par un $\varepsilon$-AFND s'il existe un chemin d'un état initial à un état final d'étiquette $u$.
>L'étiquette d'un chemin étant la concaténation des lettres vues comme mots de taille 1 et des $\varepsilon$ vus comme mots vide.

>[!example] Exemple
>si $u = u_{1}\cdots u_{n} \in \Sigma^{*}$ reconnu par un $\varepsilon$-AFND alors $\exists q_{0} \in I, \exists q_{f}\in T$.
>```mermaid
>flowchart LR
>q0--u-->qf
>```
>On ne peut pas dire qu'il existe $q_{1}\in Q$ tel que 
>```mermaid
>flowchart LR
>q0--u1-->q1
>```

>[!tip] Proposition
>Soit $X \subseteq Q, w \in \Sigma^{*}, a \in \Sigma$
>$\hat{\delta}^{*}(X,\varepsilon)=E(X)$
>$\hat{\delta}^{*}(X,wa) = E[\hat{\delta}(\hat{\delta}^{*}(X,w),a)]$

## IV - L'algorithme de Berry-Sethi

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

## V - Le théorème de Kleene

>[!tip] Définition
> Soit $\Sigma$ un alphabet. On note $\text{Reg}(\Sigma)$ l'ensemble des langages réguliers (rationnels) et $\text{Rec}(\Sigma)$ l'ensemble des langages reconnaissables par un automate.

>[!info] Remarque
>On a vu que les notions de reconnaissable par automate fini déterministe (complet, émondé si non vide, standard), par un automate non déterministe, par automate avec $\varepsilon$-transition, par automate généralisé, coïncident et on peut juste parler de "langage reconnaissable".

>[!tip] Proposition
>Soit $L,L' \in \text{Rec}(\Sigma)$ 
>On a les **propriétés de clôtures** : 
> - $\Sigma^{*} \backslash L \in \text{Rec}(\Sigma)$.
>AFDC et on échange états terminaux et non terminaux.
>- $L \cup L' \in \text{Rec}(\Sigma)$ et $L \cap L' \in \text{Rec}(\Sigma)$
>	1. Automate produit : $T'=Q_{1}\times T_{2}\cup T_{1}\times Q_{2}$ ou $T'=T_{1}\times T_{2}$ 
>	   $\rightarrow$ AFDC $\times$ AFDC
>	2. $L \cap L' = \Sigma^{*} \backslash (\Sigma^{*}\backslash L \cup \Sigma^{*} \backslash L')$
>- $L \cdot L' \in \text{Rec}(\Sigma)$

>[!info] Remarque
>On peut généraliser à une union ou intersection finie.

>[!info] Remarque
>$L = \bigcup_{u \in L} \left\{u\right\}$ donc pas de généralisation à une union infinie.
>

>[!tip] Théorème de Kleene
>$\text{Reg}(\Sigma)= \text{Rec}(\Sigma)$

>[!info] Conséquence
>Si $e \in \mathcal{E}(\Sigma)$ alors $\exists e' \in \mathcal{E}(\Sigma)$ tel que $L(e')=\Sigma^{*}\backslash L(e)$ et si $e_{1},e_{2} \in \mathcal{E}(\Sigma), \exists e \in \mathcal{E}(\Sigma)$ tel que $L(e)=L(e_{1})\cap L(e_{2})$.

>[!info] Remarque
>A partir de $e$ on construit un automate $A$ qui reconnait $\mathcal{L}(e)$.
>On construit $A'$ qui reconnait $\Sigma^{*}\backslash \mathcal{L}(A')$ et on construit $e'$ qui dénote $\mathcal{L}(A')$.

>[!note] Démo du théorème de Kleene
>$\text{Reg}(\Sigma) \subseteq \text{Rec}(\Sigma)$
>1. On a $\varnothing \in \text{Rec}(\Sigma), \varepsilon \in \text{Rec}(\Sigma), \forall a \in \Sigma, \left\{a\right\} \in \text{Rec}(\Sigma)$. $\text{Rec}(\Sigma)$ est stable par opérations régulières.
>	Donc $\text{Reg}(\Sigma) \subseteq \text{Rec}(\Sigma)$
>2. Automate de Thompson $A$ de $e$ donc $\mathcal{L}(A)=\mathcal{e}=L$
>3. Automate de Glutkov $A$ de $e$ donc $\mathcal{L}(A)=\mathcal{e}=L$
>
>$\text{Rec}(\Sigma) \subseteq \text{Reg}(\Sigma)$
>Soit $L \in \text{Rec}(\Sigma)$ et $A$ un automate tel que $L = \mathcal{L}(A)$.
>1. [HP] On applique l'algorithme de McNaughton et Yamada pour obtenir $c \in \mathcal{E}(\Sigma)$ tel que $L = \mathcal{L}(A)=L(e)$.
>2. Même chose avec élimination des états.

## VI - Langages non reconnaissables / non réguliers

>[!note]
>On sait montrer qu'un langage est reconnaissable / régulier.
>1. On trouve une expression régulière qui le dénote.
>2. On trouve un automate (de tout type) qui le reconnait.
>3. On utilise les propriétés de clôtures.

>[!question] Mais comment on montre qu'un langage n'est pas régulier ?

>[!info] Remarque
>Si $L$ est régulier, il existe un automate $A$ avec $|Q|=n \in \mathbb{N}^{*}$ qui le reconnait.

![[chap9_img12.jpg]]

>[!tip] Lemme de l'étoile
>Soit $L$ un langage régulier. Il existe $N \in \mathbb{N}^{*}$ tel que pour tout mot $u\in L$ avec $|u| \geq N$. Il existe une composition $x,y,z \in \Sigma^{*}$ avec :
>- $xyz = u$
>- $|xy| \leq N$
>- $y \neq \varepsilon$
>- $\forall k \in \mathbb{N}, xy^{k}z \in L$

>[!info] Remarque
>$N$ est le nombre d'états d'un automate qui reconnait $L$.
>![[chap9_img13.png]]
>avec $y \neq \varepsilon$
>(mais $x = \varepsilon$ et/ou $z = \varepsilon$ possible).

>[!example] Exemple
>$L = \left\{a^{n}b^{n}, n\in \mathbb{N}\right\}$ n'est pas régulier.
>Supposons $L$ régulier, il vérifie donc le lemme de l'étoile. Il existe donc $N \in \mathbb{N}^{*}$ une constante donnée par ce lemme.
>On considère $u = a^{N}b^{N}$.
>Il existe $x,y,z \in \Sigma^{*}$ avec :
>- $xyz =u$
>- $|xy| \leq N$
>- $y \neq \varepsilon$
>- $xy^{*}z \subseteq L$ ($\forall k \in \mathbb{N}, xy^{k}z \in L$)
>
>On pose $k=0$
>Comme $xyz = a^{N}b^{N}$ et que $|xy| \leq N$.
>$xy$ est un préfixe de $a^{N}$ donc $\exists i,j, k \in \mathbb{N}$ tel que 
>$x = a^{i}, y =a^{j}, z = a^{k}b^{N}$
>Donc $xy^{0}z = a^{i+k}b^{N}\in L$ par le lemme de l'étoile.
>Absurde car $i+k < i+j+k = N$.

>[!info] Remarque
>Ce lemme  est utile pour montrer qu'un langage est **non régulier** en le mettant en défaut.

>[!info] Remarque
>Il s'agit d'une condition nécessaire mais non suffisant.

>[!warning] 
>On peut avoir $L$ régulier et $L'$ non régulier avec $L \cup L'$ régulier **ou non**.

>[!tip] Proposition
>Soit $L,L'$ deux langages. 
>Si $L$ est régulier et si $L \cup L'$ **n'est pas régulier** alors $L'$ n'est pas régulier.

>[!note] Démo
>Si $L'$ est régulier alors $L \cup L'$ est régulier.

>[!info] Remarque
>De même avec l'intersection, concaténation et étoile de Kleene.

>[!info] Remarque
>$L$ est régulier si et seulement si $\Sigma^{*} \backslash L$ est régulier.

>[!warning]
>Si on sait que $\left\{u \in \Sigma^{*} \ | \ |u|_{a}=|u|_{b|}\right\}$ est non régulier. On ne peut pas déduire que $L$ est non régulier.

>[!note] Démo du lemme de l'étoile
>Soit $A = (Q,\Sigma, q_{0},T,\delta)$ un AFDC. On note $N = |Q| \in \mathbb{N}^{*}$. On pose $|u| = m$
>Soit $u \in L \cap \Sigma^{\geq N}$
>Pour $i \in [|0,N|]$ on note $v_{i}=u_{1}\cdots u_{i}$ le préfixe de $u$ de longueur $i$.
>La fonction $\varphi : \begin{cases}[|0,N|] \longrightarrow Q \\ i \longrightarrow \delta^{*}(q_{0},v_{i}) \end{cases}$  est non injective.
>Donc $\exists i,j \in [|0,N|], i<j$ tels que $\delta^{*}(q_{0},v_{i})=\delta^{*}(q_{0},v_{j})$.
>On pose $x=u_{1}\cdots u_{i}, y = u_{i+1}\cdots u_{j}, z = u_{j+1}\cdots u_{m}$
>On a bien : 
>- $xyz = u$
>- $|xy| \leq N$
>- $y = u_{i+1}\cdots u_{j} \neq \varepsilon$ car $i<j$.

>[!info] Remarque
>$x = \varepsilon$ si $i = 0$
>$z = \varepsilon$ si $j=m$
>Soit $k \in \mathbb{N}$, montrons que $xy^{k}z \in L$.
>On a $\delta^{*}(q_{0},x)=\delta^{*}(q_{0},v_{i})$
>$\delta^{*}(q_{0},xy)=\delta^{*}(q_{0},v_{j})$
>Par récurrence sur $k \in \mathbb{N}$.
>$\delta^{*}(q_{0},xy^{k})=\delta^{*}(q_{0},xy)=\delta^{*}(q_{0},x)$
>Vrai pour $k=0$
>On suppose le résultat pour $k \in \mathbb{N}$.
>On a $\delta^{*}(q_{0},xy^{k+1})=\delta^{*}(\delta^{*}(q_{0},xy))$
>

