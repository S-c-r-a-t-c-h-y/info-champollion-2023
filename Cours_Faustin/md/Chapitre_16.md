# Chapitre 16 : Grammaire et langages non contextuels

```toc
```

>[!done] Rappel
>Nous avons déjà étudié la classe des langages réguliers (ou rationnels) qui est une classe de langages simples mais déjà expressif. Ils correspondent : 
>- Les langages dénotés par une expression régulière (motif).
>- La plus petite classe de langage contenant $\varnothing, \left\{\varepsilon\right\}$, les langages d'une lettre et stable par opérations régulières.
>- Les langages reconnaissables par automates finis (déterministe ou non, avec $\varepsilon$-transition ou non).
> 
>Nous avons vu que certains langages n'étaient pas réguliers : $\left\{a^{n}b^{n}, n \in \mathbb{N}\right\}$ n'est pas régulier (lemme de l'étoile).
>
>**Intuition** : Les langages réguliers sont ceux que l'on peut reconnaître avec un automate qui est à **mémoire bornée** (mémoire : dans les états et transition de l'automate $\rightarrow$ le fameux $N$ du lemme de l'étoile).
>
>On a indiqué, de manière informelle que si on ajoute une bande mémoire infinie et un curseur, on obtient une machine de Turing.
>La classe des langages reconnus par des machines de Turing qui s'arrêtent toujours sont des langages décidables.

>[!example] Exemple
>Le langage de mots qui représentent un programme OCaml qui s'arrête toujours n'est pas décidable.

>[!note]
>Il existe de nombreuses classes intermédiaires et même une hiérarchie (hiérarchie de Chomsky [HP]).
>
>Une classe intermédiaire intéressante est celle du langage non contextuel. Ce sont ceux reconnu par un **automate à pile** [HP].

>[!example] Exemple
>$\left\{a^{n}b^{n}, n \in \mathbb{N}\right\}$
>On empile les $a$ puis en dépilant les $a$, on a les $b$.

>[!note]
>Comme les langages réguliers, il existe de nombreuses caractérisations des langages :
>- non contextuels
>- sans contexte
>- hors-contexte
>- algébriques
> 
> La caractérisation au **programme** langage **généré** par une **grammaire non contextuelle**

## I - Introduction informelle aux grammaires

>[!note]
>On se donne un ensemble de règles de réécriture, des symboles terminaux (un alphabet $\Sigma$, des symboles non terminaux et un autre alphabet $V$) et symbole $S \in V$ initial.

>[!example] Exemple 1
>$\Sigma = \left\{a,b\right\}$
>$V = \left\{S\right\}$
>$\begin{cases} S \rightarrow aSb \\ S \rightarrow \varepsilon \end{cases}$
>
>$S \Rightarrow \varepsilon$
>$S \Rightarrow aSb \Rightarrow aaSbb \Rightarrow aaa S bbb \Rightarrow a^{3}b^{3}$
>Cette grammaire va générer $\left\{a^{n}b^{n}, n \in \mathbb{N}\right\}$.

>[!note]
>On s'intéresse aux mots de $\Sigma^{*}$ que l'on peut obtenir à partir de $S$ par application des règles de **production**.

>[!example] Exemple 2
>$\Sigma = \left\{a,b,(,),+,\cdot, *,\varepsilon, \varnothing \right\}$
>$V = \left\{S,A\right\}$
>$S \rightarrow A\ |\ (S)\ |\ S \cdot S\ |\ S+S\ |\ S *$
>$A \rightarrow \varepsilon |\ \varnothing \ |\ a \ |\ b$
>$S \Rightarrow (S \cdot S)\Rightarrow (S \cdot (S)) \Rightarrow (S* \cdot (S)) \Rightarrow (A * (S))\Rightarrow (b* \cdot (A)) \Rightarrow (b * \cdot (\varepsilon))$
>représente $b^{*}\cdot \varepsilon$.

>[!example] Exemple 3
>\<GV> $\rightarrow$ \<V>\<GN>
>\<P> $\rightarrow$ \<GN>\<GV>
>\<GN> $\rightarrow$ \<D>\<N>
>\<D> $\rightarrow$ le | la | ma | ...
>\<N> $\rightarrow$ fille | garçon | danse | ...
>\<V> $\rightarrow$ aime | ...
>
>On peut générer à partir de \<P> : la fille aime la fille.

>[!info] Application
>- Etude des langages naturelles
>- Définition formelle de la syntaxe des langages (programmation, balisage, HTML, etc)
>- Compilation
>- Analyse formelle de programmes
>- ETC ...

## II - Formalisation

>[!tip] Définition
>Une grammaire non contextuelle est un quadruplet $G=(V,\Sigma, R,S)$ avec :
>- $V$ : un alphabet de symboles non terminaux
>- $\Sigma$ : un alphabet de symboles terminaux (avec $V \cap \Sigma = \varnothing$ 
>- $R \subseteq V \times (V \cup \Sigma)^{*}$ un **ensemble fini de règles de production**
>  si $(X,\alpha) \in R$, on note $X \rightarrow \alpha$ la règle.
>- $S \in V$ symbole initial

>[!info] Remarque
>Du côté gauche **un** symbole non terminal (non contextuel)

>[!info] Remarque
>Conventionnellement, on note les symboles de $V$ des capitales (majuscules) et des mots de $(V \cup \Sigma)^{*}$ pour des lettres grecques minuscules. 

>[!info] Remarque
>Lorsque l'on dispose de plusieurs règles $X \rightarrow \alpha$, $X \rightarrow \beta$ et $X \rightarrow \gamma$. On note $X \rightarrow \alpha\ |\ \beta\ |\ \gamma$.

>[!info] Remarque
>Toutes les grammaires au programmes sont "non contextuelles", on peut juste dire "grammaire".

>[!tip] Définition
>Soit $G=(V,\Sigma, R,S)$ une grammaire sans contexte et $\alpha, \beta \in (V\cup \Sigma)^{*}$.
>On note $\alpha \rightarrow \beta$ et in dit que $\alpha$ se dérive immédiatement en $\beta$.
>$\exists (X,\alpha) \in R$ une règle, $\alpha_{1},\alpha_{2} \in (V\cup \Sigma)^{*}$ tel que $\alpha = \alpha_{1} X \alpha_{2}$ et $\beta = \alpha_{1} \alpha \alpha_{2}$
> $\alpha_{1}X \alpha_{2} \Rightarrow \alpha_{1}\alpha \alpha_{2}$
> 
> Pour $n \in \mathbb{N}$, on note $\alpha \Rightarrow^{n} \beta$. On dit que $\alpha$ se dérive en $n$ étapes en $\beta$ si $\exists \gamma{0},\gamma_{1},\cdots,\gamma_{n} \in (V\cup \Sigma)^{*}$ tel que $\alpha=\gamma_{0}\Rightarrow\gamma_{1}\Rightarrow \cdots \Rightarrow \gamma_{n} = \beta$.
> On note $\alpha \Rightarrow^{*}\beta$ et on dit que $\alpha$ se dérive en $\beta$ s'il existe un $n \in \mathbb{N}$ tel que $\alpha \Rightarrow^{n} \beta$

 >[!example] Exemple
>$aaSbb \rightarrow aaaSbbb$

>[!info] Remarque
>$\Rightarrow^{*}$ est la clôture reflexive et transitive de $\Rightarrow$.

>[!tip] Définition
>Soit $G=(V,\Sigma, R,S)$ une grammaire hors contexte et $u \in \Sigma^{*}$. On dit que $G$ **génère** $u$ si $S \Rightarrow^{*}u$.
>L'ensemble $\mathcal{L}(G) = {u \in \Sigma^{*}|S \Rightarrow^{*}u}$ est appelé langage **engendré** par $G$.

>[!warning] Attention 
>$\mathcal{L}(G) \in \Sigma^*$ 

>[!tip] Définition
>Un langage $L \subseteq \Sigma^{*}$ est dit algébrique ou non contextuel s'il existe une grammaire G non contextuelle telle que $L = \mathcal{L}(G)$.

>[!example] Exemple
> $S \rightarrow aSb\ |\ \varepsilon$
> $G = (\{S\},\{a,b\},\{S,aSb\},\{S,\varepsilon\},S)$
> $\mathcal{L}(G)=\{a^{n}b^{n}, n \in \mathbb{N}\}$
> 
> Montrons que $L \subseteq \mathcal{L}(G)$
> Soit $u \in L, \exists n \in \mathbb{N}, u=a^{n}b^{n}$
> Pour $k \in \mathbb{N}$ notons $\gamma_{k}=a^{k}Sb^{k}\in (V\cup \Sigma)^{*}$
> Montrons par récurrence sur $k \in \mathbb{N}$ que $S \Rightarrow^{k}\gamma_{k}$.
> Initialisation : 
> $S \Rightarrow^{0}S$
> Hérédité :
> Supposons le résultat vrai pour $k \in \mathbb{N}$.
> On a $S \Rightarrow^{k}\gamma_{k}=a^{k}Sb^{k}\Rightarrow a^{k}aSbb^{k}$ avec la règle $S\rightarrow aSb$ d'où $S \Rightarrow^{k+1}a^{k+1}Sb^{k+1}$
> et pour $k=n$ on a $S \Rightarrow a^{n}Sb^{n} \Rightarrow a^{n}\varepsilon b^{n}=a^{n}b^{n}$ avec la règle $S \rightarrow \varepsilon$
> Donc $a^{n}b^{n}\subseteq \mathcal{L}(G)$.
> 
> Montrons que $\mathcal{L}(G)\subseteq L$
> Montrons par récurrence sur $n \in \mathbb{N}$ que si $S \Rightarrow^{n}\alpha$ alors $\alpha = \begin{cases} a^{n-1}b^{n-1}\ \text{si}\ n \geq 1 \\ \text{ou} \\ \gamma_{n} \end{cases}$
> Initialisation : 
> Pour $n=0$ si $S \Rightarrow^{0}\alpha$ alors $\alpha = S = \gamma_{0}$. La propriété est vérifiée.
> Hérédité : 
> Supposons la propriété vraie aux rang $n \in \mathbb{N}$ et supposons $S \Rightarrow^{n+1} \alpha$ avec $\alpha \in (V\cup \Sigma)^{*}$
> $\exists \beta \in (V\cup \Sigma)^{*}$ tel que $S \rightarrow^{n}B \Rightarrow \alpha$
> Par HR, $\beta \begin{cases} a^{n-1}b^{n-1}\ \text{si}\ n \geq 1 \\ \text{ou} \\ \gamma_{n} \end{cases}$
> On a $\beta \Rightarrow \alpha$
> $\exists \beta_{1},\beta_{2} \in (V,\Sigma)^{*}$ tel que $\beta = \beta_{1}S\beta_{2}$ car toutes les règles sont de la forme $S \rightarrow \delta$
> et $\alpha = \beta_{1}\delta \beta_{2}$
> Comme $a^{n-1}b^{n-1} \in \Sigma^{*}$ donc le cas 1 est impossible et comme $|\gamma_{n}|_{S}=1$ la seule possibilité est $\beta_{1}=a^{n}, \beta_{2}=b^{n}$ et donc $\beta = a^{n}Sb^{n}$
> ou $\begin{cases} \alpha = a^{n+1}Sb^{n+1}=\gamma_{n+1}\ \text{avec la règle}\ S\rightarrow aSb \\ \alpha=a^{n}b^{n}\ \text{avec la règle}\ S\rightarrow \varepsilon \end{cases}$
> Et la propriété est vérifiée au rang $n+1$.

>[!tip] Propriété
>La classe des langages non contextuels est stable par opérations régulières (union, concaténation, étoile)

>[!note] Démo
>Soit $L_{1},L_{2}$ deux langages algébriques sur un même alphabet $\Sigma$.
>Soit $G_{1}=(V_{1},\Sigma, R_{1},S_{1})$ et $G_{2}=(V_{2},\Sigma, R_{2},S_{2})$ deux grammaires engendrant respectivement ces deux langages.
>On peut supposer $V_{1}\cap V_{2}= \varnothing$ quitte à renommer et $S \not \in V_{1}\cup V_{2}$.
>$L_{1}\cup L_{2}$ est engendré par $G_{\cup}=(V_{1}\sqcup V_{2} \sqcup \{S\}, \Sigma, R_{1}\sqcup R_{2}\sqcup \{S\rightarrow S_{1},S \rightarrow S_{2}\},S)$
>$S \rightarrow S_1|S_2$
>
>Dans la concaténation : 
>$S\rightarrow S_{1}S_{2}$
>
>Dans l'étoile de Kleene
>$S\rightarrow SS|S_{1}|\varepsilon$

>[!tip] Proposition
>Les langages réguliers sont algébriques.

>[!note] Démo
>$\varnothing$ est algébrique $G = (\{S\},\Sigma,\varnothing,S)$
>$\varepsilon$ est algébrique $G = (\{S\},\Sigma,\{S\rightarrow \varepsilon\},S)$
>$\forall a \in \Sigma$ est algébrique $G = (\{S\},\Sigma,\{S\rightarrow a\},S)$
>On conclut avec la propriété précédente.

>[!warning] Attention
>La classe des langages algébriques n'est pas close par complémentaire ni intersection.

>[!info] Remarque
>L'inclusion est stricte puisque $\{a^{n}b^{n},n \in \mathbb{N}\}$ est "non contextuel" ou "algébrique" mais non régulier.

## III - Arbre d'analyse | syntaxique | de dérivation

>[!tip] Définition
>Soit $G=(V,\Sigma, R,S)$ une grammaire et $u \in \mathcal{L}(G)$. Un arbre d'analyse syntaxique est un arbre tel que : 
>- La raison a pour étiquette $S$
> - Chaque nœud interne a pour étiquette un symbole non terminal de $V$.
> - Les feuilles sont étiquetées par $\Sigma \cup \{\varepsilon\}$
> - Une feuille d'étiquette $\varepsilon$ est fille unique
> - Les étiquettes des feuilles, lues de gauche à droite, forment le mot $u$.
> - Si un nœud interne d'étiquette $X$ a pour fils des nœuds d'étiquettes $\alpha_{1},\cdots,\alpha_{n}\in R$

>[!example] Exemple
>$S \rightarrow (S)|S+S|S\times S| 0 | 1 |2|\cdots|9$
>$0\times 1 +2$ :
>```mermaid
>flowchart TD
A[S] --> B[S] --> C[0]
A --> D[*]
A --> E[S] --> F[S] --> G[1]
E --> H[+]
E --> I[S] --> J[2]
>```

>[!bug] Exercice
>$G$ : $S \rightarrow aSb|aXb$, $X  \rightarrow AX|\varepsilon$, $A \rightarrow a|b$
>Le langage est $L(a(a|b)^{*}b)$
>$abbab \in \mathcal{L}(G) ?$

>[!tip] Définition
>Soit $G$ une grammaire et $u \in \mathcal{L}(G)$.
>- On dit que $u$ est ambigu pour $G$ s'il existe plusieurs arbres d'analyse syntaxique distincts pour $u$.
>- On dit que $G$ est ambiguë s'il existe au moins un mot ambigu par $G$.

>[!example] Exemple
>$G: S \rightarrow aSb|aXb$, $X  \rightarrow AX|\varepsilon$, $A \rightarrow a|b$ est ambiguë
>
>$G' : S \rightarrow aXb$, $X \rightarrow AX| \varepsilon$, $A \rightarrow A|b$ n'est pas ambiguë

>[!warning] Attention
>L'ambiguïté est une propriété de la **grammaire** pas du langage. 
>On peut avoir pour un même langage, une grammaire ambiguë et une autre qui ne l'est pas.

>[!info] Remarque
>Il existe des langages intrinsèquement ambigus : toute grammaire qui l'engendre est ambiguë.

>[!tip] Définition
>Deux grammaires $G,G'$ sont faiblement équivalentes si $\mathcal{L}(G)=\mathcal{L}(G')$.

>[!info] Remarque
>Faiblement car on n'impose rien sur la forme des dérivations.

>[!info] Dérivation Gauche
>Toujours prendre le symbole non terminal le plus à gauche.

>[!example] Exemple
>$0 * 1 + 2$
>$S \Rightarrow S * S  \Rightarrow 0 * S  \Rightarrow 0 * S + S  \Rightarrow 0*1+S  \Rightarrow 0*1+2$

>[!tip] Définition
>Soit $G = (V,\Sigma,R,S)$ une grammaire.
>- Une dérivation immédiate à gauche est une dérivation de la forme $uXu \rightarrow u \beta \alpha$ avec $u \in \Sigma^{*}, (X, \beta) \in R, \alpha \in (V\cup \Sigma)^{*}$
>- Une dérivation gauche est une dérivation dont toutes les dérivations immédiates sont gauches.

>[!info] Remarque
>De même à droite

>[!tip] Proposition
>Les dérivations à gauches (ou à droite) sont en bijection avec les arbres d'analyse syntaxique. (parcours préfixe)

>[!note] Démo
>A un arbre de dérivation $T$ dont la racine est un symbole $\alpha \in \Sigma \cup V$
>On associe la dérivation gauche $d(T)$ : 
>- si $\alpha \in \Sigma \cup \{\varepsilon\}$ (autrement dit, on a une feuille) alors $d(T)=\alpha$ (dérivation de longueur 0)
>- sinon $\alpha = X$ avec $\forall i \in [|1,n|], \alpha_{i}\in (V\cup \Sigma)$ et $\alpha \alpha_{1} \cdots \alpha_{n}\in R$
>On construit par induction en dérivant $d(\alpha_{1}),\cdots d(\alpha_{n})$.
> $\forall i \in [|1,n|], d(\alpha_{i}): \alpha_{i} \Rightarrow^{*}u_{i}$ avec $u_{i}\in \Sigma^{*}$
> On considère la dérivation gauche $X \Rightarrow \alpha_{1} \alpha_{2} \cdots \alpha_{n} \Rightarrow^{*}u_{1} \alpha_2 \cdots \alpha_{n}\Rightarrow^{*}u_{1}\cdots u_{n}$.
> 
> Inversement, à une dérivation gauche $S \Rightarrow^{*} \alpha \in (V \cup \Sigma)^{*}$, on associe un arbre de dérivation partiel (les feuilles sont des ($\Sigma \cup V \cup \{\varepsilon\}$)) et les étiquettes lues de gauches à droite donc $\alpha$.
> Si la dérivation est de taille nulle, $S \Rightarrow^{0}\alpha$ alors $\alpha = S$ et on construit une racine.
> Sinon, la dérivation est de la forme $S \Rightarrow^{*}uX \beta \Rightarrow u \gamma \beta$ avec $u \in \Sigma^{*}$, $\beta, \gamma \in (\Sigma\cup V )^{*}$ et $X \rightarrow \gamma \in R$.
> Par induction, on construit l'arbre partiel pour $S \Rightarrow^{*}uX\beta$ la première feuille dont l'étiquette n'est pas dans $\Sigma\cup \{\varepsilon\}$ est $X$. On remplace la feuille par l'arbre.
> On obtient un arbre de dérivation partiel pour $S \Rightarrow^{*} u \gamma \beta$.

