# Chapitre 15 : Algorithmes d'approximation

```toc
```

## I - Motivation

>[!note]
>Savoir qu'un problème est NP-complet élimine tout espoir de le résoudre en temps polynomial (sauf si P = NP). La résolution d'un tel problème nécessite, aujourd'hui, pratiquement toujours d'explorer un nombre potentiel de solution candidate (même après élagage dans un approche de retour sur trace). 
>Mais il ne faut pas baisser les bras pour autant, ni même espoir de résoudre le problème de manière plus ou moins satisfaisante.
>- Il n'existe pas d'algorithme polynomial dans le **pire** cas. Mais des instances pire cas sont parfois assez exotiques. Un algorithme (exponentiel) peut très bien se comporter correctement en **pratique**, en **moyenne**.
>- On peut imaginer des heuristiques efficaces pour une approche de retour sur trace (et éliminer 99.99999% de l'espace de recherche) (ça reste exponentiel).
>- On peut utiliser des algorithmes probabilistes qui donne une solution correcte dans la plupart des cas ou une solution exacte de manière souvent rapide.
>- On peut parfois se contenter d'une "bonne" solution à défaut d'une solution optimale. On utilise des **algorithmes d'approximation**. Il est en général essentiel de quantifier la qualité de cette approximation. Un grand nombre de problèmes NP-complets peuvent être résolus en faisant certaines concessions : soit le temps de calcul, soit la qualité des solutions : **compromis**.

## II - Facteurs d'approximation

>[!note]
>Dans ce chapitre, on ne considère que des problèmes d'optimisation : 
><u>INSTANCE</u> : $x \in X$
><u>SOLUTIONS</u> : $Y(x) \subseteq Y$ des solutions admissibles pour $x$.
><u>OPTIMISATION</u> : une fonction de coût $f : \begin{cases} X \times Y \longrightarrow \mathbb{R}_{+}^{*} \\ (x,y)\longmapsto f(x,y) \end{cases}$. Pour $x \in X, y \in Y, f(x,y)$ est le coût de la solution $y$ pour $x$. 
>On cherche $C^{*}(x)=\underset {y \in Y(x)} {\text{min}\ f(x,y)}$ et, le plus souvent, une solution $y^{*}$ telle que $c^{*}(x)=f(x,y^{*})$.
>Un algorithme $A$ (non exact) pour un problème $P$ calcule pour $x \in X$ une solution $y \in Y(x), A=(x)$.
>On note $c_{A}(x)=y$.

>[!question] Comment quantifier la qualité de la solution $y$ ?
>On a $0 < c^{*}(x) \leqslant y=c(x)$.
>On aimerait par exemple garantir que $x \in X, |c^{*}(x)-c(x)| \leqslant b$ avec $b \in \mathbb{R}_{+}$.
>Il y a très peu de problème NP-complets approximales avec cette définition.

>[!example] Exemple
>EDGE-COLOR :
>Est-il possible de colorier les arêtes d'un graphe $G=(S,A)$ avec $k$ couleurs.
>Il faut au moins $d(c)$ couleurs et au maximum $d(c)+1$.

>[!note]
>On va plutôt prendre une facteur **multiplicatif**.

>[!tip] Définition
>Soit $P$ un problème d'optimisation $(X, x \longmapsto Y(x),f)$. Dans une instance de $x \in X$, on considère $c^{*}(x) = \underset {y \in Y(x)} {\text {min}\ f(x,y)}$ la valeur optimale pour $x \in X$. 
>On considère un algorithme $A$ pour $P$. Pour $x \in X$, on note $y = A(x)$ la solution renvoyée par $A$ et $c_{A}(x)=f(x,y)$ la valeur de cette solution.
>Pour $\alpha \geqslant 1$ on dit que $A$ est un algorithme d'approximation de $P$ de facteur $\alpha$, ou que $A$ est une $\alpha$-approximation de $P$ si $\forall x \in X, c_{A}(x)\leqslant \alpha c^{*}(x)$.
>Autrement dit, la solution renvoyée par $A$ est à un facteur $\alpha$ près de la solution optimale.

>[!info] Remarque
>On a bien sûr $c^{*}(x)\leqslant c_{A}(x)$.
>Au final : $0 < c^{*}(x) \leqslant c_{A}(x)\leqslant \alpha c^{*}(x)$.

>[!info] Remarque
>$\frac {c_{A}(x)} {c^{*}(x)} \leqslant \alpha$.

>[!example] Exemple
>Une 2-approximation donne une solution au pire deux fois plus grande que la solution optimale.

>[!info] Remarque
>si $\alpha \leqslant \beta$, une $\alpha$-approximation est une $\beta$-approximation.
>Le meilleur ratio d'approximation possible (le meilleur facteur d'approximation pour un problème $P$ d'optimisation) : $\underset {A\ \text{algorithme d'approximation de}\ P} {\text{inf}}\ \underset {x \in X} {\text sup}\ \frac {c_{A}(x)} {c^{*}(x)}$.

>[!info] Remarque
>si $A$ est une 1-approximation de $P$ alors $A$ résout exactement le problème $P$.

>[!note]
>On a présenté tout ceci dans le contexte de la minimisation. Si $P$ est un problème d'optimisation. Alors pour être une $\alpha$-approximation avec $\alpha \leqslant 1$. Il faut que $\forall x \in X, c^{*}(x)\geqslant c_{A}(x) \geqslant \alpha c^{*}(x) > 0$.

>[!info] Remarque
>On peut aussi parler de $\alpha(n)$-approximation où $\alpha$ dépend de $n=|x|$ la taille de l'entrée.

>[!info] Remarque
>Les algorithmes **gloutons** sont souvent de bons algorithmes d'approximation. 

# III - Exemples

### A-  COUVERTURE-MIN

>[!note]
>INSTANCE : $G=(S,A)$
>SOLUTION : $X \subseteq A, \forall \left\{x,y\right\} \in A, x \in X\ \text{ou}\ y \in X$
>OPTIMISATION : $\text{min}\ |X|$

#### 1 - Idée 1 

>[!note]
>$A$ renvoie toujours $X=S$. 
>Soit $\alpha \geqslant 1$ si $|A| = 1$, la solution renvoyée a pour coût $|S|$ la solution optimale 1 et $|S| \leqslant \alpha$. Ceci $\forall n = |S| \in \mathbb{N}$.

>[!warning] Ce c'est pas $\alpha(x)$ mais $\alpha$.

>[!info] Remarque
>C'est une $n$-approximation cependant.

#### 2 - Idée 2

>[!note]
>$A$ :
>```pseudo
>	\begin{algorithm}
>	\begin{algorithmic}
>		\Procedure{A}{}
>			\State $X \gets \varnothing$
>			\While{'il reste des arêtes non couvertes}
>				\State prendre une des ces arêtes $\left\{x,y\right\}$
>				\State $X<- X \cup \left\{x\right\}$
>			\EndWhile
>			\State Renvoyer X
>		\EndProcedure
>	\end{algorithmic}
>	\end{algorithm}
>```
>$A$ renvoie bien une couverture et est bien polynomial.
>Ce n'est pas une $\alpha$-approximation.
>$A$ :
>```pseudo
>	\begin{algorithm}
>	\begin{algorithmic}
>		\Procedure{A}{}
>			\State $X \gets \varnothing$
>			\While{'il reste des arêtes non couvertes}
>				\State prendre une des ces arêtes $\left\{x,y\right\}$
>				\State $X<- X \cup \left\{x\right\}\cup \left\{y\right\}$
>			\EndWhile
>			\State Renvoyer X
>		\EndProcedure
>	\end{algorithmic}
>	\end{algorithm}
>```

>[!info] Remarque
>L'algorithme ainsi modifié le calcule le couplage maximal de $G$.
>

>[!note]
>Reformulation :
>On calcule un couplage maximal (algorithme glouton).
>On renvoie les sommets couverts par ce couplage.
>$A$ est bien polynomial et renvoie bien une couverture.
>En effet, si $\left\{x,y\right\} \in A$ pas couvert. Alors ses extrémités sont libres et rajouter cette arête donne un couplage contenant strictement le couplage maximal. Absurde.

>[!question] Comment montrer que c'est une 2-approximation ?
>

>[!note]
>On doit montrer que $c_{A}(x)\leqslant \alpha c^{*}(x)\leqslant \alpha |A|$.
>Soit $X$ une solution optimale ($x$ comme toutes les arêtes).
>Soit $X_{A}$ la solution renvoyée par $A$ et $B$ le couplage (maximal).
>$c_{A}(x)=|X_{A}|=2|B|$.
>Si $a \in B$ alors elle est couverte par un sommet de $X$.
>Chaque arête de $B$ doit être couverte par une sommet différent de $X$ car $B$ est un couplage.
>Donc $|X| \geqslant |B|$
>Donc $2|X| \geqslant 2 |B| = |X_{A}|$ d'où $2c^{*}(x) \geqslant c_{A}(x)$. 
>Donc cet algorithme est une 2-approximation.
>$c^{*}(x) \leqslant c_{A}(x)$ mais aussi $c_{A}(x)\leqslant 2 c^{*}(x)$
>Donc $c^{*}(x)\leqslant c_{A}(x)\leqslant 2c^{*}(x)$.

### B - VOYAGEUR-DE-COMMERCE

>[!note]
>INSTANCE : $G=(S,A,\omega)$ pondéré **complet**, $\omega:A \longrightarrow \mathbb{N}^{*}$ avec $|w(A)| \leqslant 2$.
>SOLUTION : Un cycle hamiltonien de $G$.
>OPTIMISATION : $\text{min}\ \sum\limits_{i=0}^{|S|-1}\omega(s_{\pi(i)},s_{\pi(i+1)mod [|S|]})$

>[!info] Remarque
>Comme $G$ est complet, toute permutation des sommets correspond à un cycle et un cycle peut-être représenté par une permutation $\pi$.
>A une permutation $\pi$ correspond le cycle hamiltonien $(\pi_{0},\pi_{1},\cdots,\pi_{|S|-1},\pi_{0})$.

>[!note] Version décisionnelle
>INSTANCE :  $G=(S,A,\omega)$ pondéré **complet**, $\omega:A \longrightarrow \mathbb{N}^{*}$ et $p \in \mathbb{N}^{*}$ un seuil.
>QUESTION : Existe-t-il une permutation $\pi \in G_{|S|}$ de poids $\sum\limits_{i=0}^{|S|-1}\omega(s_{\pi(i)},s_{\pi(i+1)mod [|S|]}) \leqslant p$.

>[!tip] Proposition
>Il est NP. Un certificat est une permutation $\pi \in G_{|S|}$ et on sait calculer en temps polynomial en $|S|+|A|$ le poids du chemin correspondant et vérifier que ce poids est inférieur à $p$.

>[!info] Remarque
>Ce problème est NP-complet même si on suppose que $\omega$ vérifie **l'inégalité triangulaire**.
>$\forall x,y,z \in S, w(x,z)\leqslant w(x,y)+w(y,z)$

>[!note] Démo
>On a déjà vu qu'il était NP. On va réduire le problème du cycle hamiltonien.
>CYCLE-HAMILTONIEN $\leqslant_{p}$ VOYAGEUR-DE-COMMERCE-DECISION
>A un graphe $G=(S,A)$ on associe le graphe $G'=(S',A',w)$ avec $\omega A' = \mathcal{P}_{2}(S)=\left\{\left\{x,y\right\}\ |\ s,t \in S, s \neq t\right\}$ et $w :\begin{cases} A' \longrightarrow \mathbb{N}^{*} \\ a \mapsto \begin{cases} \omega(a) = 1\ \text{si}\ a \in A \\ \omega(a) =2\ \text{sinon} \end{cases} \end{cases}$ 
>avec $|w(A)| \leqslant 2$
>>[!info] Remarque
>>$\omega$ vérifie l'inégalité triangulaire car $a+b \leqslant c$ est toujours vraie pour $a,b,c \in \left\{1,2\right\}$.
>
>On prend $b= |S|$
>Cette transformation est bien polynomiale.
>$|G'| = |S| + |A'| \leqslant |S| +|S|^{2}$
>$|\omega| = 2 |A'| \leqslant 2 |S|^{2}$ car chaque poids est de taille constante.7
>$b = log |S|$
>
>Reste à montrer que $G$ possède un cycle hamiltonien ==si et seulement si== $G'$ possède un chemin $\pi$ de poids $\omega(\pi)\leqslant |S|$.
>- Si $G$ possède un cycle hamiltonien $c$, c'est aussi un cycle hamiltonien de $G'$, dont toutes les arêtes sont de poids 1. Donc $\omega(c) = |S|$ et $G'$ possède bien un cycle hamiltonien de poids au plus $|S|$.
>- Inversement, si $c$ est un cycle de $G'$ de poids au plus $|S|$. Il possède $|S|$ arêtes de poids 1 ou 2. Donc la somme est inférieur à $|S|$. Donc toutes les arêtes sont de poids 1, elles sont toutes dans $G$ qui admet donc un cycle hamiltonien.

>[!info] Algorithme d'approximation pour le voyageur de commerce avec poids strictement positifs et qui vérifie l'inégalité triangulaire
>Considérons une solution optimale au problème du voyageur du commerce.
>Un $|S|$-cycle $C^{*}$.
>Enlevons une arête quelconque à $C^{*}$.
>On obtient donc un arbre $T$ couvrant.
>On a $\omega(C^{*})> \omega(T)$.
>Si $T^{*}$ est un arbre couvrant de poids minimal, on a donc $\omega(C^{*})>\omega(T)\geqslant \omega(T^{*})$.
>On triche un peu et on suppose qu'on peut passer deux fois par chaque arête.
>On calcule $T^{*}$, un MST (minimum spanning tree).
>On parcourt les arêtes de $T^{*}$ deux fois. On obtient un chemin ferme $U$. avec $\omega(U)=2\omega(T^{*})$.
>On construit une tournée à partir du chemin $U$  en ignorant les sommets déjà bus sen prenant le raccourcis direct. On obtient un cycle hamiltonien $C$.
>Par inégalité triangulaire, le coup $\omega(C) \leqslant \omega(U)$.
>On a trouvé une solution $C$, $2\omega(C) \geqslant 2 \omega(T^{*})\geqslant \omega(U)\geqslant \omega(C)\geqslant \omega(C^{*})> 0$.
>Donc $2\omega(C^{*})\geqslant \omega(C)$. 
>On a construit un 2-approximation.

>[!info] Remarque
>Il existe une $\frac {3} {2}$-approximation.

>[!info] Remarque
>Impossibilité d'une $\alpha$-approximation pour le problème du voyageur de commerce général (si P $\neq$ NP).

>[!note] Démo
>Procédons par l'absurde. Supposons disposer d'une $\alpha$-approximation $A$ avec $\alpha \geqslant 1$. Pour le voyageur de commerce (cas complet et poids dans $\mathbb{N}^{*}$ sinon trivial). 
>Construisons alors un algorithme polynomial pour le problème du cycle hamiltonien. 
>Pour un graphe $G=(S,A)$, on associe le graphe $G'=(S,A',w)$ comme précédemment, mais au lieu de prendre $\omega(a)=2$ si $a \not \in A$, on pose $\omega(a)=\alpha |S|+1$.
>Cette construction est bien polynomiale.
>On applique notre algorithme $A$ d'approximation sur ce graphe $G'$ (temps polynomial).
>Si la tournée (a priori non optimale) renvoyée est $\omega(c)\leqslant |S|\alpha$ on renvoie vrai sinon faux.
>
>Considérons un cycle hamiltonien de $G'$:
>- S'il ne passe que par des arêtes de $G$ alors son poids est $|S|$.
>- S'il passe par un moins une arête qui n'est pas dans $G$, son poids est au moins $|S|+|S|\alpha = |S|(\alpha+1)$.
>
>S'il existe un cycle dans $G$, alors la solution optimale est de taille $|S|$. Donc la solution renvoyée par l'algorithme d'approximation est de taille au plus $\alpha |S|$. On renvoie vrai.
>Inversement, s'il n'existe pas de cycle hamiltonien de $G$, la solution apportée ne peut qu'être un cycle de poids au moins $|S|(\alpha+1) > |S| \alpha$ et on renvoie bien faux.

## III - Bilan

>[!note]
>Il existe plusieurs types de problèmes NP-complets issus d'un problème d'optimisation.
>- Ceux pour lesquels il n'existe pas du tout d'$\alpha$-approximation et ce pour tout $\alpha$. (si P $\neq$ NP)
>- Ceux pour lesquels il existe une $\alpha$-approximation avec une valeur minimale d'$\alpha$. (Exemple : seulement pour $\alpha \geqslant 2$, COUVERTURE) (si P $\neq$ NP)
>- Ceux pour lesquels pour tout $\varepsilon >0$, il existe une $(1+\varepsilon)$-approximation. (Exemple : sac-à-dos)
