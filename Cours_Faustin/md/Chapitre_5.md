# Chapitre 5 : Structure de données pour ensemble disjoints

```toc
```

## Introduction

>[!note]
>Dans de nombreuses application, on peut avoir besoin de gérer une **partition** d'un ensemble à $n$ éléments.

>[!info] Remarque
>En particulier, on peut ainsi représenter une relation d'équivalence. Deux éléments étant équivalant si et seulement si ils sont dans la même classe de partition.

>[!done] Objectif
>Dans ce chapitre, on s'intéresse à la conception d'une structure pour gérer efficacement une collection dynamique d'ensembles (finis) disjoints.

>[!tip] Définition
>$S = S_{1}\sqcup S_{2} \sqcup ... \sqcup S_{c}$ de $c$ classes.
>Les ensembles $S_i$ sont appelés les classes de $S$ (penser classes d'équivalences). 

>[!info] Remarque
>Le mot dynamique veut dire "qui va évoluer au fil du temps" par opposition à statique qui va veut dire fixé une fois pour toutes au début.

>[!example] Exemple : Représentation de graphes
> Par matrice d'adjacence au liste d'adjacences : pas du tout efficace pour un ensemble de sommets qui varie.
> $\rightarrow$ table de hachages de listes d'adjacences.

>[!example] Application à connaitre
>On cherche à maintenir les composantes connexes d'un graphe non orienté, mais avec des arêtes qui peuvent être ajoutées et / ou retirées.
>
>On peut chercher les composantes connexes par un parcours en $O(|S| + |A|)$ mais qu'il faudrait relancer à chaque fois.

## I - Unir et trouver

### A - Représentant

>[!note] 
>On peut représenter chaque classe par un **identifiant**, par exemple $i$ pour la classe $S_i$.
>On peut aussi choisir un **représentant** qui est un élément particulier de la classe.

>[!example] Exemple
>Dans $\mathbb{Z} / \mathbb{nZ}$ on représente la classe de $i \in [|0,n-1|]$ par $i$ (ou par $i + 3n$).
>Chaque classe est ainsi représentée par un unique représentant.

>[!info] Remarque
>Dans certaines applications, on peut imposer des propriétés particulières sur ce représentant (le plus petit par exemple).
>
>On demande en général que deux requêtes renvoient toujours le même représentant.

>[!note] Notation
>Si $x \in S$ on note $S_x$ la classe de $x$ et $\overline{x}$ le représentant de la classe de $S_x$.
>On a donc $S_{x}= S_{\overline{x}}$

>[!info] Remarque
>Pour déterminer si deux éléments $x,y \in S$ sont équivalents, c'est-à-dire appartenant à la même classe, il suffit de comparer leur représentants $\overline{x} = \overline{y}$ ?
>On s'intéresse donc à savoir **trouver** (efficacement) le représentant d'une classe ou d'une élément de cette classe : $x \mapsto \overline{x}$.

>[!tip] Définition
>La collection d'ensembles étant dynamique, on peut vouloir réaliser **l'union** ou la fusion de deux classes, par exemple réunir les classes des éléments $x$ et $y$ de $S$.
>On appelle cette opération : **unir** (pas union car mot clé réservé en C).
>
>Ceci explique le nom de cette structure de données : **unir et trouver** | **union-find**

### B - Représentation par des nœuds (ici en C)

>[!note] Implémentation
>Pour réaliser cette structure de données, on **peut** représenter chaque élément de $S$ par un objet que l'on va appeler nœud qui comporte cet élément plus éventuellement d'autres informations utilies à la structure.
>```C
>struct noeud {
>	int val; // ou autre type
>	... // plus tard
>};
>typedef struct noeud noeud; 
>```

>[!note] On souhaite obtenir les primitives suivantes : 
>- `noeud * creer(int x)` qui permet de créer un nœud correspondant à un singleton $\left\{x \right\}$
>Cela revient à ajouter une classe formée d'un élément à notre structure. Son représentant est $x$.
>- `noeud* trouver(noeud* x)` qui détermine le représentant $\overline{x}$ associée à un élément $x$.
>- `void unir(noeud*x, noeud* y)` réalise la fusion des classes de $S_x$ et $S_y$ c'est-à-dire $S_{x}\cup S_{y}$.
>C'est-à-dire, si $\mathcal{P}$ est la partition de $S$. $\mathcal{P} \leftarrow \mathcal{P} \backslash \left\{S_{x},S_{y}\right\} \cup \left\{S_{x}\cup S_{y}\right\}$.
>Les deux ensembles $S_x$ et $S_{y}$ sont supposés disjoints ou égaux avant l'opération.

>[!info] Remarque
>Après opération $S_{x}= S_y$

>[!info] Remarque
>En général, le représentant de `unir(x,y)` sera $\overline{x}$ ou $\overline{y}$ (mais ce n'est pas obligatoire).

>[!info] Remarque
>$unir(x,y) = unir(\overline{x}, \overline{y})$.
>Lors de la réunion, on peut prendre n'importe quel élément des classes.
>`unir(x,y) = unir(trouver(x),trouver(y))`

>[!info] Remarque
>On peut toujours supposer que l'on a **commencé** par les $n$ appels à créer.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!note]
>On note $n$ le nombre d'éléments de $S$ et donc ce qui revient ou même le nombre d'appels à créer. On note $m$ le nombre total d'appels à créer, trouver et unir.

>[!info] Remarque
>On a $n\leq m$.

>[!info] Remarque
>Au plus $n$ appels à unir réalisant quelque chose.

### C - Représentation par une structure

>[!note]
>Sans faire de généralité, on suppose que $S = \left\{0,...,n-1 \right\}$ (sinon on numérote les éléments et on utilise une table d'association).
>
>On représente l'information dans une structure à part.
>On suppose ici que l'on commence par créer les $n$ singletons.

>[!example] Exemple OCaml
>```OCaml
>type partition
>val creer : int -> partition
>(* créé la partition {{0},{1},...,{n}} constitué de n singleton *)
>val trouver : partition -> int -> int
>(* trouve le représentant d'un élément *)
>val unir : partition -> int -> int -> unit
>```

>[!info] Remarque
>On peut faire en C ce que l'on peut faire en OCaml et vice versa.

## II - Représentation naïve

>[!note] Implémentation
>On implémente `partition` par un tableau tel que $cl(x)$ contient la représentation de $x$ qui est **toujours** le plus petit représentant de la classe.
>```OCaml
>type partition = int array
>let creer n = Array.init n (fun i -> i)
>(* Complexité : O(n) *)
>let trouver p i = p.(i)
>let unir p x y = 
>	let rx = trouver p x in
> 	let ry = trouver p y in
> 	let substitue t a b = 
> 		for i = 0 to Array.length t -1 do
> 			if t.(i) = a then t.(i) = b
> 		done;
> 	in substitue p (max rx ry) (min rx ry)
>(* Complexité : O(n) *)
>```

>[!info] Remarque
>On peut commencer la boucle `for` à $a$.
>`for i = a to Array.length t -1` car l'identifiant est toujours plus petit.

>[!info] Remarque
>`Trouver` est très efficace mais `unir` ne l'est pas.

>[!bug] Exercice
>Quelle peut être la compléxité d'une séquence de $n-1$ `union` dans le pire cas. $\rightarrow O(n^2)$

## III - Représentation par une forêt

>[!note] Représentation
>On peut organiser les nœuds de la manière suivantes : chaque nœud pointe vers un autre nœud $n$ de sa classe. Le représentant pointe vers lui-même (et c'est le seul). 
>On s'interdit les cycles. Ceci forme bien une **partition**. On a bien le représentant d'une classe en remontant les pointeurs.

>[!info] Remarque
>On peut aussi faire pointer les représentations vers `NULL`.

>[!info] Remarque
>On obtient une forêt (graphe acyclique, réunion de graphes connexes acyclique $\rightarrow$ arbres).

>[!info] Remarque
>Ici on ne représente que les pointeurs **parents**.

>[!info] Implémentation en C
>```C
>struct noeud {
>	int val;
>	struct noeud* parent:	
>};
>typedef struct noeud noeud:
>
>noeud* creer(int val) {
>	noeud* n = malloc(sizeof(noeud));
>	n->val = val;
>	n->parent = n;
>	return n;
>}
>
>noeud* trouver(noeud x){
>	while(x->parent != x) {
>		x = x->parent;
>	}
>	return x;
>}
>
>void unir(noeud* x, noeud* y) {
>	noeud rx = trouver(x);
>	noeud ry = trouver(y);
>	rx->parent = y;
>}
>```

>[!note]
>La création d'un nœud est en $O(1)$. `trouver` est linéaire en la profondeur du nœud dans l'arbre, dans le pire cas c'est en $O(n)$. `unir` est en $O(1)$ plus l'appel à trouver donc en $O(n)$ dans le pire cas.

>[!bug] Exercice
>Trouver une suite d'opération qui réalise ce pire cas.

>[!note]
>On peut aussi faire :
>```C
>void unir(noeud* x, noeud* y) {
>	noeud* rx = trouver(x);
>	noeud* ry = trouver(y);
>	rx->parent = ry;
>}
>```
>qui a priori réduit la hauteur des arbres.

>[!info] Implémentation OCaml
>```OCaml
>type partition = int array
>let creer n = Array.init n (fun i -> i)
>let rec trouver p x = 
>	if p.(x) = x then x
>	else trouver p p.(x)
>let unir p x y = 
>	p.(trouver p x) <- trouver p y
>```

>[!info] Remarque 
>Et si `rx = ry` ? Pas valable tout le temps.

>[!note] 
>On va introduire deux heuristiques qui vont changer radicalement les choses.
>


## IV - Union par rang

>[!note]
>Dans `unir`, il vaut mieux faire pointer la racine de l'arbre le moins haut vers la racine de l'arbre le plus haut.

>[!tip] Définition
>Il n'est pas très difficile de garantir une forme d'équilibre des arbres pour obtenir **trouver** et **unir** en $O(log n)$.
>Il faut veiller, lors de l'union à choisir comme racine celle de l'arbre le plus haut. Ainsi la hauteur des arbres ne peut augmenter que lors de l'union de deux arbres de même hauteur.
>Bien sûr, il est hors de question de recalculer la hauteur à chaque fois. On la stock dans les nœuds.
>On va appeler cela le **rang** qui sera toujours un **majorant** de la hauteur.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Implémentation
>```C
>struct noeud {
>	int val;
>	int rang;
>	struct noeud* parent;
>};
>typedef struct noeud noeud;
>
>noeud* creer(int val) {
>	noeud* n = malloc(sizeof(noeud));
>	n->val = val;
>	n->parent = n;
>	n->rang = 0;
>	return n;
>}
>
>// trouver est identique
>
>void unir(noeud* x, noeud* y) {
>	noeud* rx = trouver(x);
>	noeud* ry = trouver(y);
>	if (rx = ry) {return ;}
>	if (rx->rang > ry->rang) {
>		ry->parent = rx;
>	} else {
>		rx->parent = ry;
>		if (rx->rang == ry->rang) {
>			ry->rang ++;
>		}
>	}
>}
>```

## V - Compression de chemins

```C
noeud* trouver(noeud* x) {
	if (x->parent != x) {
		x->parent = trouver(x->parent);
	}
	return x->parent;
}
```

>[!bug] Exercice
>Refaire ces deux heuristiques en OCaml.

>[!info] Remarque 
>Evidemment on les combine.

## VI - Analyse de la complexité

>[!note]
>On a vu que sans **heuristique** (: guide, piste pour orienter la recherche) :
>- union par rang
>- compression de chemin
>
>La complexité de `unir` et `trouver` est en $O(n)$.

>[!tip] Propriété
>En ajoutant l'heuristique d'union par rang (et éventuellement la compression de chemin)
>Si $x$ n'est pas un représentant alors rang($x$) $<$ rang(père($x$))

>[!note] Démo
>Soit $y$ le père de $x$. Au moment où $y$ devient le père de $x$, c'est nécessairement dans `unir`.
>Il a deux cas : 
>- soit rang($x$) $<$ rang($y$)
>- soit rang($x$) $=$ rang($y$)
>dans ce cas on incrémente le rang de $y$.
>
>Dans les deux cas, on a rang($x$) $<$ rang($y$) après l'appel à `unir`.
>De plus, le rang de $x$ est alors fixé par toujours (le rang d'un non représentant ne peut pas changer et un non représentant ne peut pas le devenir).
>D'un autre côté, le rang($y$) ne peut pas croître.
>Donc on aura toujours rang($x$) $<$ rang(père($x$)).

>[!info] Conséquence
>Les rangs sont strictement décroissants sur une branche quelconque.

>[!info] Conséquence
>Le rang d'un ancêtre **propre** de $x$ est strictement plus grand que le rang de $x$.

>[!info] Conséquence
>La propriété reste vraie même avec compression de chemin. Dans ce cas, on peut associer à un nœud $x$ un nouveau père qui est un ancêtre de $x$ donc de rang strictement supérieur.

>[!tip] Propriété
>Un nœud $x$ de rang $K$ est racine d'un arbre de hauteur au plus $K$. Autrement dit, le rang majore la hauteur.

>[!note] Démo
>Les nœuds sur un chemin de $x$ à une feuille sont de rangs dans $[|0,K|]$ tous différents puisque le rang de $x$ est $K$, le rang d'une feuille est positif et strictement décroissant. Il y a donc au plus $K+1$ sommets sur ce chemin qui est donc de longueur au plus $K$.

>[!tip] Propriété
>Un nœud de rang $K$ est racine d'un arbre comportant au moins $2^K$ nœuds.

>[!note] Démo
>On procède par récurrence sur $K \in \mathbb{N}$.
>***Initialisation*** : si $K=0$, un nœud de rang 0 est racine d'un arbre qui le contient au moins (et même exactement) lui même et donc de taille au moins 1.
>***Hérédité*** : On suppose la propriété vraie au rang $K \in \mathbb{N}$, Soit $x$ de rang $K+1$. 
>Comme $x$ est de rang $K+1$, il est devenu père d'un nœud $y$ dans un appel à `unir` avec, au moment de cette liaison $x$ de rang $K$ et $y$ de rang $K$ (seul moyen pour avoir un increment de rang).
>Par hypothèse de récurrence, $x$ et $y$ sont racines d'arbres comportant au moins $2^K$ nœuds et donc $x$ comporte $2^{K+1}$ nœuds au moins.
>Même en cas de compression de chemin comme $x$ est représentant, tous ses représentants restent ses descendants.
>Il est possible d'avoir d'autres appels à `unir` avec $x$ comme racine, qui ne peut qu'augmenter le nombre de nœuds de l'arbre.

>[!tip] Propriété
>Il y a au plus $\frac {n} {2^K}$ nœuds représentants de rang $K$.

>[!note] Démo
>Notons $\left\{x_{1},...,x_{p}\right\}$ les représentants de rang $K$ et $N_{x_i}$ les nœuds de l'arbre de racine $x_i$. 
>On a donc $\forall i \in [|1,p|]$, $|N_{x_{i}}| \geq 2^K$
>De plus les $N_{x_i}$ sont deux à deux disjoints car un nœud ne peut pas posséder qu'un seul ancêtre de rang $K$ (les rangs sont strictement décroissant sur une branche).
>$\sum\limits^{p}_{K=1}2^{K}\leq \sum\limits^{p}_{i=1}|N_{x_{i}|}\leq n$ donc $p \leq \frac {n} {2^K}$.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Conséquence
>Il y a au plus 0 nœud de rang supérieur à $\lfloor log_{2}n\rfloor + 1$

>[!info] Remarque
>On vient de voir que tous les arbres de la forêt ont une hauteur majorée par $\lfloor log_{2}n\rfloor$. Il sont équilibrés. On a donc `trouver` et `unir` en $O(log(n))$.

>[!tip] Proposition (admis)
>En utilisant les deux heuristiques la complexité est en réalité exellente.

>[!info] Remarque
>Une série de $m$ opérations dont $n$ appels à créer est en complexité $O(m \alpha(n))$ où $\alpha$ est une forme de réciproque de la fonction d'Ackermann. On a $\alpha(n) \leq 4, \forall n \in [|0,10^{100}|]$.
Autrement dit, la complexité amortie de cette structure est on $O(\alpha(n))$ (en gros $\theta(1)$).
