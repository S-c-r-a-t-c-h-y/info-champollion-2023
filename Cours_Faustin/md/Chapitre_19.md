# Chapitre 19 : Séparation et évaluation

```toc
```

>[!note]
>La technique de "séparation et évaluation" ou "Branch and Bound" est une technique qui permet d'accélérer la recherche exhaustive (brut force) (par élagage d'une partie de l'espace de recherche) lors de l'application d'une stratégie de retour sur trace (backtracking) pour la résolution d'un **problème d'optimisation**.

>[!tip] Définition
>**Backtracking** (pour des problèmes de recherche/décision) : structurer l'espace de recherche pour une recherche exhaustive avec élagage (décomposition du problème en solutions partielles que l'on complète peu à peu avec arrêt si impossible).

>[!tip] Définition
>**Branch and bound** : même chose mais pour des problèmes d'optimisation.

## I - Problèmes d'optimisation

>[!note]
>On considère un problème d'optimisation $P$ pour lequel une instance $x \in X$. On cherche à minimiser une quantité $f(x,y)$ pour les solutions $y \in Y(x)$ possibles à l'instance $x$. On cherche $\underset {y \in Y(x)} {\text{min}} f(x,y)$.

>[!info] Remarque
>Le cas de maximisation se traite de manière symétrique.

>[!example] Exemple 1
>MAXSAT est un problème d'optimisation reliée à SAT où l'on cherche à maximiser le nombre de clauses satisfaites.
>On va plutôt considérer MINUNSAT.
>Etant donné une formule $x$ sous forme normale conjonctive, on cherche une valuation $y$ sur les variables de $x$ qui ne satisfait pas un nombre minimal de clauses.
>$f(x,y)$ le nombre de clauses de $x$ non satisfaites par y.
>$\varphi = (x_{1}\vee x_{2}\vee x_{3})\wedge (x_{1}\vee \lnot x_{3}\vee x_{4}) \wedge (x_{1}\vee \lnot x_{4})\wedge (\lnot x_{1}\vee x_{2}\vee x_{3})\wedge (\lnot x_{1}\vee \lnot x_{2}) \wedge (\lnot x_{1}\vee \lnot x_{3}) \wedge (x_{2}\vee \lnot x_{3})$
>$\wedge (\lnot x_{2}\vee x_3)$

>[!info] Remarque
>Si on sait résoudre efficacement MAXSAT, on sait résoudre efficacement CNF-SAT (et SAT).

>[!example] Exemple 2
>Dans le problème du voyageur du commerce à un graphe $G=(S,A,w)$ non orienté pondéré (souvent complet et à poids strictement positifs). On cherche un cycle hamiltonien de poids minimal dans $G$.
>$f(x,y) = w(y)$

>[!info] Remarque
>Si on sait résoudre efficacement le voyageur de commerce, on sait résoudre le problème du cycle hamiltonien.
>

## II - Séparation

>[!tip] Définition 
>La première étape de l'approche par backtracking par branch and bound est la **séparation** de l'espace de recherche (branch). Elle consiste à donner à l'espace des solutions une **structure** arborescente adaptée. On cherche à séparer récursivement (branch) l'espace des solutions en plusieurs sous-espaces.
>On obtient alors des solutions partielles qu'il reste à compléter.

>[!example] Exemple 1
>Pour SAT et MAXSAT, l'ensemble des solutions est l'ensemble des valuations à $n$ variables (celle de $x$). On peut représenter cet ensemble sous la forme d'un arbre binaire en associant à chaque nœud une variable et en **séparant** les solutions suivant que cette variable vaut $V$ ou $F$. La valuation initiale est $\varnothing$. A chaque étape de l'arbre correspond **une valuation partielle** et le sous arbre enraciné en ce nœud correspond à toutes les manières de le prolonger.

>[!bug] Exercice
>Dessiner l'arbre complet correspondant à $\varphi$, évaluer les feuilles et donner une optimale pour MAXSAT.

>[!example] Exemple 2
>Pour le voyageur de commerce, pour un graphe $G=(S,A,w)$, une solution partielle est un chemin élémentaire de $a \in S$ à $b \in S$ passant par les sommets $X \subseteq S$. On note alors $a \overset {\alpha} {\rightarrow} b$. On cherche alors à compléter ce chemin par un voisin $x$ de $b$ qui n'est pas dans $X$ (sauf si $X=S$ dans ce cas, on cherche à revenir sur $a$).
>La racine est $a \overset {\{a\}} {\rightarrow} a$ pour un sommet $a \in S$ arbitraire.
>
>![[chap19_img1.jpg]]
>
>Sans élagage, on effectue un **parcours en profondeur** de l'arborescence. Il s'agit d'une recherche exhaustive.

## III - Retour sur trace

>[!tip] Définition
>L'idée est d'améliorer la recherche exhaustive en **élaguant** de grandes parties de l'espace de recherche (99.999 %, mais attention, la complexité reste exponentielle).
>Si pour un nœud, on est capable de déterminer qu'il est désormais impossible de compléter la solution partielle en une solution totale. On peut s'arrêter là (élagage).
>
>Pour un problème d'optimisation, on va faire la même chose, mais il faut être capable de déterminer si pour un nœud, il n'y a plus d'espoir de trouver une meilleure solution qu'une déjà trouvée.

## IV - Evaluation

>[!note]
>On peut souvent déjà calculer une évaluation partielle pour une solution partielle $\hat{f}(x,\hat{y})$.

>[!example] Exemple
> MAXSAT : pour une solution partielle $\hat{y}$, les clauses déjà entièrement non satisfaites par $\hat{y}$ si elles ne seront pas non plus un prolongement de $\hat{y}$.
> ![[chap19_img2.jpg]]

>[!example]
>Pour le voyageur de commerce, une solution partielle est le poids de la solution partielle.

>[!tip] Définition 
>On suppose donc disposer d'une **heuristique** $h$ qui évalue pour une solution partielle $\hat{y}$ le coût $h(\hat{y})$ qui cherche à approcher le coût minimal de toute complétion de $\hat{y}$ en une solution totale $y$ (ou $+\infty$ si aucune solution n'est possible). ($h(\hat{y}) \approx f(x,y)$ pour le meilleur $y$ possible)
>Pour **élaguer sans erreurs** une partie de l'espace de recherche, il est nécessaire que cette heuristique sous-estime ce coût. Une telle heuristique est dite **admissible**. Une telle heuristique est admissible si pour tout $x \in Y$ et pour toute solution partielle $\hat{y}$ pour $x$ et pour toute solution totale $y$ de $x$ qui prolonge $\hat{y}$ en $h(\hat{y}) \leqslant f(x,y)$.

>[!example] Exemple
>MAXSAT : Pour une valuation partielle $\hat{\nu}$ pour une formule $\varphi$, on peut prendre comme heuristique : 
>- le nombre de clauses déjà non satisfaite par $\hat{\nu}$ 

>[!info] Remarque
>Deux clauses $c_{1},c_{2}$ comportant un littéral $x$ pour l'un et $\lnot x$ pour l'autre, le reste des littéraux étant déjà déterminés par $\hat{\nu}$ et ces clauses n'étant pas déjà satisfaites. Alors, tout prolongement de $\hat{\nu}$ aura une des deux clauses non satisfaites.

>[!example] Exemple
>Voyageur de commerce : Quelle heuristique pour une solution partielle $a \xrightarrow{\alpha} b$ : 
>- le poids de $w(a \rightarrow b)$
>- le poids d'un arbre couvrant de poids minimal de $S\backslash X$
>- le min des arêtes de $a$ à $S \backslash X$
>- le min des arêtes de $h$ à $S \backslash X$
>
>$\Rightarrow$ **minore** le coût de toute solution qui prolonge $a \xrightarrow{\alpha} b$

## V - Elagage

>[!tip] Définition
>L'**élagage** consiste à couper des branches lors de l'exploration lorsqu'on est certain que le nœud ne peut pas donner une meilleure solution que celle déjà obtenue.
>La structure de l'algorithme est donc la suivante : 
>- Explorer l'ensemble des solutions, structuré sous la forme d'un arbre.
>- Mémoriser la valeur de la meilleure solution.
>- Pour un nœud, calculer une borne minimale (heuristique) de toute complétion de la solution partielle. Si on trouve plus que la solution mémorisée, on élague.
	
```pseudo
	\begin{algorithm}
	\caption{Elagage}
	\begin{algorithmic}
	\Input Une instance $x$ de $P$
	\Output La valeur exacte d'une solution minimale.
	\State $f_{min} \leftarrow + \infty$
	\State $y_{min} \leftarrow \varnothing$
	\State $BnB(\hat{y})$ 
	\Comment{compléter la solution partielle de $y$}
	\If{ $\hat{y}$ est totale}
		\If{$f(x,y) < f_{min}$}
			\State $f_{min}$ $\leftarrow f(x,\hat{y})$
			\State $y_{min}$ $\leftarrow y$
		\EndIf
	\Elif{$h(\hat{y}) <$ $f_{min}$}
		\For{chaque manière z de compléter $\hat{y}$ en une étape}
			\State $BnB(\hat{y}z)$
		\EndFor
	\EndIf
	\State $BnB(\varnothing)$
	\Return $y_{min}$
 	\end{algorithmic}
	\end{algorithm}
```

>[!info] Remarque
>Au lieu d'initialiser $fmin$ à $+\infty$, on peut prendre une solution approchée (algorithme glouton, approximation, algorithme probabilisé, etc ...)
>

>[!info] Remarque
>Il convient souvent de bien choisir l'ordre des branches

>[!example] Exemple
>![[chap19_img3.jpg]]

>[!bug] Exercice
>Appliquer BnB avec l'heuristique du cours et l'ordre lexicographique si ambiguïté.

>[!bug] Exercice
>De même avec MAXSAT, la formule $\varphi$ du cours et l'heuristique du cours.
