# Chapitre 8 : Arbres couvrants

```toc
```

>[!note]
>Dans ce chapitre, on ne considère que des graphes non orientés, connexes et sans boucles.

>[!tip] Définition
>Un graphe **pondéré** est un triplet $G=(S,A,p)$ avec $S,A$ un graphe et $p : A \longrightarrow \mathbb{R} / \mathbb{N} / \mathbb{Z}/ \mathbb{C}$ de pondération des **arêtes**.
>Si $a \in A, p(a)$ est le poids de l'arête $a$.

## I - Arbres couvrants

>[!todo] Rappel
>Un **arbre** est un graphe connexe acyclique.

>[!tip] Propriété
>Soit $G=(S,A)$ un graphe (non orienté). Alors :
>- Si $G$ est connexe, $|A| \geq |S|-1$.
>- Si $G$ est acyclique, $|A| \leq |S| -1$.

>[!info] Remarque
>Les arbres correspondent à la rencontre entre ces deux mondes et on a $|A| = |S|-1$.

>[!tip] Propriété
>Si $G=(S,A)$ est un graphe non orienté alors les propositions suivantes sont équivalentes :
>- $G$ est un arbre ($G$ est connexe acyclique).
>- $G$ est connexe et $|A| \geq |S| -1$.
>- $G$ est acyclique et $|A| \leq |S| -1$.

>[!info] Remarque
>C'est un très bon exercice de savoir redémontrer ces deux propriétés.

>[!tip] Définition
>Un arbre couvrant d'un graphe $G=(S,A)$ (pondéré ou non) est un sous-graphe $T=(S,A')$ avec $A' \subseteq A$ qui est un arbre.
>C'est un sous-graphe connexe acyclique qui possède tous les sommets. Un arbre **couvrant** a les mêmes sommets.

>[!example] Exemple
>![[chap8_img1.jpg]]
>Le graphe en vert est un graphe couvrant le graphe.

>[!info] Remarque
>Un arbre couvrant d'un graphe $S=(S,A)$ correspond à la donnée d'un ensemble d'arêtes $A' \subseteq A$. On a alors tout sommet de $S$ est incident à une arête de $A'$. On dit que $A'$ couvre les sommets du graphe.

>[!todo] Rappel
>$(x,y)$ est une arête incidente à $x$ et $y$ qui lui sont incident.

>[!info] Remarque
>Un arbre couvrant est un sous-graphe connexe minimal qui couvre tout les sommets.

>[!info] Remarque
>Les arbres couvrants ont de nombreuses applications : constructions de réseaux électriques, téléphoniques, etc ...
>
>Les sommets correspondent à des villes et le poids arêtes correspondent au coût de constructions d'une ligne entre ceux villes. On veut tout interconnecter au plus bas prix.

>[!tip] Propriété
>Un graphe (non orienté non nécessairement connexe) est connexe si et seulement s'il possède au moins un arbre couvrant.

>[!note] Démo
>- Si le graphe $G=(S,A)$ possède un arbre couvrant $T=(S,A')$ alors tous sommets $x,y \in S$ sont reliés dans $T$ donc dans $G$. Donc $G$ est connexe.
>- L'arborescence de parcours d'un graphe connexe est un arbre couvrant de ce graphe.

>[!info] Remarque
>Si le graphe n'est pas connexe. On travaille sur les composantes connexes. On peut parles de forêt couvrante.
>Ici on va supposer les graphes connexes.

>[!tip] Définition
>Soit $G=(S,A,p)$ un graphe (non orienté) pondéré connexe avec $T=(S,A')$ un arbre couvrant de $G$.
>Le poids de $T$, noté $p(T)$ est la quantité $p(T)= \sum\limits_{a \in A} p(a)$.

>[!example] Exemple
![[chap8_img2.png]]

>[!tip] Définition
>Un arbre couvrant de poids minimal d'un graphe $G$ est un arbre couvrant de $G$ dont le poids est le plus petit ou égal au poids de tout autre arbre couvrant de $G$.

>[!example] Exemple
><span style="color: #26B260"> T </span> et <span style="color:#FF0000"> T </span> ne sont pas des arbres couvrant minimaux (au poids minimal).
>>[!question] Est ce que <span style="color: #FF00AA"> T </span> l'est ?

>[!info] Remarque
>Il n'y a pas unicité a priori de l'arbre couvrant de poids minimal. (cf TD : c'est le cas si toutes les arêtes ont un poids différents).

>[!info] Remarque
>Ici encore les applications sont nombreuses.

>[!question] Mais comment faire pour trouver un algorithme qui trouve un arbre couvrant de poids minimal ?

## II - Algorithme de KRUSKAL

>[!note] 
>L'algorithme de Kruskal est un algorithme **glouton** qui permet de résoudre ce problème.

>[!todo] Rappel
>Intuitivement un algorithme glouton effectue à chaque étape le choix optimal **à cette** étape, sans remettre en question ses choix passés.

>[!info] Remarque
>Un algorithme glouton doit **toujours** être accompagné d'une preuve de sa correction (si on veut l'utiliser pour trouver une solution optimal; sinon il s'agit le plus souvent d'un algorithme d'**approximation**).

>[!info] Idée
>On crée un arbre $T$ initialement vide.
>A chaque étape prendre la plus petite arête disponible admissible (c'est-à-dire qui ne crée pas de cycle dans $T$).
>Autrement dit : 
>Ajouter, si possible, toutes les arêtes dans l'ordre de poids croissant.

>[!tip] Algorithme de Kruskal
>```pseudo
>    \begin{algorithm}
 >   \caption{Algorithme de Kruskal}
>  \begin{algorithmic}
> 	 \Function{Kruskal}{$G=(S,A,p)$}
> 		 \State Trier $A$ selon $p$
> 		 \State $T \leftarrow (S, \emptyset)$
> 		\State $c$ = creer-partition($|S|$)
> 		\For{chaque $(x,y) \in A$ trié par ordre de poids croissant}
> 			\If{trouver($x$)$\neq$ trouver($y$)}
> 				\State ajouter($(x,y),T$)
> 				\State unir($x,y$)
> 			\EndIf
> 		\EndFor
> 		\State Renvoyer T
>  \EndFunction
>  \end{algorithmic}
>  \end{algorithm}
>```

>[!info] Remarque
>On peut commencer par trier la liste. Ceci permet ensuite simplement de considérer les arêtes par poids croissant.
>
>Comment savoir si une arête est admissible, c'est-à-dire si elle crée un cycle dans le graphe dans $T$.
>On peut faire un parcours en profondeur pour détecter si le graphe devient acyclique.
>$\rightarrow$ Ok mais coûteux.

>[!tip] Proposition
>L'algorithme de Kruskal construit un arbre couvrant.
>*(de poids minimal $\rightarrow$ plus tard)*

>[!note] Démo
>Au départ $T=(S,\emptyset)$.
>***Invariant*** :
>$T = (S,A')$ est un sous graphe acyclique.
>Bien ***conservé*** car on n'ajoute une arête seulement si elle ne crée pas de cycle.
>***Après*** : $|A|$ ajouts, comme le graphe est connexe il y a eu au moins $|S|-1$ ajouts.
>Donc $T$ est connexe car acyclique avec au moins $|S|-1$ ajouts.
>

>[!info] Remarque
>On peut s'arrêter après $|S|-1$ ajouts.

>[!note]
>Pour savoir efficacement que la prochaine arête candidate est admissible, il suffit de vérifier que les deux sommets qu'elle ne relie ne sont pas dans une même composante connexe de $T$ (le sous-graphe acyclique en construction).
>
>L'ensemble des composantes connexe de $T$ est une partition des sommets de $T$, c'est une **collection d'ensemble disjoints dynamique**. 
>On peut également utiliser l'algorithme **unir et trouver**. 
>Pour savoir s'il faut ajouter une arête $x,y \in A$, on vérifie que `trouver(x)`$\neq$`trouver(y)`.
>Si c'est le cas, on ajoute à $T$ et on fusionne les composantes connexe avec `unir(x,y)`. 

>[!info] Remarque
>On peut faire un arrêt anticipé si on a fait $|S|-1$ ajouts.
>Dans ce cas, la partition comporte une unique composante.

>[!info] Remarque
>On peut aussi utiliser une file de priorité pour gérer l'ensemble $A$ et extraire à chaque étape l'arête minimale restante.

>[!info] Remarque
>Il peut arriver aussi que les arêtes sont déjà triées.

>[!todo] Cf feuille

>[!info] Analyse de la complexité
>Le trie des arêtes se font en $O(|A|log(|A|)) = O(|A|log(|S|))$ avec un tri semi-linéaire (tri par tas, tri fusion, tri rapide (en moyenne)).
>Il y a ensuite : 
>- |A| étapes pour la boucle $\rightarrow O(|A|)$
>- Un appel à `creer_parition` $\rightarrow O(|S|)$
>- Au plus $2|A|$ appel à `trouver`. $\rightarrow O(2|A|log(|S))$
>- Exactement $|S|-1$ appels à unir $\rightarrow O(|S|log(|S|))$
>  
>**Complexité totale** : $O(|A|log(|S|))$
>
>En réalité, `unir` et `trouver` sont en $O(log^*(|S|))$ en amorti.
>On a donc une complexité plutôt de l'ordre de $O(|A|log(|S|)) + O(|A|)$
>
>La complexité est ici dominé par le tri : 
>- Si les arêtes sont déjà triés $O(|A|)$.
>- Avec une tri rapide en $O(|A|)$ on a aussi une complexité en $O(|A|)$.

>[!info] Tri radix
>On trie par chiffre des puissances croissantes avec un tri **stable** (qui ne change pas l'ordre relatif de ce qui est fait avant).

>[!info] Remarque
>Soit $T = (S,B)$ un arbre couvrant d'un graphe $G=(S,A)$ et $a \in A \backslash B$ alors $F=(S,B \cup \left\{a\right\})$ (que l'on note $T+a$) comporte un cycle. Enlever une arête $b$ de ce cycle donne arbre couvrant $T-a+b = (S,B\backslash \left\{b\right\}\cup \left\{a\right\})$.

>[!info] Correction algorithme de Kruskal : **Idée**
>Invariant : à chaque étape le sous-graphe en cours de construction est un sous-graphe d'un arbre couvrant de poids minimal.
>
>Il est toujours possible de completer le sous-graphe en cours de construction en un arbre couvrant de poids minimal.

>[!info] Remarque
>L'algorithme glouton ne fait jamais de choix aux conséquences irrémédiables.

>[!tip] Proposition
>L'algorithme de Kruskal appliqué à un graphe non orienté connexe $G  = (S,A,p)$ renvoie un arbre couvrant $T=(S,B)$ de poids minimal.

>[!note] Démo
>Montrons tout d'abord que $T$ est bien un arbre couvrant.
>On a $T=(S,B)$ avec $B\subseteq A$ donc les sommets de $T$ sont $S$. On a pour invariant que le sous-graphe en cours de construction est acyclique et est de la forme $S,B'$ avec $B' \subseteq A$. 
>Si on s'arrête en atteignant $|S|-1$ étapes, on a un sous-graphe acyclique à $|S|-1$ arêtes qui est donc connexe, il s'agit d'un arbre.
>
>Montrons que ceci est toujours le cas.
>Supposons que l'on a considérer toutes les arêtes et que le sous-graphe obtenu n'est pas connexe. Il comporte au moins deux composantes connexes. Comme $G$ est connexe et ne comporte qu'une seule composante connexe, deux de composantes connexes de $T$ sont reliées dans $G$ par arête $\left\{x,y\right\} \in A \backslash B$. 
>Or, cette arête $\left\{x,y\right\}$ a été envisagé dans l'algorithme de Kruskal. A ce moment, elle ne reliait pas les composantes connexe qui contenait $x$ et $y$, sinon ce serait toujours le cas. Elle a donc due être ajoutée. Absurde
>
>Montrons maintenant que la propriété suivante est un invariant en notant $\underline{T} = (S,\underline{B})$ le sous-graphe en cours de construction : "$\underline{T}=(S,\underline{B})$ est un sous-graphe d'un arbre couvrant $T^*=(S,B^*)$ de poids minimal de $G$".
>***Initialisation*** : 
>Avant le premier tour de boucle, on a $T=(S, \emptyset)$. Il existe un arbre couvrant $T^*$ de poids minimal (car l'ensemble des arbres couvrants est fini non vide) et $\underline{T}$ est bien un sous-graphe de $T^*$.
>***Hérédité*** :
>Supposons la propriété vraie au début d'un tour de boucle. On a donc $\underline{T}=(S,\underline{B})$ sous graphe d'un arbre couvrant$\underline{T^*}=(S,\underline{B^*})$ de poids minimal de $G$.
>Dans un passage dans la boucle, distinguons 3 cas :
>- L'arête $\left\{x,y\right\}$ n'est pas ajouté à $I$, on a $\overline{T}=\underline{T}$ et la propriété reste vraie avec $\overline{T^*}=\underline{T^*}$
>- L'arête $\left\{x,y\right\} \in B^*$, dans ce cas $\overline{T}=(S,\underline{B} \cup \left\{x,y\right\})$. On a encore $\overline{T}$ sous-graphe de $\overline{T^*}=\underline{T^*}$ arbre couvrant de poids minimal.
>- L'arête $\left\{x,y\right\} \in A\backslash B^*$. On $\overline{T}=(S,\underline{B} \cup \left\{x,y\right\})$ pas sous-graphe de $\overline{T^*}$. Le sous-graphe $T^{*}+\left\{x,y\right\}$ comporte un cycle (contenant $\left\{x,y\right\}$). Comme $x$ et $y$ ne sont pas dans une même composante connexe de $T$, le chemin de $x$ à $y$ dans $\underline{T^*}$ qui utilise le reste du cycle, ne peut pas être en entier dans $\underline{T}$. Donc il existe une arête $a \in \underline{B^{*}} \backslash \underline{B}$ sur ce cycle. On ne peut pas encore avoir rencontré $a$, sinon on aurait ajouté $a$. En effet, comme $\underline{T}$ est sous-graphe de $T^*$, il ne peut pas y avoir d'autre chemin reliant dans $\underline{T}$ les extrémités de $a$ (sinon cela  crée un cycle dans $\underline{T^*}$).
>	Comme on considère les arêtes dans l'ordre croissant, on a donc $p(\left\{x,y\right\})\leq p(a)$.
>	On considère $\overline{T^{*}}= \underline{T^*}-a+\left\{x,y\right\}$ qui est encore un arbre couvrant.
>	$p(\overline{T^*})=p({\underline{T^{*}})-p(a)+p(\left\{x,y\right\})\leq}p(\underline{T^*})$.
>	Donc $\overline{T^*}$ est aussi de poids minimal (et $p(\overline{T^*})-p(\underline{T^*})$).
>
>L'invariant est conservée $\overline{T}$ est un sous-graphe de $\overline{T^*}$ qui est un arbre couvrant de poids minimal.
>
>***Correction*** :
>A la fin de la boucle $T=(S,B)$ est un arbre couvrant, sous-graphe d'un arbre couvrant de poids minimal, donc égal à cet arbre de poids minimal. 


>[!warning]
>```SHELL
>gcc -g -Wall -Wextra -fsanitize=address,undefined -o nom.exe nom.c
>```
