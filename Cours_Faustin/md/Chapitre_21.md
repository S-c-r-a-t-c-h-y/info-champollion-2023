# Chapitre 21 : Apprentissage automatique (IA)

```toc
```

## I - Motivations, formalisations, exemples

### A - Introduction

>[!note]
>En anglais, **machine learning**. C'est un sous-domaine de 'l'intelligence artificielle" de manière générale (mais c'est très difficile à définir).
>On va ici se placer dans le cadre des problèmes de **prédiction**. On suppose avoir une entrée $x \in X$ et on cherche à **prédire** un certain $y \in Y$. 

>[!example] Exemple
>Allo-Ciné : note spectateur, note critique
>Détection de SPAM 
>Catégorisation d'images
>Classification de passages courts
>Traduction automatique

>[!note]
>Beaucoup de ces problèmes sont très **complexes** (font intervenir des capacités humaines, d'où le terme intelligence artificielle) : langues naturelles, vision, reconnaissance d'objets. Ils ne peuvent pas (en général) simplement être résolus par des règles écrites "à la main".

>[!tip] Définition
>**Apprentissage automatique** : inférer automatiquement des règles à partir de données : on conçoit des algorithmes qui sont capables de trouver les "bonnes" règles de classification pour un problème donné (ils "apprennent" les règles "automatiquement").

>[!tip] Définition
>**Apprentissage statistique** : utilisation d'exemples (en grande quantité) pour collecter des statistiques pour choisir ensuite de manière probabiliste la "meilleure" solution.

>[!tip] Heuristiques
>**Idée générale** : identifier des "motifs" "automatiquement" et construire du modèle de ces données.

### B - Formalisation

>[!note]
>On considère un espace d'entrée $X$ et un espace de sortie $Y$. On cherche à construire une fonction $f : X \rightarrow Y$ qui à une entrée $x \in X$ associe sa **prédiction** $f(x)=y \in Y$.
>On parle de **classification binaire** si $|Y| =2$ et de classification multi-classe si $Y$ est fini, on prend $Y = [|0,C-1|]$ avec $C= |Y|$ classes.
>On peut (et on cherche) à prendre $X = \mathbb{R}^{d}$, c'est-à-dire, à **représenter** un objet par un vecteur de $\mathbb{R}^d$.
>![[chap21_img4.jpg]]

>[!info] Remarque
>On va faire les dessins avec $d=2$, mais en pratique $d$ est très grand (plusieurs $10^{3},10^{6},10^{9}$ ou plus).

>[!danger] Problème : Fléau de la grande dimension (curse of dimensionality)
>En grande dimension, le volume croît tellement vite que les objets se retrouvent "isolés".

>[!note]
>En OCaml, on utilise donc les types suivants : 
>```OCaml
>type data = float array
>type label = int
>```
>L'objectif est de construire une fonction `classify : data -> label`.

>[!info] Remarque
>Dans les modèles **paramétriques**, la fonction de prédiction $f_{\theta}$ dépend de paramètres $\theta \in H$(H entouré). Ce sont ces paramètres que l'on cherche à apprendre.
>![[chap21_img3.jpg]]

>[!note] 
>On peut adopter des modèles très complexes mais il faut faire très attention à ne pas trop coller aux données d'apprentissage.
>Risque de **surapprentissage** (over fitting).

## II - Apprentissage supervisé

>[!note] 
>Dans l'apprentissage supervisé, on suppose que l'on a notre disposition un **ensemble d'apprentissage** $D = \{(x_{i},y_{i})\}_{i \in [|1,n|]}$.

>[!note]
>En OCaml 
>```OCaml
>type datasete = (data*label) list
>```

>[!note]
>On peut utiliser cet ensemble pour construire notre fonction de classification. 
>L'objectif est de construire une fonction qui classe déjà bien les exemples de $D$.

>[!warning] Attention
>Le véritable objectif est de bien classer de **nouvelles** données pas d'apprendre de manière trop précise les données de $D$ (sinon surapprentissage).
>Le but est de considérer un modèle de $D$ qui permet de généraliser au delà de $D$.

## III - Evaluation

### A - Ensemble d'apprentissage et de test

>[!note]
>Il faut prendre en garde à ne pas évaluer les performances sur les mêmes données que celle d'apprentissage.
>On suppose donc disposer d'un ensemble $T = \{(x_{i},y_{i})\}_{i \in [|0,n|]}$ "indépendant" de $D$ pour faire les tests.

>[!info] Remarque
>Un ratio 90% / 10% fonctionne souvent.
### B - Taux d'erreur

>[!note]
>On peut mesurer le taux d'erreur sur l'ensemble d'apprentissage et de test, c'est le pourcentage de réponses fausses, c'est-à-dire, $\frac {|\{i \in [|0,n|]\ |\ f(x_{i})\neq y_{i}\}|} {m}$.

### C - Surapprentissage

![[chap21_img2.jpg]]

### D - matrice de confusion

>[!note]
>On peut souhaiter comprendre de manière plus précise quelles sont les erreurs réalisées par notre classification grâce à la **matrice de confusion**.

>[!tip] Définition
>La matrice de confusion est une matrice $M$ de taille $C\times C$ telle que le coefficient $M_{i,j}$ pour $i,j \in [|1,C|]$ correspond au nombre de fois où on a classé un exemple de vrai classe $i$ dans la classe $j$.

>[!example] Exemple
> $\begin{pmatrix} 6 & 0 & 2 \\ 0 & 5 & 0 \\ 1 & 1 & 5\end{pmatrix}$
>colonne : classe prédite
>ligne : classe réelle

>[!note]
>On trouve sur la diagonale les données correctement classées. Le taux d'erreur est donc le nombre d'éléments hors diagonale rapporté au nombre d'éléments total.

>[!example] Exemple
>Ici : 4/20 = 20 %

>[!note] 
>Mais on peut lire d'autres choses. Toutes les données de classe 1 ont été correctement classés.

>[!info] Remarque
>Avec ceux clauses $\{V,F\}$ la matrice à cette forme.
>$\begin{pmatrix} VP & FN \\ FP & VN \end{pmatrix}$
>$VP$ : vrai positif
>$VN$ : vrai négatif
>$FP$ : faux positif
>$FN$ : faux négatif

## IV - Algorithme des k-plus proches voisins (k-pp)

>[!note]
>En anglais : $k$-nearest-neighbors ($k$-nn)

>[!tip] Idée
>L'idée est toute simple. Pour prédire la classe d'une entrée $x$, on cherche les $k$ plus proches voisins de $x$ et on classe $x$ suivant la classe majoritaire de ses voisins.
>On va se donner en paramètre $k \geq 1$ et une distance $d : \mathbb{R}^{d} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{+}$.
>![[chap21_img1.jpg]]

>[!example] Exemple
>Distance euclidienne (associée à la norme 2)
>Distance Manhattan (associé à la norme 1)
>ou autre chose.

>[!note] 
>En cas d'égalité entre les classes, on peut choisir de manière aléatoire ou arbitraire.

>[!info] Remarque
>$k=1$ ?
>$k=n$ ?

>[!question] Comment choisir $k$ ?
>On peut procéder par validation croisée.
>On conserve une partie des exemples pour faire un petit ensemble de tests. On entraîne sur le reste et on choisit le $k$ qui convient le mieux sur l'ensemble de test pour l'évaluation (celui qui réalise le plus petit taux d'erreur; les k-pp sont dans l'ensemble d'apprentissage).

>[!question] Comment rechercher les $k$-pp de $p$ ?
>- On peut trier les $n$ points par distances croissantes à $p$ et prendre les $k$ premiers. Complexité : $O(d \times n \times log(n))$
>- On conserve les $k$ meilleurs trouvés en parcourant les $n$ points pour les mettre à jour. Complexité : $O(n \times d \times k)$
>- On peut mettre les $k$-pp trouvés dans une file de priorité (inversée). 
>Pour un nouveau point $x \in \mathbb{R}^{d}$ on calcule $d=d(p,x)$ si $d$ est plus petite que le maximum des distances dans la file, on l'insère dans la file. 
>Complexité : $O(d \times n \times log(k))$.
>En pratique $n$ est très grand, même $n\times log(k)$ est trop grand.
>- On pourrait pondéré le vote par l'inverse de la distance (que l'on calcule de toute façon).
>- D'autres distances que la distance euclidienne.

## V - Arbres d-dimensionnels

>[!warning] Attention
>Le $k$ dans les arbres $k$-dimensionnels est le $d$ de dimension.

>[!info] Idée
>Stocker les points dans une structure de données adoptée pour accélérer la recherche des $k$-pp, sans avoir à systématiquement à considérer les $n$ points en entier.
>$\Rightarrow$ élagage spatial
> $\approx$ ABR spatial imparfait
> 
> On peut séparer l'espace en zone de recherche.
> ![[chap21_img5.jpg]]
> ![[chap21_img6.jpg]]

>[!tip] Définition
>Un arbre $d$-dimensionnel est un arbre binaire de recherche où les éléments sont comparés à chaque niveau selon une coordonnée différente (abscisse, ordonnée, abscisse, ordonnée, etc ...).

>[!example] Exemple
>A la profondeur $p$, on utilise la coordonnée $p\ mod\ d$

>[!info] Remarque
>Pour construire cet arbre, on peut prendre le point d'abscisse médiane, séparer en deux les autres points et recommencer récursivement. 

>[!question] Comment rechercher les $k$-pp dans un tel arbre ?

>[!info]
>Considérons une étape où l'on recherche les $k$-pp d'un point $x$ pour un nœud de l'arbre $y$ avec une comparaison suivant la dimension de $s$ qui sépare l'espace par un hyperplan $H$ en $P_{\leq}$ et $P_{\geq}$.
>![[chap21_img7.jpg]]
>On commence par chercher (récursivement) les $k$-pp de $x$ dans $P_{\leq}$, notons les $(p_{1},\cdots,p_{k})$.
>On note $d= \underset {i \in [|1,k|]} {\text{max dist}(x,p_{i})}$ et $d_{H}= \text{dist}(x,H)$
>Si $d< d_{H}$ alors aucun point de $E \backslash P_{\leq}$ ne peut être plus proche de $x$ que $(p_{1},\cdots,p_{k})$. On a trouvé les $k$-pp . On peut s'arrêter (élagage).
>Sinon, on recherche récursivement dans $P_{\geq}$. On trouve $(p'_{1},\cdots,p'_{k})$.
>Les $k$-pp sont dans $\{p_{1},\cdots,p_{k},y,p'_{1},\cdots,p'_{k}\}$.

>[!info] Remarque
>Il manque un cas : s'il y a moins de $k$ points dans $P_{\leq}$, on prend $d=+\infty$ (et donc on explore à coup sûr l'autre côté).

## VI - Arbres de décision

>[!note] 
>On se place dans le cas où chaque coordonnée (attribut) peut prendre deux valeurs ($\{F,V\}, \{0,1\}$, etc).
>On a donc $x \in X = \mathbb{B}^{d}$ (où $\mathbb{B}^{d}$ avec seulement $\{0,1\}$).
>On parle alors **d'attribut**.

>[!example] Exemple
>On a présenté des livres informatiques à des élèves et on leur demande s'ils les recommandent. On a relevé quatre attributs : 
>- $E$ contient des exercices
>- $C$ contient le corrigé des exercices
>- $F$ est en français
>- $L$ propose des exemples dans un vrai langage de programmation.
>
>![[chap21_img8.png]]
>Il y a quatre attributs et deux classes possibles. Il s'agit d'un problème de classification binaire.

>[!info] Remarque
>On peut proposer un classifieur sous la forme d'un arbre de décision. A chaque nœud correspond un attribut, on va à gauche si cet attribut est $F$ et à droite s'il est $V$.
>Les feuilles correspondent le résultat de la classification si l'on suit cette branche.

>[!example] Exemple
>Jeu : Qui est-ce ?

```mermaid
flowchart TD
A[E] --F-->B[C]
A--V-->C[C]
B-->D[F]
B--> varnothing
D --> E[L]
D--> F[L]
E--> I[-]
E-->J[+]
F-->G[-]
F-->H[-]
C-->K[F]
C-->L[F]
K-->M[idem]
L-->N[idem]
```

>[!info] Remarque
>On peut simplifier l'arbre 
>Sans perte : 
>```mermaid
>flowchart TD
>A[L] -->B[-]
>A --> C[-]
>``` 
>Avec perte : 
>![[chap21_img11.jpg]]

>[!info] Remarque
>Il peut manquer des informations 
>```mermaid
>flowchart TD
>A[L] -->$\varnothing$
>``` 
>On peut prendre la classe majoritaire du sous-arbre enracinée en $C$.
>

>[!info] Remarque
>Il peut y avoir des contradictions dans les exemples 
>![[chap21_img10.jpg]]
>On peut prendre la classe majoritaire.

>[!info] Remarque 
>Le choix des attributs à chaque étape peut avoir une influence sur la hauteur de l'arbre.
>$\rightarrow$ On va chercher à construire l'arbre de la manière la plus compacte possible.

>[!info] Remarque
>On n'est pas obligé de choisir le même ordre par les attributs des deux côtés.

>[!question] Comment choisir les attributs pour construire l'arbre à partir des données ?

>[!tip] Définition
>**ID3** (Iterative dichotomiser 3) : algorithme pour sélection l'ordre du choix des attributs sur chaque branches.

>[!tip] Définition - Proposition
>Pour choisir le meilleur attribut, on va utiliser l'entropie de Shannon qui est une mesure de l'incertitude ou de l'information.
>Pour un ensemble $S$ de données de classes $+$ et $-$.
>On note $n_{+}$  le nombre de données de $S$ d'étiquette $+$, $n_{-}$  le nombre de données de $S$ d'étiquette $-$ et $n= n_{-} + n_{+}$  le nombre de données de $S$ d'étiquette $+$.
>On note $p_{+} = \frac {n_{+}} {n}$ et $p_{-} = \frac {n_{-}} {n}$.
>L'entropie de $S$ est $H(S) = -p_{+}log_{2}(p_{+})-p_{-}log_{2}(p_{-})$.
>On convient que $0 log_{2}(0)=0$.
>Si toutes les classes sont de les mêmes, alors $H(S)=0$.
>Si $n_{+}=n_{-}=\frac {n} {2}$ alors $H(S)=1$ (entropie maximale).

>[!example] Exemple
>Dans l'exemple $n_{+}=4$, $n_{-}=8$ et $H(S) \approx 0.92$

>[!note] Remarque
>Pour choisir entre les différents attributs, on va étudier comment chaque attributs modifie l'entropie.

>[!example] Exemple
>Si on prend les donnés $S_{E=V}$
>![[chap21_img10.jpg]]

>[!tip] Définition
>Le gain en entropie, pour un choix d'un attribut est $G(E)=H(S)- \frac {|S_{E=F}|} {|S|}H(S_{E=F})-\frac {|S_{E=V}|} {|S|}H(S_{E=V})$.

>[!example] Exemple
>Ici $G(E) \approx 0.012$

>[!bug] Exercice
>Vérifier que : 
>- $G(E) \approx 0.012$
>- $G(C) \approx 0.044$
>- $G(F) = 0$
>- $G(V) \approx 0.456$

>[!tip] Définition
>$H(x)=\sum\limits_{x \in X}-p(x)log_{2}(p(x)) \in [|0,log_{2}(x)|]$

>[!example] Exemple
>Pour $S_{L=V}$ 
>$G(E)= H(S_{L=V})- \frac {4} {6} H(S_{L=V, E=V})-\frac {2} {6} H(S_{L=V, E=F}) \approx 0.92 - \frac {4} {6} \times 0.81 - \frac {2} {6} \times 1 \approx 0.044$ 

>[!bug] Exercice
>calculer pour $S_{L=V}$ : 
>- $G(E) \approx 0.044$
>- $G(C) \approx ?$
>- $G(F) \approx ?$ 
>
>```mermaid
>flowchart TD
>B[L] --> A[-] 
>B--> C[C]
>C -->D[?]
>C --> E[?]
>```
>En déduire le choix (vérifier que c'est $C$) et terminer la construction

>[!tip] Algorithme ID3
>Pour construire un arbre de décision d'un ensemble $S$ de donnée et d'attributs $A$.
>On procède récursivement : 
>Si cas de base 
>- $A$ est vide : on renvoie une feuille avec la classe majoritaire
>- $S$ vide : renvoie Feuille avec la classe majoritaire du parent
>- Tous les éléments ont la même classe : Feuille (cette classe)
>
>Sinon :
>- Choisir l'attribut $a \in A$ qui maximise le gain. Construire arbre de racine $a$, de fils gauche : ID3($S_{a=F},A\backslash \{a\}$) et de fils droit : ID3($S_{a=V},A\backslash \{a\}$) 

## VII - Apprentissage non supervisé 

>[!tip] Définition
>En apprentissage non supervisé, on ne distingue plus des étiquettes mais uniquement des donnés $D = \{x_{i}\}_{i \in I}$. 
>L'objectif est le même : prédire les classes des objets.

>[!example] Exemple
>On dispose d'images d'animaux et il faut les classer en classe d'animaux similaires en classes $\{1,\cdots, C\}$.

>[!note]
>Bien sûr, on donnera une étiquette de classe arbitraire, mais on espère que cela coïncide avec les classes les classes réelles.

>[!tip] Définition 
>De manière générale, on cherche à grouper un ensemble d'objets en classes de manière à ce que des objets similaires se retrouvent dans la même classe.

>[!info] Remarque
>On peut vouloir aussi que des objets non similaires ne se retrouvent pas dans la même classe et ce n'est pas forcément compatible.

>[!tip] Définition
>Formellement, étant donné un ensemble $D=\{X_{i}\}_{i \in [|0,n-1|]}$, on cherche à partitionner ces objets en $C$ classe $\{0,\cdots, C-1\}$.
>On peut décrire une telle partition (**cluster**) matrice $Z=(z_{i}^{c})_{i \in [|0,n-1|], c \in [|0,C-1|]}$
>Avec $z_{i}^{c} = \begin{cases} 1\ \text{si } x_{i} \text{ est dans la classe c} \\ 0\ \text{sinon} \end{cases}$

>[!info] Algorithme des $k$-moyennes
>**k n'a rien a voir avec le k des k-pp ni le k des k-arbres**, ici $k$ est le nombre de classes (algorithme des C-moyenne)
>
>L'idée est toute simple. Chaque clusters (classes) est représentée par un centroïde qui est un point de $\mathbb{R}^{d}$. On associe à chaque point $x\in \mathbb{R}^{d}$ classe correspondant au centre le plus proche.
>Inversement, le centroïde va être le centre de sa classe.

>[!tip] Notation
>- $x_{i}\in \mathbb{R}^{d}$ point à placer
>- $z_{i}^{c}\in \{0,1\}$ pour $i \in [|0,n-1|], c \in [|0,C-1|]$ affectation des points au classes
>- $\mu_{c}\in \mathbb{R}^{d}$ pour $c \in [|0,C-1|]$ est le centroïde de la classe $c$

>[!info] Remarque
>Les centroïdes ne sont pas (nécessairement) des points de $D$.

>[!info] Objectif
>Minimiser les sommes des distances (au carré) entre les points et le contre de leur classe.
>$J(\mu,Z)=\sum\limits_{i=0}^{n-1}\sum\limits_{c=0}^{C-1}z_{i}^{c}d(x_{i},\mu_{c})^{2}$
>$J$ : mesure de distorsion, l'inertie, fonction objectif
>
>On cherche à minimiser $J$, c'est-à-dire à trouver les **meilleurs** centres possibles et les meilleurs affectations des points aux classes (multi-objectif).

>[!info] Remarque
>Ce problème est NP-difficile

>[!info] Remarque
>Si on connait les centres $\mu = (\mu_{0},\cdots,\mu_{C-1})$ on peut choisir de manière optimale la classe d'un point comme étant celle du centre le plus proche.
>
>Inversement, si on connaît les classes des points, on peut montrer que le meilleur choix des centres est le barycentre des points de la classe.
>
>Pour $i \in [|0,n-1|], c \in [|0,C-1|], z_{i}^{c} = \begin{cases} 1 \text{ si } c = argmin\ d(x_{i},\mu_{c}) \\ 0 \text{ sinon} \end{cases}$
>
>On affecte $x_{i}$ au centre $\mu_{c}$ le plus proche ($z_{i}^{c}=1$ et $z_{i}^{c'}$ si $c \neq c'$).
>
>Pour $c \in [|0,C-1|], \mu_{c}= \frac {\sum\limits_{i=1}^{n}z_{i}^{c}x_{i} \text{ (moyenne des points de classe)}} {\sum\limits_{i=1}^{n}z_{i}^{c} \text{ (nombre de points dans la clase)}}$.
>
>On va adopter une minimisation sur $\mu$ puis sur Z et ainsi de suite.
>Initialement, on choisit les centres (à définir comment). Et on répète : 
>1. Calculer les affectations des classes.
>2. Recalculer des nouveaux centres.
>
>Jusqu'à **convergence**.

>[!tip] Proposition (HP)
>**Convergence** : On admet que cette méthode converge.

>[!info] Remarque
>$J$ est décroissant suivant, $\mu$ est strictement décroissante suivant $Z$.

>[!info] Remarque
>Le nombre d'affection possibles de clases est fini.

>[!info] Remarque
>En pratique, on se donne un critère d'arrêt.
>- On se fixe un nombre maximal d'itérations.
>- On s'arrête lorsqu'il y a plus beaucoup de changement : $|\overline{J}-\underline{J}| \leqslant \varepsilon$

>[!info] Choix des points initiaux 
>- On peut choisir au hasard $k$ points parmi les $n$ comme points de départ.
>- On peut aussi prendre les points les plus éloignés
>- On peut aussi prendre une partition au hasard et commencer à l'étape 2.

>[!info] Minimum local
>Cette méthode converge (s'arrête) mais aucune garantie d'aller à un minimum global.

>[!info] Solution
>On relance plusieurs fois l'algorithme avec des points initiaux aléatoires et on choisit la configuration avec $J$ minimum.
>
>Dans le pire cas, on peut avoir des résultats très mauvais mais en pratique cela se comporte plutôt bien.
>
>On utilise souvent cette méthode comme point de départ à des méthodes plus avances.
>EM : Expectation-minimisation qui généralise $k$-means.
>$z_{i}^{c} \in [0,1]$ distribution des probabilités de l'affectations $x_{i}$ à la classe $c$.

>[!question] Comment choisir le nombre de classe ? 
>On ne peut pas utiliser $J$ qui est minimal pour $n$ classe, on a alors $J=0$. 
>Il faut des informations extérieures, a priori, heuristiques.

>[!example] Exemple
>Si on cherche à classer des chiffres, on peut prendre 10 classes.

>[!info] Performances
>Cette méthode marche plutôt bien si les points sont séparés et que les classes correspondent à des distribution similaires.
>

## VIII - Classification hiérarchique ascendante 

>[!example] Exemple Dendogrammes en biologie

>[!tip] Définition
>On cherche à créer une arborescence de clusters (regroupement hiérarchique).
>Les groupes sont imbriqués entre eux et organisés sous la forme d'un arbre.
>Formellement, on considère des données $D = \{x_{i}\}_{i \in [|0,n-1|]}$ et on cherche à construire une hiérarchie $H\leq P(D)$ telle que : 
>- $D \in H$ (en bout de la hiérarchie tous les éléments sont dans une même classe)
>- $\forall x \in D, \{x\}\in H$ (en bas de la hiérarchie, chaque élément est seul dans sa classe.)
>- $\forall h,h' \in H : \begin{cases} h \cap h' = \varnothing \\ h \subseteq h' \\ h' \subseteq h \end{cases}$ 
>
>2 façon de construtre la hiérarchie
>- On peut construire la hiérarchie de manière descendante (top down) : on part de $D$ et on divise avec un certain critère.
>- On peut construire la hiérarchie de manière ascendante (button up) : on part des feuilles et on regroupe avec un certain caractère. 
>Tous les individus sont seuls dans leur classes et on fusionne des classes déjà créées jusqu'à arriver sur une seule classe.

>[!info] Algorithme CHA (classification hiérarchique ascendante)
>**Idée** : à chaque étape, on fusionne les deux classes les **plus proches.**
>On se donne une distance $D : P(D)\times P(D) \mathbb{R}^{+}$ une distance **interclasse**.
>
>**CHA** : 
>Entrée : $D = \{x_{i}\}_{i \in [|0,n-1|]}$
>Sortie : $H \subseteq P(D)$ une classification hiérarchique.
>${C}=\{\{x_{i}\}, i \in [|0,n-1|]\}$ 
>$H ={C}$ 
>tant que $|{C}| \geq 2$ faire
>	$P,P' \leftarrow \underset {A,B \in \mathcal{C}, A \neq B} {argmin \ D(A,B)}$
>	$H \leftarrow H \cup \{P\cup P'\}$
>	${C} \leftarrow {C} \backslash \{P,P'\} \cup \{P \cup P'\}$
>fin
>$H \leftarrow H \cup \{D\}$
>renvoyer $H$

>[!info] Choix de la distance de $D$
>Il faut définir une distance (similarité) entre deux **clusters** pas simplement entre deux éléments.
>
>On suppose disposer d'une fonction $d  : \mathbb{R}^{d} \times \mathbb{R}^{d} \rightarrow \mathbb{R}^{+}$.

>[!info] Il existe de nombreux choix 
>**Idée** : 
>- $D(C_{1},C_{2})=\frac {\sum\limits_{x_{i}\in C_{1}} \sum\limits_{x_{j}\in C_{2}} d(x_{i},x_{j})^{2}} {|C_{1}|\ |C_{2}|}$
>- $D(C_{1},C_{2})= \underset {x_{i} \in C_{i}, x_{j} \in C_{2}} {max \ d(x_{i},x_{j})}$
>- $D(C_{1},C_{2})= \underset {x_{i} \in C_{i}, x_{j} \in C_{2}} {min \ d(x_{i},x_{j})}$
>- $D(C_{1},C_{2}) = \underset {x_{1} \in C_{1}} {max} \ \underset {x_{j} \in C_{2}} {min} \ d(x_{i},x_{j})$

>[!info] Critère d'arrêt et partition
>On peut s'arrêter avant d'avoir atteint $C = \{D\}$.
>
>On peut se donner divers critère d'arrêt, comme un nombre de classes à obtenir, ou un nombre d'éléments par classe, etc.

