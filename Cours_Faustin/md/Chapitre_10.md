# Chapitre 10 : Théorie des jeux

```toc
```

>[!note]
>On s'interroge à l'étude théorique et algorithmique des jeux à deux joueurs et en particulier à l'élaboration de stratégie.

>[!summary] Quelque dates :
>- 1952 : Premier jeu : Tic Tac Toe
>- 1997 : Deep Blue bat le champion d'échecs Garry Kasparov
>- 2017 : Alpha Go bat le champion du monde de Go Ke Jie
>- 2019 : Alpha Star bat un champion du monde Serral.

>[!note]
>On ne va considérer que des jeux à deux joueurs qui jouent alternativement à **information parfaite** : à tout moment, les deux joueurs ont la connaissance complète et exhaustive du jeu. On ne considère que des jeux à somme nulle : la somme des gains est nulle et même du même type : victoire/échec.

>[!example] Exemple
>Poker, Starcraft 2, jeux coopératifs presque tous les jeux de cartes.
>

## I - Lemme à deux joueurs

>[!example] Exemple
>[Tic Tac Toe](https://accromath.uqam.ca/2023/01/a-propos-du-tic-tac-toe/)

>[!note]
>On peut caractériser un jeu par un arbre des configurations possibles.
>
>Chaque nœud représente un **état** qui comporte **toute** l'information ainsi que le joueur à qui c'est le tour de jouer. (on dit que le joueur contrôle cet état).
>Les fils d'un état sont ses états auxquels on peut accéder en faisant un coup.

>[!example] Exemple
>Pour le Tic Tac Toe, il y a 549 946 nœuds dans l'arbre.

>[!note]
>On va plutôt représenter un jeu par **un graphe biparti orienté** (en général acyclique).
>Il y a alors 5478 sommets et 16167 arcs.

>[!info] Remarque
>Plusieurs chemins peuvent mener d'une configuration à une autre.
>Un état est contrôlé par l'un des deux joueurs.
>Un arc $X\rightarrow Y$ entre un sommet contrôlé par $X$ et un sommet contrôlé par $Y$ correspond à un coup possible à partir de la configuration $X$.

>[!info] Remarque
>Les puits du graphe (sommets sans successeurs) sont appelés états **terminaux**. Ce sont les états gagnant pour un joueur pour l'arbre ou un match nul.

>[!example] Exemple
>Pour Tic Tac Toe, il y a 968 états terminaux, dont 625 gagnants pour $X$, 316 gagnants pour $Y$ et 16 matchs nuls.

>[!tip] Définition
>Un **jeu** à deux joueurs est un quadruplet $J=(G,s_{0},T_{1},T_{2})$ avec :
>-  $G = (S_{1}\sqcup S_{2},A)$ un graphe **biparti orienté** (acyclique) (donc $A \subseteq S_{1}\times S_{2} \cup S_{2}\times S_{1}$).
>- $s_{0}\in S$ appelé état initial.
>- $T_{1}\subseteq S$ et $T_{2}\subseteq S$ et $T_{1}\cap T_{2}= \varnothing$ appelés état gagnant pour le joueur 2 des **puits** de $G$.
>
>Les sommets de $S_{1}$ sont appelés état du joueur 1 (ou contrôlés par le joueur 1, c'est-à-dire c'est à 1 de jouer).
>
>Les **puits** de $G$ sont appelés **états terminaux**.
>
>On note $T$ les puits de $G$, on a : 
>$T = T_{1}\sqcup T_{2}\sqcup T_{=}$ avec $T_{=}$ les états terminaux correspondant à un match nuls.
>On note $X_{1}=S_{1}\backslash T$ l'ensemble des sommets contrôlés par le joueur 1 non terminaux.

>[!info] Remarque
>Si le graphe est acyclique alors il y a toujours au moins un puits.

>[!tip] Définition
>Une **partie** d'un jeu $J=((S_{1}\sqcup S_{2},A),s_{0},T_{1},T_{2})$ est **chemin fini** du graphe $(S_{1}\sqcup S_{2},A)$ qui vient de $s_{0}$ et qui arrive sur un sommet $L \in T$.
>
>La partie est gagnée par le joueur 1 si $L \in T_{1}$, gagnée par le joueur 2 si $L \in T_{2}$ et nulle si $L \in T_{=}$.

>[!tip] Définition
>Une **stratégie** pour le joueur $i \in [|1,2|]$ est une fonction $f: X_{i} \longrightarrow S_{3-i}$ telle que pour tout $X_{i}$, non terminal contrôlé par le joueur $i, f(x) \in S_{3-i}$ et que le coup à jouer à partir du sommet $x$ telle que $(x,f(x)\in A)$.
>
>A tout état du joueur 1 (non terminal), on associe un coup à jouer.

>[!info] Remarque
>Ici, une stratégie ne considère que l'état actuel. On parle de stratégie sans **mémoire**.

>[!tip] Définition
>Une partie $s_{0}\in S \rightarrow s_{1} \rightarrow \cdots \rightarrow s_{n}\in T$ est jouée suivant une stratégie $f$ pour le joueur $i$ si pour tout $K \in [|0,n-1|]$, si $s_{K}\in X_1$ alors $x_{k+1}=f$. C'est-à-dire si tout coup pour le joueur $i$ est joué en utilisant $f$.
>
>On dit qu'une stratégie $f$ pour le joueur $i\in [|1,2|]$ est gagnante pour le joueur $i$ à partir d'une position $s \in S$, si toute partie de $G=(S,s,T_{1},T_{2})$ commence en ce sommet $s \in S$ joués suivant $f$ est gagnante pour $i$.
>Un tel sommet $s$ est appelé **position gagnante**.

>[!info] Remarque
>$f$ est une stratégie gagnante à partir de $s$ si, quelque soit l'adversaire, jouer en suivant $f$ permet de gagner à coup sûr.

>[!tip] Définition
>Un sommet $s\in S$ est une position gagnante s'il existe une stratégie gagnante pour laquelle cette position est gagnante.

>[!tip] Définition
>Une stratégie gagnante pour le joueur $i \in [|1,2|]$ pour un jeu $J=(G,s_{0},T_{1},T_{2})$ est une stratégie gagnante pour ce joueur à partir de la position initiale $s_0$. Elle existe si et seulement si $s_{0}$ est une position gagnante pour ce joueur.

>[!info] Remarque
>L'ensemble des sommets se positionnent en :
>- Les positions gagnantes pour 1
>- Les positions gagnantes pour 2
>- Les autres.

>[!warning] 
>Un position contrôlée par le joueur $i$ peut être gagnante pour $i$ ou pour l'autre ou pour aucun des deux.

>[!info] Remarque 
>Si $s \in T$ alors
>- Si $s \in T_{1}$ alors $s$ est une position gagnante pour 1.
>- Si $s \in T_{2}$ alors $s$ est une position gagnante pour 2.
>- Si $s\in T_{=}$ alors $s$ n'est une position gagnante pour aucun des deux.

>[!note]
>Soit $x \in X_{i}$ et $V_{x_{i}}$ ses **successeurs**.
>S'il existe $y \in V_{x_{i}}$ un position gagnante pour $i$ alors $x$ est une position gagnante pour $i$.
>Si pour tout $y \in V_{x_{i}}$, $y$ est une position gagnante pour $3-i$ alors $x$ est une position gagnante pour $3-i$

>[!info] Implémentation du Tic Tac Toe

```ocaml
(* On pourrait prendre `0` et `1` *)
type joueur = X | O

type etat = {
  (* Joueur Ã  qui c'est le tour *)
  tour : joueur;
  (* Persistant : il faudra faire des copies *)
  grille : joueur option array array;
  (* Nombre de cases libres *)
  libres : int;
  (* Indique s'il y a dÃ©jÃ  un gagnant *)
  gagnant : joueur option;
}

(* Taille de la grille *)
let n = 3

let nom = function
  | X -> "X"
  | O -> "O"

let suivant = function
  | X -> O
  | O -> X

let tour etat = etat.tour

let initial = {
  tour = X;
  grille = Array.make_matrix n n None;
  libres = n * n;
  gagnant = None;
}

let terminal etat = etat.gagnant != None || etat.libres = 0

let gagnant etat =
  assert (terminal etat);
  etat.gagnant

(* VÃ©rifie si jouer en `(i, j)` permet de gagner *)
let gagne grille joueur (i, j) =
  let indices = List.init n (fun k -> k) in
  List.for_all (fun k -> grille.(i).(k) = Some joueur) indices
  || List.for_all (fun k -> grille.(k).(j) = Some joueur) indices
  || (i = j && List.for_all (fun k -> grille.(k).(k) = Some joueur) indices)
  || (i = n - 1 - j && List.for_all (fun k -> grille.(k).(n - 1 - k) = Some joueur) indices)

(* Joue un coup en (i, j) et renvoie le nouvel Ã©tat. On suppose la case vide. *)
let coup etat (i, j) =
  let joueur = etat.tour in
  let g = Array.map Array.copy etat.grille in
  assert (g.(i).(j) = None);
  g.(i).(j) <- Some joueur;
  {
    tour = suivant joueur;
    grille = g;
    libres = etat.libres - 1;
    gagnant = if gagne g joueur (i, j) then Some joueur else None;
  }

let coups etat =
  if terminal etat then []
  else begin
    let res = ref [] in
    for i = 0 to n - 1 do
      for j = 0 to n - 1 do
        if etat.grille.(i).(j) = None then begin
          res := (coup etat (i, j)) :: !res
        end
      done
    done;
    !res
  end

let affiche etat =
  for i = 0 to n - 1 do
    for j = 0 to n - 1 do
      Printf.printf "%s" (
        match etat.grille.(i).(j) with
        | Some j -> nom j
        | None -> "."
      );
      if j < n - 1 then print_char ' ';
    done;
    print_newline ()
  done;
  Printf.printf "\nC'est Ã  %s de jouer !\n\n" (nom etat.tour)
```

```ocaml
open Jeu

let strategie_aleatoire etat =
  let coups_possibles = Array.of_list (coups etat) in
  coups_possibles.(Random.int (Array.length coups_possibles))

let partie () =
  let rec jouer etat =
    affiche etat;
    if terminal etat then begin
      match gagnant etat with
      | Some j -> Printf.printf "Le joueur %s a gagnÃ© !\n\n" (nom j)
      | None -> Printf.printf "Match nul.\n\n"
    end else begin
      jouer (strategie_aleatoire etat)
    end
  in
  jouer initial

let () =
  for i = 0 to 9 do
    Printf.printf "*** Partie nÂ°%d ***\n\n" i;
    partie ()
  done

let nb_etats_arbre =
  let nb = ref 0 in
  let rec parcours etat =
    incr nb;
    List.iter parcours (coups etat)
  in
  parcours initial;
  !nb

let nb_etats =
  let ns = ref 0 in
  let na = ref 0 in
  (* Utilisation comme un dictionnaire : la valeur est toujours `None` *)
  let h = Hashtbl.create 1000 in
  let rec parcours etat =
    if not (Hashtbl.mem h etat) then begin
      incr ns;
      Hashtbl.add h etat None;
      List.iter (fun e -> incr na; parcours e) (coups etat)
    end
  in
  parcours initial;
  !ns, !na

(* Possible uniquement pour un graphe acyclique *)
let rec attracteur joueur etat =
  if terminal etat then
    gagnant etat = Some joueur
  else if tour etat = joueur then
    List.exists (fun e -> attracteur joueur e) (coups etat)
  else
    List.for_all (fun e -> attracteur joueur e) (coups etat)

let _ = attracteur (tour initial) initial

(* Possible uniquement pour un graphe acyclique *)
let calcul_attracteurs () =
  (* Associe Ã  un Ã©tat son attracteur *)
  let h = Hashtbl.create 1000 in
  let rec parcours etat =
    if not (Hashtbl.mem h etat) then begin
      if terminal etat then Hashtbl.add h etat (gagnant etat)
      else begin
        let attr = List.map parcours (coups etat) in
        let joueur = tour etat in
        let res =
          if List.exists (fun a -> a = Some joueur) attr then
            Some joueur
          else if List.for_all (fun a -> a = Some (suivant joueur)) attr then
            Some (suivant joueur)
          else
            None
        in
        Hashtbl.add h etat res
      end
    end;
    Hashtbl.find h etat
  in
  let _ = parcours initial in
  h

let attr = calcul_attracteurs ()

let affiche_attracteur = function
  | Some joueur -> Printf.printf "Attracteur : %s\n\n" (nom joueur)
  | None -> Printf.printf "Attracteur : -\n\n"

let () = Hashtbl.iter (fun k v -> affiche k; affiche_attracteur v) attr
```

## II - Calculs des attracteurs

>[!tip] Définition
>L'attracteur d'un joueur est l'ensemble des positions gagnantes pour $i$. On le note $A(i)\subseteq S$.

>[!note]
>On vient de voir un moyen de calculer les attracteurs pour les deux joueurs.

>[!note]
>Pour $i \in [|1,2|]$ et pour $n \in \mathbb{N}$, on définit par récurrence la suite $A_{n}(i) \subseteq S$. Une suite d'ensemble de positions gagnantes en, au plus, $n$ étapes.

>[!note] Démo
>Pour $n=0$ et $i \in [|1,2|]$, $A_{0}(i)=T_{i}$ (états terminaux où $i$ gagne sans jouer, on a 0 coups).
>$A_{n+1}(i)=A_{n}(i)\cup \left\{x \in S_{i}\ |\ \exists y \in A_{n}(x),(x,y)\in A\right\} \cup \left\{x \in A_{3-i} \ |\ \forall y \in S, (x,y) \in A, y \in A_{n_{i}}(i)\right\}$.

>[!tip] Proposition
>$A(i)= \bigcup_{n \in \mathbb{N}}A_{n}(i)$ est l'attracteur du joueur $i$ et $\forall x \in A(i), \exists n \in \mathbb{N}$ tel que $x \in A_{n}(i)$.

>[!question] Calculer les attracteurs en pratique ?
>$\rightarrow$ On calcule le graphe des configurations et on effectue un parcours en largeur à partir de $T_{i}$ et $T_{3-i}$. Dans le graphe miroir.

>[!done] Rappel
>Pour $i \in [|1,2|]$, on définit la suite $(A_{n}(i))_{n \in \mathbb{N}}$ des attracteurs en $n$ coups au plus pour le joueur $i$. Par récurrence : 
>$A_{0}(i)=T_{i}$
>$A_{n+1}(i)=A_{n}(i) \cup \left\{x \in X_{i}\ | \ \exists y \in A_{n}(i), (x,y)\in A\right\} \cup \left\{x \in X_{3-i}\ | \ \forall y \in V(x), y \in A_{n}(i) \right\}$.
>L'ensemble des positions gagnantes pour le joueur $i$ est $A(i) = \bigcup_{n \in \mathbb{N}}A_{n}(i)$ et si $x \in A(i), \exists n \in \mathbb{N}$ tel que $x \in A_{n}(i)$.

>[!info] Remarque
>A deux stratégies $f_{1}$ et $f_{2}$ pour les joueurs 1 et 2 correspond à une unique partie $s_{0} \longrightarrow f_{1}(s_{0})\longrightarrow f_{2}(f_{1}((s_{0}))\longrightarrow \cdots$ si $s_{0}\in S$

>[!Note]
>Construction d'une stratégie "optimale". Optimale au sens où elle gagne le plus vite possible ou moins vite possible.
>Construisons $f_{i} : X_{i}\longrightarrow S_{3-i}$ Pour $x \in X_{i}$.
>- Si $x \in A(i)$, soit $n \in \mathbb{N}$ le plus petit $n\in \mathbb{N}$ tel que $x \in A_{n}(i)$. On a $n>0$ car $x \notin T$. Par définition de $A_{n}(i)$, puisque $x \notin A_{n-1}(i), \exists y \in A_{n-1}(i)$ tel que $(x,y) \in A$. On pose $f(x)=y$.
>- Si $x\in A(3-i)$, on considère $n \in \mathbb{N}$ le plus petit tel que $x\in A_{n}(3-i)$. On a $n>0$. Par construction, $\forall y \in V(x), y \in A_{n-1}(i)$. $\exists y \in V(x)$ tel que $y \notin A_{n-2}(3-i)$ (si $x\geq 2$). Sinon on aurait $x \in A_{n-1}(i)$ et absurde par choix de $n$.
>Soit $y$ un tel voisin. On pose $f(x)=y$
>- Si $x\notin A(i)\cup A(3-i)$ alors $\exists y \in S_{3-i}\backslash A(3-i)$. On pose $f(x)=y$.

## III - MinMax et heuristiques

### A - Algorithme MinMAx

>[!tip] Définition
>Une manière équivalente de déterminer les positions gagnantes est d'associer un **score** à chaque état.
>- $+ \infty$ si la position est gagnante pour le joueur 1
>- $- \infty$ si la position est gagnante pour le joueur 2
>- 0 si la position gagnante n'est pas gagnante pour aucun des deux joueurs.
>  
>  L'attracteur du joueur 1 est alors les positions de son score.

>[!note]
>Déterminer si une position est gagnante revient à déterminer son score.
>
>**Si le graphe est acyclique** cela peut se faire de la manière suivante récursive :
>- si $x$ est terminal son score est $+\infty,0,-\infty$ suivant que la configuration est gagnante, null ou perdante.
>- Si $s$ est contrôlé par 1 alors son score est $\text{max}\ \left\{\text{score}(y)\ |\ y \in V(s)\right\}$
>- Si $s$ est contrôlé par 2 alors son score est $\text{min}\ \left\{\text{score}(y)\ |\ y \in V(s)\right\}$

>[!info] Remarque
>Le joueur 1 est souvent appelé le joueur Max et le joueur 2 le joueur Min.

>[!info] Remarque
>Pour que le calcul soit efficace, il faut mémoriser les configurations pour ne pas recalculer plusieurs fois la même chose. **Mémoïsation**

>[!info] Remarque
>En pratique, on le fait rarement.

### B - Heuristiques

>[!note] 
>Dans la très grande majorité des cas, les configurations possibles sont trop nombreuses pour calculer les attracteurs (ou effectuer MinMax en entier).

>[!example] Exemple
>Au Domineering, il y a 4632 états pour une grille $4\times 4$. Un million pour $5\times 5$. 40 billiards 
>($4\times 10^{16}$) pour le jeu classique $8\times8$.
>Au Go, on estime à $10^{130}$ le nombre de configuration.

>[!note]
>On va introduire la notion d'**heuristique**(guide, aide).

>[!tip] Définition
>Une **heuristique** évalue la "qualité" d'une configuration. C'est une fonction  de $S \longrightarrow \overline{\mathbb{R}}$.
>On impose en général :
>$h(e)=+\infty$ alors $e\in A(1)$ et $h(e)=-\infty$ alors $e \in A(2)$.

>[!info] Remarque
>Une heuristique doit être :
>- une "bonne" **estimation** de la situation
>- **facile à calculer**

>[!example] Exemple d'heuristique
>1. Le nombre de coups possibles pour le joueur 1 moins le nombre de coups possibles pour le joueur 2.
>2. On joue $10^{6}$ parties aléatoires à partir de cette configuration et on renvoie la différence du nombre de parties gagnées. (Monte Carlo MinMAx)

>[!info] Algorithme MinMax avec heuristique
>1. On choisit une profondeur maximale d'exploration $p \in \mathbb{N}$ (on ne considère pas le sous-graphe des configurations à distance au plus de $p$ de $s_{0}$)
>2. On applique l'algorithme MinMax à ceci près que si on atteint la profondeur maximale, on traite les états comme terminaux en utilisant l'heuristique comme score.

>[!info] Remarque
>Bien entendu, l'exploration étant limitée et l'heuristique imparfaite, rien ne garantit que l'on prédit correctement une "bonne stratégie".
>La qualité de l'algorithme dépend de la qualité de l'heuristique et de la profondeur d'exploration.

>[!info] Elagage $\alpha - \beta$
>Comme dans un algorithme de retour de sur trace (backtracking), l'algorithme MinMax explore l'intégralité de l'arbre juste qu'à une profondeur $p$. Dans certaines situations, il est possible **d'élaguer**.
>On va garder en mémoire une plage $[\alpha,\beta]$ des scores "utiles". Si on a la garantie que le score d'un nœud sort de cette plage, alors il est inutile de poursuivre son exploration.

>[!example] Exemple
>![[chap10_img1.png]]

>[!tip] Définition
>L'**élagage** $\alpha$ permet **de couper des branches** lorsqu'on cherche à calculer le score d'un nœud Min.
>L'**élagage** $\beta$ permet **de couper des branches** lorsqu'on cherche à calculer le score d'un nœud Max.

>[!info] Remarque
>Les scores obtenus peuvent différer avec ceux de l'approche MinMax sans élagage mais celui de la racine sera le même.

>[!info] Remarque
>L'ordre dans lequel on parcourt les fils a une importance. Il vaut mieux explorer les nœuds les plus prometteurs en premier. Cela permet d'élaguer d'avantage.

>[!info] Implémentation de MinMax avec élagage $\alpha - \beta$

```ocaml
(* Pour un noeud Max `etat` avec une profondeur d'exploration `p`. *)
let rec max_min p etat =
  if p = 0 || terminal etat then heuristique etat
  else
    (* On cherche le maximum des scores des fils *)
    let rec maxi = function
      | [] -> neg_infinity
      | x :: xs -> max (min_max (p - 1) x) (maxi xs)
    in
    maxi (coups etat)
(* On procède mutatis mutandis pour un noeud Min. *)
and min_max p etat =
  if p = 0 || terminal etat then heuristique etat
  else
    let rec mini = function
      | [] -> infinity
      | x :: xs -> min (max_min (p - 1) x) (mini xs)
    in
    mini (coups etat)

(* Pour un noeud Max `etat` avec une profondeur d'exploration `p`. On
   sait que seul un score dans [alpha, beta] sera utile par la suite.
   Pour un noeud Max, si on trouve une valeur supérieur à  `beta` on
   peut s'arrêter et renvoyer `beta`. Inversement, inutile de renvoyer
   une valeur inférieure à  `alpha`. *)
let rec max_min p alpha beta etat =
  if p = 0 || terminal etat then heuristique etat
  else
    (* On cherche le maximum des scores des fils. La meilleure valeur
       trouvée jusqu'ici est `alpha`. *)
    let rec maxi alpha beta = function
      | [] -> alpha
      | x :: xs ->
        let score_x = min_max (p - 1) alpha beta x in
        (* Elagage bêta : si on trouve un fils de score au moins bêta,
           le score de ce noeud Max sera donc au moins bêta. Cela ne
           sert plus à  rien de continuer et on peut prendre bêta. *)
        if score_x >= beta then beta
        else
          (* Sinon, le score de ce neoud Max sera au moins `score_x`. On
           met donc à  jour alpha pour indiquer qu'il ne sert à  rien de
           trouver une valeur plus faible que `score_x`. *)
          maxi (max alpha score_x) beta xs
    in
    maxi alpha beta (coups etat)
(* On procède mutatis mutandis pour un noeud Min. *)
and min_max p alpha beta etat =
  if p = 0 || terminal etat then heuristique etat
  else
    let rec mini alpha beta = function
      | [] -> beta
      | x :: xs ->
        let score_x = max_min (p - 1) alpha beta x in
        if score_x <= alpha then alpha
        else
          mini alpha (min score_x beta) xs
    in
    mini alpha beta (coups etat)
```

