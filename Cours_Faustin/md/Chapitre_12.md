# Chapitre 12 : Couplages

```toc
```

>[!tip] Définition
>Un couplage est un ensemble d'arêtes **indépendantes**, c'est-à-dire sans extrémités connexes.
![[chap12_img1.png]]

>[!note]
>Cette notion apparaît naturellement lorsqu'il s'agit de **coupler** deux à deux des objets, certaines associations seulement étant possibles (celles des arêtes du graphe).

>[!example] Exemple
>Ensemble de personnes à marier (sans polygamie).

>[!example] Exemple
>Une assemblé de personnes parlant diverses langues. On veut former des groupes de 2 qui peuvent se comprendre.
>On modélise alors par un graphe avec une arête entre deux personnes (sommets) si elles parlent la même langue.

>[!note] 
>Le problème général est posé comme un problème d'optimisation. Plus il y a d'arêtes mieux c'est.
>On cherche à maximiser le nombre de couples.

## I - Couplage dans un graphe

>[!tip] Définition
>Soit $G=(S,A)$ un graphe (non orienté). Un **couplage** de $G$ est un ensemble $C \subseteq A$ d'arêtes **indépendantes**. Un sommet $s \in S$ est incident à au plus une arête de $C$.
>Un couplage est dit : 
>- maximal (ou maximal pour l'inclusion) s'il n'est strictement inclus dans un couplage de $G$ (on ne peut plus ajouter d'arêtes).
>- maximum (ou optimal ou maximal pour le cardinal) si son cardinal est plus grand ou égal que celui de tout couplage de $G$.
>- parfait si tout sommet de $G$ est incident à une arête du couplage.

>[!tip] Définition
>Soit $G=(S,A)$ un graphe et $C\subseteq A$ un couplage de $G$. On dit que $s \in S$ est **couvert** par $C$ si $s$ est extrémité d'une arête de $C$.
![[chap12_img2.png]]

>[!info] Remarques 
>- Comme un couplage ne peut contenir aucune arête ($C=\varnothing$), l'ensemble des couplages d'un graphe est non vide et fini (car $A$ est fini). Donc tout graphe possède un couplage optimal.
>- Tout arête d'un graphe est un couplage de ce graphe de cardinal 1.
>- Un couplage est maximal si et seulement s'il n'existe pas deux sommets libres incidents (sinon il suffit d'ajouter cette arête).
>- Les couplages maximum n'ont pas nécessairement le même cardinal.
>- Un couplage maximal n'est pas toujours optimal, mais la réciproque est vraie.
>- Le cardinal d'un couplage est inférieur à $\frac {|S|} {2}$. Le graphe doit alors être d'**ordre** pair.
>- Un graphe complet d'ordre pair admet un couplage parfait.

>[!example] Exemple : Etoile $S_{n}$ pour $n=3$
>Plusieurs sommets reliés par un même sommet. 
>Tout couplage non vide est de cardinal 1.

>[!question] Comment trouver un couplage maximal ?
>On ajoute les arêtes si on peut.

>[!question] Comment trouver un couplage optimal ?

>[!tip] Définition
>Soit $G=(S,A)$ un graphe et $C$ un coulage de $G$. 
>- Un **chemin alternant** de $G$ pour $C$ est un chemin élémentaire de longueur strictement positif dont les arêtes sont alternativement dans $C$ et dans $A\backslash C$.
>- Un **chemin améliorant** est un chemin alternant d'extrémités libres.

>[!info] Remarque
>La longueur d'un chemin améliorant est impaire et les arêtes à ses extrémités sont dans $A\backslash C$.

>[!info] Remarque
>On trouve aussi augmentant pour améliorant, chaîne améliorante etc...

>[!tip] Lemme du chemin améliorant
>Soit $C$ un couplage de cardinal $k$ d'un graphe $G=(S,A)$ et $D$ un chemin améliorant pour $C$ dans $G$. 
>On note $C \Delta D = C \backslash D \cup D \backslash C$ l'ensemble des arêtes qui sont dans le couplage ou exclusif dans le chemin. Alors $C\Delta D$ est un couplage de $G$ de cardinal $k+1$.

>[!note] Démo
>$|C \Delta D| = |C \backslash D| + |D \backslash C| = |C| +1$
>Une arête de $D$ à $A \backslash D$ n'est pas dans le couplage. En effet, les extrémités de $D$ sont libres donc ne sont pas incidents à une arêtes du couplage. Les autres sont couverts par une arête de $D$. Donc elles ne peuvent pas non plus être incidentes à une arête de $C \backslash D$. 
>$C \backslash D$ reste une couplage.
>$D \backslash C$ est un couplage puisque l'on prend une arête sur deux dans un chemin dont les sommets sont tous deux à deux distincts. Donc toutes ces arêtes sont indépendantes.
>De plus aucun sommet n'est incident à la fois à une arête de $C \backslash D$ et de $D\backslash C$ qui sont donc indépendantes.
>Donc $C \Delta D = C\backslash D \cup D \backslash C$ est un couplage.
>Etant donné un couplage $C$ on peut donc chercher des chemins améliorants pour trouver un meilleur couplage.

>[!tip] Théorie Berge 1957
>Soit $G=(S,A)$ un graphe et $C$ un couplage de $G$. Le couplage $C$ est optimal si et seulement s'il n'admet aucun chemin améliorant.

>[!note] Démo
>Le lemme ci-dessus établit la condition nécessaire. En effet, si $C$ possède un chemin améliorant alors il n'est pas optimal. 
>Dans la condition suffisante, on procède par contraposée.
>Soit $C$ un couplage non optimal, montrons que $C$ possède un chemin améliorant.
>On considère un couplage optimal $C' \subseteq A$.
>On a donc $|C| < |C'|$.
>On considère $C \Delta C'$ et notons $H = (S, C \Delta C')$.
>Considérons un sommet de $H$ il est incident provenant de $C$ et au plus une arêtes provenant de $C'$.
>Donc dans $H$, tous les sommets ont un degré dans $\left\{0,1,2\right\}$.
>Le long d'une chaîne ou d'un cycle de $H$, les arêtes alternent entre celle de $C$ et que de $C'$
>Donc les cycles sont de longueur paire et contiennent autant d'arêtes de $C$ et de $C'$. 
>Comme $|C'| > |C|$, donc au moins une composante connexe de $H$ contient plus d'arêtes de $C'$ que de $C$. Ce n'est pas un cycle donc une chaîne. Cette chaîne est améliorante car elle alterne des arêtes de $C$ et de $C' \backslash C$ avec plus d'arêtes de $C' \backslash C$ que de $C$.

>[!tip] Propriété
>Un graphe dont tous les sommets sont de degré inférieur ou égale à 2, est une réunion de chaînes et de cycles.

>[!bug] Exercice
>Le prouver

>[!info] Remarque
>On en déduit un algorithme pour trouver un couplage optimal.
>```pseudo
>	\begin{algorithm}
>	\begin{algorithmic}
>		\Input Un graphe $G=(S,A)$
>		\Output Un couplage $C$ optimal
>		\Procedure{couplage}{g}
>			\State $C \gets \varnothing$
>			\While{il existe un chemin améliorant $D$ pour $C$}
>				\State $C \gets C \Delta D$
>			\EndWhile
>			\State Renvoyer C
>		\EndProcedure
>	\end{algorithmic}
>	\end{algorithm}
>```

>[!question] Comment trouver des chemins améliorants ?
>On sait faire en temps polynomial HP.

>[!info] Remarque
>Justifions plus précisément que les extrémités du chemin construit sont bien libres pour $C$.
>Ces extrémités sont incidentes à une arête de $C' \backslash C$ (car il y a strictement plus d'arêtes de $C' \backslash C$ que de $C \backslash C'$ sur ce chemin alternant). Cette extrémité n'est donc pas incidente à une autre arête de $C'$ (car $C'$ couplage).
>Si cette extrémité est incidente à une arête de $C$, celle-ci n'étant pas dans $C'$, elle serait dans $C\backslash C'$ donc dans $H$. Ce sommet serait de degré 2 alors qu'il est de degré 1.

## II - Graphes bipartis

>[!tip] Définition
>Un **stable** ou **ensemble indépendant** est un sous-ensemble $X \subseteq S$ de sommets deux à deux non adjacents. Autrement dit, aucune arête de $G$ n'a ses deux extrémités dans $X$. Autrement dit, le sous-graphe induit par $X$ est isolé.

>[!tip] Définition
>Un graphe est **biparti** si on peut partitionner $S= X \sqcup Y$ en deux ensemble $X$ et $Y$ indépendants. Autrement dit, toute arête a une extrémité dans $X$ et l'autre dans $Y$.
>On note $G=(X,Y,A)$.

>[!info] Remarque
>Le cas des graphes bipartis est celui où la notion de couplage est la plus naturelle. Typiquement, on a deux ensembles d'objets $X$ et $Y$ ($X$ les candidats, $Y$ les écoles) et un ensemble d'arêtes entre des sommets de $X$ et des sommets de $Y$.

>[!example] Exemple
>Auteurs / articles
>Agents / tâches

>[!note]
>Un avantage des graphes bipartis (autre le fait que de nombreux problèmes de couplages se modélisent par des graphes bipartis) est que l'on sait déterminer s'il existe des chemins augmentants relativement facilement, et en trouver le cas échéant.

>[!info] Remarque
>Un chemin augmentant dans un graphe biparti commence par un sommet libre de $X$, alterne entre des sommets de $X$ et $Y$ et termine en un sommet libre de $Y$.

>[!info] Remarque
>Quand on va de $X$ à $Y$, on prend une arête de $A\backslash C$ et quand on va de $Y$ à $X$ on prend une arête de $C$.

>[!info] Remarque
>Pour déterminer si un graphe biparti avec un couplage $C$ possède un chemin augmentant, il suffit de faire un parcours un profondeur à partir des sommets libres de $X$ jusqu'à un sommet libre de $Y$, en utilisant uniquement les arêtes de $A \backslash C$ entre $X$ et $Y$ et de $C$ entre $Y$ et $X$. 
>Un tel chemin sera bien augmentant et s'il existe un chemin augmentant, il s'agit d'un tel chemin.
>Complexité : $O(|S|+|A|)$.

>[!info] Autre représentation possible
>A un graphe $G=(S,A)$ et un couplage C, on peut associer un graphe **orienté** $G_{C}(S',A')$ comme suit :
>- On se donne deux nouveaux sommets $s,f \notin S$ et $S'=S \cup \left\{s,f\right\}$.
>- Il y a un arc entre $s$ et tout sommets libres de $X$.
>- Il y a un arc entre tout sommets libres de $Y$ et de $f$.
>- Il y a un arc $x \rightarrow y$ pour tout $x \in X, y \in Y$ tel que $\left\{x,y\right\} \in A\backslash C$.
>- Il y a un arc $y \rightarrow x$ pour tout $x \in X, y \in Y$ tel que $\left\{x,y\right\} \in C$.


>[!tip] Propriété
>Les chemins augmentants de $G$ pour le couplage $C$ sont en bijection avec les chemins élémentaires se $s$ vers $f$ dans $G_{C}$.
>On a donc un algorithme pour trouver un couplage optimal dans un graphe biparti en $O(|S|(|S|+|A|))$.

>[!note] Démo
>A chaque fois que l'on trouve un chemin augmentant, on ajoute au moins une arête au couplage et deux nouveaux sommets couverts.
>Il y a donc au plus $\frac {|S|} {2}$ recherche de chemins augmentants, chacune étant en complexité $O(|S|+|A|)$.
>Le calcul $C \leftarrow C \Delta D$ peut se faire en $O(|S|+|A|)$.

>[!done] Rappel
>Recherche de couplage optimal : 
>```pseudo
>	\begin{algorithm}
>	\begin{algorithmic}
>		\Input Un graphe $G=(S,A)$
>		\Output Un couplage $C$ optimal
>		\Procedure{couplage}{g}
>			\State $C \gets \varnothing$
>			\While{il existe un chemin augmentant $D$ pour $C$}
>				\State $C \gets C \Delta D$
>			\EndWhile
>			\State Renvoyer C
>		\EndProcedure
>	\end{algorithmic}
>	\end{algorithm}
>```
