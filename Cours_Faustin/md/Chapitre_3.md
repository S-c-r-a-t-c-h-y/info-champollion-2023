# Chapitre 3 : Langages réguliers

```toc
```

## I - Motivations

>[!note] 
>- Mon collègue de maths m'a envoyé un mail en début d'année : CTRL - F Quibel
>- Ma collègue de physique : CTRL - F C\_\_ier avec un l ou c
>>[!note]
>>CTRL - F amélioré : $C \cdot * (o/l).* ier$
>
>- ACGGTACTGACGTC, Quelles sont les sous-séquences de la forme AGT???GAC
>- Trouver tous les print_int qui sont imbriquées dans une boucle et pas les autres.
>- Trouver les liens hypertexte dans un fichier texte pour les colorés.

## II - Opération régulières

>[!info]
>Les trois opérations **réunion**, **concaténation** et **étoile de Kleene** sont appelées **opération régulières**.

>[!faq] Question
>On peut se poser la question de la classe de langage que l'on peut obtenir à partir de langages tout simple et uniquement de ces trois opérations.

>[!info]
>Cela donne naissance à ce que l'on appelle les **langages réguliers**.

>[!tip] Définition
>Soit $\Sigma$ un alphabet. La classe des langages **réguliers** ou **rationnels**, notée $Reg(\Sigma)$ est définie par induction par : 
>- $\emptyset \in Reg(\Sigma)$
>- $\left\{ \varepsilon \right\} \in Reg(\Sigma)$
>- $\left\{a\right\} \in Reg(\Sigma)$ pour $a \in \Sigma$
>- $L_1,L_2 \in Reg(\Sigma) \Rightarrow L_1 \cup L_2 \in Reg(\Sigma)$
>- $L_1,L_2 \in Reg(\Sigma) \Rightarrow L_1 \cdot L_2 \in Reg(\Sigma)$
>- $L_1 \in Reg(\Sigma) \Rightarrow L^* \in Reg(\Sigma)$
> 
>Les langages réguliers sont donc exactement les langages que l'on peut contruire à partir des langages $\emptyset, \left\{\varepsilon \right\}, \left\{a\right\}$ et par opérations régulières.
>

>[!todo] Rappel
>"par induction" veut dire que $Reg(\Sigma)$ est **la plus petite classe de langages** qui contient $\emptyset, \left\{\varepsilon\right\}$, tous les langages $\left\{a\right\}$ avec $a \in \Sigma$ et qui sont **stables** par opérations régulières.

>[!info] Conséquence
>Si $L \in Reg(\Sigma)$ est un langage régulier : 
>- soit $L = \emptyset$
>- soit $L = \left\{\varepsilon\right\}$
>- soit $\exists a \in \Sigma$ tel que $L = \left\{a\right\}$
>- soit $\exists L_1,L_2 \in Reg(\Sigma)$ tel que $L = L_1 \cup L_2$ ou $L = L_1 \cdot L_2$ ou $L = L_1^*$

>[!info] Remarque
>$\left\{\varepsilon\right\}$ est redondant (inutile) car $\emptyset^* = \left\{\varepsilon\right\}$.

>[!tip] Propriété
>$\Sigma^* \in Reg(\Sigma)$ est régulier.

>[!note] Démo
>Si $\Sigma = {a_1,...,a_n}, \Sigma = \bigcup_{i \in I} {a_i}$ (union fini de langages réguliers)
>Donc $\Sigma^*$ est régulier par étoile d'un langage régulier.

>[!tip] Propriété
>Tout langage fini est régulier.

>[!note] Démo
>On commence par montrer que si $u = u_1...u_n \in \Sigma^*$ 
>Par récurrence sur $n = |u|$. Si $n = 0$ alors $u = \varepsilon$ et $\left\{\varepsilon\right\}$ est régulier par cas de base. Si le résultat est vrai pour $n \in \mathbb{N}$ et si $|u| =n+1$ alors par HR sur $v = u_1...u_n$  On a $\left\{v\right\}$ régulier. De plus $\left\{u_{n+1}\right\}$ est régulier par cas de base. Donc $\left\{v\right\} \cdot \left\{u_{n+1}\right\}$ est régulier par concaténation.
>On a ensuite $L = \bigcup_{i \in I} {u_i}$ avec $\forall i \in I, u_i \in L$ et $|I|$ fini.
>Donc $L$ est régulier par union finie de langages réguliers.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Remarque
>Si toute réunion (même infinie) de langages réguliers est un langage régulier alors tous les langages seraient réguliers.
>$L = \bigcup_{u \in L} \left\{u\right\}$.

>[!warning] 
>Les langages réguliers ne sont pas stables par réunion infinie : seulement finie.

>[!info] Remarque
>- **régulier** : nouveau programme
>- **rationnel** : ancien programme

## III - Expressions régulières

>[!note] 
>On va se donner une **notation** pour dénoter / décrire simplement les langages réguliers ainsi que pour spécifier des **motifs** à rechercher dans un texte.

>[!info] Remarque
>En anglais : regular expression : regexp ou regex ou re

>[!tip] Définition
>Soit $\Sigma$ un alphabet. On définit inductivement les expressions régulières, notées $\mathcal{E}(\Sigma)$, par induction :
>- $\varnothing \in \mathcal{E}(\Sigma)$
>- $\varepsilon \in \mathcal{E}(\Sigma)$
>- $a \in \mathcal{E}(\Sigma), \forall a\in \Sigma$
>- $e_1, e_2 \in$ $\mathcal{E}(\Sigma)$ $\Rightarrow (e1|e2) \in$ $\mathcal{E}(\Sigma)$
>- $e_1, e_2 \in$ $\mathcal{E}(\Sigma)$ $\Rightarrow (e1 \cdot e2) \in$ $\mathcal{E}(\Sigma)$
>- $e \in$ $\mathcal{E}(\Sigma)$ $\Rightarrow e^{\star} \in$ $\mathcal{E}(\Sigma)$
> 
> Une expression régulière est donc une fonction construite à partir des cas de bases  et des opérations.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Remarque 
>$(e_1|e_2)$ se lit $e_1$ ou $e_2$
>>[!warning]
>>On trouve souvent $e_{1} + e_{2}\text{(pas le même que le + standart)}$ 

>[!example] Exemple
>$\Sigma = \left\{a,b\right\}$
>>[!done]
>>$(a|b)$
>>$((a|b) \cdot \varnothing)$
>>$((a \cdot \varepsilon)^\star(b))$
>
>>[!error] 
>>$\star \cdot (a())$
>>bonjour
>>$a \cdot b \cdot c$
>>$((a|b))^\star$

>[!note] Implémentation OCaml
>```ocaml
>type regex = 
>	|Empty
>	|Epsilon
>	|Lettre of char
>	|Or of regex * regex
>	|Concat of regex * regex
>	|Star of regex
>```

>[!example]
>$((a^\star \cdot b) | c)$ sur $\Sigma = \left\{a,b,c\right\}$
>```ocaml
>let e = Or(Concat(Star(Lettre 'a'), Lettre 'b'), Lettre 'c')
>```

>[!info] Remarque :
>Arbre associé à une expression régulière :
> ```mermaid
>
> flowchart TD
>
> A["|"] --> B["."] --> C["*"] --> D["a"]
>
> B --> E["b"]
>
> A --> F["c"]
>
>```

>[!tip] Définition
>La taille et la hauteur d'une expression régulière sont celle de son arbre associé.

>[!bug] Exercice
>Ecrire une fonction taille : regex -> int et une fonction hauteur : regex -> int qui calcule respectivement la taille et la hauteur d'une expression régulière.

>[!info] Remarque
>Le parenthésage donne une syntax très lourde.

>[!info] Règle d'abus
>On enlève les $\cdot$ concaténation implicites
>On enlève les parenthèses pour associativité $+$ et $\cdot$
>On utilise les exposants
>On définit des priorités : $\star$ prioritaire sur $\cdot$ prioritaire sur $+$
>On introduit l'exposant $+$

>[!warning] 
>$a^{+}$ n'est pas une regex, c'est une notation pour $a a^{\star}$

>[!info] Remarque
>analogie avec $(+, \times, -1)$

>[!tip] Définition
>Une expression régulière est **linéaire** si chaque caractère de $\Sigma$ y apparaît au plus une fois. On a donc $e \in \mathcal{E}(\Sigma)$ linéaire si et seulement $\forall a \in \Sigma, |e|_{a} \leq 1$

>[!example] Exemple
>$((ab)c^\star | ef)^\star d$ est linéaire.
>$((a | b)c)^\star a$ n'est pas linéaire.
>$a^{+}$ n'est pas linéaire.

>[!warning] 
>Les abus ne doivent être utilisés que pour simplifier les écritures lorsqu'ils n'y a pas d'ambiguité.
>Toujours revenir à la bonne formalisation si on étudie des propriétés.

>[!example] Exemple
>arbre associé à : $a^3$

## IV - Les expressions aux langages réguliers

>[!tip] Définition
>A toute expression régulière, $e \in \mathcal{E}(\Sigma)$, on associe un langage $L(e)$ appelé langage dénoté par $e$, par induction :
>- si $e = \varnothing$ alors $L(e) = \emptyset$
>- si $e = \varepsilon$ alors $L(e) = \left\{\varepsilon\right\}$
>- si $e = a$ avec $a \in \Sigma$ alors $L(e) = \left\{a\right\}$
>- si $e = (e1|e2)$ avec $e1,e2 \in$ $\mathcal{E}(\Sigma)$ alors $L(e) = L(e1) \cup L(e2)$
>- si $e = (e1 \cdot e2)$ avec $e1,e2 \in$ $\mathcal{E}(\Sigma)$ alors $L(e) = L(e1) \cdot L(e2)$
>- si $e = e_1^\star$ avec $e_1 \in$ $\mathcal{E}(\Sigma)$ alors $L(e) = L(e1)^*$

>[!info] Remarque
>Abus de notation : $L(ab^{*}(b)) = ab^*+b$

>[!tip] Propriété
>Un langage $L \subseteq \Sigma^*$ est un langage régulier si et seulement s'il est dénoté par une expression régulière, c'est-à-dire si et seulement s'il existe une expression régulière $e \in$ $\mathcal{E}(\Sigma)$ telle que $L = L(e)$. On dit que $e$ dénote $L$ et que $L$ est dénoté par $e$.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!warning] 
>$e$ n'est pas unique

>[!example] Exemple
>$L((a|b)) = L((b|a))$

>[!info] Remarque
>C'est souvent la définition pour les langages réguliers (définition alternative).

>[!note] Démo 
>Par induction
>Dans un sens, si un langage est régulier alors il existe une expression régulière qui le dénote :
>- si $L = \varnothing$, alors $L = L(\varnothing)$
>-  si $L = \left\{\varepsilon\right\}$, alors $L= L(\varepsilon)$
>- Si $L = \left\{a\right\}$ avec $a \in \Sigma$, alors $L = L(a)$
>- Si $L = L_1 \cup L_2$ avec $L_1,L_2$ réguliers alors par induciton $\exists e_1,e_2 \in$ $\mathcal{E}(\Sigma)$ telles que $L_1=L(e_1)$ et $L_2=(e_2)$ et $L=L((e_1|e_2))$
>- Etc ...
>
>Dans l'autre sens on fait de même par induction sur les expressions régulières.
>- Si $e = \varnothing$ alors $L(e) = \varnothing$ est régulier (cas de base)
>- Etc

>[!tip] Propriété
>Un langage $L \in \Sigma^*$ est un langage régulier si et seulement s'il est dénoté par une expression régulière c'est-à-dire si et seulement s'il existe une expression régulière
> $e\in$ $\mathcal{E}(\Sigma)$

>[!tip] Définition
>On dit que deux expressions régulière $e_1,e_{2}\in \mathcal{E}(\Sigma)$ sont **équivalentes**, ce que l'on note $e_{1}\equiv e_2$ si elles dénotent le même langage, c'est-à-dire si $L(e_1)=L(e_2)$.

>[!example] Exemple
>$a|a^{*} | \varnothing = a^*$
>On cherche une expression "la plus simple possible" à équivalence près.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Remarque
>$(a|b) \neq (b|a)$ mais $(a|b) \equiv (b|a)$

>[!info] Remarque
>Ceci justifie les abus de notation associative $(a \cdot (a \cdot a)) \equiv ((a \cdot a) \cdot a)$.

