```toc
```
## Note DS 1

>[!example] Exemple
>$a \in \mathbb{N^*}$
>$a^{n}$ avec $n-1$ multiplications :
```C
double expo(double a, int n)
{
	double res = a;
	for (int i = 1; i < n; i++)
	{
		res = res * a;
	}
	return res;
}
```

>[!info] Remarque
>En C, les boucles `for` sont des boucles `while` déguisées.(sucre syntaxique)
```C
for (int i = 0; i < n; i++)
	{
	//
	}
// Equivalent à
{
	int i = 0;
	while (i < n)
	{
	//
	i++;
	}
}
```

>[!done] Rappel
>Un **variant** de boucle est une quantité entière naturelle $c \in \mathbb{N}$ strictement décroissante.

>[!example] Exemple
>$n-i$ :
>- $\in \mathbb{Z}$
> - $\geq 0$ 
> - décrémente de 1 donc strictement décroissante. 

>[!info] Remarque
>De manière plus générale, on peut prendre une quantité strictement décroissante dans un ensemble bien fondé.

>[!info] Remarque
>On peut aussi prendre une suite de $\mathbb{Z}$ strictement décroissante minorée ou strictement croissante majorée.

>[!done] Rappel
>Un **invariant** de boucle est une **propriété** qui est : 
>- vérifiée avant le tout premier tour de boucle (***Initialisation***)
>- conservée par un tour de boucle (si c'est vrai avant de rentrer dans la boucle et que l'on rentre dans la boucle (c'est-à-dire si on a la condition de boucle) alors c'est encore vrai en sortant du tour de boucle) (***conservation***)

>[!example] Exemple d'invariant
>" 42 = 42 " 
>"$\mathcal{P}: res = a^i$, on a fait $i-1$ multiplications et $i\leq n$"
>Montrons que $\mathcal{P}$ est un invariant.
>***Initialisation*** : Avant le premier tour de boucle on a $res = a, i = 1$, on a fait 0 multiplication et $i \leq n$ car $n \in \mathbb{N}$.
>***Conservation*** : On suppose que $\underline{res} = a^\underline{i}$, que l'on a fait $i-1$ opérations et $\underline{i} \leq n$. Si on rentre dans la boucle, on a de plus $\underline{i} < n$.
>Après la ligne $\overline{res}= \underline{res}* a$ on a $\overline{res} = a^{\underline{i}+1}$ et on a fait $\underline{i}-1+1=\underline{i}$ multiplications.
>Après incrément $\overline{i}=\underline{i}+1$
>Donc $\overline{res}=a^\overline{i}$, on a fait $\overline{i}-1$ multiplications et $\overline{i} \leq n$ et même $\overline{i} \leq n$. 
>L'invariant est conservé.
>***Correction*** : On veut montrer que la fonction $a^n$ en faisant $n-1$ multiplications. 
>Après le dernier tour de boucle on a :
>- $res = a^i$ avec $i-1$ multiplications (***invariant***)
>- $i \geq n$ (***négation de la condition de boucle***)
>
>Donc $res = a^n$ et $n-1$ opérations car 

## Note DS 2

Lire le sujet 
Donner des noms explicites
Commenter et expliquer les fonctions
toutes fonctions auxiliaires sans commentaire $\rightarrow$ non lues


```OCaml
let rec ajout x a = 
	match a with 
		|V -> N(v,x,V)
		|N(g,y,d) -> if x < y then N(ajout x g,y,d) else N(g,y,ajout x d)
```
**Preuve par induction**
Ce n'est pas : On ajoute une feuille à un arbre de taille $n$.

**Soigneusement** = rigoureusement en suivant la méthode 

```OCaml
a::b::c
(* 'a 'b 'a list
= *)
a::(b::c)
(*!=*)
(a::b)::c
(*'a list 'a list list*)
```

Ne pas faire de fonction récursive terminal si ce n'est pas demandé ou nécessaire !

```OCaml
List.iter : ('a -> unit) -> 'a list -> unit
List.iter f [a0;a1;...;a(n-1)] = f a0; f a1; .... ; f a(n-1); ()

List.map : ('a -> b) -> 'a list -> 'b list
List.map f [a0;a1;...;a(n-1)] = [f a0; f a1; .... ; f a(n-1)]
```

>[!question] $G$ convexe, $|S| \geq 2, s \in S$. Montrer qu'il existe un arête incidente à $s$. 
>Soit $a \in S \backslash \left\{s\right\}$, possible car $|S|\geq 2$, comme $G$ est connexe, il existe un chemin $s=x_{0},x_{1},\dots,x_{n}=a, (x_{0},x_{n})$ est incident à $s$ avec $n \geq 1$ car $u \neq s$.


