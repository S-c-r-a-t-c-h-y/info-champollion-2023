# Chapitre 1 : Arbres binaires de recherche équilibrés

```toc
```

## I - Rappels

>[!info] 
>On définit un arbre binaire par induction.
 
>[!warning]
>Deux grandes définitions possibles (et autant de possible que de sujet de concours).

>[!tip] Définition : arbre vide 
> Un arbre est soit un arbre vide, soit un nœud constitué de deux sous-arbres : gauche et droit.

>[!info] Remarque
> Une feuille est alors un nœud avec deux fils vides.
>

>[!tip] Définition : arbre avec définition feuille-nœud
>Un arbre est soit une feuille soit un nœud constitué de deux sous-arbres : gauche et droit.

>[!info] Remarque
>Il n'y a pas d'arbre vide dans ce cas.

>[!todo] Rappel
>La définition par induction considère des cas de bases et des règles inductives et construit le plus petit ensemble compatible avec cela.
>$\rightarrow$ preuve par induction / récurrence

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!note]
>On peut ajouter des étiquettes, ou plein d'informations supplémentaires sur les nœuds.

## II - Arbres binaires de recherche

>[!tip] Définition
>Un arbre binaire de recherche est un arbre binaire étiqueté sur un ensemble muni d'un **ordre total** qui vérifie de plus la propriété suivante : 
>L'étiquette de **tout** nœud est inférieure (ou égale) à celle de **tous** les nœuds de son sous-arbre droit et supérieures (ou égale) à celles de son sous-arbre gauche.

>[!example]
>Ce n'est pas un ABR mais localement vrai
>```mermaid
>flowchart TD
>A[3] --> B[2] --> C[0]
>A --> D[8]
>B --> E[4]
>```

>[!tip] Propriété
>Un arbre binaire est un arbre binaire de recherche si et seulement si la liste des étiquettes du parcours infinie est croissante.
>(preuve par induction)

>[!info] Remarque
>En général, on suppose tous les éléments distincts.

>[!info] Intérêt
>La recherche, l'insertion et la suppression sont efficaces en $\theta(h)$ avec $h$ la hauteur de l'arbre.
>Dans le meilleur cas "l'arbre est équilibré" et sa hauteur est logarithmique en le nombre de nœuds. Les opérations sont en $O(log(n))$ avec $n$ le nombre de nœuds.
>Mais dans le pire cas, on peut avoir $h=n-1$. C'est le cas si on insère des éléments triés.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Remarque
>On peut montrer que si les données sont insérées dans un ordre aléatoire alors en espérance la hauteur de l'arbre est en $O(log(n))$.

## III - Arbres équilibrés et notations

>[!tip] Définition
>Soit $A$ un ensemble ABR et $C>0$, on dit que $A$ est $C$-équilibré si pour tout $a \in A, h(a) \leq C log(n(a))$.

>[!note]
>On cherche donc à construire des familles d'arbres équilibrés pour garantir des opérations en $O(log(n))$.
>On cherche donc des mécanismes pour équilibrer les arbres avec un **petit** surcoût (idéalement en temps constant).
>$\rightarrow$ AVL, rouge-noir, B-arbre,...

>[!tip] Définition
>Une rotation est une opération sur un ABR qui permet de réduire un déséquilibre tout en maintenant les propriétés d'un ABR. Elles sont utilisées pour les structures de données d'arbre équilibrés.

>[!example]
>```mermaid
>flowchart TD
>A[x] --> B[y]
>A --> C[a]
>B --> D[b]
>B --> E[c]
>```
>Après rotation
>```mermaid
>flowchart TD
>A[y] --> C[c]
>A --> B[x]
>B --> D[a]
>B --> E[b]
>```
>On a bien conservé la caractérisation ABR.
>Coût de la rotation : $O(1)$ (en fonction de l'implémentation).

>[!bug] Exercice
>Faire le dessin pour la rotation droite.

>[!info] Implémentation OCaml et rotation gauche
>```OCaml
>type 'a arbre = 
>	|V
>	|N of 'a arbre * 'a * 'a arbre
>
>let rotation_gauche t = 
>	match t with 
>		|N (a, x, N (b, y, c)) -> N (N (a, x, b), y, c)
>		_ -> t
>```

>[!info] Implémentation C et rotation gauche
> ```c
>typedef struct noeud {
>	int cle;
>	struct noeud* gauche;
>	struct noeud* droit;
>	struct noeud* parent;
>} noeud;
>
>void roration_gauche(noeud* x){
>	noeud* y = x->droit;
>	x->droit = y->gauche;
>	y->gauche = x;
>}
>```

>[!bug] Exercice
> - **Sans regarder**, écrire les programmes pour les rotations droites en OCaml et en C.
> - Faire le dessin pour droite-gauche.

>[!note] 
>En pratique ces quatres rotations (deux simples, deux doubles) suffisent pour tout traiter.

>[!info] Implémentation OCaml rotation gauche-droite
>```OCaml
>let rotation_gauche_droite a =
>	match a with
>		|N (l, z, r) -> rotation_droite (rotation_gauche N(l, z, r))
>		| _ -> a
>
>let rotation_gauche_droite a =
>	match a with 
>		|N(N(N(a,x,b),y,c),z,d) -> N(N(a,x,b),y,N(c,z,d))
>		| _ -> a
>```

## IV - Arbre AVL

>[!tip] Définition
>Les arbres AVL sont des arbres binaires de recherche "bien équilibrés" (la différence de hauteur en tout nœud est au plus 1).
>D'après la dernière question du DM 1, ces arbres sont donc équilibrés (on a $h = O(log(n))$ ).
