# Chapitre 6 : Parcours en profondeur et applications

```toc
```

>[!info] Remarque
>A graphe $G=(V,E)$ has **vectrices** $V$ ($v \in V$ is a **vertex**) and **edges** $E$ ($e \in E$ is an **edge**).

## I - Représentation des graphes

>[!note]
>Dans toute cette partie, pour simplifier, on suppose que $S = \left\{0,n-1\right\}$.
>Les algorithmes de parcours ont besoin de **deux** primitives sur un graphe $G=(S,A)$ : 
>- Une fonction `size : graphe -> int` qui calcule $n=|S|$, **l'ordre** du graphe, c'est-à-dire le nombre de sommets.
>- Une fonction `neighbors : graph -> int -> int list` qui renvoie la liste des **successeurs** (ou **voisins**) d'un sommet du graphe.

>[!info] Remarque
>Une représentation par liste d'adjacence permet de réaliser ces deux primitives en $O(1)$.

>[!example] Exemple d'implémentation
>```OCaml
>type graphe = int list array
>
>(*Crée un graphe à n sommets isolés*)
>let create n = Array.make n []
>
>let size = Array.lenght
>
>let has_edge g u v = 
>	List.mem v g.(u)
>
>let add_edge g u v = 
>	if not (has_edge g u v) then g.(u) <- v :: g.(u)
>
>let neighbors g u = g.(u)
>```

>[!info] Remarque
>Pour un graphe non orienté, il y a juste à modifier la fonction `add_edge`.
>```OCaml
>let add_edge g u v =
>	if not (has_edge g u v) then begin 
>		g.(u) <- v :: g.(u);
>		g.(v) <- u :: g.(v)
>	end
>```

>[!info] Remarque
>Ceci est **un** exemple d'implémentation, mais toute implémentation est possible puisque l'on n'utilise que l'interface.

## II - Parcours en profondeur

### A - Le squelette de l'algorithme

>[!tip] Définition
>Parcours en profondeur : depth-first-search (DFS)
>Il s'agit d'un algorithme **fondamental**.

>[!info] Remarque
>Enormément d'algorithme **utilisent** le parcours en profondeur ou en sont des **adaptations/extensions**. Il faut être absolument au clair avec ce chapitre.

>[!Summary] Idées
>1) Explorer tant que c'est possible avant de faire machine arrière (backtracking)
>2) On marque les sommets pour ne pas les reparcourir.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Implémentation
>**Algorithme**
>```pseudo
>    \begin{algorithm}
 >   \caption{Parcours en profondeur}
>  \begin{algorithmic}
>        \Function{Explorer}{$u$}
>            \If{$u$ non marqué}
>                \State Marquer($u$)
>            \EndIf
>            \For{chaque sommet $u->v$}
>                \State Explorer($v$)
>            \EndFor
>        \EndFunction
>        
>        \Function{DFS}{$g$}
>            \State démarquer chaque sommet dans $S$
>            \For{chaque sommet $u$ dans $S$}
>                \State Explorer($u$)
>            \EndFor
>        \EndFunction
>    \end{algorithmic}
>    \end{algorithm}
>```
>
>**Implémantaion OCaml**
>
>```OCaml
>let dfs g =
>	let n = size g in 
>	let vu = Array.make n false in 
>	let rec explorer u =
>		if not vu.(u) then begin
>			vu.(u) <- true;
>			List.iter explorer (neighbors g u)
>		end
>	in 
>	for i = 0 to n-1 do
>		explorer i
>	done;
>```

>[!info] Remarque
>On peut aussi (en pratique, cela accélère légèrement les choses) supposer dans `explorer` que le sommet n'est pas marqué et effectuer cette vérification avant tout appel à `explorer`.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Autre implémentation (être plus flexible)
>```OCaml
>let rec explorer g u vu = 
>	assert (not vu.(u));
>	vu.(u) <- true;
>	explorer_voisins g (neighbors g u) vu
>and explorer_voisins g voisin vu =
>	match voisins with
>		|[] -> ()
>		|v::suite -> 
>			if not vu.(v) then begin
>				explorer g v vu
>			end;
>			explorer_voisins g suite v
>
>let dfs g = 
>	let n = size g in 
>	let vu = Array.make n false in
>	for i = 0 to n-1 do
>		if not vu.(i) then explorer g i vu
>	done
>```

>[!info] Remarque
>Cet algorithme fonctionne à **l'identique** que le graphe soit orienté ou non.

>[!note] 
>Pour $G=(S,A), u \in S$ on note `accessible(u)` l'ensemble du sommets accessibles à partir de $u \in G$ (c'est sa composante connexe **si** le graphe est non orienté).

>[!tip] Propriété 
>En supposant qu'aucun sommet de `accessible(u)` n'est marqué, la fonction `explorer(u)` parcours et marque chaque sommet de `accessible(u)` et exactement ceux-là.
>
>Complexité avec `size` et `neighbors` en $O(1)$  est $O(|S|+|A|)$.

### B - Forêts de parcours

>[!note]
>On peut facilement modifier l'algorithme précédent pour retenir les **arcs** qui nous ont permis de découvrir un sommet pour la première fois. Ces arcs sont appelés **arcs de parcours**.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!tip] Proposition
>L'ensemble des sommets et des arcs de parcours forment une forêt (graphe acyclique) appelée forêt de parcours.
>Comme pour `unir` et `trouver` on peut représenter cette forêt par un tableau `père`.

>[!info] Remarque
>On peut se passer de `vu` avec la convention `pere.(u) = -1` si $u$ n'est pas marqué.

>[!info] Remarque
>Il n'y a pas unicité d'une forêt de parcours.

>[!example] Exemple
>```mermaid
>flowchart RL
subgraph Forêt 2
direction LR
I[A]
J[C] --> K[D] --> L[B]
end
subgraph Forêt 1
direction LR
E[A] --> F[B] --> G[C] --> H[D]
end
subgraph Graphe
direction LR
A --> B --> C --> D --> B
end
>```

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Implémentation
>```OCaml
>let dfs g =
>	let n = size g in 
>	let pere = Array.make n (-1) in
>	let rec explorer p u =
>		if pere.(u) = -1 then begin 
>			pere.(u) <- p;
>			List.iter (explorer u) (neighbors g u)
>		end
>	in
>	for i = 0 to n-1 do
>		explorer  i i
>	done;
>	pere	
>```

### C - Préviste et postvisite

>[!note]
>En l'état, l'algorithme ne fait rien d'autre que parcourir et marquer les sommets.

>[!tip] Définition
>On peut effectuer quelque chose lorsqu'on rencontre le sommet (**prévisite**) ou lorsque l'on en a terminé avec lui (**postvisite**).
>C'est-à-dire soit avant, soit après avoir exploré récursivement les voisins.

>[!info] Implémentation
>```OCaml
>let rec explorer u =
>	if not vu.(u) then begin
>		vu.(u) <- true;
>		previsiter u;
>		List.iter explorer (neighbors u);
>		postvisiter y
>	end
>```

>[!info] Remarque
>Les fonctions `previsiter` et `postvisiter` peuvent faire quelque chose d'intéressant.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!example] Exemple
>```OCaml
>let previsiter u =
>	Printf.printf "%d; " u
>let postvisiter u = ()
>```

>[!info] Remarque
>Un parcours dans **l'ordre préfixe** s'obtient avec `privister` et dans **l'ordre postfixe ou suffixe** avec `postvisiter`.

>[!note]
>Il peut être utile de retenir le temps de première découverte et de fin de traitement de chaque sommet.
>```OCaml
>let horloge = ref 0 in
>let pre = Array.make n (-1) in
>let post = Array.make n (-1) in
>let previsiter u =
>	incr horloge;
>	pre.(u) <- !horloge;
>in 
>let postvisiter u =
>	incr horloge;
>	post.(u) <- !horloge
>```

>[!info] Remarque
>On va noter `pre(u)` et `post(u)` les temps de débuts et fin de traitement de $u$.
>Remarquons que l'on affecte la valeur `pre(u)` immédiatement après avoir ajouté $u$ sur la pile (de récursion) et que l'on affecte la valeur `post(u)` immédiatement avant d'enlever $u$ de la pile.
>Donc l'intervalle de temps [`pre(u);post(u)`] correspond exactement à l'intervalle de présence de $u$ sur la pile.
>On dit que $u$ est **actif** ou **en cours de traitement** ou sur la pile, pendant cet intervalle.
>On dit aussi que $u$ commence au temps `pre(u)` et termine au temps `post(u)`.
>On note $\underset{u}[\ \underset{u}]$ l'intervalle [`pre(u);post(u)`].
>
>Il s'ensuit que pour deux sommets $u,v \in S$ les intervalles [`pre(u);post(u)`] et [`pre(v);post(v)`] sont disjoints ou l'un est inclus dans l'autre. De plus, si [`pre(u);post(u)`] contient [`pre(v);post(v)`] si et seulement si $v$ a été découvert pendant l'exploration de $u$.
>$\underset{u}[ \ \underset{v} [\ \underset{v}]\ \underset{u}]$

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!tip] Propriété
>Notons $u_{1},...,u_p$ les sommets empilés. Alors $\forall i \in [|1,p|], \ u_i$ a été découvert par $u_{i-1}$. Donc $(u_{i-1},u_i)$ est un arc de parcours.

>[!tip] Propriété
>Si $u$ se trouve en même temps que $v$ sur la pile et si $u$ précède $v$ sur cette pile alors $u$ est ancêtre de $v$ dans la forêt de parcours, $v$ a été découvert pendant l'exploration de $u$ et on a $\underset{u}[ \ \underset{v} [\ \underset{v}]\ \underset{u}]$.

>[!tip] Propriété
>L'ordre total sur les sommets donné par pré s'appelle **ordre de prévisite** et celui donné par post s'appelle **ordre de postvisite**.
>A un moment $t$ de l'horloge pour un sommet $s \in S$ il y a trois possibilités :
>1) $t < pre(u)$, on dit alors que $u$ est **vierge**. Le sommet n'a encore jamais été exploré. L'appel `explorer(u)` n'a pas eu lieu.
>2) $t \in [pre(u), post(u)]$, on a alors $u$ est **actif**, en cours de traitement, sur la pile. L'appel à `explorer(u)` a eu lieu mais n'est pas terminer.
>3)$t > post(u)$, on dit que $u$ est **terminé**. L'appel `explorer(u)` est terminé.

### D - Classification des arcs (cas orienté)

>[!tip] Définition
>On peut distinguer quatre catégories possibles pour un arc $a \in A$ de $G = (S,A)$ après un parcours en profondeur.
>1) **Les arcs de parcours** (tree edge) qui font partie de l'arborescence de parcours.
>2) **Les arcs arrières** (back edge) qui relient un sommet à un de ses ancêtres.
>3) **Les arcs avant** (forward edge) qui relient un sommet à un de ses descendants propres.
>4) **Les arcs transverses** qui sont les autres.

>[!tip] Propriété
>Considérons un arc $(u,v)$. La catégorie dépend de l'état de $v$ lors du traitement de cet arc, c'est-à-dire lorsqu'on s'intéresse au voisin $v$ de $u$ pendant l'exploration de $u$. A ce moment, $u$ est actif.
>1) Si $v$ est **vierge**, alors on aura $u$ qui sera le père de $v$ (`pere(v)=u`) et on aura $(u,v)$ un arc de **parcours**. On aura l'imbrication $\underset{u}[ \ \underset{v} [\ \underset{v}]\ \underset{u}]$.
>2) Si $v$ est **actif**, alors il est sur la pile (avant $u$ puisque l'on est en train de traiter $u$). Donc $u$ est un descendant de $v$. On a alors un arc **arrière** et on aura l'imbrication $\underset{v}[ \ \underset{u} [\ \underset{u}]\ \underset{v}]$.
>3) Si $v$ est **terminé**, $v$ a été entièrement traité avant $u$. Donc en particulier $u$ n'est pas accessible à partir de $v$. Le type d'arc dépend du moment où on a découvert $v$.
> 	  1) Si on a découvert $v$ avant $u$ ($pre(v) < pre(u)$). On a alors $\underset{v}[ \ \underset{v} [\ \underset{u}]\ \underset{u}]$ et on a un arc **transverse**.
> 	  2) Si $v$ a été découvert après $u$ (on aura $pre(v)>pre(u)$) comme $u$ n'est pas terminé, $v$ a été découvert pendant l'exploration des descendants de $u$. Donc $v$ est descendant de $u$. On a un arc **avant** et l'imbrication $\underset{u}[ \ \underset{v} [\ \underset{v}]\ \underset{u}]$.

>[!info] Remarque
> $\underset{u}[ \ \underset{u} [\ \underset{v}]\ \underset{v}]$ est impossible en présence d'un arc $(u,v)$.

>[!info] Remarque
>On peut a posteriori les classifications des arêtes à partir des tableaux `pre` et `post`.

>[!tip] Proposition
>Considérons un parcours en profondeur d'un graphe $G = (S,A)$ et deux sommets $u,v \in S$ les propositions suivantes sont équivalentes : 
>1. $u$ est ancêtre de $v$ dans la forêt de parcours.
>2. $\underset{u}[ \ \underset{v} [\ \underset{v}]\ \underset{u}]$.
>3. Au moment de l'appel à `explorer(v)`, $u$ est actif.
>4. Juste avant l'appel `explorer(u)`, il existe un chemin de $u$ à $v$ dont tous les sommets sont vierges.

>[!bug] Exercice
>Faire la démo.

>[!info] Remarque
>Dans le cas d'un graphe non orienté, il n'y a que deux types d'arêtes : les arêtes de parcours et les autres.

### E - Graphes orientés acycliques

>[!note]
>Les graphes orientés acycliques (en anglais : directed acyclic graph (dag)) jouent un rôle important en informatique et en particulier en programmation dynamique.

>[!tip] Définition
>Un sommet sans prédécesseur est appelé une source.
>Un sommet sans sucesseur est appelé un puits.


>[!info] Remarque
>Un sommet isolé est à la fois une source et un puits.

>[!tip] Proposition
>Un graphe orienté acyclique comporte une source et un puits.

>[!bug] Exercice
>Faire la démo.

>[!info] Remarque
>Si lors d'un parcours en profondeur, on identifie un arc arrière $(u,v)$, autrement dit $\underset{v}[ \ \underset{u} [\ \underset{u}]\ \underset{v}]$, c'est-à-dire $v$ actif, alors $u$ est descendant de $v$ dans la forêt de parcours : il existe un chemin $x_{0}...x_{k}$ avec $x_{0}=v, x_{k}=u$, donc $x_{0}...x_{k}v$ est un cycle du graphe (ou du moins, on peut en extraire un cycle).
>
>Inversement, s'il existe un cycle dans le graphe. Soit $v_0$ le premier sommet de ce cycle exploré par le parcours. Notons $v_{0}...v_{k}v_{0}$ le cycle. Alors $(v_{k},v_{0})$ sera un arc arrière car $v_k$ sera visité pendant l'exploration de $v_0$.

>[!tip] Proposition
>Un graphe $G=(S,A)$ comporte un cycle si et seulement si pour tout parcours en profondeur de ce graphe, ce parcours comporte un arc arrière.

>[!note] 
>En pratique, pour détecter un arc arrière, on peut vérifier dans le traitement d'un sommet $u$ si on rencontre un sommet $v$ actif (commencé mais non terminé, sinon c'est un arc transverse ou avant).

>[!info] Implémentation
>On peut utiliser trois status et un tableau de marque de type `statut array` plutôt que `bool array`.
>```OCaml
>type statut = 
>	|Vierge
>	|Actif
>	|Fini
>```

>[!info] Remarque
>- On peut aussi utiliser les tableaux `pre` et `post` pendant le parcours ou même à la fin, après le parcours.
>- **Rappel** : on a un arc arrière $(u,v)$ si et seulement si $\underset{v}[ \ \underset{u} [\ \underset{u}]\ \underset{v}]$, donc il suffit aussi, après le parcours, d'itérer sur les arcs à la recherche d'un arc $(u,v)$ avec `pre(v)`<`pre(u)`<`post(u)`<`post(v)`.
>- Tout ceci est bien plus simple pour un graphe non orienté. Il y a un cycle si et seulement si on retombe sur un sommet déjà vu (différent de son père). Il y a simplement besoin de deux status (donc un `bool array`).

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

### F - Tri topologique

>[!note] 
>Les dags sont utiles pour modéliser des dépendances ou des causalités. Chaque nœud est une tâche et il y a un arc entre deux tâches si l'une doit être effectuée avant l'autre.
>

>[!question]
>Dans quel ordre effectuer ces tâches. S'il y a un cycle, peu d'espoir. Ce qui motive les dag. La question est alors de linéariser, ou tirer topologiquement. Intuitivement, cela consiste à la représenter sur une ligne avec toutes les flèches qui vont de gauche à droite.

>[!tip] Définition
>Un ordre topologique (ou un tri topologique) est une relation d'ordre total $<$ sur $S^2$ compatible avec $A$, c'est-à-dire que si $(u,v) \in A$, alors $u < v$.

>[!note]
>On peut alors se demander quels sont les dags qui peuvent être linéarisés ?
>Il sont tous linéarisable.

>[!tip] Proposition
>Pour un dag, si $(u,v) \in A$, alors $\text{post(u)} > \text{post(v)}$.

>[!note]
>Sinon, aurait un arc arrière, donc un cycle dans un dag : absurde.
>
>Donc la relation $u < v$ définie par $\text{post(u)} > \text{post(v)}$ est un ordre topologique. Il suffit donc de trier les sommets par ordre décroissant de fin de traitement.

>[!info] Remarque
>Il suffit par exemple de les mettre dans une pile lorsque l'on en a terminé avec eux.
>
>Trouver un ordre topologique, vérifier que l'on est un dag et absence d'arcs arrières ne sont qu'une même chose.

>[!note]
>Mais alors, comment trouver un puits ?
>Il suffit de prendre le premier sommet terminé d'un parcours. De même, pour trouver une source, il suffit de prendre le dernier sommet terminé d'un parcours.

## III - Composantes fortement connexes (d'un graphe orienté)

### A - Composantes connexes dans un graphe non orienté

>[!note]
>Il est très facile de modifier le parcours en profondeur pour identifier les composantes connexes d'un graphe non orienté.

>[!info] Remarque
>Si $u \in S$ alors `explorer(u)` visite `accessible(u)` qui est la composante connexe de $u$.
>```OCaml
>let id = ref 0
>let composante = Array.make n (-1)
>let previsiter s = composante.(s) <- !id
>```
>De plus, dans le parcours en profondeur, on ajoute :
>```OCaml
>for i = 0 to n-1 do
>	if composante.(i) = -1 then begin
>		explorer i;
>		incr id
>	end
>done;
>composantes, id
>```

### B  - Composantes fortement connexes pour un graphe orienté

>[!note]
>Les choses sont un peu plus délicates, dans un graphe orienté, la relation "accessible" n'est plus une relation d'équivalence. 
>On considère la relation "être mutuellement accessible" qui est la relation d'équivalence sur $S$.

>[!tip] Définition
>Dans un graphe orienté, les composantes fortement connexes sont les classes d'équivalences pour la relation "être mutuellement accessible".

>[!info] Remarque
>Dans $G=(S,A)$, on identifie souvent une composante (fortement) connexe $S' \subseteq S$ avec le sous graphe engendré par $S'$. Les composantes fortement connexes peuvent donc être vues comme des sous graphes.

>[!tip] Définition
>Un graphe orienté est fortement connexe s'il existe un chemin entre tous les sommets $u,v \in S$.
>Ceci est équivalent à posséder une unique composante fortement connexe.

>[!note]
>On peut alors se demander comment déterminer si un graphe $G=(S,A)$ possède exactement $|S|$ composantes fortement connexe ? C'est-à-dire que chaque sommet est seul dans sa composante fortement connexe. Cela revient à vérifier qu'il n'y a pas de cycles. Ce que l'on sait faire en $O(|S|+|A|)$.

>[!info] Remarque
>Une composante fortement connexe est un sous graphe fortement connexe maximal.

>[!tip] Définition
>Soit $G=(S,A)$ un graphe orienté et $\chi = \chi_{1},...,\chi_{k}$, la partition correspondant à ses composantes fortement connexes. Le méta graphe de $G$ est le graphe orienté $\text{meta}(G) = (x,A_{\chi})$ dans le quel $X,Y \in A_\chi$ si et seulement si $X \neq Y$ et il existe $x \in X, y \in Y$ avec $(x,y) \in A$. C'est le graphe des composantes connexes de $G$, deux composantes étant reliées s'il existe un arc entre un sommet de l'un et un sommet de l'autre.

>[!tip] Proposition
>Si $G=(S,A)$ est un graphe orienté, alors $\text{meta}(G)$ est un dag.

>[!note] Intuition
>La structure d'un graphe orienté se décompose en deux niveaux. Vu de très loins, un graphe orienté est un dag (son méta-graphe) et si on zoom un peu sur les méta-sommets, on trouve des sommets inter-connectés.

### C - Calcul naïf

>[!note] 
>Il est très simple de calculer la composante fortement connexe d'un sommet $u \in S$ en $O(|S|+|A|)$. Un simple parcours en profondeur à partir de ce $u$ donne `accessible(u)`.
>
>Un parcours à partir de $u$ dans un le graphe miroir (ou graphe inverse, dans lequel on inverse le sens de chaque arête) donne $\text{co-accessible(u)}= \left\{v \in S\ |\ u \in \text{accessible(v)}\right\}$.
>
>La composante fortement connexe de $u$ est $\text{accessible(u)} \cap \text{co-accessible(u)}$. On a donc un algorithme en $O(|S|+|A|)$ à condition de pouvoir calculer le graphe miroir en temps linéaire, ce que l'on sait faire si le graphe est représenté par listes d'adjacences. On sait donc vérifier si un graphe est fortement connexe en $O(|S|\times|A|)$, et on sait trouver les composantes fortement connexes en $O(|S|\times(|S|+|A|))$ en utilisant l'approche précédente pour chaque sommet.
>
>On peut faire mieux en $O(|S|+|A|)$.

### D - Algorithme de Kosaraju-Sharir

>[!todo] Rappel
>L'appel à `explorer(u)` visite exactement `accessible(u)` en supposant aucun de ces sommets marqués. 
>- Si on effectue un appel à `explorer(u)`, on a en particulier sa composante fortement connexe (mais éventuellement plus que ça).
>- Si on choisit $u$ dans un puits du méta-graphe, alors `explorer(u)` visite exactement sa composante fortement connexe (qui est ce méta-sommet).
>
>Une fois ces sommets supprimés (donc cette composantes fortement connexe), on peut recommencer avec le reste, et ainsi de suite.
>
>Mais alors, comment trouver un sommet dans un puits du méta-graphe ?
>Trouver un sommet dans un puits du méta-graphe n'est pas évident, mais ce n'est pas difficile de trouver un sommet dans une composante connexe source.

>[!tip] Proposition
>Le sommet post-visité en dernier est dans `composantes` comme source (une source du méta-graphe).

>[!tip] Proposition
>Soit $X$ et $Y$ deux composantes fortement connexes d'un graphe $G=(S,A)$ avec $x \in X$ et $u \in Y$ tel que $(x,y)\in A$.
>Autrement dit, $X$ et $Y$ sont deux sommets du méta-graphe avec $(X,Y)$ un arc du méta-graphe.
>Dans ce cas, le plus grand post du $X$ est plus grand que le plus grand post de $Y$.

>[!note] Démo
>1er cas : 
>On visite un sommet $X$ en premier. Notons $x_0$ le tout premier sommet de $X$ visité (avant tout ceux de $Y$).
>L'appel `explorer(x_0)` va explorer au moins tout $X$ et tout $Y$. Donc tout $Y$ est postivisité avant que $x_0$ ne termine.
>Donc `post(x_0)` est plus grand que tout les post de $Y$.
>
>2eme cas : 
>On visite $Y$ en premier, par un sommet $y_{0}\in Y$. Aucun sommet de $X$ n'est accessible à partir d'un sommet $y$. Dans ce cas on visite entièrement $Y$ avant de commencer à visiter $X$.
>Dans tous les post de $X$ sont supérieur à tous les points de $Y$.

>[!info] Remarque
>On en déduit de la propriété précédente : 
>le sommet de plus grand post est dans une composante fortement connexe source.
>$\forall s \in S \ \backslash \left\{s_{0}\right\}, post(s_0)>post(s)$.
>
>On aimerait un sommet dans une composante puits par source.

>[!tip] Proposition
>Le méta-graphe du graphe miroir (tous les arcs inversés) est le miroir du méta-graphe.
>Pour trouver un sommet $s \in S$ dans une composante fortement connexe puits du graphe, on prend le dernier sommet traité lors d'un parcours en profondeur du graphe miroir.
>On relance un parcours dans le graphe à partir de $s$, ce qui va trouver exactement la composante connexe de $s$. Puis on recommence.
>
>En réalité, **on ne refait pas** les parcours, on se contente de reprendre le même en ignorant les sommets marqués.

>[!info] Algorithme de Kosaraju-Sharir pour $G$
>1) Calculer le graphe miroir $\tilde{G}$. $O(|S|+|A|)$
>2) **Parcours en profondeur** de $\tilde{G}$ en notant les temps de fin de traitement (post). $O(|S|+|A|)$.
>    On peut mettre les sommets terminés dans une pile.
>3) On effectue un parcours classique de $G$ pour chercher les **composantes connexes** (même chose que dans un graphe non orienté) en utilisant l'ordre **inverse de fin de traitement** lors du choix d'un nouveau sommet à explorer. $O(|S|+|A|)$
> 	  - tri de post
> 	  - la pile est trié par ordre inverse de posttraitement.
