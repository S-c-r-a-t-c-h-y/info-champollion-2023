# Chapitre 11 : Décidabilité et calculabilité

```toc
```

## I - Représentation des donnés

## II - Problèmes de décision

### A - Définition intuitive

### B - Formalisation

## III - Procédure effective

### A - Thèse de Church-Turing (Hors programme)

### B - Modèle de calcul retenu

### C - Machine universelle

## IV - Décidabilité

### A - Problème de l'arrêt

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

### B - Reductions

>[!info] ARRET-VIDE : 
>**Instance** : un code source `f_s` d'une fonction $f : \Sigma^{*} \longrightarrow \Sigma^{*}$
>**Question** : $f(\varepsilon)$ se termine-t-il ?

>[!note] Montrons que ARRET-VIDE n'est pas **décidable**
>On suppose que l'on dispose d'une fonction `arret_vide : string -> string -> bool` qui décide ce problème.
>```ocaml
>let arret f_s x = 
>	let g_s = "let g g = eval " ^ f_s^ " "^x in 
>	arret_vide g_s  
>```
>`arret_vide g_s` termine si et seulement si `g` termine sur "" si et seulement si `eval f_s x` termine si et seulement si `f x` termine.
>Absurde donc `arret_vide` n'existe pas car `arret` n'existe pas.
>
>On a montré que le problème de l'**ARRET** se réduit au problème de l'**ARRET VIDE** $\text{ARRET} \preceq_T \text{ARRET VIDE}$.

>[!note] Montrons que $\text{ARRET VIDE} \preceq_{T} \text{ARRET}$ et donc qu'il sont équivalents.
>Montrons que $\text{ARRET} \preceq_{\exists} \text{ARRET}_{\exists}$.
>```ocaml
>let arret f_s x = 
>	let g_s = "let g g = eval "^fs ""^x in 
>	arret_existe g_s
>```
> Montrons que $\text{ARRET}_{\exists} \preceq \text{ARRET}_{\forall}$.
> ```ocaml
> let arret_vide f_s =
> 	let g_s = "let g y = eval "^f_s^{| ""|}
> 	arret_tous g_s
> 
> 
> let arret_vide f = 
> 	let g y = f () in 
> 	arret_tous g
>```
