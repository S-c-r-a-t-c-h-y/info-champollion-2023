# Chapitre 2 : Langages formels

```toc
```

## Introduction

>[!note] On va formaliser la notion de **langage**.
>Il faut être capable d'exprimer formellement les problèmes informatiques pour les résoudre dans un certain langage.
>Un langage est un ensemble de mots : syntaxe (suite de mots), sémantique et grammaire.

>[!example] Exemple
>- *compilation* : toto.c $\rightarrow$ toto.ex (langage c $\rightarrow$ langage machine avec gcc)
>- *Bio informatique* : ADN AGTCCGAT $\rightarrow$ problèmes valides, études,
>  Recherche de motif : Ctrl S, Ctrl F, regex..
>

## I - Mots

### A - Alphabets et mots

>[!tip] Définition
>Un **alphabet** est un ensemble *fini* non vide. Les éléments sont appelés *symboles*, *lettres* ou *caractères*.
>On le note souvent $\Sigma$ ou $A$.

>[!example] Exemple
>$\Sigma = \left\{a\right\}$
>$\Sigma = \left\{a,h\right\}$
>$\Sigma$ : ensemble des caractères du code ASCII ou de l'Unicode.

>[!info] Remarque
>La seule chose qui importe est le cardinal.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!tip] Définition
>Un **mot** sur un alphabet $\Sigma$ est une suite finie de symboles. Le mot $u=(u_{1},u_{2},u_{3},u_{4},...u_{n}) \in \Sigma^{n}$ est noté $u=u_{1}u_{2}u_{3}u_{4}...u_{n}$. La longueur que l'on note $|u|$ est $n$. 
>Le **mot vide** de longueur nulle est noté $\varepsilon$ ou $\Lambda$
>

>[!example] Exemple
>$\varepsilon$
>aaaaaaa
>abbaaba
>ceciestunmot

>[!info] Remarque
>si $\Sigma= \left\{a,b,...,z,\_\right\}$ ceci\_est\_un\_mot
>$u_{1}u_{2}u_{3}u_{4}...u_{n} = \varepsilon$ ssi $n=0$.

>[!info] Notation
>L'ensemble des mots de longueur $k\in \mathbb{N}$ est noté $\Sigma^k$. L'ensemble de tous les mots est noté $\Sigma^{*}$. L'ensemble des mots non vide est noté $\Sigma^+$.
>On a $\Sigma^{*} = \bigcup_{k \in \mathbb{N}} \Sigma^{k}$
>$\Sigma^{+} = \Sigma^{*} \backslash \left\{ \varepsilon \right\} = \bigcup_{k \in \mathbb{N^{*}}} \Sigma^{k}$.

>[!info] Remarque
>On peut considérer des mots infinis (X-ENS), on note $\Sigma^{\omega}$.

>[!info] Remarque
>$\Sigma^{*}$ est toujours de cardinal infini, même si $|\varepsilon| = 1$. Il est dénombrable.
>On énumère tous les mots par ordre de longueur croissante puis pour une même longueur par ordre lexicographique.

>[!info] Remarque
>On identifie abusivement :
>$\Sigma$ (lettre) et $\Sigma^{1}$(mots d'une lettre) et donc $\Sigma \subseteq \Sigma^{*}$

>[!tip] Définition
>Pour $a \in \Sigma$ et $u \in \Sigma^{*}$ la longueur de $u$ en $a$ est le nombre $|u_a|$, occurences de la lettre $a$ dans le mot $u$.
>Pour $u = u_{1}u_{2}u_{3}u_{4}...u_{n}$, 
>$|u_{a|}= |\left\{i \in [|1,n|]\ |\ u_{i}= a\right\}|$

### B - Concaténation

>[!tip] Définition
>Soit $u,v \in \Sigma^{*}$ deux mots avec $u=u_{1}u_{2}u_{3}u_{4}...u_{n}$ et $v=v_{1},v_{2},v_{3},v_{4},...v_{m}$. La concaténation de $u$ et $v$, notée $u \cdot v$ est le mot $u_{1}u_{2}u_{3}u_{4}...u_{n}v_{1}v_{2}v_{3}v_{4}...v_{m}$ formé des lettres de $u$ suivies des lettres de $v$. Il est de longueur $n+m$.

>[!example] Exemple
> $aba \cdot bab = ababab$

>[!info] Remarque
>$\forall a \in \Sigma, \forall u,v \in \Sigma^{*}$
>$|u\cdot v| = |u| + |v|$
>$|u\cdot v|_{a} = |u|_{a} + |v|_{a}$

>[!info] Remarque
>On trouve parfois $u+v$ pour $u\cdot v$.

>[!info] Remarque
>$\forall u \in \Sigma^{*}, \varepsilon u = u \varepsilon = u$

>[!info] Remarque
>$\forall u,v,w \in \Sigma^{*}, (uv)w = u(vw)$

>[!tip] Propriété
>$(\Sigma^{*},\cdot,\varepsilon)$ est un **monoïde**, c'est-à-dire la loi de composition est associative et possède un élément neutre. 

>[!warning]
>Si on écrit $u = u_1$
>$u_n$ : décomposition en lettre ou en mots ?
>Attention à l'ambiguité possible.
>Il faut **préciser**.

>[!note] Remarque
>$|\ | : \Sigma^{*} \rightarrow \mathbb{N}$ est un morphise de monoïde.

>[!info] Remarque 
>$\varepsilon$ est le seul mot inversible pour $(\Sigma^{*},\cdot,\varepsilon)$ qui n'est donc pas un groupe.
>On peut cependant simplifier à droite et à gauche.
>Si $u,v,w \in \Sigma^{*}$ avec $uv=uw$ alors $v=w$ et si $vu = wu$ alors $v=w$

>[!info] Remarque 
>$(\Sigma^{*},\cdot,\varepsilon)$ est commutatif si et seulement si $|\Sigma| = 1$

>[!bug] Exercice
>à montrer
 
>[!info] Remarque
>Pour $u,v,w \in \Sigma^{*}, uv=\varepsilon$ alors $u=v=\varepsilon$
>Pour $u\in \Sigma^{*}$ et $k \in \mathbb{N}^*$ on pose 
>$u^0 = \varepsilon$
>$u^k = u u^{k-1}$
>$\forall k,l \in \mathbb{N}$ on a $u^k u^l = u^{kl}$

## II - Préfixes, suffixes, facteurs et sous-mots

>[!tip] Définition
>Soient $u,b \in \Sigma^*$, on dit que :
>- $u$ est préfixe de $v$ s'il existe $w \in \Sigma^*$ tel que $v = uw$
>- $u$ est suffixe de $v$ s'il existe $w \in \Sigma^*$ tel que $v = wu$
>- $u$ est facteur de $v$ s'il existe $w_1 w_2 \in \Sigma^*$ tels que $v =w_1uw_2$
>
>Le préfixe / suffixe / facteur de u est **propre** si u != v et **non-trivial** si de plus $u \neq \varepsilon$.

>[!example] Exemple
>Les préfixes de $baobab$ sont $\left\{\varepsilon,b,ba,bao,baob,baoba,baobab\right\}$.

<div style="Page-break-after: always; visibility: hidden">\Pagebreak</div>

>[!info] Remarque
>$u$ est un préfixe / suffixe / facteur de lui même (avec $w = \varepsilon$ ou $w_1=w_2=\varepsilon$)

>[!info] Remarque
>Un mot de longueur $n$ possède exactement $n+1$ suffixes et préfixes et **au plus** $\binom{n}{2} +1$ facteurs.

>[!tip] Définition
>On dit que $u = u_1 u_2 u_3 .. u_n$ avec $\forall i \in [|1,n|], u_i \in \Sigma$ est un sous mot de $v$ s'il existe des mots $w_0,w_1,...,w_n \in \Sigma^*$ tels que $v = w_0u_1w_{1}...u_nw_n$
>Intuitivement les lettres de $u$ apparaissent dans $v$ dans le bon ordre.

>[!example] Exemple de sous-mots de $abcb$
> $\left\{\varepsilon, a,b,c,ab,ac,bc,bb,cb,abc,abb,acb,bcb,abcb\right\}$

>[!warning] 
>sous-mot $\neq$ facteur
>sous-mot : mêmes lettres dans le même ordre
>facteur : suite consécutive de lettres du mot

>[!info] Remarque
>Un facteur est un sous-mot mais la réciproque est fausse.

>[!warning]
>Certains sujets utilisent le terme sous-mot pour facteur.

## III - Ordres

>[!note]
>On suppose que $\Sigma$ est muni d'un ordre total, noté $<$, qui est un ordre sur les symboles.

### A - Ordre lexicographique

>[!note] 
>C'est l'ordre usuel sur les mots dans le dictionnaire.

>[!info] Remarque 
>C'est aussi celui utilisé par défaut en OCaml.
>```Ocaml
>"caml" < "python";; 
>true
>("calm",3) < ("caml",-1);;
>false
>```

>[!tip] Définition
>On définit l'ordre lexicographique sur les mots notés $\preceq_{lex}$ de la manière suivante :
>Pour $u,v \in \Sigma^*$ on a $u \preceq_{lex} v$ si :
>- soit $u$ est un préfixe de $v$ 
>- soit il existe $a,b \in \Sigma$ avec $a < b$
> 
> il existe $w,w',w" \in \Sigma^*$ tels que : 
>- $u = waw'$
>- $v=wbw"$
>  
> Le mot $w$ est le plus long préfixe commun à $u$ et à $v$, noté $w = u\wedge r$ et $a,b$ sont les premières lettres qui diffèrent.

>[!info] Remarque
>Dans le premier cas, si $u$ est un préfixe de $v$ alors $u\wedge r=u$

>[!example] Exemple
>calm < calmia
>camal < camul

>[!note]
>L'odre lexicographique prolonge l'ordre$(\Sigma,S)$ en un ordre total ($\Sigma^*,\preceq_{lex}$) (les deux ordres coincides sur $\Sigma$).
>
>$S \wedge \Sigma$ de carnidal au moins 2.
>$(a^nb)_{n \in \mathbb{N}}$ infnie strictement décroissante. $\left\{a^nb, n \in \mathbb{N}\right\}$ est une partie non vide qui n'admet pas de plus petit élément
>$ab\ succ\ aab\ succ\ aaab\ succ\ ... succ$
>
>Cet ordre n'est pas bien fondé sur $\Sigma^*$.

### B - Ordre hiérarchique

>[!note] 
>On compare d'abord par longueur puis par ordre lexicographique.

>[!tip] Définition
>On définit l'odre hiérachique pour $u,v \in \Sigma^* u \preceq v$ si 
>- soit $|u| < |v|$
>- soit $|u| =|v|$ et $u \preceq_{lex} v$
>  
> C'est un ordre total bien fondée.

>[!info] Remarque
>C'est un ordre utilie pour énumérer les mots sur $\Sigma$.

## IV - Langages

>[!tip] Définition
>On appelle langage sur $\Sigma$ une partie de $\Sigma^*$ c'est à dire un ensemble (fini ou non) de mots.

>[!example] Exemple 
>Langages sur $\Sigma$ :
>$\emptyset, \Sigma^* , \Sigma, \Sigma^q, \left\{\varepsilon\right\}$

>[!example] Exemple
>$\Sigma = \left\{a,b\right\}$
>$L_1 = \left\{a,ab,aba,abab\right\}$
>$L_2 = \left\{u \in \Sigma^* |\ |u|_a = |u|_b \right\}$
>$L_3 = \left\{a^nb^n, n \in \mathbb{N}\right\}$
>$L_4 = \left\{(ab)^n, n \in \mathbb{N}\right\}$
>
>$L_3 \subset L_2$
>$L_1 \subset L_2$
>$ba \in L_2 \left\{L_1 \cup L_3\right\}$

>[!example] Exemple
>Avec $\Sigma$ l'ensemble des caractères ASCII
>$L$ constituée des programmes OCaml syntaxiquement correct.
>$L$ constituée des programmes OCaml bien typés.
>$L$ constituée des programmes OCaml qui terminent toujours.

>[!info] **Opérations sur les langages**
>Les langages étant des ensembles, on peut leur appliquer toutes les opérations ensemblistes :
>La réunion, l'intersection, la différence, la différence symétrique, le complémentaire, ...

>[!info] Remarque
>On note souvent l'union +
>$L_1+L_2 = L_1 \cup L_2$

>[!tip] Définition
>Soit $L,M \subseteq \Sigma^*$ deux langages (ou $L,M \in P(\Sigma^*)$).
>On définit la concaténation de $L$ et $M$ notée $L \cdot M$ (ou plus souvent $LM$) par 
>$L \cdot M = \left\{u \cdot v |\ u \in L, v \in M\right\}$
>

>[!example] Exemple
>$L = \left\{a, ab, aba\right\}$
>$M = \left\{\varepsilon, a\right\}$
>$LM = \left\{a, ab, aba, a^2, aba^2\right\}$
>
>>[!info] Remarque 
>>$aab \notin LM$

>[!note] Remarque
>Cette opération n'est pas commutative.

>[!info] Remarque
>$(P(\Sigma^*),+, \emptyset)$ est un monoïde commutatif.
>$(P(\Sigma^*), \cdot, \left\{\varepsilon\right\})$ est un monoïde (non commutatif en général).

>[!info] Remarque
>$(P(\Sigma^*), +, \cdot, \emptyset, \left\{\varepsilon\right\})$ est un semi-anneau (non commutatif). On a $+$ distributif sur $\cdot$ et $\emptyset$ est absorbant pour $\cdot$.
>
>On a donc $\emptyset \cdot M = \emptyset, \forall M \subseteq \Sigma^*$
>
>Comme pour les mots, pour $M \subseteq \Sigma^*$et $k \in \mathbb{N},$ on définit 
>$M^0 = \left\{\varepsilon\right\}$ 
>$M^{k+1} = M^k \cdot M = M \cdot M^k$

>[!info] Remarque
>$M^1 = M$

>[!note] 
>Les mots de $L^n$ sont exactement les mots qui sécrivent $u = u_1u_2...u_n$ avec $\forall i \in [|1,n|], u_i \in L$

>[!warning] **ATTENTION**
>L$^2 \neq \left\{u^2, u \in L\right\}$
>$L^2 = \left\{uv, u \in L, v \in L\right\}$

>[!example] Exemple
>$L = \left\{a,b,ab\right\}$
>$L^2 = \left\{a^2,ab,ba,a^2b,aba,...,\right\}$

>[!tip] Définition Etoile de Kleene
>L'étoile de Kleene de L est le langage : $L^* = \bigcup_{n \in \mathbb{N}} L^n$
>Le langage $L^+ = \bigcup_{n \in N^* } L^n$ est appelé étoile de Kleene stricte.

>[!example] Exemple
>$L = \left\{a,b,ab\right\}$
>$L^* \neq \left\{a^n, n \in \mathbb{N}\right\} \cup \left\{(ab)^n, n \in \mathbb{N}\right\}$
>$L^* = \left\{\varepsilon, a,ab,a^2, a^b,...\right\}$

>[!info] Remarque
>$\Sigma^* = \bigcup_{n \in N} \Sigma^n = \Sigma^*$

>[!note] Remarque
>On a **toujours** $\varepsilon \in L^*$ quelque soit L
>$L^* = \bigcup_{n \in \mathbb{N}} L^n = L^0 + \bigcup_{n \in \mathbb{N}^*} L^n$
>On a $L^* = L^+ \cup \left\{\varepsilon\right\}$

>[!note] Remarque
>$\varepsilon \in L^+ \Leftrightarrow \varepsilon \in L$
>
>On a toujours $L^+ \subseteq L^*$

>[!warning]
>$\emptyset^* = \left\{\varepsilon\right\}$

>[!info] Remarque
>Pour $L \subseteq \Sigma^*$ et $(M_i)_{i \in I} \in P(\Sigma^*)^N$
>$L \cdot (\bigcup_{i \in I} M_i) = \bigcup_{i \in N} LM_i$
>$(\bigcup_{i \in I} M_i) \cdot L = \bigcup_{i \in I} M_iL$
>$L \bigcap_{i \in I} M_i \subseteq \bigcap_{i \in I} L \cdot M_i$ 
>Mais pas toujours l'égalité.

>[!example] Exemple
>$L = \left\{a,a^2\right\}$
>$M_1= \left\{a\right\}$
>$M_2 = \left\{a^2\right\}$
>$L \bigcap_{i \in I} M_i = \emptyset$ 
>$LM_1 = \left\{a^2, a^3\right\}$
>$LM_2 = \left\{a^3, a^4\right\}$
>$\bigcap_{i \in I} LM_i = \left\{a^3\right\}$

>[!bug] Exercice
>Démontrer les 3 inclusions.

>[!note] 
>L'ensemble des mots dont $u$ est préfixe est $\left\{u\right\} \cdot \Sigma^*$ (que l'on note $u \Sigma^*$) 
>$u$ est suffixe est $\Sigma^*u$
>$u$ est facteur est $\Sigma^*u\Sigma^*$
>
>Et $u \Sigma^*u$ n'est pas l'ensemble des mots qui commencent et se terminent par $u$. Ce langage est $u \Sigma^* \Sigma^*$

>[!info] Remarque
>$u\Sigma^*u \subset u \Sigma^* \wedge u \Sigma^*$
>L'inclusion réciproque est fausse sauf si $u = \varepsilon$

>[!note]
> $u \neq \varepsilon$ alors $u \in u \Sigma^* \wedge u \Sigma^*$
> mais $u \notin u \Sigma^* u$
> Soit $v \in u \Sigma^*, \exists w \in \Sigma^*$
> tel que $v = uwu$
> $|v| >= 2|u$|
> donc $|v| > |u|$ car $|u| \neq 0$ si $u \neq \varepsilon$
> 
>$u \Sigma^* u$ est l'ensemble des mots qui commencent et terminent par u sans chevauchement entre le préfixe et le suffixe $w$.

## V - Lemme de Levi

>[!tip] Lemme
>Soit $x,y,z,t \in \Sigma^*$ tels que $xy = zt$ alors $\exists! u \in \Sigma^*$ tel que 
>$\left\{x = zu\ \text{et}\ t = uy \right\}$
>ou
>$\left\{z = xu\ \text{et}\ y = ut \right\}$
>![[chap2_img1.png]]

>[!info] Application
>- Deux mots $x,y \in \Sigma^*$ commutent
>si $xy = yx$  
>- Deux mots $u,v \in \Sigma^*$ sont conjugués s'il existe $x,y \in \Sigma^*$ tels que $u =xy\ \text{et}\ v = yx$
>(on dit aussi qu'ils sont rotation l'un de l'autre)

>[!note] Démo
>***unicité***
>Si $u$ et $u'$ sont deux solutions alors 
>$x = zu$ et $x = zu'$
>ou 
>$z = xu$ et $z = xu'$
>
>En particulier
>Si $x = zu$ et $x = zu'$ alors $u = u'$ (simplification par $z$, preuve pas récurrence)
>De même dans l'autre cas.
>
>***Preuve rapide*** 
>Si $|x| \geq |z|$
>alors comme $zt = xy$ on a $z$ préfixe de $x$.
>Donc $\exists u \in \Sigma^*$ tel que $x = zu$
>On a donc $zuy = zt$ donc $uy = t$ (par simplification par $z$)
>Et de même si $|z| \geq |x|$ en se retrouvant dans l'autre cas.
>
>***Autre preuve***
>On peut poser $x = x_1x_2...x_n$ avec $\forall i \in [|1,n|] x_i \in \Sigma$ et $z = z_1...z_n$ avec $\forall i \in [|1,n|] z_i \in \Sigma$
>Si $|x| \geq |z|$ alors $m \geq n$ 
>On pose $u = x_n+1...x_m$
> Preuve par récurrence sur $|xy|$
> Montrons par récurrence forte sur $n \in \mathbb{N}$ que si $x,y,z,t \in \Sigma^*$ avec $xy = zt$ et $|xy| = n$ alors il existe $u \in \Sigma^*$ tel que $\left\{x = zu\ \text{et}\ t = uy \right\}$
>$\left\{z = xu\ \text{et}\ y = ut \right\}$
>
>*Initialisation*
>Pour $n =0$, on a $|xy| = 0$ donc $x=y=z=y=\varepsilon$ et $u = \varepsilon$ convient
>
>*Hérédité* : On suppose le résultat vrai jusqu'au rang $n$ et soit $x,y,z,t \in \Sigma^*$ avec $xy=zt$ et $|xy|=n+1$
>
>1er cas : si $x = \varepsilon$ alors $u = z$ convient et $y=ut=zt=xy=\varepsilon y$
>
>2ème cas : de même si $z = \varepsilon$ par symétrie
>
>3ème cas : $\exists a, a' \in \Sigma, \exists x',z' \in \Sigma^*$ tels que 
>$x=ax'$
>$z=a'z'$
>On a donc $ax'y=a'z't$ donc $a=a'$ et $x'y = z't$
>or $|x'y|=x$ et $x'y=z't$
>Donc par HR, $\exists u' \in \Sigma^*$ tel que $x'=z'u'$ et $t =u'y$
>ou $z' = x'u'$ et $y = u't$
>
>On choisit $u = u'$, donc on a 
>$ax' = az'u$ et $t = u'y$
>$az'=ax'$ et $y=ut$
