# Chapitre 14 : Introduction à la théorie de la complexité

```toc
```

## I - Problèmes de décision et d'optimisation

### A - Problème de décision

>[!tip] Définition
>On appelle problème de décision la donnée d'une instance et d'une question dichotomique.
>**INSTANCE** : un objet possédant certaines caractéristiques.
>**QUESTION** : une question sur cet objet dont la réponse est oui ou non.
>
>Formellement, un problème correspond à une fonction totales $f : \Sigma^{*} \longrightarrow \mathbb{B}$ avec $u \in \Sigma^{*}$ l'**encodage** d'une instance au problème et $f(u)$ la **réponse** pour cette instance.
>Cela correspond au langage $\left\{u \in \Sigma^{*}\ | \ f(u)\right\}$ des **instances positives**.
>Un problème de décision est donc un langage $L \subseteq \Sigma^{*}$.

>[!info] Remarque
>Si $f(u)=F$ alors $u$ n'est pas une instance positive, soit $u$ ne représente pas une instance.
>En général, déterminer si un mot $u \in \Sigma^{*}$ est l'encodage d'une instance d'un problème est facile (c'est d'ailleurs un problème de décision que l'on peut résoudre au préalable).

>[!info] Remarque
>Un algorithme $A:\Sigma^{*}\longrightarrow \mathbb{B}$ résout le problème $L\subseteq \Sigma^{*}$ si et seulement si $L = \left\{u \in \Sigma^{*}\ |\ A(u)\right\}$.

### B - Problème de recherche

>[!tip] Définition
>On appelle **problème de recherche** la donnée d'une instance et la description des solutions possibles pour cette instance.
>**INSTANCE** : un objet possédant certaines caractéristiques.
>**SOLUTION** : la structure d'une solution possible.
>Il s'agit de trouver explicitement une solution pour l'instance.
>
>Formellement, il s'agit d'une relation binaire sur $\Sigma^{*}$ : $\mathcal{R}\subseteq \Sigma^{*}\times \Sigma^{*}$.
>L'ensemble des solutions associées à une instance $u \in \Sigma^{*}$ est l'image de $u$ par $\mathcal{R}$.
>
>Un algorithme $A$ résout un problème de recherche $\mathcal{R}$ si tout mot d'entrée $u \in \Sigma^{*}, A(u)$ calcule un mot $v$ tel que $u\mathcal{R}v$ (c'est-à-dire trouver une solution $v$) s'il en existe une et indique qu'il n'en existe pas sinon.

>[!info] Remarque
>Ceci convient qu'il y ait une seule solution possible ou plusieurs.

>[!note] 
>A un problème de recherche, on peut associer plusieurs problèmes de décision :
>- **Problème de l'existence d'une solution**
>	Soit $\mathcal{R} \subseteq \Sigma^{*}\times \Sigma^{*}$ un problème de recherche. Le problème de décision existentiel associé est le problème définit par $f_{\exists} : \Sigma^{*}\longrightarrow \mathbb{B}$ telle que $\forall u \in \Sigma^{*}, f(u)=v \Leftrightarrow \exists v \in \Sigma^{*}$.
>- **Problème de vérification d'une solution**
>  Soit $\mathcal{R}\subseteq \Sigma^{*}\times \Sigma^{*}$ un problème de recherche. Le problème de vérification associé est $f_{\nu}: \Sigma^{*}\times \Sigma^{*}\longrightarrow \mathbb{B}$ telle que $\forall (u,v)\in \Sigma^{*}\times \Sigma^{*}, f_{\nu}(u,v)=V \Leftrightarrow u\mathcal{R}v$.
>  (est ce que $v$ est bien solution pour $u$)

>[!info] Remarque
>Pour avoir un vrai problème de décision, il faut se donner une bijection calculable entre $\Sigma^{*}\times \Sigma^{*}$ et $\Sigma^{*}$.

### C - Problème d'optimisation

>[!note]
>Dans un problème de recherche, lorsqu'il y a plusieurs solutions, on peut s'intéresser à choisir "un meilleur pour un certain critère".

>[!tip] Définition
>Un **problème d'optimisation** est un problème de recherche auquel on ajoute une fonction coût que l'on cherche à minimiser.
>**INSTANCE** : Un objet avec certaines caractéristiques.
>**SOLUTION** : La structure des solutions possibles pour cette instance.
>**OPTIMISATION** : Une fonction qui à une instance $x$ et une solution $y$ associe un coût $c(x,y)$ que l'on cherche à minimiser.
>
>Formellement, un problème d'optimisation est un problème de recherche $\mathcal{R} \subseteq \Sigma^{*}\times \Sigma^{*}$ et une fonction coût $c: \Sigma^{*}\times \Sigma^{*}\longrightarrow \mathbb{R}$.
>On cherche, pour $u \in \Sigma^{*}$ fixé, un $v \in \left\{w \in \Sigma^{*}\ | \ u\mathcal{R}v\right\}$ qui minimise $c(u,v)$.
>
>Un algorithme $A : \Sigma^{*}\longrightarrow \Sigma^{*}$ résout ce problème d'optimisation, si $\forall u \in \Sigma^{*}, A(u) = \underset {v \in \left\{w \in \Sigma^{*}\ | \ u\mathcal{R}v\right\}} {\text{argmin}} c(u,v)$.
>Si un minimum existe et signale que non, sinon.

>[!info] Remarque
>On peut encoder $\mathcal{R}$ dans $c$, par exemple en posant $c(u,v)=+\infty$ si $(u,b)\notin \mathcal{R}$.

>[!info] Remarque
>On formule ici comme un problème de minimisation, mais on peut aussi maximiser.

>[!info] Problème de seuil
>Considérons un problème d'optimisation $\mathcal{R} \subseteq \Sigma^{*}\times \Sigma^{*}$ et $c: \Sigma^{*}\times \Sigma^{*}\longrightarrow \mathbb{R}$. Le problème de décision associé à ce problème d'optimisation est le **problème de décision** : 
>INSTANCE : une instance $u \in \Sigma^{*}$ des problèmes seuils $s\in \mathbb{R}$.
>QUESTION : existe-t-il une solution $v\in \Sigma^{*}$ un problème de recherche $\mathcal{R}$ pour $u$ telle que $c(u,v) \leq s$.

## II - La classe NP

>[!warning] Attention
>La classe NP ne s'applique qu'aux problèmes de décisions.
>Pour un problème d'optimisation, il faut considérer le problème de décision associé.

>[!tip] Définition
>NP : Non-déterministe Polynomiale
>Intuitivement, le classe NP correspond aux problèmes que l'on peut résoudre en temps polynomial **si** on sait **deviner** une solution. Il suffit juste de **vérifier** que notre candidat est bien solution.
>$\rightarrow$ deviner-et-vérifier
>
>On appelle cela **oracle**.

>[!info] Remarque
>De manière générale, il s'agit de vérifier (en temps polynomiale) qu'une instance est positive étant donnée une preuve sous la forme d'un certificat (en temps polynomial).
>

>[!note]
>Dans 99% du temps, le problème de décision est un problème associé à un problème de recherche ou d'optimisation et le certificat  est la solution candidate.

>[!warning] Attention
>Cette solution doit être de taille polynomiale en les entrées et vérifier que c'est bien une solution doit se faire en temps polynomial.

>[!tip] Définition
>Un problème de décision $L \subseteq \Sigma^{*}$ est dans la classe NP si et seulement si il existe un algorithme $\mathcal{V} : \Sigma^{*}\times \Sigma^{*}\longrightarrow \mathbb{B}$ appelé vérifieur, de complexité polynomiale en la taille de ses entrées tel que
>"$u \in L \Leftrightarrow \exists v \in \Sigma^{*}$ de taille polynomiale en $u$ tel que $\mathcal{V}(u,v)=V$".
>Un $v \in \Sigma^{*}$ de taille polynomiale en $u$ est appelé un **certificat** pour $u$.

>[!info] Remarque
>Certains cours supposent que $(u,v) \longrightarrow V(u,v)$ est polynomial en $|u|$.
>Dans ce cas le certificat est nécessairement polynomial.

>[!example] Exemple
>SAT $\in$ NP
>Un certificat pour une instance $\varphi \in F$ et une valuation $\nu :\mathcal{V}_{\nu}\longrightarrow \mathbb{B}$. La taille de $\nu$ est linéaire en le nombre de variable de $\varphi$ donc polynomiale en $|\varphi|$ et on sait vérifier que $\nu \models \varphi$ en $O(|\varphi|)$ polynomial en $\varphi$.

>[!info] Remarque
>Le vérifieur est la fonction d'évaluation $(\nu,\varphi) \longrightarrow \hat{\nu}(\varphi)$.

>[!example] Contre Exemple
>Echecs généralistes. On voit mal un certificat polynomial. Une stratégie gagnante va être de taille vraiment grande.

>[!tip] Propoistion
>P $\subseteq$ NP

>[!note] Démo
>Soit $A : \Sigma^{*}\rightarrow \mathbb{B}$ qui résout un problème $L \subseteq \Sigma^{*}$.
>Construisons $\mathcal{V} : \Sigma^{*}\times \Sigma^{*} \longrightarrow \mathbb{B}$ tel que $\forall u \in \Sigma^{*}$.
>$A(u) \Leftrightarrow \exists v \in \Sigma^{*}$, polynomial en $u$, $\mathcal{V}(u,v)=V$.
>$\mathcal{V}$ procède ainsi pour $(u,v)$ jette $v$ renvoie $A(u)$.
>Pour $u \in \Sigma^*$
>Si $A(u) = V$ alors $\mathcal{V} (u,\varepsilon) = V$.
>Inversement, s'il existe $v \in \Sigma^*$ (polynomial) tel que $\mathcal{V} (u,v) = V$ alors $A(u) = V$ 
>Donc  $\mathcal{V}$ vérifie bien $L$ avec par exemple le certificat $\varepsilon$

>[!info] Remarque
>Dans la définition de problème NP, il faut bien sûr que tous les certificats soient en temps polynomial en la taille de l'instance pour le même polynôme.
>$L \in$ NP si et seulement s'il existe un vérificateur $\mathcal{V}:\Sigma^{*}\times \Sigma^{*} \longrightarrow \mathbb{B}$ polynomial en la taille de ses entrées.
>C'est-à-dire, $\exists f \in \mathbb{N}[X]$ tel que $\forall u,v \in \Sigma^{*}, \mathcal{u,v}$ est en temps $O(f(\text{max}(|u|,|v|)))$ et un polynôme $g \in \mathbb{N}[X]$ tel que $u \in L \Leftrightarrow \exists c \in \Sigma^{*}$ avec $|c|\leq g(|u|)$ et $\mathcal{V}(u,c)=V$.

>[!tip] Proposition
>NP $\subseteq$ EXP

>[!note] Démo
>Si $L \in$ NP, il existe un vérifier $\mathcal{V}$ de complexité polynomial pour un polynôme $f \in \mathbb{N}[X]$ et un polynôme $g\in \mathbb{N}[X]$ tel que $\forall u \in \Sigma^{*}, u \in L \Leftrightarrow \exists c \in \Sigma^{*}, |c| \leq g(|u|)$ et $\mathcal{V}(u,c)=V$.
>Construisons un algorithme $A$ de complexité au pire exponentielle qui détermine si $u \in L$.
>$A(u)$ : 
>énumère tous les mots $v \in \Sigma^{*}$ avec $|v| \leq g(|u|)$ et pour chacun teste $\mathcal{V}(u,v)$.
>Si on trouve $v\in \Sigma^{*}$ avec $|v| \leq g(|u|)$ alors on renvoie correctement vrai.
>Sinon c'est que $\forall v \in \Sigma^{*}$ avec $|v| \leq g(|u|), \mathcal{V}(u,v)=F$ et on renvoie correctement faux.

>[!question] Grande question de l'informatique
>P = NP ?

## III - Réductions et problèmes NP-complets

### A - Réductions polynomiales

>[!note] 
>La définition suivante permet de montrer qu'un problème est "plus facile" qu'un autre en terme de compléxité.

>[!tip] Définition
>Soit $A\subseteq \Sigma^{*}$ et $B\subseteq \Sigma^{*}$ deux problèmes de décision. On dit que $A$ se réduit en temps polynomial à $B$ ce que l'on note $A\leqslant_{p}B$ s'il existe une fonction $f : \Sigma^{*} \longrightarrow \Sigma^{*}$, qui peut se calculer en temps polynomial, telle que pour tout $u \in \Sigma^{*}, u\in A \Leftrightarrow f(u)\in B$.
>Autrement dit, $u$ est une instance positive pour $A$ si et seulement si $f(u)$ est une instance positive pour $B$.
>![[chap13_img1.jpg]]

>[!info] Remarque
>Puisque $f$ est de complexité polynomiale en $|u|$. $|f(u)|$ est polynomiale en $|u|$.
>Intuitivement, $A$ se réduit veut dire que si on sait résoudre $B$, il ne faut qu'un temps polynomial supplémentaire pour résoudre $A$.

>[!tip] Proposition 
>Soit $A \subseteq \Sigma^{*},B \subseteq \Sigma^{*}$ deux problèmes de décisions tels que $A \leqslant_{p} B$ alors :
>- si $B \in$ P alors $A \in$ P
>- si $B \in$ NP alors $A \in$ NP

>[!note] Démo
>Supposons $A \leqslant_{p} B$ et soit $f : \Sigma^{*}\longrightarrow \Sigma^{*}$ telle que $\forall u \in \Sigma^{*}, u \in A \Leftrightarrow f(u) \in B$. 
>Supposons que $B\in$ P et $Y$ un algorithme polynomial pour $B$. On peut proposer un algorithme $X$ qui résout $A$ en un temps polynomial.
>$X(u)$:
>calculer $f(u)$
>Renvoyer $Y(f(u))$. 
>On a bien $X(u)=Y(f(u))$

>[!example] Exemple
>Montrer que pour $m \leq n$, $m$-SAT$\leqslant_{p}n$-SAT, réduction "cas particulier" : $f$ est l'identité.

>[!example] Exemple
>Reprendre l'exo du td (ex7)

>[!info] Remarque
>Si $X \subseteq S$ est stable de $G = (S,A)$ alors $X$ est une clique $\overline{G}=(S,\overline{A})$.
>Calculer $\overline{G}(S,\overline{A})$ se fait en temps polynomial à partir de $G=(S,A)$ et les stables par $G$ sont les cliques de $G$ donc il existe un stable de $G$ de tailles au moins $k$ ssi il existe une clique de $G$ de taille au moins $k$.

>[!bug] Exercice
>CLIQUE $\leqslant_p$ STABLE

### B - NP-complétude

>[!tip] Définition
>Soit $A \subseteq \Sigma^{*}$ un problème de décision, on dit que : 
>- $A$ est NP-difficile (NP-hard) si pour tout $B \in$ NP, $B \leqslant_p A$.
>- A est NP-complet si $A$  est NP et NP-difficile.
> 
>Un problème est NP-difficile s'il est plus "difficile" que tous les problèmes NP.

>[!info] Remarque
>On peut étendre la notion de NP-difficile aux problèmes d'optimisation.

>[!tip] Proposition
>Si $A$ est NP-complet et si $A\in$ P alors P = NP.

>[!note] Démo
>On sait que P $\subseteq$ NP. 
>Si $B \in$ NP alors comme $A$ est NP-difficile, on a $B \leqslant_{p}A$.
>De plus comme $A\in$ P, on a $B \in$ P donc NP $\subseteq$ P.

>[!tip] Proposition
>Si $A$ est NP-difficile et si $A \leqslant_{p} B$ alors $B$ est NP-difficile.

>[!note] Démo
>Soit $C \in$ NP avec $C \leqslant_{p}A$ et par transitivité, $C\leqslant_{p}B$.
>Pour montrer qu'un problème $A$ est NP-complet :
>- On montre que $A \in$ NP
>- On trouve un problème $B$ NP-difficile (en fait NP-complet) tel que $B \leqslant_{p}A$
>Attention au sens !
>on réduit un probleme NP-complet commun au deux
>B $\leqslant_p$ A

>[!example] Exemple
>En admettant que 3-SAT est NP-complet on a montré que 3-COLOR est NP-difficile car 
>3-SAT$\leqslant_p$ 3-COLOR.
>Comme 3-COLOR $\in$ NP, 3-COLOR est NP-complet 

>[!warning] Problème 
>Il nous faut un premier premier NP-complet.

>[!tip] Théroème COOK LEVIN 1971
>SAT est NP-complet

>[!note] Démo 
>admis

>[!done] Rappel
>Un problème est NP-difficile s'il est plus difficile que tous les problèmes NP c'est-à-dire que tous problèmes NP s'y réduit en temps polynomial.

>[!tip] Proposition
>Si $A$ est NP-complet $\cap$ P alors P=NP.

>[!Info] Conséquences
>Pour montrer qu'un problème $A$ est NP-complet :
>- On montre que $A$ est NP. On explicite un vérificateur $\mathcal{V}$ polynomial pour lequel il existe un certificat polynomial exactement pour les instances positives. 
>- On montre $A$ est NP-difficile. Pour cela on propose un problème $B$ NP-complet connu et on montre que $B \leqslant_{p}A$.
>- On trouve $f : \Sigma^{*}\longrightarrow \Sigma^{*}$ polynomiale telle que $\forall u \in \Sigma^{*}, u \in A \Leftrightarrow f(u) \in B$.

>[!info] Remarque
>Ce n'est pas symétrique.
>$A$ une instance quelconque de $B$, on associe une instance spécifique de $A$ choisie.

>[!tip] Proposition
>CNF-SAT est NP-complet (CNF : forme normal conjonctive). 

>[!note] Démo
>CNF-SAT $\in$ NP, toujours avec comme certificat une valuation et comme vérificateur la fonction d'évaluation.
>Montrons que SAT $\leqslant_{p}$ CNF-SAT.
>A une instance $f \in \mathcal{F}$ de SAT et on lui associe sa FNC avec l'algorithme du TP n°9.
>On obtient une formule $\overline{\varphi}$ en FNC équivalente à $\varphi$ donc $\varphi \in$ CNF-SAT.
>***NON*** la transformation $\varphi \rightarrow \overline{\varphi}$ est exponentielle dans le pire cas.
>Il existe des formules $\varphi \in \mathcal{F}$ telles que tout FNC équivalente à $\varphi$ est de taille exponentielle en $\varphi$.
>
>**Nouvelle idée** :
>à une formule $\varphi$ on associe la formule $T(\varphi)$ la transformée de Tseitin de $\varphi$.
>D'après le DM 4, cette transformation est polynomiale en $|\varphi|$ et produit une formule $T(\varphi)$ de taille polynomiale.
>La réduction est correcte car $\varphi$ satisfiable si et seulement si $T(\varphi)$ satisfiable.

>[!done] Rappel
>Pour $n \geq 0$, $n$-SAT est le problème de savoir si une formule $\varphi$ en FNC avec des clauses de taille au plus $n$ est satisfiable.

>[!tip] Proposition
>Pour $n \geq 3$, $n$-SAT est NP-complet.

>[!note] Démo
>Si $n \geq 3$, on a 3-SAT$\leq n$-SAT (cas particulier). Il suffit de montrer 3-SAT est NP-difficile.

>[!info] Remarque
>$\forall n \in N$, $n$-SAT est dans NP.
>Montrons que 3-SAT est NP-difficile.
>CNF-SAT $\leqslant_{p}$ 3-SAT.
>Si $\varphi$ est une formule en FNC.
>Soit $c$ une clause de $\varphi$ de taille strictement plus que 3 (sinon on la garde).
>$(l_{1}\vee l_{2}\vee l_{3}\vee l_{4}\vee \cdots \vee l_{k}), k \geq 3$. on introduit des nouvelles variables $y_{2}, \cdots, y_{k-1}$ et on pose $c_{k}$ la formule $(l_{1}\vee l_{2}\vee y_{2})\wedge (\overline{y_{2}}\vee l_{3}\vee y_{3})\wedge \cdots \wedge (\overline{y_{k-1}}\vee l_{k-1}\vee l_{k})$.
>On fait alors pour chaque clause (avec des $y_i$ différents pour chaque clause) pour construire une formule $\overline{\varphi}$.
>On obtient une formule de taille polynomiale et cette transformation est polynomiale.

>[!info] remarque
>Si une clause $c$ est satisfiable par une valuation $\nu$, on construit une valuation $\overline{\nu}$ qui prolonge $\nu$ de la manière suivante suivante. Comme $c$ est satisfiable, $\exists i \in [|1,k|], \nu(l_{i})=V$. On pose pour $j \in [|2,i-1|], \overline{\nu}(\mu_j)=V$ et pour $j \in [|i,k-1|], \overline{\nu}(\mu_{j})=F$.
>Toute clause de $\overline{c}$ est satisfaite par $\overline{\nu}$.
>On prolonge ainsi $\overline{\nu}$ sur toutes les clauses et on obtient un modèle de $\overline{\varphi}$.
>Réciproquement, si $\overline{\nu}$ est une modèle de $\overline{\varphi}$. 
>On pose $\nu = \overline{\nu}_{\nu(\varphi)}$. (la restriction de $\overline{\nu}$ aux variables de $\varphi$).
>Montrons que $\nu$ est un modèle de $\varphi$.
>Soit $c$ une clause de $\varphi$. Si tous les littéraux de $\varphi$ sont évalués à $F$ alors pour $\overline{\nu}$, on doit avoir $\overline{\nu}(\mu_{2})=V$ puis $\overline{\nu}(\mu_{3})=V$ etc jusqu'à $\overline{\nu}(\mu_{k-1})=V$ et la dernière clause n'est plus satisfaite.
>Donc $\nu$ est une modèle de $\varphi$.

>[!bug] Exercice
>Montrer que l'on peut de plus supposer que toutes les clauses sont de tailles exactement 3.
>Idée : On remplace chaque occurrence d'une variable $x$ par de nouvelles variables $x_{1},\cdots x_{|\varphi|_{x}}$ et on force ces variables à avoir la même valuation.

>[!bug] Exercice
>Montrer que l'on peut supposer que chaque variable apparaît au plus trois fois.

>[!info] Remarque
>Pour $n \leq 2$, $n$-SAT $\in$ P

>[!note] Démo
>Pour $n=0$ une formule 0-SAT est $\bigwedge\limits_{i=1}^{k}\bot$ satisfiable si et seulement si $K=0$ (0 clauses).

>[!info] Remarque
>1-SAT : satisfiable si et seulement si aucun littéral apparaît avec sa négation.
>2-SAT : DM 5
