# Info MP2I - 2023 Champollion

Ce repo contient **tout le code** effectué pendant l'année scolaire 2023-2024 à Champollion en MPI.

> ***Ce code n'est pas prévu pour être réutilisé !***

## Téléchargement

Vous pouvez à la fois cloner le repo ou copier les bouts de code que vous voulez.
